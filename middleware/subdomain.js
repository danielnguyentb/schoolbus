/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

(function() {

    //See Readme.md

    module.exports = function(req, res, next) {

        var arr = req.subdomains;
        // SECURITY NOTE: Only a single-layer subdomain is allowed
        var subd = arr[arr.length - 1];

        //If its already been set, carry on
        if (req.session && req.session.subd && (req.session.subd == subd)) {
            // return next();
        } else {
            //If session made, put it in there for easier storage and recovery
            if (subd != "www") {
                // if (req.session) req.session.subd = subd;
                // else
                req.headers.subd = subd;
            }

        }

        // jump to next middleware in stack
        next();
    };
})();
