/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */
(function (r) {


	var fs = require('fs')
		, gm = require('gm');

	var pub = {};		//public funcs
	var _fn;			//utilities

	var _uploadTmpDir = __dirname + '/../uploads/tmp/';

	module.exports = function (fn) {
		_fn = fn;

		return pub;
	};

	//This called after the controller / next-in-line has processed the upload
	//Should remove temporary file
	pub.cleanup = function (filepath, sizes) {
		//All async, dont care about results
		fs.unlink(filepath);

		if (sizes && sizes.length) {
			var len = sizes.length;
			for (var i = 0; i < len; ++i) {
				fs.unlink(filepath + "." + sizes[i]);
			}
		}
	};

	//This is main handler for images
	pub.processImg = function (req, res, next) {
		handleUpload("img", req, res, next);
	};

	pub.processFile = function (req, res, next) {
		handleUpload("file", req, res, next);
	};

	//=========  Private functions below ======================

	//Sends back the "error" form data expected by
	var sendBackError = function (res, error, preventRetry) {

		var data = {success: false, error: error, preventRetry: preventRetry};

		res.send(JSON.stringify(data), {'Content-Type': 'text/plain'}, 404);
		//this also ends the POST request
	};

	var securityDeny = function (errtype, data, tmpfile, req, res) {

		_fn.GetClientIP(null, true, function (e, r) {

			data.w = "fu";		//file upload
			data.xip = r;

			_fn.audit.securityAlert(errtype, req.session.cid, data, 4);

			pub.cleanup(tmpfile);

			config.error("Image upload error : " + errtype + " file: " + data.fname);

			sendBackError(res, "denied", true);
		});
	};

	//------- This is the main upload handler
	//
	//If uploading on xhr, we can capture and stream the data to the file (and possibly support progress bars etc)
	//However, if plain old file submit form, this function will be called at the end of the process
	var handleUpload = function (type, req, res, next) {

		//Taken from https://github.com/valums/file-uploader/blob/master/server/nodejs.js

		//Either its a FORM based upload, or file has already finished the upload
		if (!req.xhr || req.files) {

			securityVerify(req, res,
				{
					type: type,
					uuid: req.body.qquuid,
					tmpfile: req.files.qqfile.path,
					fname: req.files.qqfile.name
				},
				next);

		} else {

			var skip = req.body.skip;
			if(skip) return next();

			//Hasn't even started the upload (VERY unlikely)

			config.error("uploads.handleUpload : Hadn't started upload!?");

			var origname = req.header('x-file-name');
			var fname = req.body.name;

			var uuid = req.body.qquuid;

			var tmpfile = _uploadTmpDir + uuid;

			// Open a temporary writestream
			var ws = fs.createWriteStream(tmpfile);

			//NOTE: These are all callbacks - so will happen async without holding up entire node app
			ws.on('error', function (err) {
				console.error("upload - could not open writestream for " + tmpfile + " for uploaded : " + fname);
				sendBackError(res, "Could not write to file");
			});

			//When fully uploaded
			ws.on('close', function (err) {
				securityVerify(req, res,
					{
						type: type,
						uuid: uuid,
						tmpfile: tmpfile,
						fname: fname
					},
					next);
			});

			// Writing filedata into writestream
			req.on('data', function (data) {
				ws.write(data);
			});
			req.on('end', function () {
				ws.end();
			});
		}
	};

	//--------- Main security verify function - see comments for all confirmations
	//tmpfile = full path to tmp file
	//fname = original filename as uploaded
	var securityVerify = function (req, res, data, next) {

		var tmpfile = data.tmpfile;
		var fname = data.fname;

		//CHECK 1 - does destination file have "../"?
		if (fname.indexOf("../") != -1) {
			return securityDeny("UPLOAD::PATHINJECTION", data, tmpfile, req, res);
		}

		config.log("Receive at server completed : " + fname);

		//--------- IMAGE FLOW -----------------
		if (data.type == "img") {

			//CHECK 3 - CONFIRM MIME FORMAT FROM IMAGICK (CAN IT READ IT?)
			config.log("file clean - mime-check : " + fname);
			config.log("tmpfile path : " + tmpfile);
			gm(tmpfile)
				.identify(function (e, d) {
					if (e) {
						data.e = e;
						return securityDeny("UPLOAD::NOTIMAGE", data, tmpfile, req, res);
					}
					//CHECK 4, 5, 6
					imgResizeCheck4(req, res, d, tmpfile, data, next);

				});

		} else if (data.type == "file") {

			//Its possible that even though a file was uploaded, it really was an image
			//if so, pass it through that loop

			gm(tmpfile)
				.identify(function (e, d) {
					if (!e && d) {
						//is an image
						//CHECK 4, 5, 6
						imgResizeCheck4(req, res, d, tmpfile, data, next);
					} else {
						//is a regular file

						//CHECKS 4, 5
						data = hashFilename(data, req);

						//Rename to hashed file - dont use rename since it doesn't work across
						//partitions
						_fn.file.move(tmpfile, _uploadTmpDir + data.hash, function (e, r) {
							//get file stat.
							fs.stat(_uploadTmpDir + data.hash, function (e, r) {
								if (e) {
									config.error(e);
									return sendBackError(res, "file stat error");
								}
								req.uplFile = data;
								req.uplFile.t = 'file';
								req.uplFile.s = r.size;
								next();
							});
						});
					}
				});
		}
	};

	var hashFilename = function (data, req) {
		//CHECK 4 - Disallow special chars in filename
		data.fname = _fn.sanitize(data.fname).xss().replace(/[^a-zA-Z0-9\.]/g, "");

		//CHECK 5 - Use hashed filename (different from uuid)
		data.hash = _fn.sec.md5(data.type + "##" + data.fname + "##" + req.session.user + data.uuid);

		data.tmpfile = _uploadTmpDir + data.hash;

		return data;
	};

	//Just a continuation of the security checks (func was getting too large)
	var imgResizeCheck4 = function (req, res, d, tmpfile, data, next) {

		data = hashFilename(data, req);


		//CHECK 4 - CONVERT AND RESAVE IMAGE TO SAME SIZE
		config.log("img resize-check : " + data.fname + " hashed: " + data.hash);
		console.log(_uploadTmpDir + data.hash);
		gm(tmpfile).resize(d.size.width - 2, d.size.height - 2).write(_uploadTmpDir + data.hash, function (e) {
			if (e) {
				return securityDeny("UPLOAD::CONVERR", {e: e, fn: tmpfile}, tmpfile, req, res);
			}

			//FINALLY - pass to next middleware after setting relevant info (for next) in session

			req.uplFile = data;
			req.uplFile.format = d.format.toLowerCase();
			req.uplFile.w = d.size.width- 2;
			req.uplFile.h = d.size.height - 2;
			req.uplFile.s = r.size;
			req.uplFile.t = 'img';
			req.uplFile.tmpfile = _uploadTmpDir + data.hash;
			req.uplFile.hash =data.hash;
			next();

		});

	}

})(require);