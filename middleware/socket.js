/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 *
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */
//Socket.IO middleware

(function(ns, r) {
    "use strict";

    var _fn = r('../routes/utils');
    var Step = r('step');
    var session = r('express-session');
    var RedisStore = r('connect-redis')(session);
    var cookie = r('cookie');
    var signature = r("cookie-signature");
    var redis = r('socket.io-redis');
    var jwt = r('jsonwebtoken');
    var io, store, managers;

    ns.init = function(app, db, mn) {

        var htserver = app.get('wcl:server');
        managers = mn;

        io = r('socket.io')(htserver);
        io.adapter(redis({
            key: 'wcl',
            pubClient: db.use('PUB'),
            subClient: db.use('SUB')
        }));

        store = new RedisStore({
            client: db.use('L1'),
            prefix: 'wcl:sess:'
        });

        io.use(socketAuthenticate);
        io.on('connection', onSocketConnect);

    };

    var socketAuthenticate = function(socket, next) {

        var data = socket.handshake;
        var token = data.query.token;

        if(token) {

            //Connect socket through another app
            // verifies secret and checks exp
            jwt.verify(token, config.session.secret, function(err, decoded) {

                if (err) {
                    config.error('Authentication error');
                    return next('User not authorized through passport', false);
                } else {

                    data.session       = data.session || {};
                    data.session.uId   = decoded.uId;
                    data.session.uname = decoded.uname;
                    data.session.perms = decoded.perms;
                    data.session._role = decoded._role;
                    data.session.roles = decoded.roles;

                    //Join to room by self
                    socket.join('wcl#user#' + decoded.uId, function(_e) {
                        if(_e) config.error(_e);
                    });

                    //Join to comp room;
                    if(decoded.company_id && decoded.company_id != 0) {
                        socket.join('wcl#comp#' + decoded.company_id, function (_e) {
                            if (_e) config.error(_e);
                        });
                    }

                    return next(null, true);
                }
            });
        } else {

            if(!data.headers || !data.headers.cookie) return next('Can\'t read cookies', false);

            var cookieParsed = cookie.parse(data.headers.cookie);
            var sessionID = cookieParsed[config.session.key];

            if(data.xdomain && !sessionID)
                return next('Can not read cookies from CORS-Requests.', false);

            if (sessionID) {
                //- Get session send from client
                sessionID = sessionID.replace("s:", "");
                sessionID = signature.unsign(sessionID, config.session.secret);
                store.get(sessionID, function (e, r) {
                    //- Check authentication of user
                    if (e)
                        config.error(e);
                    if (!r || !r.uname) {
                        config.error('Authentication error');
                        return next('User not authorized through passport', false);
                    } else {

                        //Join to room by self
                        socket.join('wcl#user#' + r.uId, function(_e) {
                            if(_e) config.error(_e);
                        });

                        //Join to comp room;
                        if(r.company_id) {
                            socket.join('wcl#comp#' + r.company_id, function (_e) {
                                if (_e) config.error(_e);
                            });
                        }

                        data.session = r;
                        return next(null, true);
                    }
                });

            } else {
                return next('User not authorized through passport', false);
            }
        }
    };

    var onSocketConnect = function(socket) {
        socket.on('driver:location:push', function(data) {
            console.log(data);
            locationPushLBA(data, _fn.noop);
        });
    };

    var locationPushLBA = function(data, cb) {

        var date = data.date || _fn.date.format(new Date(), 'YYYY-0MM-0DD');
        var starttime = data.starttime||new Date().getTime(), roundtrip = Math.abs((new Date().getTime()) - starttime);

        var companyId = data.company_id || 0;
        data.route = data.route || {};
        data.pos = data.pos || {};

        Step(
            function getCfg() {
                managers.config.getBcConfig(companyId, this.parallel());

                if(data.route.id){
                    var ploc_point = data.pos.lat +','+ data.pos.long;
                    console.log(ploc_point);
                    managers.route.gpsLog(data.route.id, ploc_point, data.uId, '', roundtrip, function(){});
                }

                if(!data.route.id || !data.route.driverInfo) managers.user.getUserById(data.uId, this.parallel());
            },
            function getParents(e, cfg, u) {
                if(e) throw e;

                cfg = cfg || {};

                if(u && u.length && u[0].length) {
                    u = u[0][0];

                    data.route.driverInfo = {
                        id: u.id,
                        name: u.fullname || u.username,
                        email: u.email,
                        avatar: u.image
                    };
                }

                if(!data.route.id || cfg.parent_map == 0 || cfg.map_tracking == 0) return [];

                managers.child.getListParentByRouteTrip(data.route.id, date, this);
            },
            function pushLBA(e, r) {
                if(e) throw e;

                if(r && r.length && r[0].length) {
                    var parentMap = {};
                    r[0].forEach(function(v) {
                        if(!v.parent_id) return true;

                        if(!parentMap[v.parent_id]) parentMap[v.parent_id] = [];

                        parentMap[v.parent_id].push({
                            child_id: v.child_id,
                            child_name: v.child_name
                        });
                    });

                    var i, parentData;
                    for(i in parentMap) {
                        if(!parentMap.hasOwnProperty(i)) continue;

                        parentData = _fn.extend({}, data);
                        parentData.childs = parentMap[i];
                        managers.LBA.pushRoom('wcl#user#' + i, 'driver:location:pull', parentData);
                    }
                }

                if(companyId) managers.LBA.pushRoom('wcl#comp#' + companyId, 'driver:location:pull', data);

                return this;
            },
            cb
        );
    };

})(module.exports, require);