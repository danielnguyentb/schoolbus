/**
 * Created by AnhNguyen
 */
;(function(r, ns) {

    var lusca = r('lusca');

    ns.exports = function(req, res, next) {

        var url = req.url;
        var regex = /^\/api.*$/, avaRegex = /^\/avatar\/.*/;

        if(regex.test(url) || avaRegex.test(url)) {
            //RESTful API

            // Website you wish to allow to connect
            res.header('Access-Control-Allow-Origin', '*');

            // Request methods you wish to allow
            res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

            // Request headers you wish to allow
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token, x-access-locale');

            // Set to true if you need the website to include cookies in the requests sent
            // to the API (e.g. in case you use sessions)
            res.header('Access-Control-Allow-Credentials', false);

            next();
        } else {

            lusca({
                csrf         : true,
                csp          : {/* ... */},
                xframe       : 'SAMEORIGIN',
                p3p          : false,
                hsts         : {maxAge: 31536000, includeSubDomains: true},
                xssProtection: true
            })(req, res, next);
        }

    };

})(require, module);
