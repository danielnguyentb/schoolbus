/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */
//Check if user is on a mobile device - see Readme.md

module.exports = function(req, res, next) {

    var ua = req.headers['user-agent'];

    //Only iphone for now - for android would have to figure out distinguishing
    if (ua && ua.toLowerCase().indexOf("iphone") != -1) {

        //prepend "m" for mobile version
        //req.url = "/m/" + req.url;
    }

    next();
}