/*───────────────────────────────────────────────────────────────────────────*\
 │  Copyright (C) 2014 eBay Software Foundation                                │
 │                                                                             │
 │hh ,'""`.                                                                    │
 │  / _  _ \  Licensed under the Apache License, Version 2.0 (the "License");  │
 │  |(@)(@)|  you may not use this file except in compliance with the License. │
 │  )  __  (  You may obtain a copy of the License at                          │
 │ /,'))((`.\                                                                  │
 │(( ((  )) ))    http://www.apache.org/licenses/LICENSE-2.0                   │
 │ `\ `)(' /'                                                                  │
 │                                                                             │
 │   Unless required by applicable law or agreed to in writing, software       │
 │   distributed under the License is distributed on an "AS IS" BASIS,         │
 │   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  │
 │   See the License for the specific language governing permissions and       │
 │   limitations under the License.                                            │
 \*───────────────────────────────────────────────────────────────────────────*/
'use strict';

var express = require('express');

exports.defaultHandler = function (settings) {
	var handler = tryRequire(settings && settings.module, require('errorhandler'));
	return handler(settings);
};

var tryRequire = function tryRequire(moduleName, fallback) {
	var result;
	try {
		result = moduleName && require(moduleName);
	} catch (err) {
		// noop
	}
	return result || fallback;
};

exports.fileNotFound = function (template) {
	return function (req, res, next) {
		var model = {title: "404 Not Found", url: req.url};

		res.locals.base_url = config.STATIC_DOMAIN;
		res.locals.control = '404';
		res.locals.error_tpl = true;

		if (template) {
			if (req.xhr) {
				res.send(404, model);
			} else {
				res.status(404);
				res.render(template, model);
			}
		} else {
			next();
		}
	};
};


exports.serverError = function (template) {
	return function (err, req, res, next) {

		var model = {}, status = 403;
		var _fn = require('../routes/utils');

		if (err.status !== 403) {

			model = {title: "We're sorry, but something went wrong.", url: req.url, err: err};
			status = 500;

		} else {

			var cData = req.session.cData?req.session.cData:[];
			for(var i = 0; i < cData.length; i++) {
				if(cData[i].cId == req.session.cid) {
					cData = cData[i];
					break;
				}
			}
			var bData = cData.brch?cData.brch:[];
			for (i = 0; i < bData.length; i++) {
				if(bData[i].i == req.session.bid) {
					bData = bData[i];
				}
			}

			var desc = _fn.str.f("Your role in {comp}'s {brch} doesn't have the access, therefore we have redirected you to the home page.",{
				comp: (cData.cName) ? cData.cName : '',
				brch: (bData.b) ? bData.b : ''
			});

			model = {
				title: "403 Insufficient Permissions",
				desc: desc,
				url: req.url
			};
			template = 'errors/403';

		}

		console.error('Error:' + err.message);

		res.locals.base_url = config.STATIC_DOMAIN;
		res.locals.control = '403/500';
		res.locals.error_tpl = true;

		if (template) {
			if (req.xhr) {

				_fn.render(req, res, template, model);

			} else {
				res.status(status);
				res.render(template, model);
			}
		} else {
			next(err);
		}
	};
};
