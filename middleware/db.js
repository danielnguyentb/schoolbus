/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */
//DB Manager - see readme.md

(function (r) {


	var _fn = {}, count = 0, _deferred = {}, _dbs = {}, mysql =r('mysql');

	//Opens all connections to all servers except clusters ("landlordOwned") - those will
	//be deferred until used
	_fn.connect = function (dbs, callback) {
		count = dbs.length;
		if (count == 0)
			callback();

		dbs.forEach(function (db) {

			if (db.landlordOwned) {
				_deferred[db.name] = db;
				if (--count == 0)
					doAsyncCb(callback);
				return;
			}

			//else
			_dbs[db.name] = _initFuncs[db.type](db, function () {
				if (--count == 0)
					doAsyncCb(callback);
			});
		});
	};
	_fn.connectSingle = function(dbs,type, name, f){
		dbs.forEach(function (db) {
			if(db.name==name){
				console.log(db);
				//console.log(_initFuncs[type](db,f));
				return _initFuncs[type](db,f);
			}

		});
	}


	//Defining this here because utilities haven't been loaded yet
	var doAsyncCb = function (cb) {
		process.nextTick(cb.bind(null, _fn));
	}

	_fn.use = function (name, clusterdb) {
		//Load deferred ones on demand
		if (!_dbs[name] && _deferred[name]) {
			//TODO - Get landlord to tell us name of db
			var d = _deferred[name];
			_dbs[name] = _initFuncs[d.type](d, function () {
			});
			return _dbs[name];

		} else {
			if (clusterdb)
				return _dbs[name + "##--##" + clusterdb];
			return _dbs[name];
		}
	}

	//@if DEBUG
	_fn.list = function () {
		var s = '';
		for (i in _dbs)
			s += i + '; ';
		return s;
	}
	//end


	//Creates an active connection with Redis db
	var _initFuncs = {
		Redis: function (c, cb) {

			var client;
			//Connect sentinel
			if(c.endpoints && c.endpoints.length && c.masterName) {
				client = r('redis-sentinel').createClient(c.endpoints, c.masterName, (c.opt ? c.opt : {}));
			} else {
				client = r('redis').createClient(c.port, c.host, (c.opt ? c.opt : {}));
			}

			client.auth(c.auth);
			client.on("ready", function (err) {
				if (err)
					return c.error(err);
                var name=[];
                if(c.endpoints && c.endpoints.length && c.masterName) {
                    c.endpoints.forEach(function(o){
                        name.push(o.host+':'+ o.port);
                    });
                }else{
                    name.push(c.host+':'+c.port);
                }
				c.ready([c.name, '(', name.join(' '), ') ', c.dbname, 'is ready'].join(' '));
				cb();
			});
			client.on("error", function (err) {
				c.error(err);
			});
			return client;
		},
		Mongo: function (c, cb) {
			var mdb = r('mongoskin').db(c.URL, {
				auto_reconnect : true
			});
			mdb.open(function (e, r) {
				if (e || !r)
					return c.error("Mongodb is offline or db not found");

				mdb.db = r;
				c.ready([c.name, c.type, 'is connected'].join(' '));
				cb();
			});
			mdb.on('reconnect', function(e) {
				c.ready([c.name, c.type, 'is reconnected'].join(' '));
			});
			mdb.on('error', function (e) {
				c.error(e);
			});
			return mdb;
		},
		MySQL: function (c, cb) {
			var hostport = c.host.split(':');
			var ci = {
				host              : hostport[0],
				port              : hostport[1],
				user              : c.user,
				password          : c.pass,
				database          : c.db,
				multipleStatements: c.multi
			};
            var client = mysql.createPool(ci);
			client.on('connection', function(connection) {
				c.ready([c.name, c.type, 'is connected'].join(' '));
			});
			client.on('error', function(err) {
				c.error([c.name, c.type, 'is NOT connected'].join(' ') + ' ' + err);
			});
			cb();
			return client;
		},
		MSSQL: function (c, cb) {

			var sql = require('mssql');

			var connection = new sql.Connection({
				user: c.user + '@' + c.host.split('.')[0],
				password: c.pass,
				server: c.host,
				database: c.db
			}, function (err) {
				// ... error checks
				if (err) {
					c.error([c.name, c.type, 'is NOT connected'].join(' ') + ' ' + err);
				} else {
					c.ready([c.name, c.type, 'is connected'].join(' '));
				}
				cb();
			});

			return connection.request();
		}
	};

	//Finally - export the whole thing - need to use "module.exports" directly here
	module.exports = _fn;

})(require);