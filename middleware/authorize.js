/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 *
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */
//Set of middleware functions to do various levels of authorization
//Authorizes each incoming request - see Readme.md

(function(r) {

    var Step = r("step");
    var jwt = r('jsonwebtoken');

    var pub = {};
    var managers;
    var _fn;

    //Just called once to load in the managers
    module.exports = function(m, utils) {
        managers = m;
        _fn = utils;

        return pub;
    };

    //This is the default authenticate + authorize middleware
    pub.mod = {

        pendingApproval : function(req, res, next) {
            req.module = 'pending-approval';
            next();
        },

        groupManage : function(req, res, next) {
            req.module = 'group-manage';
            next();
        },

        driverManage : function(req, res, next) {
            req.module = 'driver-manage';
            next();
        },

        assistantManage : function(req, res, next) {
            req.module = 'assistant-manage';
            next();
        },

        locationManage : function(req, res, next) {
            req.module = 'location-manage';
            next();
        },

        notificationManage : function(req, res, next) {
            req.module = 'notification-manage';
            next();
        },

        logManage : function(req, res, next) {
            req.module = 'log-manage';
            next();
        },

        busManage : function(req, res, next) {
            req.module = 'bus-manage';
            next();
        },

        companyManage : function(req, res, next) {
            req.module = 'company-manage';
            next();
        },

        cardManage : function(req, res, next) {
            req.module = 'card-manage';
            next();
        },

        configManage : function(req, res, next) {
            req.module = 'config-manage';
            next();
        },

        bcConfigManage : function(req, res, next) {
            req.module = 'bc-config-manage';
            next();
        },

        routeManage : function(req, res, next) {
            req.module = 'route-manage';
            next();
        },

        calendarManage : function(req, res, next) {
            req.module = 'calendar-manage';
            next();
        },

        childManage : function(req, res, next) {
            req.module = 'child-manage';
            next();
        }
    };

    pub.act = {

        get : function(req, res, next) {
            req.action = 'get';
            next();
        },
        post : function(req, res, next) {
            req.action = 'post';
            next();
        },
        put : function(req, res, next) {
            req.action = 'put';
            next();
        },
        delete : function(req, res, next) {
            req.action = 'delete';
            next();
        }

    };

    pub.check = function(req, res, next) {
        if (!req.session.uId) {
            req.session.returnTo = req.url;
            return res.redirect('/');
        }

        req.ext = {};

        next();
    };

    pub.apiAuth = function(req, res, next) {

        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        // decode token
        if (token) {

            // verifies secret and checks exp
            jwt.verify(token, config.session.secret, function(err, decoded) {
                if (err) {
                    return res.json({ error: true, msg: 'Failed to authenticate token.' });
                } else {
                    req.isApi = true;

                    req.session             = req.session || {};
                    req.session.uId         = decoded.uId;
                    req.session.uname       = decoded.uname;
                    req.session.avatar      = decoded.avatar;
                    req.session.company_id  = decoded.company_id;
                    req.session.bc_mode     = decoded.bc_mode;
                    req.session.perms       = decoded.perms;
                    req.session._role       = decoded._role;
                    req.session.roles       = decoded.roles;

                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                error: true,
                msg: 'No token provided.'
            });

        }
    };

    pub.permission = function(req, res, next) {
        if(req.module && req.action) {
            if(!req.session.perms[req.module] || req.session.perms[req.module].indexOf(req.action) == -1) {
                res.send(404);
                return false;
            }

        }
        next();
    };

    pub.mustLoggedOut = function(req, res, next) {
        if (req.session.uId) {
            return res.redirect('/');
        }
        next();

    };

    //This restricts person by role
    pub.mustBe = function(role) {

        return function(req, res, next) {

            if (!req.session || !req.session.role || !(req.session.role > role))
                return res.send(404);

            next();
        };
    };

    //This does a general check to ensuer the referring page
    pub.checkReferral = function(req, res, next) {

        if (!req.headers.referer)
            return res.send(404);

        var ref = req.headers.referer.replace("http://", "").replace("https://", "").split("?")[0];

        if (ref.split("/")[0] != req.headers.host)
            res.send(404);

        next();

    };

    var throw403 = function(cb, msg) {
        if (!msg)
            msg = 'Not Allowed!';
        var err = new Error(msg);
        err.status = 403;
        return cb(err);
    };


})(require);
