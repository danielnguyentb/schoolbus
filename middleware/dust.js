//We require that dustjs, and the dustjs-helpers have been loaded. The way we invoke this function will ensure that.
(function (r) {
	module.exports = function (dust, cfg) {

		if(process.env.NODE_ENV == 'development') {

			dust.optimizers.format = function (ctx, node) {
				return node
			};

		}

		/**
		 * iterate helper, loops over given object or current context object.
		 * Inspired: https://github.com/akdubya/dustjs/issues/9
		 *
		 * Example:
		 *    {@iterate on=obj}{key}-{value}{~n}{/iterate}
		 *
		 * @param on, if omitted current context will be used.
		 */
		dust.helpers.iterate = function (chunk, context, bodies, params) {
			params = params || {};
			var obj = params['on'] || context.current();
			for (var k in obj) {
				if (typeof obj[k] !== "function") chunk = chunk.render(bodies.block, context.push({key: k, value: obj[k]}));
			}
			return chunk;
		};

		/**
		 * iterate helper, loops over given object or current context object.
		 * Inspired: https://github.com/akdubya/dustjs/issues/9
		 *
		 * Example:
		 *    {@in_array on=obj value=v}{key}-{value}{~n}{/in_array}
		 *
		 * @param on, if omitted current context will be used.
		 */
		dust.helpers.in_array = function (chunk, context, bodies, params) {
			params = params || {};
			var obj = params['on'] || context.current();
			var value = params['value'] || '';
			value = value.split('|');
			var isin = false;
			if (value && value.length > 0 && obj && obj.length > 0) {
				for (var i in value) {
					if (obj instanceof Array) {
						if (obj.indexOf(value[i]) !== -1) {
							isin = true;
						}
					} else {
						if (obj[value[i]]) {
							isin = true;
						}

					}
				}
			}


			if (isin) {
				chunk = chunk.render(bodies.block, context);
			}
			return chunk;
		};

		dust.helpers.isAllow = function(chunk, context, bodies, params) {
			params = params || {};
			var mod = params['mod'];
			var act = params['act'];
			act = act.split('|');

			var perms = context.get('per');
			var isAdmin = context.get('admin');
			var allow = false;

			if(isAdmin) allow = true;
			else {

				perms = (perms && perms[mod]) ? perms[mod] : [];
				act.forEach(function(v) {
					if(perms.indexOf(v) != -1) allow = true;
				});
			}

			if (allow) {
				chunk = chunk.render(bodies.block, context);
			}
			return chunk;
		};

		dust.helpers.slugify = function (chunk, context, bodies, param) {

			var content = param.key;
			content = content
				.toString().toLowerCase()
				.replace(/\s+/g, '-')        // Replace spaces with -
				.replace(/[^\w\-]+/g, '')   // Remove all non-word chars
				.replace(/\-\-+/g, '-')      // Replace multiple - with single -
				.replace(/^-+/, '')          // Trim - from start of text
				.replace(/-+$/, '');         // Trim - from end of text

			return chunk.write(content);
		};
                
		dust.helpers.pIdx = function (chunk, context, bodies, params) {
			params = params || {};
			var p = params['p']||0,n = params['n']||0;
			p=isNaN(p)?0:p-1;
			n=isNaN(n)?0:n;

			return bodies.block(chunk, context.push(context.stack.index + 1 +p*n));
		};

		return function dust_helper(req, res, next) {
			next();
		}
	}

})(require);