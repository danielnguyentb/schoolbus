/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

 Middleware libraries for express built (NOT for public distribution)

uploads.js
==========
Middleware - single place to handle all file and image uplaods
securely
 
Middleware automatically handles refusals / exits / auditing
without moving forward towards controller

Uploads to /tmp/ and does thorough checking - after verified
passes a key / uuid of the image to a "processingCallback"
which will then finally call the controller

  

trailingslashes.js
==================

Removes trailing slashes - otherwise ajx calls can break

subdomain.js
============

Sub-domain middleware - if req.session exists, sets / affirms req.session.subd
Otherwise sets req.headers.subd  (if no session yet)

This will be called before routes are set - but the idea is to minimize its use so it
doesn't do its thing for every single request

Note: The regex courtesy of https://github.com/WilsonPage/express-subdomain-handler


db.js
=====

Db helper
but this will have our specific needs for db management

1- Not all DB connections are made on app load - some may defer to load ondemand
2- Instead of differnet DBs - we're talkign about different "services" and their
   respective DBs. So its expected that each of these will be in a different place
3- This module will also delegate the db schema validation and interfacing with landlord.js

authorize.js
============

This middleware isn't invoked for each request but on a per-route basis decided by the modules.

This has two functions - check() and mustBe(). 

check() - For each incoming request:
- Check if user is logged in (session exists)
- Check that current user is authorized for current sub-domain, otherwise
  change company

mustBe() - User must exist and must be a certain role to go through that route

This middleware is added to the utility object ``_fn`` as ``_fn.auth`` The modules use this as follows

	app.get("/lobby", _fn.auth.mustBe(managers.user.roles.ADMIN), routes.lobbyIndex);


mobilecheck.js
==============

If incoming request is from a mobile device, prepends /m/ to the route
Mostly this is an experiment - in theory, this lets the modules decide to handle mobile requests
differently but from within the same code / module.

Also in theory this wont create a 302 redirect and switching to a different domain for user
so user would only see the same domain. But we'll see how it goes once tested
