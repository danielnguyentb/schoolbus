### Setup step:

1. Do git pull to get lastest version app from repo

2. Copy app.backup to app.js, config-dev.backup to config-dev.js.

3. Open app.js go to line 72 and update corresponding domain/ip
domain: '172.20.9.254' to domain: 'abc.com' or domain: '172.20.255.193'
if we use multiple domain, just comment line 72
4. Setup Redis server, you can use redis binary in software folder or download : http://redis.io/download.

5. Create mysql db with db name= sint, user = `sint`@`%`, password= sint1234!@#$ and import lastest db from folder software/dbs/mysql_lastest1_6.sql.zip (require mysql server v 5.5)

6. Setup GraphicsMagick with binary file in software folder or download from  http://www.graphicsmagick.org/download.html

7. Install mongodb
	- extract lastest mongodb dump from software/dbs/mongodb.zip
    http://docs.mongodb.org/manual/tutorial/install-mongodb-on-red-hat-centos-or-fedora-linux/
    and restore with command:
    - If using local mongodb and without authentication
    mongorestore PATH_TO_PROJECT/software/mongodb_dump/DCBS
    - If using local/remote mongodb and authentication
    mongorestore --host mongodb1.example.net --port 27017 --username user --password pass PATH_TO_PROJECT/software/mongodb_dump/DCBS
    (http://docs.mongodb.org/manual/reference/program/mongorestore/#bin.mongorestore)

8. Open config-dev.js and change

- update Redis instance host/port(or end points if use Redis sentinel)  & auth key (line 93-159 of config-dev.js), we can check auth key in redis.conf "requirepass" option line 248.
IF use normal Redis instance:
	host : '127.0.0.1',
	port : 6379,
If use Redis sentinel:
 endpoints : [
           	 {host: '172.21.21.3', port: 26379},
             {host: '172.21.21.2', port: 26379},
		 	 {host: '172.21.21.1', port: 26379}
        	],
masterName : 'master_gluuon',

- update MySQL db host/user/pass/db (line 161->174)
- update SMTP info (if needed) line 238-241
- get reCaptcha key (https://www.google.com/recaptcha/intro/index.html) and update reCaptcha key line 222-225.

9. Install wkhtmltopdf lib:
- install with binary in folder software or download from http://wkhtmltopdf.org/downloads.html
10. Install ghostscript:
- install with binary in folder software or download from http://www.ghostscript.com/download/gsdnld.html

11. Make sure folder uploads writeable


12. Go to app folder run
$ npm install

13. Installing grunt, go to app folder and run:
- npm install -g grunt-cli
- npm install grunt
- npm install grunt-config-dir

14. Install pm2
npm install pm2 -g
15. run app:
$ pm2 start app.js
for list running process: pm2 list

16. default username/password: Jupiter/jupiter

17. For change mssql setting of company, please do following step:
- go to WUMP => ACP => Company List => Select company we want change db info
- Click on edit button
- change db info and save

18.  Setup cron for grunt inside code folder
*/15 * * * * cd TCRG_FOLDER && grunt
