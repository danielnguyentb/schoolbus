/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

 /*
  * Img compression utilities
  *
  * Essentially a wrapper around imagick + compression / optimization tools
  */

(function(r){

	var fs = r('fs');
	var im = r('gm');
	var Step = r('step');
	// var process_exec = r('child_process').exec;


	var pub = {};
	var _fn;

	//---------- public funcs -----------------
	//---------- public funcs -----------------
	//---------- public funcs -----------------
	//---------- public funcs -----------------

    module.exports = function(fn) {
    	_fn = fn;
    	return pub;
    }

	//Resizes an image to multiple sizes
	pub.makeThumb = function(tmpfile, sizes, boundForFlash, callback){

		var dims;

		Step(
			function getDims(){
				im(tmpfile).identify(this);
			},

			function checkFlash(e, r){
				if (e) throw e;

				dims = r.size;

				if (boundForFlash) resizeForFlash(tmpfile, r, this);
				else if (sizes.length) return 1;
				else throw "ok";		//nothing to do
			},

			function doResize(e, r){
				if (e) throw e;
				//DESIGN NOTEs:
				//1- If image is already smaller than desired size, wont up-size, just copy to thmb
				//2- Resize the larger of the w / h to target size

				var group = this.group();
				var sz = sizes.length;

				var h = dims.h, w = dims.w;
				var aspect = parseFloat(w / h);

				var fw, fh, size, dest;
				for (var i = 0; i < sz; i++) {
					size = sizes[i];
					dest = tmpfile + "." + size;

					if ((w <= size) && (h <= size)) {
						//already smaller, just copy-it over to size filename
						_fn.file.copy(tmpfile, dest, group());
						continue;
					}

					if (w > h){
						fw = size;
						fh = aspect * fw;
					} else {
						fh = size;
						fw = parseFloat((1 / aspect)) * fh;
					}
					doIMResize(tmpfile, tmpfile + "." + sizes[i], dims.format, fw, fh, group());
				}
			},
			callback
		);

	};

	//--------------- private funcs
	//--------------- private funcs
	//--------------- private funcs

	//resize for flash limits
	var resizeForFlash = function(tmpfile, dims, callback){

		//This for flash
	    var MAX_AREA = 16000000;      // actual limit is 16777215, being conservative
	    var MAX_SIDE = 8000;          // actual limit is 8191, being conservative
	    var RESIZE_IMAGE_FOR_FLASH = true;

	    // Pure PDF Maker will crash if any image is > 9 Million pixels
	    var MAX_PDF_AREA = 9000000;


		var w = dims.width, h = dims.height;
		var aspect = parseFloat(w/h);

		var needsResizing = false;

		if (w > MAX_SIDE){
			w = MAX_SIDE;	h = w / aspect;
			needsResizing = true;
		}

		if (h > MAX_SIDE){
			h = MAX_SIDE;	w = h / aspect;
			needsResizing = true;
		}

		var factor;
		if ((w * h) > MAX_AREA){
			factor = Math.sqrt((w * h) / MAX_AREA);
			w = Math.floor(w / factor);
			h = Math.floor(h / factor);

			needsResizing = true;
		}

		//Pure PDF crashes if any image > 9 mil px
		if ((w * h) > MAX_PDF_AREA) {
			factor = Math.sqrt((w * h) / MAX_PDF_AREA);
			w = Math.floor(w / factor);
			h = Math.floor(h / factor);

			needsResizing = true;
		}

		if (needsResizing) {
			doIMResize(tmpfile, tmpfile + ".flashbound", dims.format, w, h,
				function(e,so, serr){
					_fn.file.overwriteRename(tmpfile + ".flashbound", tmpfile, callback);
				});
		}
		else return _fn.doAsync(callback);
	};

	//actually calls the resize
	var doIMResize = function(filefrom, fileto, format, w, h, callback){

		config.log("making thumbnail : " + filefrom);

		im(filefrom).resize(w, h).write(fileto, callback);

	};


})(require);