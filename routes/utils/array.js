/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

(function () {

	var pub = {};

	var _fn;

	module.exports = function (utils) {
		_fn = utils;

		return pub;
	};

	pub.last = function (arr) {
		return arr[arr.length - 1];
	};

	// Find an entry in an array. Returns the array index or null
	pub.find = function (arr, item) {
//        if (config.browser.isIE && !config.browser.isIE9) {
//            for (var t = 0; t < arr.length; t++)
//                if (arr[t] == item)
//                    return t;
//            return null; 
//        } else {
		//ES5 has native indexOf - use it in modern browsers... not supported in IE8
		var i = arr.indexOf(item);
		return (i === -1) ? null : i;
//        }
	};

	//Sometimes arrays contain objects - in that case, find an object from the
	//array by comparing a particular key in that object
	pub.findInKey = function (arr, whichKey, item) {
		for (var t = 0; t < arr.length; t++) {
			if (arr[t][whichKey] == item)
				return t;
		}
		return -1;
	};

	//Works similar to findInKey but returns ALL items found
	pub.findAllInKey = function (arr, whichKey, item) {
		var ret = [];
		if (!arr) return ret;

		for (var t = 0; t < arr.length; t++) {
			if (arr[t][whichKey] && (arr[t][whichKey] == item))
				ret.push(arr[t]);
		}
		return ret;
	};

	// Return whether an entry exists in an array
	pub.contains = function (arr, item) {
		return pub.find(arr, item) != null;
	};

	// Return whether one of a list of values exists in an array
	pub.containsAny = function (arr, items) {
		for (var i = 0; i < items.length; i++) {
			if (pub.contains(arr, items[i]))
				return true;
		}
		return false;
	};

	// Push a new value into an array only if it is not already present in the array. If the optional unique parameter is false, it reverts to a normal push
	pub.pushUnique = function (arr, item, unique) {
		if (unique != undefined && unique == false)
			arr.push(item);
		else {
			if (pub.find(arr, item) == null)
				arr.push(item);
		}
	};

	pub.pushArray = function (arr, ar) {
		for (var i = 0; i < ar.length; ++i) arr.push(ar[i]);
	};

	pub.pushArrayUnique = function (arr, ar) {
		for (var i = 0; i < ar.length; ++i)
			pub.pushUnique(arr, ar[i], true);
	};

    pub.arrayMerge = function() {
        var a, i, result = [], args = arguments;
        for (i = 0; i < args.length; i++) {
            if (typeof(args[i]) == 'object') {
                for (a in args[i]) {
                    if(!args[i].hasOwnProperty(a)) continue;

                    result = result.concat(args[i][a]);
                }
            } else {
                result = result.concat(args[i]);
            }
        }

        return result;
    };

    pub.toArray = function(obj) {
        var arr = [], i;

        if(obj) {
            for(i in obj) {
                if(!obj.hasOwnProperty(i)) continue;

                arr.push(obj[i]);
            }
        }

        return arr;
    };

	Array.prototype.remove = function (value) {
        var idx = this.indexOf(value);
		if (idx !== -1) this.splice(idx, 1);

        return true;
	};

    Array.prototype.insert = function (index, item) {
        this.splice(index, 0, item);
    };

	Array.prototype.pushUnique = function (a, b) {
		b != void 0 && b == false ? this.push(a) : this.find(a) == null && this.push(a)
	};

	Array.prototype.find = function (a) {
		a = this.indexOf(a);
		return a === -1 ? null : a
	};

	
	Array.prototype.pushArray = function (a, unique) {
		for (var b = 0; b < a.length; ++b) {
	        if(unique) this.pushUnique(a[b]);
	        else this.push(a[b]);
	    }
	};

	Array.prototype.minByKey = function(k){
		if(!this.length) return null;

		this.sort(function (a, b) {
		    return a[k] - b[k]
		});
		return this[0];
	};

	/*
	 * A "true" Async array.map()- doesn't just work if doing I/O operations but
	 * each iteration is in a separate event loop tick
	 *
	 * Parameters: 
	 *  	a - Array
	 *		f - Map function
	 *		cb - Callback for when all done
	 */
	pub.mapAsync = function (a, f, cb) {

		//Results of map stored here
		var r = [];

		var l = a.length;
		var i = 0;		//index

		_fn.doAsync(function step(arr, itr, len, res, fn, callback) {

			if (len == itr) return callback(null, res);

			var nextstep = step;

			//Could've just done res.push(fn[itr++]) but that wouldn't
			//work for I/O operations - but this will work, even if
			//this inner callback flows through immediately
			fn(arr[itr++], function (e, r) {
				if (e) return callback(e);

				if (r) res.push(r);

				_fn.doAsync(nextstep, arr, itr, len, res, fn, callback);
			});

		}, a, i, l, r, f, cb);
	};


})();