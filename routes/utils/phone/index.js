/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

 /*
  * Phone activities wrapper - uses Twilio API
  */

(function(c,r){

	// This for twilio
	var sys = require('sys'),
	    TwilioClient = require('twilio').Client,
	    client = new TwilioClient(c.twilio.sid, c.twilio.tok, "wwcom.com"),
	    phone;


	//public
	var pub = {};
	// var _fn;

	module.exports = function(){
		// _fn = fn;

		// This for twilio
		phone = client.getPhoneNumber('+84979464262');
		phone.setup(function(){});



		return pub;
	}

	pub.sms = function(to, txt, callback, _fn){

		to = to.replace("-","");


		// This is the Twilio approach
         phone.sendSms(to, txt, null, function(sms) {


             sms.on('processed', function(reqp, resp) {

		     });

         	callback(null, true);
         });

	};

})(config,require);