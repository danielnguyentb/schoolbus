/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 *
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */
/*
 * Root of custom Utility funcs and class (general utility
 * funcs used by all mods) - for module specific utility funcs
 * add them in the module
 */

(function (r) {

    var _fn = {};
    var db = require("../../middleware/db.js");
    var Step = r('step');
    var mongoHelper = r('mongoskin').helper;
    var makara = r('makara');
    var fs = require('fs');
    //Empty callback func
    _fn.empty = function () {
    };

    //sub-modules
    _fn.audit = r("./auditor.js");
    _fn.date = r("./date.js");
    _fn.file = r("./file.js");                              //file utils
    _fn.array = r("./array.js")(_fn);
    _fn.str = r("./string.js");
    _fn.sec = r("./security/");
    _fn.enc = r("./encrypt.js")(_fn);
    _fn.auth = r("../../middleware/authorize.js");          //this middleware authorizes users
    _fn.uploads = r("../../middleware/uploads.js")(_fn);    //secure upload handler
    _fn.img = r("./img/")(_fn);                             //thumbnailer
    _fn.pag = r("./pagination.js");
    _fn.wclCalx = r("./wclCalx/wclCalx.js");
    _fn.reCaptcha = r("./recaptcha.js");

    r("./moreValidator.js");                                //extend the validator lib

    _fn.sanitize = r("validator").sanitize;
    _fn.check = r("validator").check;
//    _fn.phone = r("./phone/")(_fn);
    _fn.email = r("./email/")(_fn);

    _fn.noop = function () {
    };

    //gen mongodb objectid
    _fn.dbId = function (str) {
        try {
            return mongoHelper.toObjectID(str);
        } catch (e) {
            return null;
        }
    };

    //Mongodb aggregate framework
    _fn.aggregate = function (_dbname, collection, pipeline, callback) {
        var command = {aggregate: collection, pipeline: pipeline};
        db.use(_dbname).executeDbCommand(command, function (e, r) {
            if (e)callback(e, null);
            if (r.documents && r.documents[0] && (r.documents[0].ok == 1)) {
                callback(null, r.documents[0].result);
            } else {
                var error = {error: 'Aggregate Failed! No result found'};
                if (r.documents && r.documents[0] && r.documents[0].errmsg) {
                    error = {error: r.documents[0].errmsg};
                }
                callback(error, null);
            }
        });
    };

    _fn.getBundle = function(locale) {

        var getFunc = {
            _lingo : {},

            get : function(string) {

                if(this._lingo[string]) return this._lingo[string];
                else return string;
            }
        };

        return function(req, res, next) {

            Step(
                function getBundle() {

                    makara.getBundler(req).get(locale, {}, this)
                },
                function bindBundle(e, r) {
                    if(e) throw e;
                    getFunc._lingo = r;
                    req.lingo = getFunc;
                    next();
                }
            )
        };
    };

    //function return value within two function in Step
    _fn.r = function (r, callback) {
        callback(null, r);
    };

    //get MIME type
    _fn.getMIME = function (ext) {
        ext = ext.toLowerCase();
        var mime = {
            'html': 'text/html',
            'htm': 'text/html',
            'txt': 'text/plain',
            'csv': 'text/csv',
            'vcf': 'text/vcard',
            'vcard': 'text/vcard',
            'gif': 'image/gif',
            'jpg': 'image/jpeg',
            'jpeg': 'image/jpeg',
            'svg': 'image/svg+xml',
            'png': 'image/png',
            'tif': 'image/tiff',
            'tiff': 'image/tiff',
            'igs': 'model/iges',
            'iges': 'model/iges',
            'msh': 'model/mesh',
            'mesh': 'model/mesh',
            'silo': 'model/mesh',
            'vrml': 'model/vrml',
            'wrl': 'model/vrml',
            'wmv': 'video/x-ms-wmv',
            'sxw': 'application/vnd.sun.xml.writer',
            'sxg': 'application/vnd.sun.xml.writer.global',
            'sxd': 'application/vnd.sun.xml.draw',
            'sxc': 'application/vnd.sun.xml.calc',
            'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xls': 'application/vnd.ms-excel',
            'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'ppt': 'application/vnd.ms-powerpoint',
            'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'doc': 'application/msword',
            'odt': 'application/vnd.oasis.opendocument.text',
            'fodt': 'application/vnd.oasis.opendocument.text',
            'fodp': 'application/vnd.oasis.opendocument.presentation',
            'odp': 'application/vnd.oasis.opendocument.presentation',
            'ods': 'application/vnd.oasis.opendocument.spreadsheet',
            'fods': 'application/vnd.oasis.opendocument.spreadsheet',
            'odg': 'application/vnd.oasis.opendocument.graphics',
            'fodg': 'application/vnd.oasis.opendocument.graphics',
            'rtf': 'text/rtf',
            'zip': 'application/zip',
            'mp3': 'audio/mpeg',
            'pdf': 'application/pdf',
            'ps': 'application/postscript',
            'tgz': 'application/x-gzip',
            'gz': 'application/x-gzip',
            'tar': 'application/x-tar',
            'rar': 'application/x-rar-compressed',
            'vcf': 'text/vcf',
            'ogg': 'application/ogg',
            'xul': 'application/vnd.mozilla.xul+xml',
            'kml': 'application/vnd.google-earth.kml+xml',
            "tex": 'application/x-latex',
            "exe": "application/octet-stream"
        };
        if (mime[ext]) {
            return mime[ext];
        } else {
            return 'application/octet-stream';
        }
    };

    _fn.makeAjxUrl = function() {

        var args = ['c', 's', 'e', 'w'];
        var route = '/#', params = [], value;
        for(var i in args) {
            if(!args.hasOwnProperty(i)) break;

            value = arguments[i];
            if(value === undefined || value === true) continue;
            if(value === false) value = '_';

            params.push(args[i] + '=' + value);
        }

        return route + params.join('&');
    };

    _fn.render = function (req, res, tpl, data) {

        //If is ajax, return part content
        if (req.xhr) {
            Step(
                function getPart() {

                    data.xhr = true;
                    res.render(tpl, data, this);
                },
                function response(e, r) {
                    if (e) res.send({error: true, e: e.message});
                    else res.send({
                        e: (data.error) ? data.error : false,
                        t: (data.title) ? data.title : '',
                        c: r,
                        u: (req.originalUrl) ? req.originalUrl : '',
                        p: res.locals.curPos,
                        s: data.rslug ? data.rslug : false
                    });
                }
            );
        } else {
            res.render(tpl, data);
        }

    };

    //Generate query tring suffix with criteria
    _fn.suffixFCriteria = function (opts) {

        var ext = [], query = '';
        if (opts.order && opts.order != '') ext.push(['_o=', opts.order].join(''));
        if (opts.direction && opts.direction != '') ext.push(['_d=', opts.direction].join(''));
        if (opts.num && opts.num != '' && opts.num != 10) ext.push(['_n=', opts.num].join(''));

        var criteria = '';
        if (opts.criteria) {

            var arr = [];
            for (var i in opts.criteria) {
                if (opts.criteria.hasOwnProperty(i) && opts.criteria[i] != '') {

                    arr.push(['_c[', i, ']=', opts.criteria[i]].join(''));
                }
            }
            criteria = arr.join('&');
        }
        if (criteria) ext.push(criteria);

        if (ext.length) query += '?' + ext.join('&');

        return query;
    };

    /*
     * Confirms logged in
     */
    _fn.loggedIn = function (req) {
        return (req.session && req.session.uId);
    }

    //Returns subdomain
    _fn.subd = function (req) {

        var sb = (req.session && req.session.subd) || req.headers.subd;

        return sb;
    };

    _fn.trim = function (str) {
        if(typeof str === 'undefined' || str === null) return "";
        if(typeof str !== 'string') str = str.toString();

        return this.sanitize(str).trim();
    };

    _fn.generateUUID = function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + s4() + s4();
    };

    //Makes any call async with any number of arguments
    //So it happens on next tick of event loop
    _fn.doAsync = function (cb) {
        var ar = arguments;
        if (ar.length > 1) {
            process.nextTick(function () {
                cb.apply(null, Array.prototype.slice.call(ar, 1));
            });
        } else {
            process.nextTick(cb);
        }
    };

    /*
     * Gets IP Address of request across proxies
     */
    _fn.GetClientIP = function (req, pp, cb) {
        if (pp) {
            _fn.HTTPGet("http://checkip.dyndns.org/", function (e, r) {
                if (e) return cb(e);

                cb(e, r.replace("<html><head><title>Current IP Check</title></head><body>Current IP Address: ", "")
                    .replace("</body></html>\r\n", ""));
            });
        } else {
            if (!req || !('headers' in req)) return '0.0.0.0';
            return req.headers['x-forwarded-for']   /*For proxies */
                || req.connection.remoteAddress                                 /*next two for HTTP */
                || req.socket.remoteAddress
                || req.connection.socket.remoteAddress;
            /*for HTTPS */
        }
    };

    // Http client
    _fn.HTTPGet = function (url, cb) {
        require('easyhttp').get(url, function (html, obj) {
            var err = null, r = html;
            if (html.length == 0) err = {error: 'No html returned'};
            cb(err, r);
        });
    };

    //Converts assoc array keys to a linear array
    _fn.keysToArray = function (arr) {
        var ret = [];
        for (var kk in arr) {
            if (typeof arr[kk] != "Function") ret.push(kk);
        }
        _fn.r();
        return ret;
    };

    //Returns the current port of the app
    _fn.port = function (req) {
        var port = req.app.settings.port;
        if ((port == 80) || (port == 443)) return null;
        return port;
    };

    //This will take an array of subdomain / name and convert the subdomains to 
    //a full path based on current URL
    //This is used to render the company links at the top of each page in lobby
    _fn.getUICompanyLinks = function (req) {
        var ret = [];
        var companies = req.session.userCompanies,
            except = req.session.company.subd;

        // console.log(except);
        // console.log(companies);
        // console.log(req.session.company);

        var port = _fn.port(req);

        var basepath = req.protocol + "://%0.wwcom.com" + (port ? ":" + port : "") + req.url;

        for (var i = 0; i < companies.length; ++i) {
            if (companies[i].s != except) {
                ret.push({
                    name: companies[i].n,
                    url: basepath.replace("%0", companies[i].s)
                });

            }

        }
        return ret;
    };

    //Base Lobby params mostly for rendering the frame
    _fn.getBaseParams = function (req, title, mainFunc) {
        return {
            activeCompany: req.session.company.id,
            activeCompanyName: req.session.company.name,
            activeCompanyLogo: req.session.company.logo,
            allCompanies: _fn.getUICompanyLinks(req),

            // uid: req.session.user,
            name: req.session.userfullname,
            userphotoid: req.session.user + "." + req.session.userProfile.av[0],
            title: req.session.company.name + " - " + title,
            mainFunc: mainFunc,
            dataHashes: [],
            role: req.session.userRole,
            passCodeLbl: "",
            flashmsg: {
                text: "",
                styles: "hidden"
            }
        };
    };

    //Format number to $0,000,000
    _fn.fNumber = function (thousands, decimal, precision, prefix) {
        return {
            display: function (d) {
                console.log(d);
                var negative = d < 0 ? '-' : '';
                d = Math.abs(parseFloat(d));

                var intPart = parseInt(d, 10);
                var floatPart = precision ?
                decimal + (d - intPart).toFixed(precision).substring(2) :
                    '';

                return negative + (prefix || '') +
                    intPart.toString().replace(
                        /\B(?=(\d{3})+(?!\d))/g, thousands
                    ) +
                    floatPart;
            }
        };
    };

    //CONVERT ERROR SYSTEM
    _fn.errSystem = function (e) {
        var err = '';
        if (!e.message) return '';
        switch (e.message.toLowerCase()) {
            case "connect etimedout":
            case "connect econnrefused":
                err = 'Can\'t connect to server.';
                break;
            default:
                err = e.message;
        }
        return err;
    };

    //Merge objects;
    _fn.extend = function (target) {
        var sources = [].slice.call(arguments, 1);

        sources.forEach(function (source) {
            for (var prop in source) {
                if (source.hasOwnProperty(prop)) target[prop] = source[prop];
            }
        });

        return target;
    };

    //Merge array;
    _fn.merge = function (target) {
        var sources = [].slice.call(arguments, 1);

        sources.forEach(function (source) {
            source.forEach(function(v) {
                target.push(v);
            });
        });

        return target;
    };

    _fn.parseFloat = function(str) {
        var fv = str ? parseFloat(str) : 0;

        return !isNaN(fv) ? fv : 0;
    };

    _fn.isNumeric = function(v) {
        return !isNaN(v) && parseFloat(v) == v;
    };

    _fn.stringPart = function(str, num){
        var arrPart = [], len = str.length;
        var arrStr = str.split('');
        arrStr.forEach(function(c, i) {
            if ((i + num) < (len+1) ) arrPart.push(str.substr(i, num));
            
        });
        return arrPart;
    };

    _fn.checkPass = function(obj, pass, cb){


        obj['invalid'] = false;
        var regx = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[!@#\$%\^&\*\)\(,.\{\}\[\]\|`-_+=*/]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[!@#\$%\^&\*\)\(,.\{\}\[\]\|`-_+=*/]))|((?=.*[0-9])(?=.*[!@#\$%\^&\*\)\(,.\{\}\[\]\|`-_+=*/])))(?=.{8,})");

        var cond1 =  regx.test(pass);
        var cond2 = true;
        var letter = 'abcdefghijklmnopqrstuvwxyz',
            letterRv = letter.split("").reverse().join(""),
            digit = '0123456789',
            digitRv = digit.split("").reverse().join("");
        var arrPart = _fn.stringPart(pass, 3);
        for(var i = 0; i < arrPart.length; i++){
            var tmp = arrPart[i].toLowerCase();
            if(letter.indexOf(tmp) != -1 || letterRv.indexOf(tmp) != -1
                || digit.indexOf(tmp) != -1 || digitRv.indexOf(tmp) != -1){
                cond2 = false;
            }
        }
        if(!cond1){
            obj['invalid'] = true;
            obj['code'] = 101;
            cb();
        }else if(!cond2){
            obj['invalid'] = true;
            obj['code'] = 102;
            cb();
        }else{
            var eng = [];
            fs.readFile("public/dic/words.txt", function(e, words) {
               eng = words.toString().split('\n').filter(function(word) {
                   word = _fn.trim(word);
                   return word.length >= 4 && pass.match(new RegExp(word, 'i'));
               });
               if(eng.length){
                    obj['invalid'] = true;
                    obj['code'] = 103;
               }
               cb();
            });

        }
    };


    _fn.absentMess = function(req,  jsonMess){
        var util = require( "util" );
        var lingo       = req.lingo;
        var from = jsonMess.from ? jsonMess.from : '',
            to   = jsonMess.to ? jsonMess.to : '',
            childName   = jsonMess.childName ? jsonMess.childName : '';

        return util.format(lingo.get('absent_from'), childName, from, to);
    }

    //Finally - export the whole thing - need to use the actual name thus
    module.exports = _fn;

})(require);