/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

 /*
  * Quick file-system utils
  */

(function(r){

	var fs = r("fs");
    var Step = r("step");
	var mime = r("mime");
	var Converter=r("csvtojson").core.Converter;
	var BBParser = r('babyparse');
	var pub = {};

	//-------- public funcs

	pub.move = function(source, dest, callback) {
    	doFilePipe("move", source, dest, callback);
    };

    //Moves file and thumbnail variants
    pub.moveVariants = function(source, vs, dest, callback) {

        dest = __dirname + "/../.." + dest;
        Step(
            function doMove(){
                var group = this.group();
                //first the base file, then variants
                doFilePipe("move", source, dest, group());
                for (var i = 0; i < vs.length; ++i){

                    doFilePipe("move", source + "." + vs[i], dest + "." + vs[i], group());
                }
            },
            callback
        );
    };
	//Remove dir delete a folder recursively, async with callback 
	pub.removeRecursive = function(path,cb){
		fs.stat(path, function(err, stats) {

			if(err){
				config.error(err);
				cb(err,stats);
				return;
			}
			if(stats.isFile()){
				fs.unlink(path, function(err) {
					if(err) {
						cb(err,null);
					}else{
						cb(null,true);
					}
					return;
				});
			}else if(stats.isDirectory()){
				// A folder may contain files
				// We need to delete the files first
				// When all are deleted we could delete the 
				// dir itself
				fs.readdir(path, function(err, files) {
					if(err){
						cb(err,null);
						return;
					}
					var f_length = files.length;
					var f_delete_index = 0;

					// Check and keep track of deleted files
					// Delete the folder itself when the files are deleted

					var checkStatus = function(){
						// We check the status
						// and count till we r done
						if(f_length===f_delete_index){
							fs.rmdir(path, function(err) {
								if(err){
									cb(err,null);
								}else{
									cb(null,true);
								}
							});
							return true;
						}
						return false;
					};
					if(!checkStatus()){
						for(var i=0;i<f_length;i++){
							// Create a local scope for filePath
							// Not really needed, but just good practice
							// (as strings arn't passed by reference)
							(function(){
								var filePath = path + '/' + files[i];
								// Add a named function as callback
								// just to enlighten debugging
								pub.removeRecursive(filePath,function removeRecursiveCB(err,status){
									if(!err){
										f_delete_index ++;
										checkStatus();
									}else{
										cb(err,null);
										return;
									}
								});

							})()
						}
					}
				});
			}
		});
	};

	//Write file from data
	pub.write = function(dest,data, callback) {
		dest = __dirname + "/../.." + dest;
    	fs.writeFile(dest, data, callback);
    };
	//Read file from data
	pub.read = function(dest, callback) {
		dest = __dirname + "/../.." + dest;
		fs.readFile(dest, callback);
    };
	pub.getFile =function(path, callback){
		path=__dirname+'/../..'+path;
 		Step(
 			function readit(){
	 			fs.readFile(path, this.parallel());
	 			fs.stat(path, this.parallel());
			    this.parallel()(null, mime.lookup(path));
 			},
 			callback

		)
 	};
    //Copy list file to export folder
    pub.copyListFile = function(lstfile, source, dest, callback) {

        dest = __dirname + "/../.." + dest;
        source = __dirname + "/../.." + source;

        Step(
            function doCopy(){
				 var group = this.group();
                for (var i = 0; i < lstfile.length; ++i){
                    doFilePipe("copy", source + "/images/" + lstfile[i].k, dest + "/" + lstfile[i].n, group());
                }
            },
            callback
        );
    };

    pub.copy = function(source, dest, callback) {
    	doFilePipe("copy", source, dest, callback);
    };

    //Renmaes a file but if TO already exists, removes it
    pub.overwriteRename = function(from, to, callback){
		fs.unlink(to, function(e,r){
            //Dont use rename - that doesn't work across partitions
            pub.move(from, to, callback);
		});
    }

    //Ensures a directory path exists, making directories along the way
    //Is done as async as possible
    pub.ensureDir = function(path, callback){

        var p = path.split("/");
        p.shift();      //remove the first

        var base = __dirname + "/../..";

        var chk = base;
        function asyncEnsureFile(e){

            if (!p.length) return callback(null, path);

            chk = chk + "/" + p.shift();
            fs.exists(chk, function(exists){
                if (!exists) {
                    fs.mkdir(chk, '0777', asyncEnsureFile);
                } else asyncEnsureFile();
            });
        };

        asyncEnsureFile();
    };

    //-------- private funcs

    //Common code for copy and move
    var doFilePipe = function(type, source, dest, callback){

		var is = fs.createReadStream(source);
        is.on('error', function(err) {
            callback(type + ' error - cannot read src');
        });

        is.on('end', function() {
            if (type == "move") fs.unlinkSync(source);
            callback(null, dest);
        });
        var os = fs.createWriteStream(dest);
        os.on('error', function(err) {
            callback(type + ' error - cannot write to dest');
        });

        is.pipe(os);
    };

    pub.handleParseFile = function(path, ih, cb) {
        fs.readFile(path, 'utf-8', function (e, d) {
            if(!e) {
                d = d.split("\n");

                if(d.length && d[0] != '' && d[0].toString().indexOf('sep=') !== -1) d = d.slice(1);//Remove first line with sep=

                d = d.join("\n");

                var data = BBParser.parse(d, {header: ih});

                if(!data.errors.length) {
                    cb(null, data.data);
                    return true;
                }
            }

            cb(e || Error('An error occurred while processing please try again later'));
            return false;
        });

		//var fileStream = fs.createReadStream(path);
		////new converter instance
		//var csvConverter = new Converter({constructResult: true});
        //
		////end_parsed will be emitted once parsing finished
		//csvConverter.on("end_parsed",function(jsonObj){
         //   console.log(jsonObj);
		//	//cb(null, (ih ? jsonObj : data));
		//});
        //
        //csvConverter.on("record_parsed",function(resultRow, rawRow, rowIndex){
         //   console.log(rawRow);
         //   data.push(rawRow);
        //});
        //
        //
		////read from file
		//fileStream.pipe(csvConverter);
    };

	module.exports = pub;

})(require);