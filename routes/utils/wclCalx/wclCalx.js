/**
 * Created by AnhNguyen
 */
;
(function (r, ns) {
    "use strict";

    var calxEle = r('./calxElement.js');
    var Step = r('step');
    var moment = r('moment');
    var ObjectId = r('mongoskin').ObjectID;

    var instance = function (exec, referDiff, controls, dataProperties, fn, mn, extraData) {

        var _init = function () {

            _isExec = exec ? true : false;
            _referDifferent = referDiff ? true : false;
            _controls = controls;
            _dataProperties = dataProperties || {};
            _fn = fn;
            _manager = mn;
            _eData = extraData;
        };

        this.hasExec = function() {
            return _isExec != false;
        };

        this.addElement = function (handlerId, selfId, element, content, force, forceFormula) {
            if (!_elems[handlerId]) _elems[handlerId] = {};
            var ele = new calxEle(handlerId, selfId, this, element, content, _fn, _manager, forceFormula);
            _elems[handlerId][selfId] = ele;

            var pass = true;

            if (!force) pass = this.checkFormulaElem(handlerId, selfId);

            if (pass) triggerUpdateDependencies(handlerId, selfId, force);

            return ele;
        };

        this.removeElement = function (handlerId, selfId) {

            if (!_elems[handlerId] || !_elems[handlerId][selfId]) return false;
            var ele = _elems[handlerId][selfId];

            var dependant = ele.getDependant();
            delete _elems[handlerId][selfId];

            var v;
            for (var i in dependant) {
                if (!dependant.hasOwnProperty(i)) break;

                v = dependant[i];
                v.forEach(function (_v) {

                    _self.checkFormulaElem(i, _v);
                });
            }

        };

        this.verifyElement = function() {

            var i, j, v;
            for(i in _elems) {
                if(!_elems.hasOwnProperty(i)) break;
                v = _elems[i];

                for(j in v) {
                    if(!v.hasOwnProperty(j)) break;

                    _self.checkFormulaElem(i, j);
                }

            }
        };

        this.addRefValues = function(values) {

            _mapValues = values;
            return this;
        };

        this.getRefValues = function() {
            return _mapValues;
        };

        this.setElementValue = function(handlerId, selfId, value) {
            if(_mapValues[handlerId] && _mapValues[handlerId].controls[selfId]) {
                _mapValues[handlerId].controls[selfId].value = value;
            }

            return this;
        };

        this.getTableValue = function(handlerId, table, column) {
            if(!_mapValues[handlerId] || !_mapValues[handlerId].controls[table]) return [];
            var value = [];
            _mapValues[handlerId].controls[table].value.forEach(function(v) {
                if(v[column] !== undefined) value.push(v[column]);
            });

            return value;
        };

        this.setTableValue = function(handlerId, tableName, column, value) {
            if(!_mapValues[handlerId] || !_mapValues[handlerId].controls[tableName]) return this;

            var idx = 0;
            _mapValues[handlerId].controls[tableName].value.forEach(function(v, i) {
                if(v[column]) {
                    if(Array.isArray(value)) _mapValues[handlerId].controls[tableName].value[i][column] = value[idx];
                    else _mapValues[handlerId].controls[tableName].value[i][column] = value;
                }
                idx++;
            });

            return this;
        };

        this.getExtraData = function() {
            return _eData;
        };

        this.getElementValue = function(handlerId, selfId) {
            if(!_mapValues[handlerId] || !_mapValues[handlerId].controls[selfId]) return '';
            return _mapValues[handlerId].controls[selfId].value;
        };

        this.getElement = function (handlerId, selfId) {
            if (!_elems[handlerId] || !_elems[handlerId][selfId]) return false;
            return _elems[handlerId][selfId];
        };

        this.checkFormulaElem = function (handlerId, selfId) {

            if (!_elems[handlerId] || !_elems[handlerId][selfId]) return false;

            var obj = _elems[handlerId][selfId];
            var pass = true;
            var $ele = obj.getElement();
            var isForce = obj.isForceFormula();

            if (obj.hasFormula()) {
                try {
                    validDepend(selfId, handlerId, obj.getDependencies(), isForce);
                } catch (e) {
                    e = this.getCodeMessage(e.message);
                    throw e;
                }
            }

            if (pass) {

                //Check relate to dependant
                var v, dependants = obj.getDependant();
                for (var i in dependants) {
                    if (!dependants.hasOwnProperty(i)) break;

                    v = dependants[i];
                    v.forEach(function (_v) {

                        _self.checkFormulaElem(i, _v);
                    });
                }

            }

            return pass;
        };

        this.getCodeMessage = function (_code) {

            var code = {
                100: {
                    message: "Formula: Reference to self",
                    code   : "#ERROR!"
                },
                101: {
                    message: "Formula: Invalid reference document",
                    code   : "#ERROR!"
                },
                102: {
                    message: "Formula: Invalid reference variable",
                    code   : "#ERROR!"
                },
                103: {
                    message: "Formula: Don't allow reference to another document",
                    code   : "#ERROR!"
                }
            };

            return code[_code] ? code[_code] : {code: "#ERROR!", message: "Formula: Error"};
        };



        /**
         * Library get variable async
         */
        this.getSystemRegistry = function(folder, field, cond, params) {

            if(checkBreak(folder, field)) return undefined;

            cond = cond || [];

            var key = ['getSystemRegistry', folder, field, cond].join('-');
            if(_cacheAsync[key] !== undefined) return _cacheAsync[key];

            params._isAsync = true;
            params._async++;
            Step (
                function valid() {
                    folder = _fn.sanitize(_fn.trim(folder)).xss();
                    field = _fn.sanitize(_fn.trim(field)).xss();

                    _fn.check(folder, '#ERROR').notEmpty();
                    _fn.check(field, '#ERROR').notEmpty();

                    if(folder == 'Accounting') _manager.wdo.getObjectFactoryByKey('accsSystem').getAccs(_eData.ownerType, _eData.ownerId, this);//_manager.dcbs.getAccs(_eData.uId, _eData.ownerType, _eData.ownerId, this);
                    else _manager.dcbs.getUserRegistryByFolder(_eData.uId, _eData.ownerType, _eData.ownerId, folder, this);
                },
                function(e, r) {
                    if(e) throw e;
                    if(!r) throw Error('#NAME?');

                    if(folder == 'Accounting') {
                        var bookName = Array.isArray(cond) && cond.length ? cond[0] : (r.DefaultBook || '');

                        if(!r.books || bookName == '' || !r.books[bookName]) throw Error('#NAME?');

                        var book = r.books[bookName];

                        if(!book[field]) throw Error('#NAME?');

                        return book[field];
                    } else {

                        if(!r[folder]) throw Error('#NAME?');
                        var data = r[folder];
                        if(!data[field]) throw Error('#NAME?');

                        return data[field];
                    }
                },
                function response(e, r) {
                    if(e) {
                        _cacheAsync[key] = e.message;
                        params._error = e;
                    } else _cacheAsync[key] = r;

                    params._async--;
                    if(params._async == 0 && params._hasDone) {
                        params._elem.exec(true);
                    }
                }
            );

            return undefined;
        };

        this.updateStatusFunc = function(params, docPicker, name, value) {
            if(checkBreak(params, docPicker, name)) return undefined;

            if(!_eData.lastProc || _eData.act != 'done') return false;

            if(!Array.isArray(docPicker)) docPicker = [docPicker];
            if(!Array.isArray(value)) value = [value];
            var key = ['updateStatus', docPicker.join('_'), name].join('-');
            if(_cacheAsync[key] !== undefined) return _cacheAsync[key];

            params._isAsync = true;
            params._async++;
            Step (
                function exec() {

                    if(docPicker.length) {
                        var group = this.group();
                        docPicker.forEach(function(v, i){
                            fnUpdateStatus(params, v, name, value[i], group());
                        });
                    } else return this;
                },
                function response(e, r) {

                    if(e) {
                        _cacheAsync[key] = e.message;
                        params._error = e;
                    } else _cacheAsync[key] = true;

                    params._async--;
                    if(params._async == 0 && params._hasDone) {
                        params._elem.exec(true);
                    }
                }
            );

            return undefined;
        };

        var fnUpdateStatus = function(params, docPicker, name, value, cb){
            if(checkBreak(docPicker, name, value)) return undefined;

            var docId, trxId, isTbl, na, tblId, rowIndex;

            Step(
                function valid() {

                    var dpa = docPicker.split('-');

                    docId = dpa[0];
                    trxId = dpa[1];
                    isTbl = isTableElem(name);
                    na = isTbl ? name.split('.') : [];
                    tblId = dpa[2] || null;
                    rowIndex = dpa[3] || null;

                    _fn.check(docId, 'Impact Script: Document for updateStatus is empty.').notEmpty();
                    _fn.check(trxId, 'Impact Script: Transaction for updateStatus is empty.').notEmpty();

                    if(!value) throw Error('Impact Script: Value for updateStatus is invalid.');

                    _fn.doAsync(this);
                },
                function getWDO(e, r) {
                    if(e) throw e;

                    _manager.wdo.find({
                        _object_type: 1,
                        _object_id: _fn.dbId(docId)
                    }, this.parallel()); //Find WDO for Document;

                    _manager.wdo.find({
                        _object_type               : 4,
                        _object_id                 : _fn.dbId(docId),
                        "_header._workflow_data_id": _fn.dbId(trxId)
                    }, this.parallel()); //Find TrxData WDO;
                },
                function getData(e, wd, wt) {
                    if(e) throw e;
                    if(!wt) throw Error('Impact Script: Could not find transaction data for updateStatus.');

                    _fn.r(wt, this.parallel());
                    _manager.wdo.getFillingData(wd._id, wd, this.parallel());
                    _manager.wdo.getFillingData(wt._id, wt, this.parallel());
                },
                function execUpdate(e, wt, d, dt) {
                    if(e) throw e;
                    if(!d || !dt) throw Error('Impact Script: Could not find transaction data for updateStatus.');
                    var dcMap = {}, controls = dt.controls;
                    if(d.controls) {

                        d.controls.forEach(function(v) {
                            dcMap[v.name] = v.id;
                            });
                        }
                    
                    var ctrlId, $set = {};
                    if(tblId){
                        if(!isTbl) {
                            ctrlId = dcMap[name];
                            if(!ctrlId || !controls[ctrlId]) throw Error('Impact Script: Could not find item name for updateStatus.');

                            //controls[ctrlId]._value = calcValue(controls[ctrlId]._value, value);
                            $set['_controls.' + ctrlId + '._value'] = calcValue(controls[ctrlId]._value, value);
                        }else{
                            ctrlId = dcMap[na[0]];
                            if(!ctrlId || !controls[ctrlId]) throw Error('Impact Script: Could not find item name for updateStatus.');
                            controls[ctrlId]._value.forEach(function(v, i) {
                                if(typeof v[na[1]] !== "undefined") {
                                    if(rowIndex == null){
                                        //controls[ctrlId]._value[i][na[1]] = calcValue(v[na[1]], value);
                                        $set['_controls.' + ctrlId + '._value.' + i + '.' + na[1]] = calcValue(v[na[1]], value);
                                    }
                                    else if(i == rowIndex){
                                        //controls[ctrlId]._value[i][na[1]] = calcValue(v[na[1]], value);
                                        $set['_controls.' + ctrlId + '._value.' + i + '.' + na[1]] = calcValue(v[na[1]], value);
                                    }
                                }
                            });
                        }
                        
                    }else{

                        if(!isTbl) {
                            ctrlId = dcMap[name];

                            if(!ctrlId || !controls[ctrlId]) throw Error('Impact Script: Could not find item name for updateStatus.');

                            //controls[ctrlId]._value = calcValue(controls[ctrlId]._value, value);
                            $set['_controls.' + ctrlId + '._value'] = calcValue(controls[ctrlId]._value, value);
                        } else {
                            ctrlId = dcMap[na[0]];
                            if(!ctrlId || !controls[ctrlId]) throw Error('Impact Script: Could not find item name for updateStatus.');

                            controls[ctrlId]._value.forEach(function(v, i) {
                                if(typeof v[na[1]] !== "undefined") {
                                    //controls[ctrlId]._value[i][na[1]] = calcValue(v[na[1]], value);
                                    $set['_controls.' + ctrlId + '._value.' + i + '.' + na[1]] = calcValue(v[na[1]], value);
                                }
                            });
                        }
                    }

                    if(Object.keys($set).length != 0) _manager.wdo.getObjectFactoryByKey('wfData').update(dt._id, $set, this);
                    return this;
                },
                cb
            );

            return undefined;
        };


        /**
         * StatAdd - Statistical Impacts
         */
        this.statAddFunc = function(params, BookName, AccountCode, Description, Period, BaseUnit, BaseAmount1, BaseAmount2, BookingUnit, BookingAmount1, BookingAmount2, SubAccount) {
            if(checkBreak(BookName, AccountCode, Description, Period, BaseUnit, BaseAmount1, BaseAmount2, BookingUnit, BookingAmount1, BookingAmount2)) return undefined;

            var key = ['statAdd', BookName, AccountCode, Period, BaseUnit].join('-');
            if(_cacheAsync[key] !== undefined) return _cacheAsync[key];

            params._isAsync = true;
            params._async++;

            var uid = _eData.uId, companyId = _eData.ownerType, branchId = _eData.ownerId;
            var args = Array.prototype.slice.call(arguments, 1), res = {
                general: {},
                account: {}
            };
            var books = _eData.bookList || {};

            Step(
                function calc() {
                    if(BookingUnit !== undefined) {
                        if(BookingAmount1 === undefined || BookingAmount2 === undefined) throw Error('#PARAM?');
                        //if(Array.isArray(BookingAmount1) && BookingAmount1.length != BookingAmount2.length) throw Error('#PARAM?');
                    }

                    var arrLen = 1;
                    args.forEach(function(v) {
                        arrLen = (Array.isArray(v) && v.length > arrLen) ? v.length : arrLen;
                    });

                    var idx, tmp, ele, _break, book, acc, path, i;
                    var decRegex = /^(?:-?(?:[0-9]+))?(?:\.[0-9]*)?(?:[eE][\+\-]?(?:[0-9]+))?$/;
                    for(idx = 0, i = 0; idx < arrLen; idx++) {
                        tmp = [];
                        _break = false;

                        for(i = 0; i < 10; i++) {

                            ele = args[i] || '';
                            if(Array.isArray(ele)) {
                                if(ele[idx] === undefined) {
                                    _break = true;
                                    break;
                                } else tmp.push(ele[idx]);
                            } else tmp.push(ele);
                        }

                        //Validate statistical item
                        try {

                            if(books[tmp[0]]) { //BookName is found in Accounting Setup

                                book = books[tmp[0]];
                                if(!Object.keys(book.acc).indexOf(tmp[1]) == -1) throw Error('AccountCode not exists'); //AccountCode not exists in referential table

                                if(/[0-9]{2}\-[0-9]{4}/.test(tmp[3])) throw Error('Period format is invalid.'); //Period not follow the format 99-YYYY

                                acc = book.acc[tmp[1]];
                                if(tmp[4] != book.cur) throw Error('BaseUnit not same BaseUnit'); //BaseUnit not same BaseUnit

                                if(acc.Currency && acc.Currency != "" && tmp[7] && tmp[7] != acc.Currency) throw Error('Currency entered not the same with fixed currency in Referential Table'); //BookingUnit not exists

                                if(!decRegex.test(tmp[5]) || !decRegex.test(tmp[6])) throw Error('BaseAmount1 & BaseAmount2 not valid');
                                if(tmp[8] != '' && !decRegex.test(tmp[8])) throw Error('BookingAmount1 not valid');
                                if(tmp[9] != '' && !decRegex.test(tmp[9])) throw Error('BookingAmount2 not valid');
                            }

                        } catch(_e) {
                            _break = true;
                        }

                        if(!_break){

                            path = books[tmp[0]] ? res.account : res.general;
                            if(!path[tmp[0]]) path[tmp[0]] = {};
                            if(!path[tmp[0]][tmp[3]]) path[tmp[0]][tmp[3]] = {};
                            if(!path[tmp[0]][tmp[3]][tmp[4]]) path[tmp[0]][tmp[3]][tmp[4]] = [];

                            path[tmp[0]][tmp[3]][tmp[4]].push(tmp);
                        }
                    }

                    return res;
                },
                function getStats(e, r) {
                    if(e) throw e;

                    var trxId = _eData.transInfo._id;
                    var g1 = this.group(),
                        g2 = this.group();

                    var type, book, period, p, py, pm, unit, stats = {};
                    for(type in r) {
                        if(!r.hasOwnProperty(type)) continue;

                        for(book in r[type]) {
                            if(!r[type].hasOwnProperty(book)) continue;

                            for(period in r[type][book]) {
                                if(!r[type][book].hasOwnProperty(period)) continue;

                                p = period.split('-');
                                py = parseInt(p[0], 10);
                                pm = parseInt(p[1], 10);

                                //DSF validate Period Lock;
                                _manager.dcbs.isPeriodLock(book, py, pm, companyId, branchId, g1());

                                for(unit in r[type][book][period]) {
                                    if(!r[type][book][period].hasOwnProperty(unit)) continue;

                                    r[type][book][period][unit].forEach(function(v) {
                                        stats[[book, period, v[1]].join('-')] = {
                                            book: book,
                                            period: period,
                                            acc: v[1]
                                        };
                                    });
                                }

                                _manager.dcbs.getStats(trxId, book, period, companyId, branchId, g2());
                            }
                        }
                    }

                    _fn.r(stats, this.parallel());
                    _fn.r(r, this.parallel());
                },
                function clearData(e, p, r, r2, s) {
                    if(e) throw e;

                    //DSF validate Period Lock;
                    if(p && p.length) {
                        var i, l = p.length;
                        for(i = 0; i < l; i++) {
                            if(p[i]) throw Error("Couldn't Execute STATADD Impact script. Period has been locked.");
                        }
                    }

                    _fn.r(s, this.parallel());

                    var ids = [];

                    if(r.length) {
                        r.forEach(function (v) {
                            if(v.length) {
                                v.forEach(function (_v) {
                                    ids.push(_v._id);
                                });
                            }
                        });
                    }

                    if(ids.length) _manager.dcbs.deleteStats(ids, this.parallel());

                    var group = this.group();
                    var v, k, p;
                    for(k in r2) {
                        if(r2.hasOwnProperty(k)) {

                            v = r2[k];
                            p = v.period.split('-');

                            _manager.dcbs.deleteStatsSum(v.book, companyId, branchId, v.acc, p[0], p[1], group());
                        }
                    }
                },
                function insert(e, r) {
                    if(e) throw e;

                    var trxId = _eData.transInfo._id;
                    var type, book, period, unit, stats = [], statMap = {}, statKey, statsDetail = [], now = new Date(), p;

                    var group = this.group();
                    for(type in r) {
                        if(!r.hasOwnProperty(type)) continue;

                        for(book in r[type]) {
                            if(!r[type].hasOwnProperty(book)) continue;

                            for(period in r[type][book]) {
                                if(!r[type][book].hasOwnProperty(period)) continue;

                                for(unit in r[type][book][period]) {
                                    if(!r[type][book][period].hasOwnProperty(unit)) continue;

                                    r[type][book][period][unit].forEach(function(v) {
                                        p = period.split('-');
                                        statKey = [book, period, v[4]].join('-');

                                        if(!statMap[statKey]) {
                                            statMap[statKey] = new ObjectId();

                                            stats.push({
                                                _id: statMap[statKey],
                                                docflow_data_id: trxId,
                                                book_name: book,
                                                period: period,
                                                user_posted: uid,
                                                time_posted: now,
                                                company_id: companyId,
                                                branch_id: branchId,
                                                base_unit: v[4],
                                                type: type
                                            });
                                        }

                                        statsDetail.push({
                                            _id: new ObjectId(),
                                            impact_id: statMap[statKey],
                                            account_id: v[1],
                                            description: v[2],
                                            sub_account: v[10],
                                            base_amount1: _fn.parseFloat(v[5]),
                                            base_amount2: _fn.parseFloat(v[6]),
                                            booking_unit: v[6],
                                            booking_amount1: _fn.parseFloat(v[8]),
                                            booking_amount2: _fn.parseFloat(v[9])
                                        });

                                        _manager.dcbs.insertStatsSum(book, companyId, branchId, v[1], v[10], v[4], period, p[0], p[1], 0, v[5], v[6], group());
                                    });
                                }
                            }
                        }
                    }

                    if(stats.length) _manager.dcbs.insertStats(stats, this.parallel());
                    if(statsDetail.length) _manager.dcbs.insertStatsDetail(statsDetail, this.parallel());
                },
                function callLock(e, r) {
                    if(e) {
                        _cacheAsync[key] = e.message;
                        params._error = e;
                    } else _cacheAsync[key] = 1;

                    params._async--;
                    if(params._async == 0 && params._hasDone) {
                        params._elem.exec(true);
                    }
                }
            );

            return undefined;
        };

        this.statLockFunc = function(params, book, period, arg) {
            if(checkBreak(book, period)) return undefined;

            //Ignore Execute formula for checkbox;
            if(arg) return params._elem.getValue();

            if(!_eData.lastProc || _eData.act != 'done') return false;

            var p = period.split('-');
            var py = parseInt(p[0], 10), pm = parseInt(p[1], 10);

            py = !isNaN(py) ? py : 0;
            pm = !isNaN(pm) ? pm : 0;

            if(!py || !pm) return false;

            var key = ['statLock', book, period].join('-');
            if(_cacheAsync[key] !== undefined) return _cacheAsync[key];

            params._isAsync = true;
            params._async++;

            var uid = _eData.uId, companyId = _eData.ownerType, branchId = _eData.ownerId;
            var fiscalStart = 1, retailEarningAcc = null;
            var snapshot = true;

            Step(
                function checkLock() {
                    _manager.dcbs.getPeriodLock(book, py, pm, companyId, branchId, this);
                },
                function getStatSum(e, r) {
                    if(e) throw e;
                    if(!r || (r && !r.lockAR && !r.lockAP && !r.lockGL)) throw Error("AR, AP and GL have to locked before you can perform period locking.");

                    _manager.dcbs.getStatsSum(0, book, companyId, branchId, py, pm, this.parallel());
                    _manager.wdo.getObjectFactoryByKey('accsSystem').getAccs(_eData.ownerType, _eData.ownerId, this);
                },
                function lock(e, r, ac) {
                    if(e) throw e;

                    if(!r || !r.length) throw Error("An error occurred while processing please try again later");

                    r = r[0];

                    if(!r.length) throw Error("Period Lock error. Not have any records to lock!");

                    var books = ac && ac.books ? ac.books : {};

                    if(ac.DefaultBook && books[ac.DefaultBook]) {
                        var book = books[ac.DefaultBook];

                        if(book.FiscalStart) {
                            var ms = {"Jan": 1, "Feb": 2, "Mar": 3, "Apr": 4, "May": 5, "Jun": 6, "Jul": 7, "Aug": 8, "Sep": 9, "Oct": 10, "Nov": 11, "Dec": 12};

                            fiscalStart = ms[book.FiscalStart] || 1;
                        }

                        if(book.RetainedEarning) retailEarningAcc = book.RetainedEarning;
                    }

                    _fn.r(books, this.parallel());
                    _fn.r(r, this.parallel());

                    if(books[book]) {
                        _manager.dcbs.getLatestLockByBookName(book, companyId, branchId, this.parallel());
                        _manager.dcbs.getAccStats(book, period, companyId, branchId, this.parallel());
                    } else {
                        _fn.r([], this.parallel());
                        _fn.r([], this.parallel());
                    }

                    var upd = {locked: true};
                    _manager.dcbs.savePeriodLock(book, py, pm, companyId, branchId, upd, this.parallel());
                },
                function prepare(e, books, s, ll, sd, r) {
                    if(e) throw e;

                    _fn.r(books, this.parallel());
                    _fn.r(s, this.parallel());
                    _fn.r(ll, this.parallel());
                    _fn.r(sd, this.parallel());

                    var accAmountMap = {}, accCodes = [];

                    if(books[book]) { //Book found in Accounting Book. This is Accounting Lock;

                        if(ll.length && ((pm - 1) != ll[0].period_month)) snapshot = false;

                        if(snapshot) {
                            s.forEach(function(v) {
                                _fn.array.pushUnique(accCodes, v.account_id, true);
                            });

                            if(sd && sd.length) {
                                var amountDiff, key;
                                sd.forEach(function(v) {
                                    if(!accAmountMap[v.account_id]) accAmountMap[v.account_id] = {accumalate1: 0, accumalate2: 0};

                                    amountDiff = v.amount1 - v.amount2;

                                    if(amountDiff < 0) {
                                        key = "accumalate2";
                                        amountDiff = (amountDiff * -1);
                                    } else key = "accumalate1";

                                    accAmountMap[v.account_id][key] += amountDiff;

                                    _fn.array.pushUnique(accCodes, v.account_id, true);
                                });

                                _fn.r(accAmountMap, this.parallel());
                            }

                            if(accCodes.length) _manager.rdmp.getCOAAccountTypeByAccountCode(accCodes.join(','), this.parallel());
                        }
                    }
                },
                function takeSnapshot(e, books, s, ll, sd, aam, r) {
                    if(e) throw e;

                    if(!aam) aam = {};

                    var data = [], now = new Date(), accMap = {};

                    if(books[book]) { //Book found in Accounting Book. This is Accounting Lock;
                        if(snapshot) {
                            var accTypeMap = {};
                            var balance = {
                                retailEarning: {
                                    credit: 0,
                                    debit: 0
                                },
                                revenue: {
                                    credit: 0,
                                    debit: 0
                                },
                                costs: {
                                    credit: 0,
                                    debit: 0
                                },
                                overheads: {
                                    credit: 0,
                                    debit: 0
                                }
                            };

                            if(r && r.length) {
                                r.forEach(function(v) {
                                    accTypeMap[v.account_code] = v.account_type.toLowerCase();
                                });
                            }

                            for (var i in aam) {
                                if (!aam.hasOwnProperty(i)) continue;

                                if (retailEarningAcc && retailEarningAcc == i) {
                                    balance.retailEarning.debit += aam[i].accumalate1;
                                    balance.retailEarning.crebit += aam[i].accumalate2;
                                } else {
                                    if (!accTypeMap[i] || !balance[accTypeMap[i]]) continue;

                                    balance[accTypeMap[i]].debit += aam[i].accumalate1;
                                    balance[accTypeMap[i]].crebit += aam[i].accumalate2;
                                }

                            }

                            var accType, amountDiff, amount1, amount2, aml1, aml2;
                            s.forEach(function (v) {
                                accType = accTypeMap[v.account_id] || "";
                                amount1 = _fn.parseFloat(v.amount1);
                                amount2 = _fn.parseFloat(v.amount2);

                                if(pm == fiscalStart && retailEarningAcc && v.account_id == retailEarningAcc) {
                                    amountDiff = 0;

                                    for(var k in balance) {
                                        if(!balance.hasOwnProperty(k)) continue;

                                        amountDiff += (balance[k].debit - balance[k].credit);
                                    }

                                    if(amountDiff < 0) aml2 += (amountDiff * -1);
                                    else aml1 += amountDiff;

                                } else if(pm == fiscalStart && ["revenue", "costs", "overheads"].indexOf(accType) !== -1) {
                                    aml1 = aml2 = 0;
                                } else {
                                    aml1 = (aam[v.account_id] ? aam[v.account_id].accumalate1 : 0);
                                    aml2 = (aam[v.account_id] ? aam[v.account_id].accumalate2 : 0);

                                    amountDiff = amount1 - amount2;

                                    if(amountDiff < 0) aml2 += (amountDiff * -1);
                                    else aml1 += amountDiff;
                                }

                                data.push({
                                    _id: new ObjectId(),
                                    book_name: v.book_name,
                                    company_id: v.company_id,
                                    branch_id: v.branch_id,
                                    account_id: v.account_id,
                                    sub_account: v.sub_account,
                                    unit: v.unit,
                                    period: v.period,
                                    amount1: amount1,
                                    amount2: amount2,
                                    accumulate1: aml1,
                                    accumulate2: aml2,
                                    time: now
                                });

                                accMap[v.account_id] = v.account_id;
                            });
                        }
                    } else {
                        //Do lock with General mode;
                        s.forEach(function (v) {
                            data.push({
                                _id: new ObjectId(),
                                book_name: v.book_name,
                                company_id: v.company_id,
                                branch_id: v.branch_id,
                                account_id: v.account_id,
                                sub_account: v.sub_account,
                                unit: v.unit,
                                period: v.period,
                                amount1: _fn.parseFloat(v.amount1),
                                amount2: _fn.parseFloat(v.amount2),
                                time: now
                            });

                            accMap[v.account_id] = v.account_id;
                        });
                    }

                    if(data) _manager.dcbs.saveStatsSum(data, this.parallel());
                    else _fn.r(null, this.parallel());

                    _fn.r(accMap, this.parallel());
                },
                function removeMSSum(e, r, am) {
                    if(e) throw e;

                    var k, group = this.group();
                    for(k in am) {
                        if(am.hasOwnProperty(k)) _manager.dcbs.deleteStatsSum(book, companyId, branchId, k, py, pm, group());
                    }

                    this.parallel()(e, null);
                },
                function response(e, r) {
                    if(e) {
                        _cacheAsync[key] = e.message;
                        params._error = e;
                    } else _cacheAsync[key] = 1;

                    params._async--;
                    if(params._async == 0 && params._hasDone) {
                        params._elem.exec(true);
                    }
                }
            );

            return undefined;
        };

        this.genSequence = function(func, args, params) {
            if(checkBreak(func, args)) return undefined;

            if(!args) args = [];
            var key = ['genSequence', func, args.join('_')].join('-');
            if(_cacheAsync[key] !== undefined) return _cacheAsync[key];

            params._isAsync = true;
            params._async++;
            var docId = _mapValues[params._handlerId].id;
            var trxId = _eData.transInfo._id.toString();

            Step (
                function genSeq() {
                    _manager.wdo.getObjectFactoryByKey('seqDesign').generateSeq(func, _eData.uId, docId, trxId, _eData.act || '', '', args, this);
                },
                function response(e, r) {
                    if(e) {
                        _cacheAsync[key] = e.message;
                        params._error = e;
                    } else _cacheAsync[key] = r.seq || "";

                    params._async--;
                    if(params._async == 0 && params._hasDone) {
                        params._elem.exec(true);
                    }
                }
            );

            return undefined;
        };

        this.getCodeTable = function(table, column, params) {
            if(checkBreak(table, column)) return undefined;

            var key = ['getCodeTable', table, column].join('-');
            if(_cacheAsync[key] !== undefined) return _cacheAsync[key];

            params._isAsync = true;
            params._async++;
            Step (
                function getCode() {

                    var mapCodeName = {
                        'CustomerVendor' : 'Customer/Vendor'
                    };

                    if(mapCodeName[table]) table = mapCodeName[table];

                    _manager.rdmp.getCodeListInforPaged(0, _eData.ownerType, _eData.ownerId, _eData.uId, table, 1, 10000000, '', this.parallel());
                    _fn.r(table.replace(/\_/gi, ' '), this.parallel());
                },
                function getData(e, r, t) {
                    if(e) throw e;
                    if(!r.length || !r[0][0] || (r[0][0].name != table && r[0][0].name != t)) throw Error('Code data is invalid');

                    r = r[0][0];
                    _manager.rdmp.searchCodeListRowPaged(r.id, _eData.uId, _eData.ownerType, _eData.ownerId, '', 1, 1000000000, '', this);
                },
                function mapData(e, r) {
                    if(e) throw e;
                    if(!r[0].length) throw Error('Error');

                    var data = [];
                    r[0].forEach(function(v) {
                        if(v[column] !== undefined) data.push(v[column]);
                    });

                    return data;
                },
                function response(e, r) {
                    if(e) _cacheAsync[key] = [];
                    else _cacheAsync[key] = r;

                    params._async--;
                    if(params._async == 0 && params._hasDone) {
                        params._elem.exec(true);
                    }
                }
            );

            return undefined;
        };

        var triggerUpdateDependencies = function (handlerId, selfId, force) {

            var i, j, _v;
            for (i in _elems) {
                if (!_elems.hasOwnProperty(i)) continue;

                for (j in _elems[i]) {
                    if (!_elems[i].hasOwnProperty(j)) break;

                    _v = _elems[i][j];
                    if (_v.hasDependency(handlerId, selfId)) {
                        _v.buildDependant();
                        if(!force) _self.checkFormulaElem(i, j);
                    }
                }
            }

        };

        var checkBreak = function() {

            var _break = false;
            for(var i in arguments) {
                if(!arguments.hasOwnProperty(i)) break;
                if(arguments[i] === undefined) {
                    _break = true;
                    break;
                }
            }

            return _break;
        };

        var mapControls = function () {

            var controls = {}, i, v, _i, _v;
            if (!_referDifferent) {
                for (i in _controls) {
                    if (!_controls.hasOwnProperty(i)) break;

                    v = _controls[i];
                    controls[v.name] = v;
                }
            } else {
                for (i in _controls) {
                    if (!_controls.hasOwnProperty(i)) continue;

                    v = _controls[i].controls;
                    controls[_controls[i].name] = {};

                    for (_i in v) {
                        if (!v.hasOwnProperty(_i)) break;

                        _v = v[_i];
                        controls[_controls[i].name][_v.name] = _v;
                    }
                }
            }

            return controls;
        };

        var validDepend = function (curName, curDoc, depends, force) {

            var col, regex = /^(.*)\.(.*)$/g, res, control;
            if (/^(.*)\.(.*)$/gi.test(curName)) {
                res = regex.exec(curName);
                curName = res[1];
                col = res[2];
            } else col = '';

            var controls = mapControls();

            if(!force) {

                if (_referDifferent) {

                    if (!controls[curDoc] || !controls[curDoc][curName]) throw Error(102);
                    control = controls[curDoc][curName];
                } else {

                    if (!controls[curName]) throw Error(102);
                    control = controls[curName];
                }

                if (control.elmType != 8 && col != '') throw Error(102);

                if (control.elmType == 8 && col != '') {

                    var _v;
                    for (var _i in control.cols) {
                        if (!control.cols.hasOwnProperty(_i)) break;

                        _v = control.cols[_i];

                        if (_v.name == col) {
                            col = _v;
                            break;
                        }
                    }

                    if (!(typeof col == 'object' && col.constructor == Object)) throw Error(102);
                }
            }

            checkCircularDepend(curName, curDoc, depends, controls);
        };

        var checkCircularDepend = function (curName, curDoc, depends, controls) {

            var v, col, res, cName, control, cCol, j;

            for (var i in depends) {
                if (!depends.hasOwnProperty(i)) break;

                v = depends[i];

                if (!_referDifferent && i != curDoc) throw Error(103);

                v.forEach(function (_v) {

                    if (i == curDoc && _v == curName) throw Error(100);

                    if (/^(.*)\.(.*)$/gi.test(_v)) {
                        res = /^(.*)\.(.*)$/gi.exec(_v);
                        cName = res[1];
                        col = res[2];
                    } else {
                        cName = _v;
                        col = '';
                    }

                    if (_referDifferent) {
                        if (!controls[i] || !controls[i][cName]) {
                            throw Error(101);
                        }
                        control = controls[i][cName];
                    } else {
                        if (!controls[cName]) {
                            throw Error(101);
                        }
                        control = controls[cName];
                    }

                    if(control.elmType == 8 && col != '') {

                        cCol = false;
                        for(j in control.cols) {
                            if(!control.cols.hasOwnProperty(j)) break;
                            if(control.cols[j].name == col) {
                                cCol = control.cols[j];
                                break;
                            }
                        }

                        if(!cCol) {
                            throw Error(101);
                        }
                    }

                    if(_elems[i] && _elems[i][_v]) checkCircularDepend(curName, curDoc, _elems[i][_v].getDependencies(), controls);
                });
            }

        };

        var isTableElem = function(name) {
            return /^(.*)\.(.*)$/gi.test(name);
        };

        var calcValue = function(ov, nv) {
            var v = ov;

            if(ov == "true" || ov == "false") {
                if(["TRUE", "True", "true", true, "T", "t", "1", 1].indexOf(nv) !== -1) v = true;
                if(["FALSE", "False", "false", false, "F", "f", "0", 0].indexOf(nv) !== -1 || nv === "") v = false;

                return v;
            }

            if(_fn.isNumeric(ov)) {
                if(nv === "") return 0;
                if(typeof nv === "number") return nv;

                var fc = nv.charAt(0);

                switch (fc) {
                    case "+":
                        v = parseFloat(ov) + parseFloat(nv.slice(1));
                        break;
                    case "-":
                        v = parseFloat(ov) - parseFloat(nv.slice(1));
                        break;
                    case "*":
                        v = parseFloat(ov) * parseFloat(nv.slice(1));
                        break;
                    case "/":
                        v = nv != "0" ? parseFloat(ov) / parseFloat(nv.slice(1)) : 0;
                        break;
                    default :
                        v = parseFloat(nv);
                        break;
                }

                return v;
            }

            return nv;
        };

        var _fn, _manager;
        var _self = this;
        var _elems = {};
        var _isExec = false;
        var _controls = {};
        var _dataProperties = {};
        var _mapValues = {};
        var _eData = {};
        var _cacheAsync = {};
        var _referDifferent = false;

        _init();

    };

    ns.exports = instance;


})(require, module);
