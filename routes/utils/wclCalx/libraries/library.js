;
(function (r, ns) {

    var _fn,
        _manager;

    var moment = r('moment');

    var formula = function (handlerId, calx, elem, fn, mn) {

        _fn = fn;
        _manager = mn;

        var self = this;

        var data = {

            MEMOIZED_FACT : [],

            SQRT2PI : 2.5066282746310002,

            WEEK_STARTS : [
                undefined,
                0,
                1,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                1,
                2,
                3,
                4,
                5,
                6,
                0
            ],

            WEEK_TYPES : [
                [],
                [1, 2, 3, 4, 5, 6, 7],
                [7, 1, 2, 3, 4, 5, 6],
                [6, 0, 1, 2, 3, 4, 5],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [7, 1, 2, 3, 4, 5, 6],
                [6, 7, 1, 2, 3, 4, 5],
                [5, 6, 7, 1, 2, 3, 4],
                [4, 5, 6, 7, 1, 2, 3],
                [3, 4, 5, 6, 7, 1, 2],
                [2, 3, 4, 5, 6, 7, 1],
                [1, 2, 3, 4, 5, 6, 7]
            ],

            WEEKEND_TYPES : [
                [],
                [6, 0],
                [0, 1],
                [1, 2],
                [2, 3],
                [3, 4],
                [4, 5],
                [5, 6],
                undefined,
                undefined,
                undefined,
                [0],
                [1],
                [2],
                [3],
                [4],
                [5],
                [6]
            ],

            DAY_NAME : [
                'Sunday',
                'Monday',
                'Thuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday'
            ],

            ERROR : [
                '#DIV/0!',
                '#N/A',
                '#NAME?',
                '#NUM!',
                '#NULL!',
                '#REF!',
                '#VALUE!',
                '#ERROR!',
                '#ERROR_MOMENT_JS_REQUIRED!',
                '#ERROR_JSTAT_JS_REQUIRED!',
                '#ERROR_AJAX_URL_REQUIRED!',
                '#ERROR_SEND_REQUEST!',
                '#UNDEFINED_VARIABLE!'
            ],

            ERRKEY : {
                jStatRequired : '#ERROR_JSTAT_JS_REQUIRED!',
                momentRequired : '#ERROR_MOMENT_JS_REQUIRED!',
                ajaxUrlRequired : '#ERROR_AJAX_URL_REQUIRED!',
                sendRequestError : '#ERROR_SEND_REQUEST!'
            },

            VARIABLE : {},

            SELF_RENDER_FORMULA : [
                'GRAPH'
            ]
        };

        this.params = {
            _async          : 0,
            _calx           : calx,
            _elem           : elem,
            _isAsync        : false,
            _hasDone        : false,
            _asyncData      : {},
            _buildDependency: false,
            _handlerId      : handlerId,
            _dependencies   : {
                'sameHandler' : [],
                'otherHandler': {}
            }
        };

        this.callFunction = function (functionName, params) {
            var category, func;

            func = functionName.toUpperCase();
            if (typeof(this[func]) == 'function') {
                return this[func].apply(this, params);
            }

            for (category in this) {
                if (typeof(this[category][func]) == 'function') {
                    return this[category][func].apply(this, params);
                }
            }

            return '#NAME?';
        };

        this.callbackExec = function() {
            if(this.params._isAsync) {
                if(this.params._async == 0) {
                    this.params._isAsync = false;

                    if(this.params._callback) this.params._callback(this.params._error || null);
                    else this.params._elem.exec(true);
                }
            } else {
                if(this.params._callback) this.params._callback(this.params._error || null);
            }

            this.params._hasDone = true;
        };

        //Original
        this.original = {

            getVariable: function (name) {
                if (this.params._buildDependency) {
                    this.params._dependencies.sameHandler.pushUnique(name);
                    return '';
                }

                return this.params._calx.getElement(this.params._handlerId, name).exec();
            },

            getTableColumn: function (table, column) {

                if (this.params._buildDependency) {
                    this.params._dependencies.sameHandler.pushUnique(Array.prototype.join.call(arguments, '.'));
                } else {
                    return this.params._calx.getTableValue(this.params._handlerId, table, column);
                }
            },

            getTableColInAnotherDoc: function (doc, table, column) {

                if (this.params._buildDependency) {
                    if (!this.params._dependencies.otherHandler[doc]) this.params._dependencies.otherHandler[doc] = [];
                    this.params._dependencies.otherHandler[doc].pushUnique([table, column].join('.'));
                    return '';
                } else {
                    return this.params._calx.getTableValue(doc, table, column);
                }
            },

            getVariableInAnotherDoc: function (doc, name) {
                if (this.params._buildDependency) {
                    if (!this.params._dependencies.otherHandler[doc]) this.params._dependencies.otherHandler[doc] = [];
                    this.params._dependencies.otherHandler[doc].pushUnique(name);
                    return '';
                } else {
                    return this.params._calx.getElement(doc, name).exec();
                }
            },

            getCodeTable: function (table, column) {
                if(!this.params._buildDependency) return this.params._calx.getCodeTable(table, column, this.params);
                return [];
            },

            getSystemRegistry : function(folder, field, cond) {
                if(!this.params._buildDependency) return this.params._calx.getSystemRegistry(folder, field, cond, this.params);
                return '';
            },

            callSequential: function (functionName, args) {
                this.params._elem.setRunable(true);

                functionName = functionName.replace(/\#/gi, '');
                if(!this.params._buildDependency) return this.params._calx.genSequence(functionName, args, this.params);
                return '';
            }
        };


        //General
        this.general = {

            VLOOKUP: function (value, colRef, approx) {
                if(this.params._buildDependency) return '';

                if(!Array.isArray(colRef) || !Array.isArray(approx) || colRef.length != approx.length) return '#REF!';

                var res, idx;
                if(Array.isArray(value)) {

                    res = [];
                    value.forEach(function(v) {
                        idx = colRef.indexOf(v);
                        if(idx == -1) res.push('#N/A!');
                        else res.push(approx[idx]);
                    });

                } else {

                    idx = colRef.indexOf(value);
                    if(idx == -1) res = '#N/A!';
                    else res = approx[idx];
                }

                return res;
            },

            STATADD : function() {
                if(!this.params._buildDependency) {
                    var args = Array.prototype.slice.call(arguments, 0);

                    args.unshift(this.params);

                    return this.params._calx.statAddFunc.apply(this.params._calx, args);
                }

                return [];
            },

            STATLOCK : function() {
                if(!this.params._buildDependency) {
                    var args = Array.prototype.slice.call(arguments, 0);

                    args.unshift(this.params);

                    return this.params._calx.statLockFunc.apply(this.params._calx, args);
                }

                return false;
            },

            UPDATESTATUS : function() {
                if(!this.params._buildDependency) {
                    var args = Array.prototype.slice.call(arguments, 0);
                    args.unshift(this.params);

                    return this.params._calx.updateStatusFunc.apply(this.params._calx, args);
                }

                return false;
            },

            DOCUMENTNAME: function () {
                return this.params._handlerId;
            },

            PROCESSNAME: function () {

                if(this.params._buildDependency) return '';

                var eData = this.params._calx.getExtraData();
                var trxInfo = eData.transInfo || {};
                var cProc = trxInfo._current_process || {};
                return cProc._process_name || '';
            },

            CURRENTUSER: function () {
                if(this.params._buildDependency) return '';

                var eData = this.params._calx.getExtraData();
                return eData.uName || '';
            },

            CREATEDUSER: function () {
                if(this.params._buildDependency) return '';

                var eData = this.params._calx.getExtraData();
                var trxInfo = eData.transInfo || {};
                return trxInfo._initiated_by_name || '';
            },
            PRINTEDUSER: function () {
                if(this.params._buildDependency) return '';

                this.params._elem.setRunable(true);

                var eData = this.params._calx.getExtraData();
                if(!eData || !eData.act || eData.act != 'print') return '';

                return eData.uName || '';
            }
        };

        //Datetime
        this.date = {
            time: function (time) {
                if(this.params._buildDependency) return '';

                var $time = time.split(':'),
                    $today = new Date(),
                    $hour = typeof($time[0]) == 'undefined' ? 0 : $time[0],
                    $minute = typeof($time[1]) == 'undefined' ? 0 : $time[1],
                    $second = typeof($time[2]) == 'undefined' ? 0 : $time[2],
                    $result = new Date($today.getFullYear(), $today.getMonth(), $today.getDate(), $hour, $minute, $second);

                return $result;
            }
        };

        //Comparator
        this.comparator = {
            concat : function (a, b) {
                var aa = Array.isArray(a),
                    ba = Array.isArray(b);

                if(!aa && !ba) return '' + a + b;

                var res = [];

                if(aa) {

                    a.forEach(function(v, i) {

                        res.push( '' + v + ( (ba && b[i]) ? b[i] : b) );
                    });
                } else {
                    b.forEach(function(v, i) {

                        res.push( '' + ( (aa && a[i]) ? a[i] : a ) + v );
                    });
                }
                return res;
            },
            greater: function (a, b) {
                var aa = Array.isArray(a),
                    ba = Array.isArray(b);

                if(!aa && !ba) return a > b;

                var res = [], fa = a, ca = a, l, i;

                if(aa) ca = b;
                else fa = b;

                l = fa.length;

                for(i = 0; i < l; i++) {
                    res.push((fa[i] > ca));
                }

                return res;
            },

            greaterEqual: function (a, b) {
                var aa = Array.isArray(a),
                    ba = Array.isArray(b);

                if(!aa && !ba) return a >= b;

                var res = [], fa = a, ca = a, l, i;

                if(aa) ca = b;
                else fa = b;

                l = fa.length;

                for(i = 0; i < l; i++) {
                    res.push((fa[i] >= ca));
                }

                return res;
            },

            less: function (a, b) {
                var aa = Array.isArray(a),
                    ba = Array.isArray(b);

                if(!aa && !ba) return a < b;

                var res = [], fa = a, ca = a, l, i;

                if(aa) ca = b;
                else fa = b;

                l = fa.length;

                for(i = 0; i < l; i++) {
                    res.push((fa[i] < ca));
                }

                return res;
            },

            lessEqual: function (a, b) {
                return a <= b;
            },

            equal: function (a, b) {
                var aa = Array.isArray(a),
                    ba = Array.isArray(b);

                if(!aa && !ba) return a == b;

                var res = [], fa = a, ca = a, l, i;

                if(aa) ca = b;
                else fa = b;

                l = fa.length;

                for(i = 0; i < l; i++) {
                    res.push((fa[i] == ca));
                }

                return res;
            },

            notEqual: function (a, b) {
                var aa = Array.isArray(a),
                    ba = Array.isArray(b);

                if(!aa && !ba) return a != b;

                var res = [], fa = a, ca = a, l, i;

                if(aa) ca = b;
                else fa = b;

                l = fa.length;

                for(i = 0; i < l; i++) {
                    res.push((fa[i] != ca));
                }

                return res;
            }
        };


        //Math
        this.math = {

            ADD : function(num1, num2) {
                if(this.params._buildDependency) return '';

                var res, tmp, na1 = Array.isArray(num1), na2 = Array.isArray(num2);
                if(na1 || na2) {
                    if(!na1 || !na2 || num1.length != num2.length) res = [];
                    res = [];
                    num1.forEach(function(v, i) {

                        tmp = num2[i];
                        if (v === '' && tmp === '') res.push('');
                        else {
                            v = isNaN(parseFloat(v)) ? 0 : parseFloat(v);
                            tmp = isNaN(parseFloat(tmp)) ? 0 : parseFloat(tmp);

                            res.push(v + tmp);
                        }
                    });

                } else {
                    if (num1 === '' && num2 === '') res =  '';
                    else {
                        num1 = isNaN(parseFloat(num1)) ? 0 : parseFloat(num1);
                        num2 = isNaN(parseFloat(num2)) ? 0 : parseFloat(num2);

                        res = num1 + num2;
                    }
                }

                return res;
            },

            SUBTRACT: function (num1, num2) {
                if(this.params._buildDependency) return '';

                var res, tmp, na1 = Array.isArray(num1), na2 = Array.isArray(num2);
                if(na1 || na2) {
                    if(!na1 || !na2 || num1.length != num2.length) res = [];
                    res = [];
                    num1.forEach(function(v, i) {

                        tmp = num2[i];
                        if (v === '' && tmp === '') res.push('');
                        else {
                            v = isNaN(parseFloat(v)) ? 0 : parseFloat(v);
                            tmp = isNaN(parseFloat(tmp)) ? 0 : parseFloat(tmp);

                            res.push(v - tmp);
                        }
                    });

                } else {
                    if (num1 === '' && num2 === '') res =  '';
                    else {
                        num1 = isNaN(parseFloat(num1)) ? 0 : parseFloat(num1);
                        num2 = isNaN(parseFloat(num2)) ? 0 : parseFloat(num2);

                        res = num1 - num2;
                    }
                }

                return res;
            },

            MULTIPLY: function (num1, num2) {
                if(this.params._buildDependency) return '';

                var res, tmp, na1 = Array.isArray(num1), na2 = Array.isArray(num2);
                if(na1 || na2) {
                    res = [];

                    if(na1) {

                        num1.forEach(function(v, i) {

                            tmp = na2 ? num2[i] : num2;
                            if (v === '' || tmp === '') res.push('');
                            else {
                                v = isNaN(parseFloat(v)) ? 0 : parseFloat(v);
                                tmp = isNaN(parseFloat(tmp)) ? 0 : parseFloat(tmp);
                                res.push(v * tmp);
                            }
                        });
                    } else {

                        num2.forEach(function(v, i) {

                            tmp = na1 ? num1[i] : num1;
                            if (v === '' || tmp === '') res.push('');
                            else {
                                v = isNaN(parseFloat(v)) ? 0 : parseFloat(v);
                                tmp = isNaN(parseFloat(tmp)) ? 0 : parseFloat(tmp);
                                res.push(v * tmp);
                            }
                        });
                    }

                } else {
                    if (num1 === '' || num2 === '') res = '';
                    else {
                        num1 = isNaN(parseFloat(num1)) ? 0 : parseFloat(num1);
                        num2 = isNaN(parseFloat(num2)) ? 0 : parseFloat(num2);

                        res = num1 * num2;
                    }
                }

                return res;
            },

            DIVIDE: function (num1, num2) {
                if(this.params._buildDependency) return '';

                var res, tmp, na1 = Array.isArray(num1), na2 = Array.isArray(num2);
                if(na1 || na2) {
                    res = [];

                    if(na1) {
                        num1.forEach(function(v, i) {

                            tmp = na2 ? num2[i] : num2;
                            if (v === '' || tmp === '') res.push('');
                            else if(tmp == 0) res.push('#DIV/0');
                            else res.push(parseFloat(v) / parseFloat(tmp));
                        });

                    } else {
                        num2.forEach(function(v, i) {

                            tmp = na1 ? num1[i] : num1;
                            if (v === '' || tmp === '') res.push('');
                            else if(tmp == 0) res.push('#DIV/0');
                            else res.push(parseFloat(tmp) / parseFloat(v));
                        });
                    }

                } else {
                    if (num1 === '' || num2 === '') res = '';
                    else if (num2 == 0) res = '#DIV/0';
                    else res = parseFloat(num1) / parseFloat(num2);
                }

                return res;
            },

            POWER: function (number, power) {
                if(this.params._buildDependency) return '';

                return Math.pow(number, power);
            },

            SUM: function () {
                if(this.params._buildDependency) return '';

                var cell, a, floatVal, stringVal = '', result = 0;

                for (a = 0; a < arguments.length; a++) {
                    if (typeof(arguments[a]) == 'object') {
                        for (cell in arguments[a]) {
                            if(!arguments[a].hasOwnProperty(cell)) break;

                            stringVal += (typeof(arguments[a][cell]) != 'undefined') ? arguments[a][cell] : '';
                            floatVal = !isNaN(parseFloat(arguments[a][cell], 10)) ? parseFloat(arguments[a][cell], 10) : 0;
                            result += floatVal;
                        }
                    } else {
                        stringVal += (typeof(arguments[a]) != 'undefined') ? arguments[a] : '';
                        floatVal = !isNaN(parseFloat(arguments[a], 10)) ? parseFloat(arguments[a], 10) : 0;
                        result += floatVal;
                    }
                }

                if (result === 0) {
                    return 0;
                } else {
                    return result;
                }
            },

            SUMIF: function () {
                if(this.params._buildDependency) return '';

                var args = Array.prototype.slice.call(arguments, 0),
                    cond = args.pop(), ca = Array.isArray(cond), cv;
                var i, v, fv, sv = '', res = 0, k;

                for (i = 0; i < args.length; i++) {
                    v = args[i];

                    if (typeof v == 'object') {
                        for (k in v) {
                            cv = ca ? cond[k] : cond;

                            if(!cv) continue;

                            sv += typeof(v[k]) !== 'undefined' ? v[k] : '';
                            fv = parseFloat(v[k]);
                            fv = !isNaN(fv) ? fv : 0;

                            res += fv;
                        }
                    } else {
                        cv = ca ? cond[i] : cond;

                        if(!cv) continue;

                        sv += typeof(v) !== 'undefined' ? v : '';
                        fv = parseFloat(v);
                        fv = !isNaN(fv) ? fv : 0;

                        res += fv;
                    }
                }

                if (res === 0 && $.trim(sv) === '') return 0;

                return res;
            },

            ROUND: function (number, digits) {
                if(this.params._buildDependency) return '';

                var res = [];
                if(Array.isArray(number)) {

                    number.forEach(function(v) {
                        v = parseFloat(v);

                        if(isNaN(v)) v = 0;

                        res.push(Math.round(v * Math.pow(10, digits)) / Math.pow(10, digits));
                    });

                } else {
                    number = parseFloat(number);

                    if(isNaN(number)) number = 0;

                    res = Math.round(number * Math.pow(10, digits)) / Math.pow(10, digits);
                }

                return res;
            },

            ROUNDDOWN: function (number, digits) {
                if(this.params._buildDependency) return '';

                var res = [], sign;
                if(Array.isArray(number)) {

                    number.forEach(function(v) {
                        v = parseFloat(v);

                        if(isNaN(v)) v = 0;

                        sign = (v > 0) ? 1 : -1;
                        res.push(sign * (Math.floor(Math.abs(v) * Math.pow(10, digits))) / Math.pow(10, digits));
                    });

                } else {
                    number = parseFloat(number);

                    if(isNaN(number)) number = 0;

                    sign = (number > 0) ? 1 : -1;
                    res = sign * (Math.floor(Math.abs(number) * Math.pow(10, digits))) / Math.pow(10, digits);
                }

                return res;
            },

            ROUNDUP: function (number, digits) {
                if(this.params._buildDependency) return '';

                var res = [], sign;
                if(Array.isArray(number)) {

                    number.forEach(function(v) {
                        v = parseFloat(v);

                        if(isNaN(v)) v = 0;

                        sign = (v > 0) ? 1 : -1;
                        res.push(sign * (Math.ceil(Math.abs(v) * Math.pow(10, digits))) / Math.pow(10, digits));
                    });

                } else {
                    number = parseFloat(number);

                    if(isNaN(number)) number = 0;

                    sign = (number > 0) ? 1 : -1;
                    res = sign * (Math.ceil(Math.abs(number) * Math.pow(10, digits))) / Math.pow(10, digits);
                }

                return res;
            },

            MROUND: function (number, multiple) {
                if(this.params._buildDependency) return '';

                number = parseFloat(number);

                if(isNaN(number)) number = 0;

                if (number * multiple < 0) {
                    throw new Error('Number and multiple must have the same sign.');
                }

                return Math.round(number / multiple) * multiple;
            }
        };


        //Statistic
        this.statistic = {

            AVERAGE: function () {
                if(this.params._buildDependency) return '';

                var range = _fn.array.arrayMerge(arguments);
                var n = range.length;
                var count = 0;
                var sigma = 0;
                var floatVal = 0;
                for (var i = 0; i < n; i++) {
                    if (range[i] !== true && range[i] !== false) {
                        floatVal = parseFloat(range[i]);
                        sigma += isNaN(floatVal) ? 0 : floatVal;
                        count++;
                    }
                }
                return count != 0 ? sigma / count : 0;
            },

            COUNT: function () {
                if(this.params._buildDependency) return '';

                return _fn.array.arrayMerge(arguments).length;
            }
        };


        //Logical
        this.logical = {

            AND: function () {
                if(this.params._buildDependency) return '';

                var result = true;
                for (var i = 0; i < arguments.length; i++) {
                    if (!arguments[i]) {
                        result = false;
                    }
                }
                return result;
            },

            OR: function () {
                if(this.params._buildDependency) return '';

                var result = false;
                for (var i = 0; i < arguments.length; i++) {
                    if (arguments[i]) {
                        result = true;
                    }
                }
                return result;
            },

            IF: function (test, then_value, otherwise_value) {
                if(this.params._buildDependency) return '';

                then_value = (typeof then_value === 'undefined') ? true : then_value;
                otherwise_value = (typeof otherwise_value === 'undefined') ? true : otherwise_value;

                var res = [];
                if(Array.isArray(test)) {

                    test.forEach(function(v , i) {
                        if(v) res.push( (Array.isArray(then_value) && then_value[i] !== undefined) ? then_value[i] : then_value );
                        else res.push( (Array.isArray(otherwise_value) && otherwise_value[i] !== undefined) ? otherwise_value[i] : otherwise_value );
                    });
                } else {
                    if (test) res = then_value;
                    else res = otherwise_value;
                }

                return res;
            }
        };

        /**
         * date formula group.
         * adapted from stoic's formula.js (http://www.stoic.com/pages/formula)
         * with modification to adapt Calx environment
         * @type {Object}
         */
        this.date = {

            TODAY: function () {
                if(this.params._buildDependency) return '';

                return moment().format('MM/DD/YYYY');
            },

            NOW: function () {
                if(this.params._buildDependency) return '';

                return moment().format('MM/DD/YYYY hh:mm A');
            },

            DATE : function(year, month, day) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }
                if(typeof(month) == 'undefined'){
                    return moment(year);
                }

                return self.date.DATEFORMAT(new Date(year, month - 1, day), 'MM/DD/YYYY');
            },

            DATEDIFF : function (start_date, end_date, period) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return moment(end_date).diff(moment.utc(start_date), period);
            },

            DATEFORMAT : function(date, format) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return moment(date).format(format);
            },

            DATEVALUE : function(date_text) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return Math.ceil((moment(date_text) - moment('1900-1-1')) / 86400000) + 2;
            },

            DAY : function(date) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return '';
                }

                return moment(new Date(date)).date();
            },

            DAYNAME : function(date) {
                if(this.params._buildDependency) return '';

                return data.DAY_NAME[this.WEEKDAY(date)-1];
            },

            DAYS : function(end_date, start_date) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined') {
                    return data.ERRKEY.momentRequired;
                }

                return moment(new Date(end_date)).diff(moment(new Date(start_date)), 'days');
            },

            DAYS360 : function(start_date, end_date, method) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                var start = moment(new Date(start_date));
                var end = moment(new Date(end_date));
                var smd = 31;
                var emd = 31;
                var sd = start.date();
                var ed = end.date();
                if (method) {
                    sd = (sd === 31) ? 30 : sd;
                    ed = (ed === 31) ? 30 : ed;
                } else {
                    if (start.month() === 1) {
                        smd = start.daysInMonth();
                    }
                    if (end.month() === 1) {
                        emd = end.daysInMonth();
                    }
                    sd = (sd === smd) ? 30 : sd;
                    if (sd === 30 || sd === smd) {
                        ed = (ed === emd) ? 30 : ed;
                    }
                }
                return 360 * (end.year() - start.year()) + 30 * (end.month() - start.month()) + (ed - sd);
            },

            EDATE : function(start_date, months) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return moment(new Date(start_date)).add('months', months).toDate();
            },

            EOMONTH : function(start_date, months) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                var edate = moment(new Date(start_date)).add('months', months);
                return new Date(edate.year(), edate.month(), edate.daysInMonth());
            },

            FROMNOW : function(timestamp, nosuffix) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return moment(new Date(timestamp)).fromNow(nosuffix);
            },

            HOUR : function(timestamp) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return (timestamp <= 1) ? Math.floor(24 * timestamp) : moment(new Date(timestamp)).hours();
            },

            MINUTE : function(timestamp) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return (timestamp <= 1) ? Math.floor(24 * 60 * timestamp) - 60 * Math.floor(24 * timestamp) : moment(new Date(timestamp)).minutes();
            },

            ISOWEEKNUM : function(date) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return moment(new Date(date)).format('w');
            },

            MONTH : function(timestamp) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return moment(new Date(timestamp)).month() + 1;
            },

            NETWORKDAYS : function(start_date, end_date, holidays) {
                if(this.params._buildDependency) return '';

                return this.NETWORKDAYSINTL(start_date, end_date, 1, holidays);
            },

            NETWORKDAYSINTL : function(start_date, end_date, weekend, holidays) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                var weekend_type = (typeof weekend === 'undefined') ? 1 : weekend;
                var weekend_days = data.WEEKEND_TYPES[weekend_type];
                var sd = moment(start_date);
                var ed = moment(end_date);
                var net_days = ed.diff(sd, 'days') + 1;
                var net_work_days = net_days;
                var day_of_week = '';
                var cd = sd;
                var holiday_dates = [];
                if (typeof holidays !== 'undefined') {
                    for (var i = 0; i < holidays.length; i++) {
                        holiday_dates[i] = moment(new Date(holidays[i])).format('MM-DD-YYYY');
                    }
                }
                var j = 0;
                while (j < net_days) {
                    day_of_week = cd.format('d');
                    if (weekend_days.indexOf(parseInt(day_of_week, 10)) >= 0) {
                        net_work_days--;
                    } else if (holiday_dates.indexOf(cd.format('MM-DD-YYYY')) >= 0) {
                        net_work_days--;
                    }
                    cd = cd.add('days', 1);
                    j++;
                }
                return net_work_days;
            },

            SECOND : function(timestamp) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return moment(new Date(timestamp)).seconds();
            },

            TIME : function(hour, minute, second) {
                if(this.params._buildDependency) return '';

                return (3600 * hour + 60 * minute + second) / 86400;
            },

            TIMEVALUE : function(time_text) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                var timestamp = moment(new Date(time_text));
                return (3600 * timestamp.hours() + 60 * timestamp.minutes() + timestamp.seconds()) / 86400;
            },

            WEEKDAY : function(date, type) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                var week_day = moment(new Date(date)).format('d');
                var week_type = (typeof type === 'undefined') ? 1 : type;
                return data.WEEK_TYPES[week_type][week_day];
            },

            WEEKNUM : function(date, type) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                var current_date = moment(new Date(date));
                var january_first = moment(new Date(current_date.year(), 0, 1));
                var week_type = (typeof type === 'undefined') ? 1 : type;
                var week_start = data.WEEK_STARTS[week_type];
                var first_day = january_first.format('d');
                var offset = (first_day < week_start) ? week_start - first_day + 1 : first_day - week_start;
                if (week_type === 21) {
                    return this.ISOWEEKNUM(date);
                } else {
                    return Math.floor(current_date.diff(january_first.subtract('days', offset), 'days') / 7) + 1;
                }
            },

            WORKDAY : function(start_date, days, holidays) {
                if(this.params._buildDependency) return '';

                return this.WORKDAYINTL(start_date, days, 1, holidays);
            },

            WORKDAYINTL : function(start_date, days, weekend, holidays) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                var weekend_type = (typeof weekend === 'undefined') ? 1 : weekend;
                var weekend_days = data.WEEKEND_TYPES[weekend_type];
                var sd = moment(new Date(start_date));
                var cd = sd;
                var day_of_week = '';
                var holiday_dates = [];
                if (typeof holidays !== 'undefined') {
                    for (var i = 0; i < holidays.length; i++) {
                        holiday_dates[i] = moment(new Date(holidays[i])).format('MM-DD-YYYY');
                    }
                }
                var j = 0;
                while (j < days) {
                    cd = cd.add('days', 1);
                    day_of_week = cd.format('d');
                    if (weekend_days.indexOf(parseInt(day_of_week, 10)) < 0 && holiday_dates.indexOf(cd.format('MM-DD-YYYY')) < 0) {
                        j++;
                    }
                }
                return cd.toDate();
            },

            YEAR : function(date) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                return moment(new Date(date)).year();
            },

            YEARFRAC : function(start_date, end_date, basis) {
                if(this.params._buildDependency) return '';

                if(typeof (moment) == 'undefined'){
                    return data.ERRKEY.momentRequired;
                }

                // Credits: David A. Wheeler [http://www.dwheeler.com/]

                // Initialize parameters
                basis = (typeof basis === 'undefined') ? 0 : basis;
                var sdate = moment(new Date(start_date));
                var edate = moment(new Date(end_date));

                // Return error if either date is invalid
                if (!sdate.isValid() || !edate.isValid()) {
                    return '#VALUE!';
                }

                // Return error if basis is neither 0, 1, 2, 3, or 4
                if ([0, 1, 2, 3, 4].indexOf(basis) === -1) {
                    return '#NUM!';
                }

                // Return zero if start_date and end_date are the same
                if (sdate === edate) {
                    return 0;
                }

                // Swap dates if start_date is later than end_date
                if (sdate.diff(edate) > 0) {
                    edate = moment(new Date(start_date));
                    sdate = moment(new Date(end_date));
                }

                // Lookup years, months, and days
                var syear = sdate.year();
                var smonth = sdate.month();
                var sday = sdate.date();
                var eyear = edate.year();
                var emonth = edate.month();
                var eday = edate.date();

                switch (basis) {
                    case 0:
                        // US (NASD) 30/360
                        // Note: if eday == 31, it stays 31 if sday < 30
                        if (sday === 31 && eday === 31) {
                            sday = 30;
                            eday = 30;
                        } else if (sday === 31) {
                            sday = 30;
                        } else if (sday === 30 && eday === 31) {
                            eday = 30;
                        } else if (smonth === 1 && emonth === 1 && sdate.daysInMonth() === sday && edate.daysInMonth() === eday) {
                            sday = 30;
                            eday = 30;
                        } else if (smonth === 1 && sdate.daysInMonth() === sday) {
                            sday = 30;
                        }
                        return ((eday + emonth * 30 + eyear * 360) - (sday + smonth * 30 + syear * 360)) / 360;

                    case 1:
                        // Actual/actual
                        var feb29Between = function(date1, date2) {
                            // Requires year2 == (year1 + 1) or year2 == year1
                            // Returns TRUE if February 29 is between the two dates (date1 may be February 29), with two possibilities:
                            // year1 is a leap year and date1 <= Februay 29 of year1
                            // year2 is a leap year and date2 > Februay 29 of year2

                            var mar1year1 = moment(new Date(date1.year(), 2, 1));
                            if (moment([date1.year()]).isLeapYear() && date1.diff(mar1year1) < 0 && date2.diff(mar1year1) >= 0) {
                                return true;
                            }
                            var mar1year2 = moment(new Date(date2.year(), 2, 1));
                            if (moment([date2.year()]).isLeapYear() && date2.diff(mar1year2) >= 0 && date1.diff(mar1year2) < 0) {
                                return true;
                            }
                            return false;
                        };
                        var ylength = 365;
                        if (syear === eyear || ((syear + 1) === eyear) && ((smonth > emonth) || ((smonth === emonth) && (sday >= eday)))) {
                            if (syear === eyear && moment([syear]).isLeapYear()) {
                                ylength = 366;
                            } else if (feb29Between(sdate, edate) || (emonth === 1 && eday === 29)) {
                                ylength = 366;
                            }
                            return edate.diff(sdate, 'days') / ylength;
                        } else {
                            var years = (eyear - syear) + 1;
                            var days = moment(new Date(eyear + 1, 0, 1)).diff(moment(new Date(syear, 0, 1)), 'days');
                            var average = days / years;
                            return edate.diff(sdate, 'days') / average;
                        }
                        break;

                    case 2:
                        // Actual/360
                        return edate.diff(sdate, 'days') / 360;

                    case 3:
                        // Actual/365
                        return edate.diff(sdate, 'days') / 365;

                    case 4:
                        // European 30/360
                        if (sday === 31) {
                            sday = 30;
                        }

                        if (eday === 31) {
                            eday = 30;
                        }
                        // Remarkably, do NOT change February 28 or February 29 at ALL
                        return ((eday + emonth * 30 + eyear * 360) - (sday + smonth * 30 + syear * 360)) / 360;
                }
            }
        };


        for (var i in this) {

            if (i != 'params') {
                this[i].params = this.params;
            }
        }
    };

    ns.exports = formula;

})(require, module);