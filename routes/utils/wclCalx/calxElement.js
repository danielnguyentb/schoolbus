/**
 * Created by AnhNguyen
 */
;
(function (r, ns) {
    "use strict";

    var lib = r('./libraries/library.js');
    var parser = r('./libraries/parser.js').parser;
    var Step = r('step');

    var instance = function (handlerId, Id, calx, ele, content, fn, mn, forceFormula) {

        var _init = function () {
            _calx = calx;
            _$ele = ele;
            _handlerId = handlerId;
            _selfId = Id;
            _fn = fn;
            _manager = mn;
            _isForce = forceFormula === true;

            //Check content is an formula string
            if (_isForce || /^\=.*$/gi.test(content)) {
                _content = content.replace(/^\=(.*)$/gi, "$1");
                if(_content == '') return false;

                _hasFormula = true;
                _lib = new lib(_handlerId, _calx, _self, _fn, _manager);
                _parser = new parser();
                buildDependency();
            } else _content = content;

            //if(_calx.hasExec() && _self.hasFormula()) _self.exec(true);

        };

        this.isForceFormula = function() {
            return _isForce !== false;
        };

        this.asyncExec = function(recalc, callback) {
            var value = this.getValue();

            if(!runable) {
                callback(null, value);

                return false;
            }

            var that = this;

            if(recalc) {
                if(_asyncRunning) {
                    _callbacks.push(callback);
                    return false;
                }

                Step (
                    function selfExec() {

                        _asyncRunning = true;
                        if(that.hasFormula()) {
                            _lib.params._buildDependency = false;
                            _lib.params._hasDone = false;
                            _lib.params._callback = this;
                            value = _parser.parse(_content, _lib);

                        } else return this;
                    },
                    function dependExec(e, r) {
                        if(e) throw e;
                        if(that.hasFormula()) {
                            delete _lib.params._callback;
                            value = _parser.parse(_content, _lib);

                            that.setValue(value);
                        } else if(_content != '') that.setValue(_content);

                        that.dependantExec(this);
                    },
                    function final(e, r) {

                        _asyncRunning = false;
                        if(_callbacks.length) {
                            var tmp = _callbacks[0];
                            _callbacks.splice(0, 1);
                            that.asyncExec(true, tmp);
                        }

                        //for(var i = 0; i < _callbacks.length; i++) {
                        //    that.asyncExec(true, _callbacks[i]);
                        //    _callbacks.splice(i, 1);
                        //    i--;
                        //}
                        //
                        //_callbacks = [];

                        callback(e);
                    }
                );

                return true;
            } else {
                callback(null, value);

                return true;
            }
        };

        this.exec = function(recalc) {
            var value = this.getValue();

            if(!runable) return value;

            if(recalc) {

                if(this.hasFormula()) {
                    _lib.params._buildDependency = false;
                    _lib.params._hasDone = false;
                    value = _parser.parse(_content, _lib);

                    this.setValue(value);
                }
            }

            return value;
        };

        this.getValue = function() {
            return _calx.getElementValue(_handlerId, _selfId);
        };

        this.setValue = function (value) {
            if(isTableElem()) {
                var res = /^(.*)\.(.*)$/gi.exec(_selfId);
                var cName = res[1];
                var col = res[2];

                _calx.setTableValue(_handlerId, cName, col, value);
            } else {
                if(Array.isArray(value)) value = '';
                _calx.setElementValue(_handlerId, _selfId, value);
            }

            return this;
        };

        this.dependantExec = function(callback) {

            Step (
                function exec() {
                    var group = this.group();

                    for(var i in dependant) {
                        if(!dependant.hasOwnProperty(i)) break;

                        dependant[i].forEach(function(v) {
                            _calx.getElement(i, v).asyncExec(true, group());
                        });
                    }
                },
                callback
            );
        };

        this.getDependencies = function () {
            return dependencies;
        };

        this.getDependant = function () {
            return dependant;
        };

        this.getElement = function () {
            return _$ele;
        };

        this.hasFormula = function () {
            return _hasFormula !== false;
        };

        this.hasDependency = function (handler, self) {
            return dependencies[handler] && dependencies[handler].indexOf(self) != -1
        };

        this.addDependant = function (handlerId, Id) {

            if (!handlerId || !Id) return false;
            if (!dependant[handlerId]) dependant[handlerId] = [];

            dependant[handlerId].pushUnique(Id);

            return this;
        };

        this.getFormula = function () {
            return _content;
        };

        this.removeDependant = function (handlerId, Id) {

            if (!handlerId || !Id) return false;
            if (!dependant[handlerId]) return false;
            if (dependant[handlerId].indexOf(Id) !== -1) {
                dependant[handlerId].splice(dependant[handlerId].indexOf(Id), 1);
            }

            return this;
        };

        this.buildDependant = function () {

            var ele;
            for(var i in dependencies) {
                if(!dependencies.hasOwnProperty(i)) break;

                dependencies[i].forEach(function (_v) {

                    ele = _calx.getElement(i, _v);
                    if (ele) ele.addDependant(_handlerId, _selfId);

                });
            }

            return this;
        };

        this.setRunable = function(mode) {
            runable = mode;

            return this;
        };

        var isTableElem = function() {
            return /^(.*)\.(.*)$/gi.test(_selfId);
        };

        var buildDependency = function () {

            dependencies = {};

            _lib.params._buildDependency = true;
            _parser.parse(_content, _lib);
            _lib.params._buildDependency = false;

            var depends = _lib.params._dependencies;

            //Make dependencies in same handler
            dependencies[_handlerId] = [];
            depends.sameHandler.forEach(function (v) {
                dependencies[_handlerId].pushUnique(v);
            });

            //Make dependencies in other handler
            for(var i in depends.otherHandler) {
                if(!depends.otherHandler.hasOwnProperty(i)) break;

                dependencies[i] = [];
                depends.otherHandler[i].forEach(function(_v) {
                    dependencies[i].pushUnique(_v);
                });
            }

            _self.buildDependant();

            return dependencies;
        };

        var _fn, _manager;
        var dependencies = {};
        var dependant = {};
        var _handlerId = '';
        var _selfId = '';
        var _self = this;
        var _calx = false;
        var _lib = false;
        var _parser = false;
        var _$ele = false;
        var _hasFormula = false;
        var _isForce = false;
        var _content = '';
        var _value = '';
        var _callbacks = [];
        var _asyncRunning = false;
        var runable = false;

        _init();
    };

    ns.exports = instance;

})(require, module);