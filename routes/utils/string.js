/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

 /*
  * String utility functions
  */

(function(){

	var _fn = {};

	//f = fill / supplant - replaces {somevar} in string with data.somevar value
	_fn.f = function(str, data)
	 {
	    return str.replace(/{([^{}]*)}/g, 
		    function(a, b) {

		        var r = data[b];
		        return typeof r === 'string' ? r: "" + r; 
		    }
	    ); 
	};

    _fn.capitalize = function(str)
    {
        return str[0].toUpperCase() + str.slice(1);
    };

	_fn.slugify = function(str)
	{
		return str.toString().toLowerCase()
			.replace(/\s+/g, '-')        // Replace spaces with -
			.replace(/[^\w\-]+/g, '')   // Remove all non-word chars
			.replace(/\-\-+/g, '-')      // Replace multiple - with single -
			.replace(/^-+/, '')          // Trim - from start of text
			.replace(/-+$/, '');         // Trim - from end of text
	};

    _fn.zeroPad = function(a, b) {
        var c = a.toString();
        c.length < b && (c = "000000000000000000000000000".substr(0, b - c.length) + c);
        return c;
    };

	module.exports = _fn;
})();