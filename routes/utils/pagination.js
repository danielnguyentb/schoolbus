/**
 * Created by Anh on 8/19/14.
 */

(function (m, r) {

	var extend = r('node.extend');
	var opt = {
		baseUrl        : '', // The page we are linking to
		prefix         : '', // A custom prefix added to the path.
		suffix         : '', // A custom suffix added to the path.

		total          : 0,// Total number of items (database results)
		perPage        : 10,// Max number of items you want shown per page
		numLinks       : 2,// Number of "digit" links to show before/after the currently viewed page
		curPage        : 0,// The current page being viewed
		usePageNumber  : true,// Use page number for segment instead of offset
		nextLink       : '<i class="fa fa-angle-right"></i>',
		prevLink       : '<i class="fa fa-angle-left"></i>',
		fullTagOpen    : '<ul class="pagination pull-right martop0">',
		fullTagClose   : '</ul>',
		firstTagOpen   : '',
		firstTagClose  : '',
		lastTagOpen    : '',
		lastTagClose   : '',
		firstUrl       : '', // Alternative URL for the First Page.
		disableTagOpen : '<li class="disabled"><a href="javascript:;">',
		disableTagClose: '</a></li>',
		curTagOpen     : '<li class="active"><a href="javascript:;">',
		curTagClose    : '</a></li>',
		nextTagOpen    : '<li class="next">',
		nextTagClose   : '</li>',
		prevTagOpen    : '<li class="prev">',
		prevTagClose   : '</li>',
		numTagOpen     : '<li>',
		numTagClose    : '</li>',
		pageQueryStr   : false,
		queryStrSegment: 'per_page',
		displayPage    : true,
		anchorAttr     : ''
	};

	m.exports = function (cfg) {

		var opts;

		var __construct = function(cfg) {

			opts = extend({}, opt);
			opts = extend(opts, cfg);
		};

		this.render = function() {

			if(opts.total == 0 || opts.perPage == 0) return '';

			var numPage = Math.ceil(opts.total / opts.perPage);

//			if(numPage == 1) return '';

			var basePage = 0;
			if(opts.usePageNumber) basePage = 1;

			if(opts.usePageNumber && opts.curPage == 0) opts.curPage = basePage;

			opts.numLinks = parseInt(opts.numLinks, 10);

			if(opts.numLinks < 1) {
				config.error('Your number of links must be a positive number');
				return '';
			}

			if(isNaN(opts.curPage)) opts.curPage = basePage;

			if(opts.usePageNumber) {
				if(opts.curPage > numPage) opts.curPage = numPage
			} else {
				if(opts.curPage > opts.total) opts.curPage = (numPage - 1) * opts.perPage;
			}

			var uriPage = opts.curPage;

			if(!opts.usePageNumber) opts.curPage = Math.floor( (opts.curPage/opts.perPage) + 1 );

			// Calculate the start and end numbers. These determine
			// which number to start and end the digit links with
			var start = ((opts.curPage - Math.round(opts.numLinks/2)) > 0) ? opts.curPage - (Math.round(opts.numLinks/2) - 1) : 1;
			var end = ((opts.curPage + Math.round(opts.numLinks/2)) < numPage) ? opts.curPage + (Math.round(opts.numLinks/2) + 1) : numPage;

			var output = '';
			var i;

			if(opts.prefix !== false && opts.curPage != 1) {

				if(opts.usePageNumber) i = uriPage - 1;
				else i = uriPage - opts.perPage;

				if(i == 0 && opts.firstUrl != '') {
					output += [opts.prevTagOpen, '<a ', opts.anchorAttr, 'href="', opts.firstUrl, '">', opts.prevLink, "</a>", opts.prevTagClose].join('');
				} else {
					i = (i == 0 )? '' : opts.prefix + i + opts.suffix;
					output += [opts.prevTagOpen, '<a ', opts.anchorAttr, 'href="', opts.baseUrl, i, '">', opts.prevLink, '</a>', opts.prevTagClose].join('');
				}
			} else output += [opts.disableTagOpen, opts.prevLink, opts.disableTagClose].join('');

			if(opts.displayPage !== false) {

				//Write the digit links
				var n;
				for (var loop = start - 1; loop <= end; loop++) {

					if(opts.usePageNumber) i = loop;
					else i = (loop * opts.perPage) - opts.perPage;

					if(i >= basePage) {
						if(opts.curPage == loop) {
							output += opts.curTagOpen + loop + opts.curTagClose;
						} else {
							n = (i == basePage) ? '' : i;

							if(n != '' && opts.firstUrl != '') {
								output += [opts.numTagOpen, '<a ', opts.anchorAttr, 'href="', opts.firstUrl, '">', loop, '</a>', opts.numTagClose].join('');
							} else {

								n = (n == '') ? 1 : n;
								n = opts.prefix + n + opts.suffix;

								output += [opts.numTagOpen, '<a ', opts.anchorAttr, 'href="', opts.baseUrl, n, '">', loop, '</a>', opts.numTagClose].join('');
							}
						}
					}

				}

			}

			if(opts.nextLink !== false && opts.curPage < numPage) {

				if(opts.usePageNumber) i = opts.curPage + 1;
				else i = opts.curPage * opts.perPage;

				output += [opts.nextTagOpen, '<a ', opts.anchorAttr, 'href="', opts.baseUrl, opts.prefix, i, opts.suffix, '">', opts.nextLink, '</a>', opts.nextTagClose].join('');
			} else output += [opts.disableTagOpen, opts.nextLink, opts.disableTagClose].join('');

			output = opts.fullTagOpen + output + opts.fullTagClose;

			return output;
		};

		__construct(cfg);

		return this;
	};

})(module, require);