/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

 /*
  * This is part of the JSFmwk used on client side as well
  */

(function(){

	var _fn = {};

	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];


	// ---------------------------------------------------------------------------------
	// Augmented methods for the JavaScript Date() object
	// ---------------------------------------------------------------------------------
	// Substitute date components into a string

	_fn.format = function(date, template)
	{
	    template = template.replace(/a/g, date.getHours() > 11 ? "pm": "am");
	    template = template.replace(/A/g, date.getHours() > 11 ? "PM": "AM");
	    template = template.replace(/YYYY/g, date.getFullYear());
	    template = template.replace(/YY/g, zeroPad(date.getFullYear() - 2000, 2));
	    template = template.replace(/MM3/g, months[date.getMonth()].substr(0,3));
	    template = template.replace(/MMM/g, months[date.getMonth()]);
	    template = template.replace(/0MM/g, zeroPad(date.getMonth() + 1, 2));
	    template = template.replace(/MM/g, date.getMonth() + 1);
	    template = template.replace(/DDD/g, days[date.getDay()]);
	    template = template.replace(/0DD/g, zeroPad(date.getDate(), 2));
	    template = template.replace(/DDth/g, date.getDate() + daySuffix(date));
	    template = template.replace(/DD/g, date.getDate());
	    template = template.replace(/0hh/g, zeroPad(date.getHours(), 2));
	    template = template.replace(/hh/g, date.getHours());
	    template = template.replace(/0mm/g, zeroPad(date.getMinutes(), 2));
	    template = template.replace(/mm/g, date.getMinutes());
	    template = template.replace(/0ss/g, zeroPad(date.getSeconds(), 2));
	    template = template.replace(/ss/g, date.getSeconds());

	    //Hours suited for "2:40pm" type things
	    var half = date.getHours() % 12;
	    if (half == 0 && date.getHours() == 12) half = 12;
	    template = template.replace(/0h2/g, half);

	    return template;
	};

    _fn.dateString = function(date, short) {
        var da = [zeroPad(date.getMonth() + 1, 2), zeroPad(date.getDate(), 2)];

        if(!short) da.unshift(date.getFullYear());

        return da.join('-');
    };

    _fn.mysqlDate = function(date) {
        var da = [
            date.getFullYear(),
            '-',
            zeroPad(date.getMonth() + 1, 2),
            '-',
            zeroPad(date.getDate(), 2),
            ' ',
            zeroPad(date.getHours(), 2),
            ':',
            zeroPad(date.getMinutes(), 2),
            ':',
            zeroPad(date.getSeconds(), 2)
        ];

        return da.join('');
    };

	var zeroPad = function(n, d)
	{
	    var s = n.toString();
	    if (s.length < d)
	    s = "000000000000000000000000000".substr(0, d - s.length) + s;
	    return (s); 
	};

	var daySuffix = function(date)
	{
	    var num = date.getDate();
	    if (num >= 11 && num <= 13) return "th";
	    else if (num.toString().substr( - 1) == "1") return "st";
	    else if (num.toString().substr( - 1) == "2") return "nd";
	    else if (num.toString().substr( - 1) == "3") return "rd";
	    return "th"; 
	};

/*	// Convert a date to local YYYYMMDDHHMM string format
	Date.prototype.convertToLocalYYYYMMDDHHMM = function()
	 {
	    return (zeroPad(date.getFullYear(), 4) + zeroPad(date.getMonth() + 1, 2) + zeroPad(date.getDate(), 2) + zeroPad(date.getHours(), 2) + zeroPad(date.getMinutes(), 2)); 
	};

	// Convert a date to UTC YYYYMMDDHHMM string format
	Date.prototype.convertToYYYYMMDDHHMM = function()
	 {
	    return (zeroPad(date.getUTCFullYear(), 4) + zeroPad(date.getUTCMonth() + 1, 2) + zeroPad(date.getUTCDate(), 2) + zeroPad(date.getUTCHours(), 2) + zeroPad(date.getUTCMinutes(), 2)); 
	};

	// Convert a date to UTC YYYYMMDD.HHMMSSMMM string format
	Date.prototype.convertToYYYYMMDDHHMMSSMMM = function()
	 {
	    return (zeroPad(date.getUTCFullYear(), 4) + zeroPad(date.getUTCMonth() + 1, 2) + zeroPad(date.getUTCDate(), 2) + "." + zeroPad(date.getUTCHours(), 2) + zeroPad(date.getUTCMinutes(), 2) + zeroPad(date.getUTCSeconds(), 2) + zeroPad(date.getUTCMilliseconds(), 4)); 
	};
*/
	/* This is buggy
	Date.prototype.getDaysAgoText = function(){
		var dayobj = new Date();
		var day = (dayObj - this); // / 24 / 60 / 60;

		if (day < 1){
			return "today";
		} else {
			day = Math.floor(day);
			if (day == 1) return "yesterday";
			else return day + " days ago";	
		}
	};
	*/

	// Static method of CLASS (NOTE: no prototype method of obj) to create a date from a UTC YYYYMMDDHHMM format string
/*	Date.convertFromYYYYMMDDHHMM = function(d)
	 {
	    //var theDate = new Date(parseInt(d.substr(0, 4), 10), 
		//NOTE - This converts from GMT into local timezone
	    var theDate = new Date(Date.UTC(parseInt(d.substr(0, 4), 10),
	    parseInt(d.substr(4, 2), 10) - 1, 
	    parseInt(d.substr(6, 2), 10), 
	    parseInt(d.substr(8, 2), 10), 
	    parseInt(d.substr(10, 2), 10), 0, 0) );

	    return (theDate); 
	};
*/
	module.exports = _fn;

})();
