
/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

 /*
  * Security related utilities

  // Hashing rules
  // - Has a fixed hardcoded part
  // - Has a randomized part - this is appended to the hash itself - always a fixed digit-width
  // - Has a coin-flip digit - determines of random Salt is at start or end of hash
  // - Algo is always fixed and hard-coded to sha512 and not stored with hash
  // - Iterations also not stored with hash

  */

(function(r){

	var _fn = {};
  var crypto = r("crypto");
  var Step = r("step");
  var random = r("secure_random/random.js");

  //Settings
  //SHA1, 126 times with fixed + random salt and 498-byte length
  //So last 14-bytes can store the random salt
  var fixedSalt = "@)F$nPD3$KVoluntaryMagik!@#%^^@#";
  var randSaltLen = 13;
  var randBytesLen = 23;
  var hashLen = 249;    //498 / 2 (for hex) - rest are 13 bytes of rand-salt + 1byte of coin-flip appended for total of 512
  var numItr = 15000;   //SECURITY NOTE: See [[Tradeoff : Password hashing options]]

  //Copying from connect/utils.js -> md5() so we dont have to include it
  _fn.md5 = function(str, encoding){
    return crypto
      .createHash('md5')
      .update(str)
      .digest(encoding || 'hex');
  };
                 
  //Identical to connect/utils.js -> uid() - useful for other general purpose too
  //Creates it async or sync
  _fn.uid = function(len,callback){
    if (callback){
      Step (
        function uid1(){
          crypto.randomBytes(Math.ceil(len * 3 / 4), this);
        },
        function uid2(e, val){
          return val.toString('base64').slice(0,len);
        },
        callback
      );
    } else {
      return crypto.randomBytes(Math.ceil(len * 3 / 4))
      .toString('base64')
      .slice(0, len);
    }
  }

  //Creates a secure (non Math.random) random integer of said number of digits
  _fn.randNum = function(len, callback){
    random.getRandomInt(Math.pow(10 , (len - 1)), Math.pow(10, len), callback);
  }

  //Creates a strong hash of the original password - this will also create an new randomized salt
  //and append it to the hash
  //Used during new account or password change etc
	_fn.hash = function(p, callback){

    var randS, key;

    Step(

      createRandomSalt,

      function bindPass(ex, randSalt){
        if (ex) throw ex;

        randS = randSalt;
        return [randSalt, p];   
      },

      hashBasedOnSalt,

      function appendSalt(e, derivedKey){
        //We'll add the random Salt + a coin-flip key to the hash
        //so that we can verify it later
        //The coin-flip determines whether to append salt at start or end of hash

        //http://stackoverflow.com/questions/11557467/node-js-crypto-pbkdf2-password-to-hex
        derivedKey = Buffer(derivedKey, 'binary').toString('hex');

        var coin = new Date().valueOf().toString().slice(-1);
        var odd = parseInt(coin) % 2;

        //string concat faster than array join
        if (odd)
          return ("" + coin + derivedKey + randS);
        else 
          return ("" + coin + randS + derivedKey);
      },

      callback
    );

  }

  //This used during login to verify the password based on the stored hash
  _fn.verifyHash = function(p, hashed, callback){

    var randS, coin, odd = 0;

    Step(
      function extractRandSalt(){
        //Appended at end

        //Based on a coin flip - the salt is either at start or end of hash
        coin = parseInt(hashed.slice(0,1));

        odd = coin % 2;
        if (odd) {
          randS = hashed.slice(-1 * randSaltLen); 
        } else
          randS = hashed.slice(1, randSaltLen + 1);

        return [randS, p];      //also bind "p" for the next step
      },

      hashBasedOnSalt,

      function checkPass(ex, derivedKey){
        if (ex) throw ex;

        //http://stackoverflow.com/questions/11557467/node-js-crypto-pbkdf2-password-to-hex
        derivedKey = Buffer(derivedKey, 'binary').toString('hex');

        //Note - String concat faster than array join
        //http://jsperf.com/string-concat-vs-array-join-10000/2
        if (odd)
          return ( ("" + coin + derivedKey + randS) == hashed);
        else
          return ( ("" + coin + randS + derivedKey) == hashed);
      },
      callback
    );
  };


  //======== Private funcs used as Steps =====================

  //Creates asyc strong crypto random salt
  var createRandomSalt = function(cb){

    if (!cb) cb = this;   //might be part of a larger Step() loop

    Step(

      function randSalt1(){
        // crypto.randomBytes(randBytesLen, this);
        _fn.randNum(randBytesLen, this);
      },

      function randSalt2(ex,bytes){
        if (ex) throw ex;

        //NOTE: If randSaltLen is going to be bigger than 13 chars, should do toString(36)
        //but (16) looks more similar to hex - so will blend in better
        randSalt = (parseFloat(bytes) * new Date().valueOf()).toString(16);

        randSalt = randSalt.slice(0,randSaltLen);

        return randSalt;
      },
      cb
    );
  }

  //Does the actual hash
  var hashBasedOnSalt = function(ex,data){
    if (ex) throw ex;

    //Data[0] = randSalt, data[1] = p

    //Will now re-hash the incoming password to check matches
    var salt = fixedSalt + data[0];

    crypto.pbkdf2(data[1], salt, numItr, hashLen, this);
  }


  //Export the public

	module.exports = _fn;

})(require);