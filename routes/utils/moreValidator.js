/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

/*
 * This adds more validation by extending the node Validator class
 */

//Add validator for alphanumberic with space
var Validator = require('validator').Validator;
Validator.prototype.isAlphaWS = function(str) {
    if (!this.str.match(/^[a-zA-Z0-9 ]+$/)) {
        return this.error(this.msg || 'Invalid characters');
    }
    return this;
}

Validator.prototype.isArrayE = function() {
    if (!Array.isArray(this.str)||this.str.join().length==0) {
        return this.error(this.msg || 'Not an array');
    }
    return this;
}

Validator.prototype.isIntNotZero = function() {
    if(!this.str||!this.isInt(this.str)||this.str==0){
	    return this.error(this.msg || 'Invalid number');
    }
    return this;
}
