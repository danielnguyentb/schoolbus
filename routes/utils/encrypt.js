/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

(function () {

	var pub = {};

	var _fn;
	var crypto = require('crypto');


	module.exports = function (utils) {
		_fn = utils;

		return pub;
	}


	pub.encrypt = function (text) {
		var cipher = crypto.createCipher('aes-256-cbc', 'sint');
		var crypted = cipher.update(text, 'utf8', 'hex');
		crypted += cipher.final('hex');
		return crypted;
	}

	pub.decrypt = function (text) {
		if(text=="")return "";
		var decipher = crypto.createDecipher('aes-256-cbc', 'sint');
		var decrypted = decipher.update(text, 'hex', 'utf8');
		decrypted += decipher.final('utf8');
		return decrypted;
	}


})();