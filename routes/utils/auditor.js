/*
 * Audit module
 * 
 * There are three places to record audits
 * - AuditDB - is a permanent long-term archive of everything
 * - AuthDB - This contains the "recent" subset of account-related data from AuditDb (as Capped collection for quick lookup)
 * - company databd - This contains the "recent" subset of activity-related data on company desks (as Capped collection for quick lookups)
 *
 * Audits will be of various types (each with own collection, even though the schema will be the same)
 * - Account alerts - Accounts / Billing related events
 * - Company desk activities
 * - INFO activities - relating to user moving from place to place, e.g. log-in, went-to-library, etc.
 * - Security alerts
 * - Application Errors
 *
 * Even though the schema for everythign is the same, we keep them in separate collections to speed up queries and better
 * memory management
 */

(function(r) {
    var db, utils;
    var _fn = {};

    _fn.setup = function(_db, _ut){
        db = _db;
        utils = _ut;
    }


    //Security Alerts
    //These should generate an email as wells
    _fn.securityAlert = function(what, companyid, data, level, callback, req) {

        utils.doAsync(
            Audit,"alert", what, companyid, data, level, callback, req
        );
    }

    //INFO type alerts relating to user behavior
    //Also cached in the Company Datadb cached collection
    _fn.FYI = function(what, companyid, companyPrimaryDb, data, level, callback, req){

        utils.doAsync(function() {
            var auditobj = getAuditObj(what,companyid, req, data, level);

            //These two done in parallel
            doAudit("info", auditobj, callback);

            //TODO: insert into log table

        });

    }

    //APP ERROR LOGS?
    config.errLogger = function(s) {

        utils.doAsync(
             Audit,'system', 2, 'APP:ERROR', '', s
         );
    }


    //Private func - general purpose func for adding it in
    var Audit = function(which, what, companyid, data, level, callback, req) {

        var auditobj = getAuditObj(what,companyid, req, data, level);

        if ((which == "alert") && (level >= 4)) {
            utils.email.internalAlert(what, JSON.stringify(auditobj));
        }

        doAudit(which, auditobj, callback);
    };

    var doAudit = function(which, auditobj, callback){

        if (!callback) callback = function(){};

	    //TODO: insert into log table
    }

    //Gets the JSON schema to insert
    //After santization
    var getAuditObj = function(what, companyid, req, data, level){

        if(!companyid) companyid = 'system';

        //1- Get who
        var who;
        if(req && req.hasOwnProperty('session') && req.session.hasOwnProperty('uname'))
              who = req.session.uname;
        else who = "system";

        var now = new Date();

        return {
            //company id and person
            cid: companyid,
            who: who,
            ip: utils.GetClientIP(req),
            on: new Date(),

            //type of event (e.g. "USER::LOGGEDIN")
            what: what,
            
            //1, 2, 3, etc - indicating additional severity
            l: level,

            //Extra data - can be any JSON structure
            d: data,

            //MONTHYEAR - that'll be used as a shard key so each DB / instance
            //only has one month's worth of data at worst
            key: utils.date.format(now, "MMYYYY")
        };
    };




    module.exports = _fn;


})(require);