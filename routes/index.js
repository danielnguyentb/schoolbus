/*
 * Class: Routes
 * Sets up the app (the builder will add stuff in here)
 * Also has "global" controllers - for things that wont land into convenient modules
 */
(function (ns, r) {

	//Anything added to "ns" will become public - i.e. stuff that should be used from other modules
	//everythign else can remain private to this closure

	//_fn will be shared across all modules

	var _fn = r("./utils");

	ns.init = function (app, express, db, i18n) {

		_fn.audit.setup(db, _fn);

		//Load all data-layers
		var managers = r("../modules/managers")(app, db, _fn);

		_fn.auth = _fn.auth(managers, _fn);		//this inits the middleware

		//Doing this here because the privateDataManger sets up routes that need
		//_fn.auth to be defined
		managers.privateData.setupRoutes(app);

		app.use('/api', app.get('wcl:api:routes'));

		//Routes
		r('../modules/acc').init(app, db, _fn, managers, i18n);
		r('../modules/auth').init(app, db, _fn, managers, i18n);
		r('../modules/users').init(app, db, _fn, managers, i18n);
		r('../modules/group').init(app, db, _fn, managers, i18n);
		r('../modules/child').init(app, db, _fn, managers, i18n);
		r('../modules/driver').init(app, db, _fn, managers, i18n);
		r('../modules/assistant').init(app, db, _fn, managers, i18n);
		r('../modules/location').init(app, db, _fn, managers, i18n);
		r('../modules/notification').init(app, db, _fn, managers, i18n);
		r('../modules/log').init(app, db, _fn, managers, i18n);
		r('../modules/bus').init(app, db, _fn, managers, i18n);
		r('../modules/card').init(app, db, _fn, managers, i18n);
		r('../modules/config').init(app, db, _fn, managers, i18n);
		r('../modules/route').init(app, db, _fn, managers, i18n);
		r('../modules/calendar').init(app, db, _fn, managers, i18n);
		r('../modules/company').init(app, db, _fn, managers, i18n);
		r('../modules/api').init(app, db, _fn, managers, i18n);


        //if (config.MODE.toLowerCase() == "dev") {
        //     _fn.GetClientIP(null, true, function(e,r){
        //         if (!e)
        //             _fn.email.systemMail("duong@wwcom.com", "ping", "ping from : " + r);
        //     });
        //}

		return managers;
	};

})(module.exports, require);
