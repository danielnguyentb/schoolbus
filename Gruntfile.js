'use strict';
module.exports = function(grunt) {
    require('grunt-config-dir')(grunt, {
        configDir: require('path').resolve('task')
        //fileExtensions: ['js', 'coffee']
    }, function(err) {
        grunt.log.error(err)
    });
    //grunt.initConfig({
    //    sint: {}
    //});

    grunt.loadNpmTasks('grunt-makara-amdify');
    grunt.registerTask('min', ['requirejs']);
    grunt.registerTask('view', ['dustjs', 'makara-amdify', 'copyto:lang', 'clean:lang']);
    grunt.registerTask('build-module', ['uglify:module']);
    grunt.registerTask('build', ['clean', 'copyto', 'cssmin', 'uglify', 'dustjs', 'makara-amdify', 'copyto:lang', 'clean:lang', 'requirejs']);
    grunt.registerTask('default', ['sint']);
};
