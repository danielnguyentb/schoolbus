/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/card-controller', [])

        .controller('CardController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'Socket', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', '$state',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, Socket, validationService, $http, AuthService, $httpParamSerializer, $rootScope, $state) {

                $scope.card = {
                    qrcode : '',
                    nfccode: '',
                    child_code: '',
                    gr_qrcode : '',
                    gr_nfccode: '',
                    guardian_card: ''
                };

                $scope.profile = {};
                $scope.card_option = 0;
                $scope.add_guardian = 0;
                $scope.card_option_select = 1;
                $scope.childs = {};
                $scope.checkChildCard = function(){
                    $http.post('/child/getByCode',$scope.card)
                    .then(
                        function (res) {
                            var data = res.data;
                            console.log(data);
                            if(data.childs.length){

                                if(data.childs[0].status == 2){
                                    Common.confirm($translate.instant('guardian_add'))
                                        .then(function() {
                                            $scope.add_guardian = 1;
                                        }, function() {
                                        });
                                    $scope.card_option = 1;
                                    $scope.profile = data.childs[0];
                                }
                                if(data.childs[0].status == 1){
                                    Common.pushNoty($translate.instant('active_card'), 'error');
                                }
                            }
                            else{
                                Common.pushNoty($translate.instant('invalid_card'), 'error');
                            }
                        },
                        function () {}
                    );
                };

                $scope.addGuardianChild = function(id){
                    var data = {
                        child_id : $scope.profile.id,
                        guardian_id : id
                    };
                    $http.post('/child/insertGuardianChild',data)
                        .then(
                        function (res) {
                            var data = res.data;
                            console.log(res.data);
                            if(data.success){
                                Common.pushNoty(
                                    $translate.instant('add_guardian_success'),
                                    'success'
                                );
                            }
                            else{
                                Common.pushNoty($translate.instant('add_guardian_fail'), 'error');
                            }
                        },
                        function () {}
                    );
                };

                $scope.checkGuardianCard = function(){
                    $http.post('/child/getGuardianByCode',$scope.card)
                    .then(
                        function (res) {
                            var data = res.data;
                            console.log(res.data);
                            if(data.guardians.length){
                                Common.confirm($translate.instant('guardian_add_to_child'))
                                    .then(function() {
                                        $scope.addGuardianChild(data.guardians[0].id);
                                    }, function() {
                                        $scope.add_guardian = 0;
                                        $scope.card.gr_nfccode = '';
                                        $scope.card.gr_qrcode = '';
                                        $scope.card.guardian_card = '';

                                    });
                            }
                            else{
                                Common.pushNoty($translate.instant('invalid_card'), 'error');
                            }
                        },
                        function () {}
                    );
                };
                $scope.card_option_submit = function(id){
                    if($scope.card_option_select == 1){
                        openCard($scope.profile);
                    }
                    else{
                        $http.get('/child')
                        .then(
                            function (res) {
                                var data = res.data;
                                if (!data.success) Common.pushNoty(data.message, 'error');
                                openSync(data.childs,$scope.profile.id);
                            },
                            function () {
                                $scope.childs = {};
                            }
                        );
                    }
                };
                $scope.onReset = function(){
                    console.log('onReset');
                    $scope.card = {
                        qrcode : '',
                        nfccode: ''
                    };
                    $scope.profile = {};
                    $scope.card_option = 0;
                    $scope.card_option_select = 1;
                };

                var openCard = function (data) {
                    $mdDialog.show({
                        controller         : 'CardDetailController',
                        templateUrl        : 'public/views/card/detail.html',
                        parent             : angular.element(document.body),
                        clickOutsideToClose: true,
                        locals             : {
                            cardDetail: data
                        }
                    });
                };
                var openSync = function (data, tempId) {
                    console.log(data);
                    $mdDialog.show({
                        controller         : 'CardSyncController',
                        templateUrl        : 'public/views/card/sync.html',
                        parent             : angular.element(document.body),
                        clickOutsideToClose: true,
                        locals             : {
                            childsList: data,
                            tempId: tempId

                        }
                    });
                };

                Socket.get().on('scan:card:pull', function(data) {
                    if($scope.add_guardian == 1){
                        if(data.type == 1){
                            $scope.card.gr_qrcode = data.code;
                            $scope.card.gr_nfccode = '';
                        }
                        else{
                            $scope.card.gr_nfccode = data.code;
                            $scope.card.gr_qrcode = '';
                        }
                        $scope.card.guardian_code = data.code;
                        $scope.checkGuardianCard();
                    }
                    else{
                        if(data.type == 1){
                            $scope.card.qrcode = data.code;
                            $scope.card.nfccode = '';
                        }
                        else{
                            $scope.card.nfccode = data.code;
                            $scope.card.qrcode = '';
                        }
                        $scope.card.child_code = data.code;
                        $scope.checkChildCard();

                    }
                    $scope.$apply();
                });

                $rootScope.$on('card-manage-update', $scope.onReset());

                $translatePartialLoader.addPart('card');
                $translate.refresh();
            }
        ])

        .controller('CardSyncController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'childsList', 'tempId',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, childsList, tempId) {
                $scope.title = $translate.instant('link-to-exist-profile');
                if(childsList) {
                    $scope.childsList = childsList
                }
                else{
                    $scope.childsList = [];
                }
                $scope.exist_profile = 0;

                $scope.sync_profile = function() {

                    if(this.form.$valid) {
                        $scope.isLoading = true;
                        var data = {
                            temp_profile: tempId,
                            child_id: $scope.exist_profile
                        };
                        $http
                            .post('/child/syncCard',data)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {
                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('card-manage-update');
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
            }
        ])

        .controller('CardDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'cardDetail',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, cardDetail) {

                if(cardDetail && cardDetail.id) {
                    $scope.title = $translate.instant('create-new-profile');
                    $scope.cardDetail = {
                        name : '',
                        id: cardDetail.id
                    };
                }

                $scope.create_profile = function() {

                    if(this.form.$valid) {
                        $scope.isLoading = true;
                        $http
                            .post('/child/updateTempProfile',$scope.cardDetail)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {
                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('card-manage-update');
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
            }
        ]);
})();
