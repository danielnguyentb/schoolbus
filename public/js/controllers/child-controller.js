/**
 * Created by AnhNguyen
 */

(function() {
    "use strict";

    angular.module('controllers/child-controller', [])

        .controller('ChildManage', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$state',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $state) {

                $scope.selected = [];
                $scope.total    = 0;
                $scope.role = AuthService.getRole();
                $scope.query = {
                    order: 'name',
                    limit: 10,
                    page : 1
                };

                $scope.loadChild = function () {

                    $scope.childs = [];

                    $scope.deferred = $http.get('/child?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(data.message, 'error');

                            $scope.childs = data.childs;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.childs = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.removeChild = function(id) {

                    Common
                        .confirm($translate.instant('confirm_remove_child'))
                        .then(
                            function() {

                                $http
                                    .delete('/child/' + id)
                                    .then(
                                        function(res) {

                                            var data = res.data;
                                            Common.pushNoty(
                                                data.message,
                                                (data.success) ? 'success' : 'false'
                                            );

                                            if(data.success) $scope.loadChild();
                                        },
                                        function() {
                                            Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                        }
                                    )
                            }
                        );
                };

                $scope.createChild = function(ev) {

                    $mdDialog.show({
                        controller         : 'CreateChildController',
                        templateUrl        : 'public/views/child/create.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: false
                    });
                };

                $scope.goDetail = function (id) {

                    $state.go('child.detail', {
                        child: id
                    })
                };

                $scope.loadChild();

                $translatePartialLoader.addPart('child');
                $translate.refresh();
            }
        ])

        .controller('CreateChildController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$state', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $state, $rootScope) {

                $scope.cancel = function() {

                    if(!this.isLoading)
                        $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http.post('/child', $scope.child)
                            .then(
                                function(res) {

                                    var data = res.data;

                                    Common.pushNoty(
                                        data.message,
                                        (data.success) ? 'success' : 'error'
                                    );

                                    if(data.success && data.cid !== undefined) {
                                        $mdDialog.cancel();
                                        $state.go('child.detail', {
                                            child: data.cid
                                        });
                                    }

                                },
                                function() {
                                    $scope.isLoading = false;
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            );
                    } else new validationService().checkFormValidity(this.form);
                };
            }
        ])

        .controller('ChildDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', 'ChildDetail', '$state', '$cookies',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, ChildDetail, $state, $cookies) {

                if(ChildDetail.status == 200) {

                    var data = ChildDetail.data;
                    data.child.status = data.child.status == 1;
                    $scope.child = data.child;

                    $scope.slcGroups = [];
                    data.group.forEach(function(v) {
                        $scope.slcGroups.push({
                            id: v.id,
                            name: v.name
                        })
                    });

                } else $state.go('child');

                $scope._csrf   = $cookies.get('xsrf-token');

                $scope.onAppendGroup = function(chip) {

                    var groups = $scope.slcGroups;
                    groups.forEach(function(v, i) {
                        if(v.id == chip.id) {

                            groups.splice(i, 1);
                        }
                    });

                    $scope.slcGroups = groups;
                    return chip;
                };

                $scope.groupSearch = function(text) {

                    return $http.get('/group?' + $httpParamSerializer({keyword: text}))
                        .then(function(res) {

                            var data = res.data;
                            if(data.success) return data.groups;
                            else return [];
                        });
                };

                $scope.confirm = function() {

                    if(this.childDetail.$valid) {

                        $scope.isLoading = true;

                        //Get group selected
                        var group = [];
                        this.slcGroups.forEach(function(v) {
                            if(v.id) group.push(v.id);
                        });
                        $scope.child.groups = group;

                        $http.post('/child/' + $scope.child.id, $scope.child)
                            .then(
                                function(res) {

                                    var data = res.data;

                                    $scope.isLoading = false;
                                    Common.pushNoty(data.message, data.success ? 'success' : 'error');
                                },
                                function() {
                                    $scope.isLoading = false;
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            );

                    } else new validationService().checkFormValidity(this.childDetail);
                };

                $scope.go = function(state) {
                    $state.go(state);
                };

                $scope.showDialog = function(type, ev) {

                    var content = '';

                    if(type == 'otp' && $scope.child.OTP != '') {

                        content += $translate.instant('user_get_otp');
                        content += '<h1 class="text-center marbottom0">' + $scope.child.OTP + '</h1>';
                    } else {

                        content += $translate.instant('user_get_qr');
                        content += '<br/><div layout="row" layout-align="center center"><img src="/child/qr/' + $scope.child.id + '/' + type + '"/></div>';
                    }

                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .content(content)
                            .ok($translate.instant('got_it'))
                            .targetEvent(ev)
                    );
                };

                $translatePartialLoader.addPart('child');
                $translate.refresh();
            }
        ])
    ;

})();
