(function (ng) {
    "use strict";

    ng.module('controllers/calendar-controller', ['ui.calendar'])

        .controller('CalendarController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', '$state',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, $state) {

                $scope.selected = [];
                $scope.total    = 0;

                $scope.query = {
                    order: 'name',
                    limit: 10,
                    page : 1
                };

                $scope.calendarForm = function (id, ev) {
                    if (id) {
                        $scope.deferred = $http.get('/calendar/' + id);

                        $scope.deferred.then(
                            function(res) {

                                var data = res.data;
                                if(data.success) {

                                    openCalendarForm(data.calendar, ev);
                                } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            },
                            function() {
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else openCalendarForm(false, ev);
                };

                var openCalendarForm = function (data, ev) {
                    $mdDialog.show({
                        controller         : 'CreateCalendarController',
                        templateUrl        : 'public/views/calendar/create.html?t=' + (new Date()).getTime(),
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: false,
                        locals             : {
                            CalendarDetail : data
                        },
                        resolve : {
                            SharedCalendars: ['$http', function($http) {
                                return $http.get('/get/calendars?id=' + (data && data.id ? data.id : ''));
                            }]
                        }
                    });
                };

                $scope.calendarStatus = function(status) {
                    var map = ['disable', 'active', 'confirm'];

                    if(map[status]) return $translate.instant(map[status]);

                    return 'N/A';
                };

                $scope.goCalendarDetail = function (id) {
                    if(!id) return false;

                    $state.go('calendar.detail', {
                        calendarId: id
                    });
                };

                $scope.loadCalendar = function () {

                    $scope.routes = [];
                    $scope.deferred = $http.get('/get/calendars?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;
                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.calendars = data.calendars;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.calendars = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadCalendar();
                $rootScope.$on('calendar-manage-update', $scope.loadCalendar);

                $translatePartialLoader.addPart('calendar');
                $translate.refresh();
            }
        ])

        .controller('CreateCalendarController', ['$rootScope', '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'AuthService', 'validationService', '$http', '$httpParamSerializer', 'CalendarDetail', 'SharedCalendars',
            function ($rootScope, $scope, $translate, $translatePartialLoader, $mdDialog, Common, AuthService, validationService, $http, $httpParamSerializer, CalendarDetail, SharedCalendars) {

                $scope.user = AuthService._uData;
                $scope.calendar = {
                    inherits: [],
                    status: 1,
                    shared_type: 1,
                    owner_id: $scope.user.uId,
                    shared_users: [],
                    owner_type: 0
                };

                if(SharedCalendars.status == 200) $scope.inherits = SharedCalendars.data.calendars;
                else $scope.inherits = [];

                if(CalendarDetail) $scope.calendar = angular.extend($scope.calendar, CalendarDetail);

                $scope.onOwnerTypeChange = function() {
                    $scope.ownerKeyword = '';
                    $scope.calendar.owner = undefined;
                };

                $scope.onUserSharedChangeRole = function() {
                };

                $scope.onShareUserSelected = function(item) {
                    if(!item || !item.id) return false;

                    $scope.calendar.shared_users.push({
                        email: item.email,
                        uid: item.id,
                        role: 1
                    });
                };

                $scope.onRemoveSharedUser = function(index) {
                    $scope.calendar.shared_users.splice(index, 1);
                };

                $scope.usersSearch = function(q, type) {
                    var t = 0;
                    if(type) {
                        t = type == 1 ? 7 : 4;
                    }

                    return $http.get('/users/search?' + $httpParamSerializer({q: q, t: t}))
                        .then(function(res) {

                            var data = res.data;
                            if(data.success) return data.users;
                            else return [];
                        });
                };

                $scope.cancel = function() {
                    if(!this.isLoading) $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http.post('/calendar', $scope.calendar)
                            .then(
                                function(res) {

                                    var data = res.data;

                                    Common.pushNoty(data.message, data.success ? 'success' : 'error');

                                    if(data.success) {
                                        $mdDialog.cancel();
                                        $rootScope.$broadcast('calendar-manage-update');
                                    }
                                },
                                function() {
                                    $scope.isLoading = false;
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            );
                    } else new validationService().checkFormValidity(this.form);
                };
            }
        ])

        .controller('CalendarDetailController', ['$rootScope', '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$state', '$stateParams', 'CalendarDetail',
            function ($rootScope, $scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $state, $stateParams, CalendarDetail) {

                $scope.calendar = {};

                if(CalendarDetail.status == 200) $scope.calendar = CalendarDetail.data.calendar;
                else $state.go('calendar');

                var onScheduleClick = function(s, e) {
                    if(!s.is_ph && !s.is_his) $scope.scheduleForm(s.id, e);
                };

                $scope.uiConfig = {
                    calendar:{
                        editable: false,
                        droppable: false,
                        header:{
                            left: 'prev',//'month agendaWeek agendaDay',
                            center: 'title',
                            right: 'next'//'today prev,next'
                        },
                        eventLimit: true,
                        aspectRatio: 1,
                        eventClick: onScheduleClick
                    }
                };

                $scope.eventSources = [];

                $scope.goBack = function() {
                    $state.go('calendar');
                };

                $scope.scheduleForm = function (id, ev) {
                    if (id) {
                        $scope.isLoading = true;

                        $scope.deferred = $http.get('/calendar/schedule/' + id);

                        $scope.deferred.then(
                            function(res) {

                                $scope.isLoading = false;
                                var data = res.data;
                                if(data.success) {

                                    openScheduleForm(data.schedule, ev);
                                } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            },
                            function() {
                                $scope.isLoading = false;

                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else openScheduleForm(false, ev);
                };

                var openScheduleForm = function (data, ev) {
                    $mdDialog.show({
                        controller         : 'CreateScheduleController',
                        templateUrl        : 'public/views/calendar/schedule-create.html?t=' + (new Date()).getTime(),
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: false,
                        onComplete         : function(){
                            $('.datepicker').datetimepicker({
                                format: 'YYYY-MM-DD'
                            }).on('dp.change', function(e) {
                                ng.element($(this)[0]).triggerHandler('input');
                            });
                            $('.timepicker').datetimepicker({
                                format: 'HH:mm'
                            }).on('dp.change', function(e) {
                                ng.element($(this)[0]).triggerHandler('input');
                            });
                        },
                        locals             : {
                            ScheduleDetail : data
                        },
                        resolve : {
                            ScheduleInherits: ['$http', function($http) {
                                if(data) return {};

                                return $http.get('/calendar/'+ $stateParams.calendarId +'/schedules?raw=1');
                            }]
                        }
                    });
                };

                $scope.loadSchedules = function() {

                    $scope.deferred = $http.get('/calendar/'+ $stateParams.calendarId +'/schedules');

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;
                            if (!data.success) Common.pushNoty(res.message, 'error');

                            if(data.success) angular.copy(data.schedules, $scope.eventSources);
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadSchedules();
                $rootScope.$on('calendar-schedule-update', $scope.loadSchedules);

                $translatePartialLoader.addPart('calendar');
                $translate.refresh();
            }
        ])

        .controller('CreateScheduleController', ['$rootScope', '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', '$state', '$stateParams', '$httpParamSerializer', 'ScheduleDetail', 'ScheduleInherits',
            function ($rootScope, $scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, $state, $stateParams, $httpParamSerializer, ScheduleDetail, ScheduleInherits) {

                var now = new Date();
                $scope.schedule = {
                    calendar_id: $stateParams.calendarId,
                    recurring: 'One',
                    yearly_order: 1,
                    yearly_weekid: (now.getDay() + 1),
                    yearly_monthid: (now.getMonth() + 1),
                    mode: 0,
                    specify_dates: [],
                    inherits: []
                };

                if(ScheduleDetail) {
                    $scope.schedule = angular.extend($scope.schedule, {
                        id: ScheduleDetail.id,
                        description: ScheduleDetail.description,
                        date_from: ScheduleDetail.from_date,
                        date_to: ScheduleDetail.to_date,
                        time_from: ScheduleDetail.checkin_time,
                        time_to: ScheduleDetail.checkout_time,
                        location: ScheduleDetail.location || null,
                        mode: ScheduleDetail.mode == 1,
                        shared: ScheduleDetail.shared,
                        recurring: ScheduleDetail.recurring,
                        inherit_id: ScheduleDetail.inherit_schedule,
                        inherits: ScheduleDetail.inherits || [],
                        specify_dates: ScheduleDetail.specify_dates || []
                    });

                    if(ScheduleDetail.recurring == 'Weekly' && ScheduleDetail.weekly_days) {
                        ScheduleDetail.weekly_days.forEach(function(v) {
                            $scope.schedule['wd' + v] = true;
                        });
                    } else if(ScheduleDetail.recurring == 'SpecifyDates'){
                        $scope.schedule.repeat_from = ScheduleDetail.repeat_from || '';
                        $scope.schedule.repeat_to = ScheduleDetail.repeat_to || '';
                    } else if(ScheduleDetail.recurring == 'Yearly') {
                        $scope.schedule.yearly_order = ScheduleDetail.yearly.the_order || 1;
                        $scope.schedule.yearly_weekid = ScheduleDetail.yearly.weekid || (now.getDay() + 1);
                        $scope.schedule.yearly_monthid = ScheduleDetail.yearly.monthid || (now.getMonth() + 1);
                    }
                } else {
                    if(ScheduleInherits.status == 200) $scope.schedule.inherits = ScheduleInherits.data.schedules || [];
                }

                if($scope.location) $scope.searchText = $scope.location.address;

                $scope.onSpecifyDateSelected = function() {
                    if(!$scope.specify_dates_selecting) {
                        $scope.specify_dates_selecting = true;

                        return false;
                    }

                    if($scope.schedule.specify_dates.indexOf($scope.special_date_selected) == -1) $scope.schedule.specify_dates.push($scope.special_date_selected);
                };

                $scope.removeSpecialDate = function($index) {
                    $scope.schedule.specify_dates.splice($index, 1);
                };

                $scope.cancel = function() {
                    if(!this.isLoading) $mdDialog.cancel();
                };

                $scope.delete = function() {
                    if(!$scope.schedule.id) return false;

                    $scope.isLoading = true;

                    $http.delete('/calendar/schedule/' + $scope.schedule.id)
                        .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(data.message, data.success ? 'success' : 'error');

                                if(data.success) {
                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('calendar-schedule-update');
                                } else {
                                    $scope.isLoading = false;
                                }
                            },
                            function() {
                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                };

                $scope.submit = function() {

                    if(this.form.$valid) {
                        if(!validateSchedule()) return false;

                        $scope.isLoading = true;

                        $scope.schedule.location_id = $scope.schedule.location ? $scope.schedule.location.id : 0;
                        $http.post('/calendar/schedule', $scope.schedule)
                            .then(
                                function(res) {

                                    var data = res.data;

                                    Common.pushNoty(data.message, data.success ? 'success' : 'error');

                                    if(data.success) {
                                        $mdDialog.cancel();
                                        $rootScope.$broadcast('calendar-schedule-update');
                                    } else {
                                        $scope.isLoading = false;
                                    }
                                },
                                function() {
                                    $scope.isLoading = false;
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            );
                    } else new validationService().checkFormValidity(this.form);
                };

                var validateSchedule = function() {
                    var schedule = $scope.schedule;

                    if(schedule.recurring == 'SpecifyDates') {
                        if(!schedule.specify_dates || !schedule.specify_dates.length) {
                            Common.pushNoty($translate.instant('specify_date_required'), 'error');

                            return false;
                        }
                    } else if(schedule.recurring == 'Weekly') {
                        var i, valid = false;

                        for(i = 1; i <= 7; i++) {
                            if(schedule['wd' + i]) {
                                valid = true;
                                break;
                            }
                        }

                        if(!valid) {
                            Common.pushNoty($translate.instant('weekly_days_required'), 'error');

                            return false;
                        }
                    }

                    return true;
                };

                $scope.locationSearch = function(q) {
                    return $http.get('/calendar/locations?' + $httpParamSerializer({q: q}))
                        .then(function(res) {

                            var data = res.data;
                            if(data.success) return data.locations;
                            else return [];
                        });
                };

                $translatePartialLoader.addPart('calendar');
                $translate.refresh();
            }
        ]);

})(angular);
