/**
 * Created by AnhNguyen
 */

(function (ng) {
    "use strict";

    angular.module('controllers/log-controller', [])

        .controller('LogController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'Socket', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', '$state',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, Socket, validationService, $http, AuthService, $httpParamSerializer, $rootScope, $state) {

                $scope.select_date  = '';
                $scope.route_select = '';
                $scope.routes       = [];
                $scope.logs         = [];
                $('.datepicker').datetimepicker({
                    format: 'YYYY-MM-DD'/*,
                     defaultDate: routeDetailData.route.plan_date*/
                }).on('dp.change', function(e) {
                    ng.element($(this)[0]).triggerHandler('input');
                    $scope.getRouteListByDate();
                });
                $scope.getRouteListByDate = function(){
                    $scope.routes = [];
                    $scope.deferred = $http.get('/routebydate?date=' + $scope.select_date);
                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(data.message, 'error');

                            $scope.routes = data.routes;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.routes = [];
                            $scope.total  = 0;
                        }
                    );
                    return $scope.deferred;
                };

                $scope.getLog = function(){
                    $scope.logs = [];
                    $scope.deferred = $http.get('/log?date=' + $scope.select_date + '&route_trip=' + $scope.route_select);
                    $scope.deferred.then(
                        function (res) {
                            var data = res.data;
                            if (!data.success) Common.pushNoty(data.message, 'error');

                            $scope.logs = data.logs;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.logs = [];
                            $scope.total  = 0;
                        }
                    );
                    return $scope.deferred;
                };
                $translatePartialLoader.addPart('log');
                $translate.refresh();
            }
        ]);
})(angular);
