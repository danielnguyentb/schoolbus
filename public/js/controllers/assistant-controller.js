/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/assistant-controller', [])

        .controller('AssistantController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope) {

                $scope.selected = [];
                $scope.total    = 0;

                $scope.query = {
                    order: 'username',
                    limit: 10,
                    page : 1
                };

                $scope.statusText = function(status) {
                    var map = ['waiting_for_approval', 'approved', 'blocked'];

                    if(map[status]) return $translate.instant(map[status]);

                    return 'N/A';
                };

                $scope.removeAssistant = function (id) {

                    Common
                        .confirm($translate.instant('confirm_remove_assistant'))
                        .then(
                        function() {

                            $http
                                .delete('/assistant/' + id)
                                .then(
                                function(res) {

                                    var data = res.data;
                                    Common.pushNoty(
                                        data.message,
                                        (data.success) ? 'success' : 'false'
                                    );

                                    if(data.success) $scope.loadAssistant();
                                },
                                function() {
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            )
                        }
                    );

                };

                $scope.assistantDetail = function (id, ev) {

                    if (id != '') {

                        $scope.deferred = $http.get('/assistant/' + id);

                        $scope.deferred.then(
                            function(res) {

                                var data = res.data;
                                if(data.success) {

                                    openAssistant(data.assistant, ev);
                                } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            },
                            function() {
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else openAssistant(false, ev);
                };

                var openAssistant = function (data, ev) {

                    $mdDialog.show({
                        controller         : 'AssistantDetailController',
                        templateUrl        : 'public/views/assistant/detail.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true,
                        locals             : {
                            assistantDetail: data
                        }
                    });
                };

                $scope.loadAssistant = function () {

                    $scope.assistants = [];

                    $scope.deferred = $http.get('/assistant?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(data.message, 'error');

                            $scope.assistants = data.assistants;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.assistants = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadAssistant();

                $rootScope.$on('assistant-manage-update', $scope.loadAssistant);

                $translatePartialLoader.addPart('assistant');
                $translate.refresh();
            }
        ])

        .controller('AssistantDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'assistantDetail',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, assistantDetail) {

                $scope.isAdmin = AuthService.isAdmin();

                if(assistantDetail && assistantDetail.id) {
                    var title = $translate.instant('edit-assistant');
                    if(assistantDetail.fullname != '' && assistantDetail.fullname != null){
                        title += assistantDetail.fullname;
                    }
                    else {
                        title += assistantDetail.username;
                    }
                    $scope.title = title;

                    $scope.assistantDetail = {
                        id    : assistantDetail.id,
                        email  : assistantDetail.email,
                        fullname  : assistantDetail.fullname,
                        username  : assistantDetail.username,
                        home_phone  : assistantDetail.home_phone,
                        mobile_phone  : assistantDetail.mobile_phone,
                        office_phone  : assistantDetail.office_phone,
                        status: assistantDetail.status
                    }
                } else $scope.title = $translate.instant('add-new-assistant');

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http
                            .post('/assistant', this.assistantDetail)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {

                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('assistant-manage-update');
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }
            }
        ]);

})();
