/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/driver-controller', [])

        .controller('DriverController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope) {

                $scope.selected = [];
                $scope.total    = 0;

                $scope.query = {
                    order: 'username',
                    limit: 10,
                    page : 1
                };

                $scope.statusText = function(status) {
                    var map = ['waiting_for_approval', 'approved', 'blocked'];

                    if(map[status]) return $translate.instant(map[status]);

                    return 'N/A';
                };

                $scope.removeDriver = function (id) {

                    Common
                        .confirm($translate.instant('confirm_remove_driver'))
                        .then(
                        function() {

                            $http
                                .delete('/driver/' + id)
                                .then(
                                function(res) {

                                    var data = res.data;
                                    Common.pushNoty(
                                        data.message,
                                        (data.success) ? 'success' : 'false'
                                    );

                                    if(data.success) $scope.loadDriver();
                                },
                                function() {
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            )
                        }
                    );

                };

                $scope.driverDetail = function (id, ev) {

                    if (id != '') {

                        $scope.deferred = $http.get('/driver/' + id);

                        $scope.deferred.then(
                            function(res) {

                                var data = res.data;
                                if(data.success) {

                                    openDriver(data.driver, ev);
                                } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            },
                            function() {
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else openDriver(false, ev);
                };

                var openDriver = function (data, ev) {

                    $mdDialog.show({
                        controller         : 'DriverDetailController',
                        templateUrl        : 'public/views/driver/detail.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true,
                        locals             : {
                            driverDetail: data
                        }
                    });
                };

                $scope.loadDriver = function () {

                    $scope.drivers = [];

                    $scope.deferred = $http.get('/driver?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(data.message, 'error');

                            $scope.drivers = data.drivers;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.drivers = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadDriver();

                $rootScope.$on('driver-manage-update', $scope.loadDriver);

                $translatePartialLoader.addPart('driver');
                $translate.refresh();
            }
        ])

        .controller('DriverDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'driverDetail',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, driverDetail) {

                $scope.isAdmin = AuthService.isAdmin();

                if(driverDetail && driverDetail.id) {
                    var title = $translate.instant('edit-driver');
                    if(driverDetail.fullname != '' && driverDetail.fullname != null){
                        title += driverDetail.fullname;
                    }
                    else {
                        title += driverDetail.username;
                    }
                    $scope.title = title;

                    $scope.driverDetail = {
                        id    : driverDetail.id,
                        email  : driverDetail.email,
                        fullname  : driverDetail.fullname,
                        username  : driverDetail.username,
                        home_phone  : driverDetail.home_phone,
                        mobile_phone  : driverDetail.mobile_phone,
                        office_phone  : driverDetail.office_phone,
                        status: driverDetail.status
                    }
                } else {
                    $scope.title = $translate.instant('add-new-driver');

                    $scope.driverDetail = {
                        status: 1
                    };
                }

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http
                            .post('/driver', this.driverDetail)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {

                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('driver-manage-update');
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }
            }
        ]);

})();
