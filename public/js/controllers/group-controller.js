/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/group-controller', [])

        .controller('GroupController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope) {

                $scope.selected = [];
                $scope.total    = 0;

                $scope.query = {
                    order: 'name',
                    limit: 10,
                    page : 1
                };

                $scope.removeGroup = function (id) {

                    Common
                        .confirm($translate.instant('confirm_remove_group'))
                        .then(
                        function() {

                            $http
                                .delete('/group/' + id)
                                .then(
                                function(res) {

                                    var data = res.data;
                                    Common.pushNoty(
                                        data.message,
                                        (data.success) ? 'success' : 'false'
                                    );

                                    if(data.success) $scope.loadGroup();
                                },
                                function() {
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            )
                        }
                    );

                };

                $scope.groupDetail = function (id, ev) {

                    if (id != '') {

                        $scope.deferred = $http.get('/group/' + id);

                        $scope.deferred.then(
                            function(res) {

                                var data = res.data;
                                if(data.success) {

                                    openGroup(data.group, ev);
                                } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            },
                            function() {
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else openGroup(false, ev);
                };

                var openGroup = function (data, ev) {

                    $mdDialog.show({
                        controller         : 'GroupDetailController',
                        templateUrl        : 'public/views/group/detail.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true,
                        locals             : {
                            groupDetail: data
                        }
                    });
                };

                $scope.loadGroup = function () {

                    $scope.groups = [];

                    $scope.deferred = $http.get('/group?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(data.message, 'error');

                            $scope.groups = data.groups;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.groups = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadGroup();

                $rootScope.$on('group-manage-update', $scope.loadGroup);

                $translatePartialLoader.addPart('group');
                $translate.refresh();
            }
        ])

        .controller('GroupDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'groupDetail',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, groupDetail) {

                if(groupDetail && groupDetail.id) {
                    $scope.title = $translate.instant('edit-group') + groupDetail.name;

                    $scope.groupDetail = {
                        id    : groupDetail.id,
                        name  : groupDetail.name,
                        desc  : groupDetail.description,
                        status: groupDetail.status == 1
                    }
                } else $scope.title = $translate.instant('add-new-group');

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http
                            .post('/group', this.groupDetail)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {

                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('group-manage-update');
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }
            }
        ]);

})();
