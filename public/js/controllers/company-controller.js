(function () {
    "use strict";

    angular.module('controllers/company-controller', [])

        .controller('CompanyController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope) {

                $scope.companies = [];
                $scope.selected = [];
                $scope.total    = 0;

                $scope.query = {
                    order: 'name',
                    limit: 10,
                    page : 1
                };

                $scope.companyMode = function(mode) {
                    var modeMap = {1: $translate.instant('bc_mode_basic')}, modes = [];

                    mode.split(',').forEach(function (v) {
                        if(modeMap[v]) modes.push(modeMap[v]);
                    });

                    return modes.join(', ');
                };

                $scope.companyDetail = function (id, ev) {

                    if (id != '') {

                        $scope.deferred = $http.get('/company/' + id);

                        $scope.deferred.then(
                            function(res) {

                                var data = res.data;
                                if(data.success) {

                                    openCompany(data.company, ev);
                                } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            },
                            function() {
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else openCompany(false, ev);
                };

                var openCompany = function (data, ev) {

                    $mdDialog.show({
                        controller         : 'CompanyDetailController',
                        templateUrl        : 'public/views/company/detail.html' + '?t=' + (new Date()).getTime(),
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true,
                        locals             : {
                            CompanyDetail: data
                        }
                    });
                };

                $scope.loadCompanies = function () {

                    $scope.companies = [];

                    $scope.deferred = $http.get('/company?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.companies = data.companies;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.companies = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadCompanies();

                $rootScope.$on('company-manage-update', $scope.loadCompanies);

                $translatePartialLoader.addPart('company');
                $translate.refresh();
            }
        ])

        .controller('CompanyDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'CompanyDetail',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, CompanyDetail) {

                $scope.bc_mode_lv1 = true;
                $scope.company = {
                    status: 1,
                    map_tracking: true,
                    parent_map: true
                };

                if(CompanyDetail && CompanyDetail.id) {
                    $scope.title = $translate.instant('edit-company') +  CompanyDetail.name;

                    $scope.company = {
                        id    : CompanyDetail.id,
                        name  : CompanyDetail.name,
                        address  : CompanyDetail.address,
                        status: CompanyDetail.status,
                        map_tracking: CompanyDetail.map_tracking == 1,
                        parent_map: CompanyDetail.parent_map == 1
                    };

                    if(CompanyDetail.mode) {
                        CompanyDetail.mode.forEach(function(v) {
                            $scope['bc_mode_lv' + v] = true;
                        });
                    }
                } else $scope.title = $translate.instant('add-new-company');

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.bcModeRequired = function(on, reqMode) {
                    $scope['bc_mode_lv' + reqMode] = on;
                    $scope['bc_mode_lv' + reqMode + '_disabled'] = on;
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $scope.company.mode = [1];
                        for(var i = 2; i <= 4; i++) {
                            if($scope['bc_mode_lv' + i]) $scope.company.mode.push(i);
                        }

                        $http
                            .post('/company', this.company)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {

                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('company-manage-update');
                                }

                                $scope.isLoading = false;
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }
            }
        ])

        .controller('StaffAttendanceController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$state',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $state) {

                $scope.loadStaffAttendance = function () {

                    $scope.items = [];
                    $scope.selected = [];
                    $scope.total = 0;

                    $scope.query = {
                        order: 'dt',
                        desc: true,
                        limit: 10,
                        page : 1
                    };

                    $scope.deferred = $http.get('/company/staff-attendance?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.items = data.items;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.items = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadStaffAttendance();

                $scope.staffAttendanceDetail = function(date) {
                    $state.go('staff-attendance-date', {
                        date: date
                    });
                };

                $translatePartialLoader.addPart('company');
                $translate.refresh();
            }
        ])

        .controller('StaffAttendanceDateController', [
            '$rootScope', '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$state', '$stateParams',
            function ($rootScope, $scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $state, $stateParams) {

                $scope.loadStaffAttendanceByDate = function () {

                    $scope.current_date = new Date($stateParams.date);
                    $scope.items = [];
                    $scope.total = 0;

                    $scope.query = {
                        order: 'dt',
                        desc: true,
                        limit: 10,
                        page : 1,
                        date: $stateParams.date
                    };

                    $scope.deferred = $http.get('/company/staff-attendance?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.items = data.items;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.items = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadStaffAttendanceByDate();
                $rootScope.$on('company-staff-update', function(staff) {
                    if(!staff.idx) return false;

                    $scope.items[staff.idx] = staff;
                });

                $scope.back = function() {
                    $state.go('staff-attendance');
                };

                $scope.onManualStaffCheckIn = function(idx, ev) {
                    var staff = $scope.items[idx];

                    if(staff.in || staff.out || staff.absent) return false;

                    staff.idx = idx;
                    $mdDialog.show({
                        controller         : 'CompanyStaffManualStatusController',
                        templateUrl        : 'public/views/company/staff-manual-status.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true,
                        locals             : {
                            Staff: staff
                        }
                    });
                };

                $translatePartialLoader.addPart('company');
                $translate.refresh();
            }
        ])

        .controller('CompanyStaffManualStatusController', [
            '$rootScope', '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', 'Staff',
            function ($rootScope, $scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, Staff) {
                $scope.staff = Staff;
                $scope.staff.manual_status = 1;

                $scope.title = $translate.instant('manual-staff-status') +  ($scope.staff.type == 2 ? $translate.instant('driver') : $translate.instant('assistant')) + '-' + $scope.staff.name;

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        var staff = this.staff;
                        $http.post('/company/staff-attendance', staff).then(function(res) {
                                    var data = res.data;

                                    Common.pushNoty(data.message, (data.success ? 'success' : 'error'));

                                    if(data.success) {
                                        $mdDialog.cancel();

                                        if(staff.manual_status == 1) staff = angular.extend(staff, {in: true, in_time: new Date()});
                                        else staff.absent = true;

                                        $rootScope.$broadcast('company-staff-update', staff);
                                    }

                                    $scope.isLoading = false;
                                },
                                function(res) {

                                    $scope.isLoading = false;
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            );

                    } else new validationService().checkFormValidity(this.form);
                };

                $translatePartialLoader.addPart('company');
                $translate.refresh();
            }
        ]);
})();
