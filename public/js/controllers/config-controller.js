/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/config-controller', [])

        .controller('ConfigController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope) {

                $scope.loadConfigs = function () {

                    $scope.configs = [];

                    $scope.deferred = $http.get('/config');

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.configs = data.configs[0];
                        },
                        function () {
                            $scope.configs = [];
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadConfigs();

                $scope.saveConfig = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;
                        $http
                            .post('/config', this.configs)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {

                                    $scope.isLoading = false;
                                    $scope.loadConfigs();
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                };

                $translatePartialLoader.addPart('config');
                $translate.refresh();
            }
        ])

        .controller('BCConfigController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope) {

                $scope.loadConfig = function () {
                    $scope.config = {
                        map_tracking: 1,
                        parent_map: 1
                    };

                    $scope.deferred = $http.get('/bc/config');

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.config = data.config;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadConfig();

                $scope.saveConfig = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;
                        $http
                            .post('/bc/config', this.config)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(data.message, data.success ? 'success' : 'error');

                                $scope.isLoading = false;
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                };

                $translatePartialLoader.addPart('config');
                $translate.refresh();
            }
        ]);
})();
