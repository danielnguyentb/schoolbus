/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/pending-controller', [])

        .controller('PendingController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$cookies', '$state', '$timeout', '$q', '$httpParamSerializer',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $cookies, $state, $timeout, $q, $httpParamSerializer) {

                $scope.selected = [];
                $scope.total = 0;

                $scope.query = {
                    order: 'username',
                    limit: 10,
                    page : 1
                };

                $scope.approveItem = function(id, type, ev) {
                    ev.preventDefault();
                    ev.stopPropagation();

                    if(!id) {

                        if(!this.selected.length) return false;
                        id = this.selected;
                    }

                    Common
                        .confirm($translate.instant( type == 'approve' ? 'confirm_approve' : 'cancel_approve' ))
                        .then(
                        function() {

                            var users = [];
                            if(Array.isArray(id)) {
                                id.forEach(function(v) {
                                    users.push(v.id);
                                });
                            } else users = [id];

                            $scope.isLoading = true;
                            $scope.deferred = $http
                                .post('/users/pending-approval', {
                                    u: users,
                                    t: type
                                })
                                .then(
                                function(res) {

                                    var data = res.data;
                                    $scope.isLoading = false;

                                    if(data.success) $scope.loadUsers();

                                    Common.pushNoty(data.message, data.success ? 'success' : 'error');
                                },
                                function() {

                                    $scope.isLoading = false;
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            )
                        },
                        function() {}
                    )
                };

                $scope.loadUsers = function() {

                    $scope.users = [];

                    $scope.deferred = $http.get('/users/pending-approval?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function(res) {

                            var data = res.data;

                            if(!data.success) Common.pushNoty(res.message, 'error');

                            $scope.users = data.users;
                            $scope.total = data.total;
                        },
                        function() {
                            $scope.users = [];
                            $scope.total = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadUsers();


                $translatePartialLoader.addPart('pending-approval');
                $translate.refresh();
            }
        ])

})();
