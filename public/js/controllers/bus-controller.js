/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/bus-controller', [])

        .controller('BusController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope) {

                $scope.selected = [];
                $scope.total    = 0;

                $scope.query = {
                    order: 'name',
                    limit: 10,
                    page : 1
                };

                $scope.removeBus = function (id) {

                    Common
                        .confirm($translate.instant('confirm_remove_bus'))
                        .then(
                        function() {

                            $http
                                .delete('/bus/' + id)
                                .then(
                                function(res) {

                                    var data = res.data;
                                    Common.pushNoty(
                                        data.message,
                                        (data.success) ? 'success' : 'false'
                                    );

                                    if(data.success) $scope.loadBus();
                                },
                                function() {
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            )
                        }
                    );

                };

                $scope.busDetail = function (id, ev) {

                    if (id != '') {

                        $scope.deferred = $http.get('/bus/' + id);

                        $scope.deferred.then(
                            function(res) {

                                var data = res.data;
                                if(data.success) {

                                    openBus(data.bus, ev);
                                } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            },
                            function() {
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else openBus(false, ev);
                };

                var openBus = function (data, ev) {

                    $mdDialog.show({
                        controller         : 'BusDetailController',
                        templateUrl        : 'public/views/bus/detail.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true,
                        locals             : {
                            busDetail: data
                        }
                    });
                };

                $scope.loadBus = function () {

                    $scope.buses = [];

                    $scope.deferred = $http.get('/bus?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.buses = data.buses;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.buses = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadBus();

                $rootScope.$on('bus-manage-update', $scope.loadBus);

                $translatePartialLoader.addPart('bus');
                $translate.refresh();
            }
        ])

        .controller('BusDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'busDetail',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, busDetail) {

                if(busDetail && busDetail.id) {
                    $scope.title = $translate.instant('edit-bus') +  busDetail.license_plate_no;

                    $scope.busDetail = {
                        id    : busDetail.id,
                        name  : '',
                        description  : busDetail.description,
                        license_plate_no  : busDetail.license_plate_no,
                        registered_country  : busDetail.registered_country,
                        snd_license_plate_no  : busDetail.snd_license_plate_no,
                        snd_registered_country  : busDetail.snd_registered_country,
                        status: busDetail.status == 1
                    }
                } else $scope.title = $translate.instant('add-new-bus');

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http
                            .post('/bus', this.busDetail)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {

                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('bus-manage-update');
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }
            }
        ])

        .controller('BusCardController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'Buses', 'Card',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, Buses, Card) {

                if(Buses.status == 200) $scope.buses = Buses.data.buses;
                else $scope.buses = [];

                $scope.busSelected = false;
                $scope.card = Card;
                $scope.busDetail = {};

                $scope.loadBusDetail = function() {
                    if(this.busDetail.id && this.busDetail.id != '') {
                        $scope.busSelected = true;
                        $http.get('/bus/' + this.busDetail.id).then(function(res) {
                            var data = res.data;

                            if(data.success) $scope.busDetail = res.data.bus;
                        }, function() {
                            Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            $scope.busDetail = {};
                        });
                    } else {
                        $scope.busSelected = false;
                        $scope.busDetail = {};
                    }
                };

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http
                            .post('/bus/card/save', {card: JSON.stringify(this.card), bus: JSON.stringify(this.busDetail)})
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) $mdDialog.cancel();
                                else $scope.isLoading = false;
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                };

                $translatePartialLoader.addPart('bus');
                $translate.refresh();
            }
        ]);
})();
