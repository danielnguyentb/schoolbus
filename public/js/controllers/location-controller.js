/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/location-controller', [])

        .controller('LocationController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope) {

                $scope.selected = [];
                $scope.total    = 0;

                $scope.query = {
                    order: 'name',
                    limit: 10,
                    page : 1
                };

                $scope.removeLocation = function (id) {

                    Common
                        .confirm($translate.instant('confirm_remove_location'))
                        .then(
                        function() {

                            $http
                                .delete('/location/' + id)
                                .then(
                                function(res) {

                                    var data = res.data;
                                    Common.pushNoty(
                                        data.message,
                                        (data.success) ? 'success' : 'false'
                                    );

                                    if(data.success) $scope.loadLocation();
                                },
                                function() {
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            )
                        }
                    );

                };

                $scope.locationDetail = function (id, ev) {

                    if (id != '') {

                        $scope.deferred = $http.get('/location/' + id);

                        $scope.deferred.then(
                            function(res) {

                                var data = res.data;
                                if(data.success) {

                                    openLocation(data.location, ev);
                                } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            },
                            function() {
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else openLocation(false, ev);
                };

                var openLocation = function (data, ev) {

                    $mdDialog.show({
                        controller         : 'LocationDetailController',
                        templateUrl        : 'public/views/location/detail.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true,
                        locals             : {
                            locationDetail: data
                        }
                    });
                };

                $scope.loadLocation = function () {

                    $scope.locations = [];

                    $scope.deferred = $http.get('/location?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.locations = data.locations;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.locations = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadLocation();

                $rootScope.$on('location-manage-update', $scope.loadLocation);

                $translatePartialLoader.addPart('location');
                $translate.refresh();
            }
        ])

        .controller('LocationDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'locationDetail',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, locationDetail) {

                if(locationDetail && locationDetail.id) {
                    $scope.title = $translate.instant('edit-location') +  locationDetail.name;

                    $scope.locationDetail = {
                        id    : locationDetail.id,
                        name  : locationDetail.name,
                        address  : locationDetail.address,
                        X  : locationDetail.X,
                        Y  : locationDetail.Y
                    }
                } else $scope.title = $translate.instant('add-new-location');

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http
                            .post('/location', this.locationDetail)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {

                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('location-manage-update');
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }
            }
        ]);
})();
