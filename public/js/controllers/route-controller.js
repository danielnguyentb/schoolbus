/**
 * Created by AnhNguyen
 */

(function (ng) {
    "use strict";

    ng.module('controllers/route-controller', [])

        .controller('RouteController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', '$state',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, $state) {

                $scope.selected = [];
                $scope.total    = 0;

                $scope.query = {
                    order: 'name',
                    limit: 10,
                    page : 1
                };

                $scope.removeRoute = function (id) {

                    Common
                        .confirm($translate.instant('confirm_remove_route'))
                        .then(
                        function() {

                            $http
                                .delete('/route/' + id)
                                .then(
                                function(res) {

                                    var data = res.data;
                                    Common.pushNoty(
                                        data.message,
                                        (data.success) ? 'success' : 'false'
                                    );

                                    if(data.success) $scope.loadRoute();
                                },
                                function() {
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            )
                        }
                    );

                };


                $scope.createRoute = function(ev) {
                    $mdDialog.show({
                        controller         : 'CreateRouteController',
                        templateUrl        : 'public/views/route/create.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: false,
                        onComplete         : function(){
                            $('.datepicker').datetimepicker({
                                format: 'YYYY-MM-DD'/*,
                                 defaultDate: routeDetailData.route.plan_date*/
                            }).on('dp.change', function(e) {
                                    ng.element($(this)[0]).triggerHandler('input');
                                });
                            $('.timepicker1').datetimepicker({
                                format: 'HH:mm'
                            }).on('dp.change', function(e) {
                                    ng.element($(this)[0]).triggerHandler('input');
                                });
                            $('.timepicker2').datetimepicker({
                                format: 'HH:mm'
                            }).on('dp.change', function(e) {
                                    ng.element($(this)[0]).triggerHandler('input');
                                });
                        },
                        resolve : {
                            'CompanyResource' : ['$http', function($http) {
                                return $http.get('/companyResource');
                            }],
                            'routeId' : ['$http', function($http) {
                                return 0;
                            }]
                        }
                    });
                };

                $scope.goRouteDetail = function (id) {
                    $state.go('route.detail', {
                        route: id
                    })
                };

                $scope.copyRoute = function(ev,routeId){

                    $mdDialog.show({
                        controller         : 'CreateRouteController',
                        templateUrl        : 'public/views/route/create.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: false,
                        onComplete         : function(){
                                                $('.datepicker').datetimepicker({
                                                    format: 'YYYY-MM-DD'/*,
                                                     defaultDate: routeDetailData.route.plan_date*/
                                                }).on('dp.change', function(e) {
                                                        ng.element($(this)[0]).triggerHandler('input');
                                                    });
                                                $('.timepicker1').datetimepicker({
                                                    format: 'HH:mm'
                                                }).on('dp.change', function(e) {
                                                        ng.element($(this)[0]).triggerHandler('input');
                                                    });
                                                $('.timepicker2').datetimepicker({
                                                    format: 'HH:mm'
                                                }).on('dp.change', function(e) {
                                                        ng.element($(this)[0]).triggerHandler('input');
                                                    });
                                            },
                        resolve : {
                            'CompanyResource' : ['$http', function($http) {
                                return $http.get('/companyResource');
                            }],
                            'routeId' : ['$http', function($http) {
                                return routeId;
                            }]
                        }
                    });
                };

                $scope.loadRoute = function () {

                    $scope.routes = [];
                    $scope.deferred = $http.get('/route?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;
                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.routes = data.routes;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.routes = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadRoute();

                $rootScope.$on('route-manage-update', $scope.loadRoute);

                $translatePartialLoader.addPart('route');
                $translate.refresh();
            }
        ])

        .controller('CreateRouteController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', '$state', 'CompanyResource', 'routeId',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, $state, CompanyResource, routeId) {

                if(CompanyResource.status == 200) {
                    $scope.CompanyResource = CompanyResource.data;
                    var driver_id = ($scope.CompanyResource.drivers.length) ? $scope.CompanyResource.drivers[0].id.toString() : 0;
                    var assistant_id = ($scope.CompanyResource.assistants.length) ? $scope.CompanyResource.assistants[0].id.toString() : 0;
                    var bus_id = ($scope.CompanyResource.buses.length) ? $scope.CompanyResource.buses[0].id.toString() : 0;
                    $scope.routeDetailData = {
                        driver_id  : driver_id,
                        assistant_id  : assistant_id,
                        bus_id  : bus_id,
                        routeid  : routeId
                    }
                }
                $scope.cancel = function() {

                    if(!this.isLoading)
                        $mdDialog.cancel();
                };

                $scope.submit = function() {
                    if(this.form.$valid) {
                        $scope.isLoading = true;

                        $http.post('/route', this.routeDetailData)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                //if(data.success) {
                                $mdDialog.cancel();
                                $rootScope.$broadcast('route-manage-update');
                                //}

                            },
                            function() {
                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else new validationService().checkFormValidity(this.form);
                };
            }
        ])

        .controller('RouteDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', '$state', 'routeDetail', 'CompanyResource', '$filter',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, $state, routeDetail, CompanyResource, $filter) {

                if(CompanyResource.status == 200) {
                    $scope.companyResource = CompanyResource.data;
                }
                if(routeDetail.status == 200) {
                    var routeDetailData = routeDetail.data;
                    var title = $translate.instant('edit-route');
                    title += routeDetailData.route.name;
                    $scope.title = title;
                    $scope.routeDetailData = {
                        id    : routeDetailData.route.id,
                        name  : routeDetailData.route.name,
                        driver_id  : routeDetailData.route.driver_id.toString(),
                        assistant_id  : routeDetailData.route.assistant_id.toString(),
                        bus_id  : routeDetailData.route.bus_id.toString(),
                        plan_date  : $filter('date')(routeDetailData.route.plan_date, 'yyyy-MM-dd'),
                        estimate_from  : routeDetailData.route.estimate_from,
                        estimate_to  : routeDetailData.route.estimate_to,
                        description  : routeDetailData.route.description,
                        status: routeDetailData.route.status == 1,
                        schedule_id: routeDetailData.route.schedule_id,
                        schedule: false
                    };
                    if(routeDetailData.route.schedule_id){
                        $scope.routeDetailData.schedule = true;
                        $scope.routeDetailData.start_date = $filter('date')(routeDetailData.schedule[0].start_date, 'yyyy-MM-dd') ;
                        $scope.routeDetailData.end_date = $filter('date')(routeDetailData.schedule[0].end_date, 'yyyy-MM-dd');
                    }
                    if(routeDetailData.schedule_detail.length){
                        for(var i = 0 ; i < routeDetailData.schedule_detail.length ; i ++){
                            var weekId = 'day'+ routeDetailData.schedule_detail[i].weekid;
                            $scope.routeDetailData[weekId] = true;
                        }
                    }
                    $scope.points = routeDetailData.points;
                }

                $scope.sortableOptions = {
                    stop: function(e, ui) {
                        var $item = $(ui.item);
                        var routeId = $item.data('rid'), point_id = $item.data('id'), prefid = 0;

                        if($item.prev().length) prefid = $item.prev().data('id');

                        $http.post('/route/updatePointOrder', {
                                rid: routeId,
                                point_id : point_id,
                                prefid : prefid
                            }).then(function(res) {
                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {
                                    $rootScope.$broadcast('route-manage-update');
                                }
                                },
                                function(res) {

                                    $scope.loadPoints();
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            });

                    }
                };

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
                $scope.go = function(state) {
                    $state.go(state);
                };

                $scope.confirm = function() {

                    $scope.isLoading = true;
                    $http
                        .post('/route/' + $scope.routeDetailData.id, this.routeDetailData)
                        .then(
                        function(res) {
                            var data = res.data;

                            Common.pushNoty(
                                data.message,
                                (data.success) ? 'success' : 'error'
                            );

                            if(data.success) {

                                $mdDialog.cancel();
                                $rootScope.$broadcast('route-manage-update');
                                $state.go('route');
                            }
                        },
                        function(res) {
                            $scope.isLoading = false;
                            Common.pushNoty($translate.instant('err_anonymous'), 'error');
                        }
                    );
                };

                $scope.loadPoints = function () {

                    $scope.points = [];

                    $scope.deferred = $http.get('/route/listPoint/' + $scope.routeDetailData.id);

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;

                            if (!data.success) Common.pushNoty(res.message, 'error');

                            $scope.points = data.points;
                        },
                        function () {
                            $scope.points = [];
                        }
                    );

                    return $scope.deferred;
                };

                $scope.addPoint = function(route_id, ev){
                    $scope.deferred = $http.get('/child');
                    $scope.deferred.then(
                        function(res) {
                            var data = res.data;
                            if(data.success) {
                                addPointForm(data.childs, route_id, ev);
                            } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                        },
                        function() {
                            Common.pushNoty($translate.instant('err_anonymous'), 'error');
                        }
                    );

                };

                $scope.pointDetail = function (id, ev) {

                    if (id != '') {

                        $scope.deferred = $http.get('/route/point/' + id);

                        $scope.deferred.then(
                            function(res) {
                                var data = res.data;
                                if(data.success) {
                                    openPoint(data.point, ev);
                                } else Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            },
                            function() {
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    }
                };

                var addPointForm = function (data, route_id, ev) {
                    var pointDetail = {};
                    pointDetail.route_id = route_id;
                    $mdDialog.show({
                        controller         : 'AddPointController',
                        templateUrl        : 'public/views/route/point-add.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true,
                        locals             : {
                            childs: data,
                            pointDetail: pointDetail
                        }
                    }).finally(function() {
                        $scope.loadPoints();
                    });
                };

                var openPoint = function (data, ev) {
                    data.futrSche = data.is_future_schedule ? true : false;
                    console.log(data);
                    $mdDialog.show({
                        controller         : 'PointDetailController',
                        templateUrl        : 'public/views/route/point-detail.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true,
                        locals             : {
                            pointDetail: data
                        }
                    }).finally(function() {
                        $scope.loadPoints();
                    });
                };

                $('.datepicker').datetimepicker({
                    format: 'YYYY-MM-DD'/*,
                    defaultDate: routeDetailData.route.plan_date*/
                }).on('dp.change', function(e) {
                    ng.element($(this)[0]).triggerHandler('input');
                });
                $('.timepicker1').datetimepicker({
                    format: 'HH:mm'
                }).on('dp.change', function(e) {
                    ng.element($(this)[0]).triggerHandler('input');
                });
                $('.timepicker2').datetimepicker({
                    format: 'HH:mm'
                }).on('dp.change', function(e) {
                    ng.element($(this)[0]).triggerHandler('input');
                });

                $translatePartialLoader.addPart('route');

                $translate.refresh();
            }
        ])
        .controller('PointDetailController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'pointDetail',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, pointDetail) {

                if(pointDetail && pointDetail.id) {
                    $scope.title = $translate.instant('edit-point') +  pointDetail.location_ad;
                    $scope.pointDetail = pointDetail
                }

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http
                            .post('/route/point', this.pointDetail)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {

                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('bus-manage-update');
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }
            }
        ])
        .controller('AddPointController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope', 'childs', 'pointDetail',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope, childs, pointDetail ) {

                $scope.title = $translate.instant('add-point');
                $scope.childs = childs;
                $scope.pointDetail = pointDetail;

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.submit = function() {

                    if(this.form.$valid) {

                        $scope.isLoading = true;

                        $http
                            .post('/route/addPoint', this.pointDetail)
                            .then(
                            function(res) {

                                var data = res.data;

                                Common.pushNoty(
                                    data.message,
                                    (data.success) ? 'success' : 'error'
                                );

                                if(data.success) {

                                    $mdDialog.cancel();
                                    $rootScope.$broadcast('bus-manage-update');
                                }
                            },
                            function(res) {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );

                    } else new validationService().checkFormValidity(this.form);
                }
            }
        ]);

})(angular);
