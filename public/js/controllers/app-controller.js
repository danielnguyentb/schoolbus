/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/app-controller', [])
        .controller('AppCtrl', [
            '$scope', '$mdSidenav', 'AuthService', '$window', '$translate', '$state', '$rootScope', '$mdDialog', 'Socket', '$http','GPSConverter',
            function ($scope, $mdSidenav, AuthService, $window, $translate, $state, $rootScope, $mdDialog, Socket, $http,GPSConverter) {
                $scope.Auth      = AuthService;
                $scope.appLoaded = true;

                $scope.parseScope = function(name) {
                    return $scope[name];
                };

                var makeMenu = function() {

                    $scope.menus = [];
                    $scope.managers = [];
                    $scope.pending_total = 0;
                    $scope.map = AuthService.isAllowed('master-map', 'get') ? true: false;

                    var resources = {
                        //'notifications': {
                        //    res : 'notifications',
                        //    perm: 'get',
                        //    ico : 'notifications'
                        //},
                        'pending-approval': {
                            res : 'pending-approval',
                            perm: 'get',
                            ico : 'access_time',
                            sub: $scope.pending_total
                        }
                    };


                    angular.forEach(resources, function(v, i) {
                        if(AuthService.isAllowed(v.res, v.perm)) {

                            $scope.menus.push({
                                icon : v.ico,
                                name : ['menus', i].join('.'),
                                state: (i == 'master-map' ? 'main' : i),
                                sub: v.sub
                            });
                        }
                    });

                    var managers = {
                        'staff-attendance': {
                            res: 'bus-manage',
                            perm: 'get',
                            ico : 'person'
                        },
                        'driver' : {
                            res : 'driver-manage',
                            perm: 'get',
                            ico : 'person',
                            badge: true,
                            scope: 'drivers_pending'
                        },
                        'assistant' : {
                            res : 'assistant-manage',
                            perm : 'get',
                            ico : 'assistant',
                            badge: true,
                            scope: 'assistants_pending'
                        },
                        'bus' : {
                            res : 'bus-manage',
                            perm : 'get',
                            ico : 'directions_bus'
                        },
                        'company' : {
                            res : 'company-manage',
                            perm : 'get',
                            ico : 'group'
                        },
                        'log' : {
                            res : 'log-manage',
                            perm : 'get',
                            ico : 'stars'
                        },
                        'location' : {
                            res : 'location-manage',
                            perm : 'get',
                            ico : 'stars'
                        },
                        'notification' : {
                            res : 'notification-manage',
                            perm : 'get',
                            ico : 'stars'
                        },
                        'card' : {
                            res : 'card-manage',
                            perm : 'get',
                            ico : 'local_offer'
                        },
                        'child' : {
                            res : 'child-manage',
                            perm: 'get',
                            ico : 'person'
                        },
                        'route' : {
                            res : 'route-manage',
                            perm: 'get',
                            ico : 'directions'
                        },
                        'calendar' : {
                            res : 'calendar-manage',
                            perm: 'get',
                            ico : 'today'
                        },
                        'user' : {
                            res : 'users-manage',
                            perm: 'get',
                            ico : 'people'
                        },
                        'group' : {
                            res : 'group-manage',
                            perm: 'get',
                            ico : 'group'
                        },
                        'config' : {
                            res : 'config-manage',
                            perm : 'get',
                            ico : 'settings'
                        }
                    };
                    angular.forEach(managers, function(v, i) {
                        if(AuthService.isAllowed(v.res, v.perm) && AuthService.inMode(i)) {
                            $scope.managers.push({
                                icon : v.ico,
                                name : ['menus', i].join('.'),
                                state: i,
                                badge: v.badge || false,
                                scope: v.scope || ''
                            });
                        }
                    });

                    $scope.pending_total = 0;
                    $scope.drivers_pending = 0;
                    $scope.assistants_pending = 0;

                    if(AuthService.getRole() == 'admin') $scope.penddingCount();

                    $scope.workersPenddingCount();
                };

                $scope.penddingCount = function(){
                    $scope.deferred = $http.get('/users/pending-approval');
                    $scope.deferred.then(
                        function(res) {
                            var data = res.data;
                            $scope.pending_total = data.total;
                        },
                        function() {
                            $scope.pending_total = 0;
                        }
                    );
                    return $scope.deferred;
                };

                $scope.workersPenddingCount = function() {
                    $http.get('/company/staff-count').then(
                        function(res) {
                            var data = res.data;

                            $scope.drivers_pending = data.drivers;
                            $scope.assistants_pending = data.assistants;
                        }
                    );
                };

                $rootScope.$on("$stateChangeSuccess", function() {
                    $scope.currentState = $state.current.name;
                });
                $rootScope.$on('AuthServiceUpdate', makeMenu);

                $scope.goToState = function(ev) {
                    $state.go(ev);
                };

                $scope.openStateInNewTab = function(target, $event) {
                    $event.stopPropagation();

                    var url = $state.href(target);

                    if(!url) return false;

                    var popup = $window.open(url, 'master_map', ',type=fullWindow,fullscreen,scrollbars=yes');
                    if ($window.focus) popup.focus();
                };

                $scope.toggleSidenav = function (menuId) {

                    $mdSidenav(menuId).toggle();
                };

                $scope.logout = function () {

                    if (!AuthService.isLogged) return false;

                    $window.location = '/logout';
                };

                makeMenu();

                if(Socket.get()) {
                    Socket.get().on('bus:register:card', function(card) {
                        $mdDialog.show({
                            controller         : 'BusCardController',
                            templateUrl        : 'public/views/bus/bus_card.html',
                            parent             : angular.element(document.body),
                            clickOutsideToClose: true,
                            resolve : {
                                'Buses' : ['$http', function($http) {
                                    return $http.get('/get/buses');
                                }],
                                'Card': function() {
                                    return card;
                                }
                            }
                        });
                    });
                }

                $translate.refresh();

            }])
        .controller('UploadController', [
            '$scope', '$mdSidenav', 'AuthService', '$window', '$translate', '$state', '$rootScope',
            function ($scope, $mdSidenav, AuthService, $window, $translate, $state, $rootScope) {

            }
        ]);

})();