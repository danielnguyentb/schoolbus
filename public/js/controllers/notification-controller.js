/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/notification-controller', [])

        .controller('NotificationController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$httpParamSerializer', '$rootScope',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $httpParamSerializer, $rootScope) {

                $scope.selected = [];
                $scope.total    = 0;

                $scope.query = {
                    order: 'name',
                    limit: 10,
                    page : 1
                };



                $scope.loadNotification = function () {
                    $scope.notifications = [];

                    $scope.deferred = $http.get('/notification?' + $httpParamSerializer($scope.query));

                    $scope.deferred.then(
                        function (res) {

                            var data = res.data;
                            var lstNoty = [];
                            if (!data.success) Common.pushNoty(res.message, 'error');
                            if(data.notifications && data.notifications.length ){
                                var messJson = {};
                                data.notifications.forEach(function(noty){
                                    try{
                                        messJson = JSON.parse(noty.message);
                                        noty.message = Common.f($translate.instant('absent_from'), messJson);
                                        console.log(noty.message);
                                    }catch(e){};

                                    lstNoty.push(noty);
                                });

                                
                            }

                            $scope.notifications = lstNoty;
                            $scope.total  = data.total;
                        },
                        function () {
                            $scope.notifications = [];
                            $scope.total  = 0;
                        }
                    );

                    return $scope.deferred;
                };

                $scope.loadNotification();

                $rootScope.$on('notification-manage-update', $scope.loadLocation);

                $translatePartialLoader.addPart('notification');
                $translate.refresh();
            }
        ]);
})();
