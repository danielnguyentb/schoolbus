/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/main-controller', ['ngCookies'])

        .factory('MainService', ['$window', '$translate', 'Common', function ($window, $translate, Common) {
            return {
                frequency: 60000,//1 min;
                timeout  : 5 * 60000, //5 min;
                waitingTime  : 2 * 60000, //2 min;
                minDistance: 30, //30 meter

                bmapOpts: function () {
                    return angular.extend({}, ($window._APP_CONFIG.bmap_opts || {}), {
                        pastRoutes    : {
                            settings: {}
                        },
                        currentRoutes : {
                            settings: {}
                        },
                        upcomingRoutes: {
                            settings: {}
                        }
                    });
                },

                toRad: function(number) {
                    return number * Math.PI / 180;
                },

                calcDistance: function(p1, p2) {
                    if(!p1 || !p2) return -1;

                    var R = 6371000; // metres
                    var φ1 = this.toRad(p1.lat);
                    var φ2 = this.toRad(p2.lat);
                    var Δφ = this.toRad(p2.lat - p1.lat);
                    var Δλ = this.toRad(p2.long - p1.long);

                    var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
                        Math.cos(φ1) * Math.cos(φ2) *
                        Math.sin(Δλ  /2) * Math.sin(Δλ / 2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

                    return R * c;
                },

                compareMarkers: function (currentMarkers, data) {
                    var changed = false, now = new Date(), lastChanged = now;

                    var markers = angular.extend({}, (currentMarkers || {}));
                    var route = data.route || {}, driverInfo = route.driverInfo || {};

                    if (!route.id) route.id = driverInfo.id;
                    else {
                        var uids = [route.driver_id];
                        if(route.assistant_id && route.assistant_id != '') uids.push(route.assistant_id);

                        uids.forEach(function (v) {
                            if (markers[v]) {
                                markers[v].deleted = true;
                                changed = true;
                            }
                        });
                    }

                    var oif = {};
                    if (markers[route.id]) {
                        oif = markers[route.id];

                        var distance = this.calcDistance({lat: oif.lat, long: oif.long}, data.pos);

                        console.log(distance);

                        if(distance > this.minDistance) changed = true;
                        else lastChanged = oif.lastChanged;
                    } else changed = true;

                    markers[route.id] = {
                        id: route.id,
                        long: data.pos.long,
                        lat: data.pos.lat,
                        icon: $window._APP_CONFIG.bmap_marker,
                        width: 30,
                        height: 30,
                        title: route.busInfo ? route.busInfo.name : driverInfo.name,
                        label: route.busInfo ? route.busInfo.name : driverInfo.name,
                        labelSuffix: !changed && oif.labelSuffix ? oif.labelSuffix : '',
                        uif: {
                            name: driverInfo.name,
                            avatar: driverInfo.avatar
                        },
                        assistant: route.assistant || 'N/A',
                        changed: changed,
                        time: now,
                        lastChanged: lastChanged
                    };

                    return {changed: changed, markers: markers};
                },

                cleanMarkersQueue: function (currentMarkers) {
                    var markers = angular.extend({}, (currentMarkers || {})), changed = false;//If after 5 min marker dont have new action, Marker will remove from system;

                    var i, now = new Date();

                    for (i in markers) {
                        if (!markers.hasOwnProperty(i)) continue;

                        if (markers[i].time && (now.getTime() - markers[i].time.getTime()) >= this.timeout) {
                            markers[i].deleted = true;

                            changed = true;
                        }

                        if(markers[i].lastChanged && (now.getTime() - markers[i].lastChanged.getTime()) >= this.waitingTime) {
                            markers[i].labelSuffix = $translate.instant('bus_waiting_title');
                            markers[i].changed = true;

                            changed = true;
                        }
                    }

                    return {changed: changed, markers: markers};
                },

                buildRoutesLineData: function (routes) {
                    if (!routes) return null;

                    var data = {pastRoutes: [], currentRoutes: [], upcomingRoutes: []}, i, route, key;

                    for (i in routes) {
                        if (!routes.hasOwnProperty(i) || !routes[i].points) continue;

                        route = routes[i];
                        key = route.daily_status == 3 ? 'pastRoutes' : (route.daily_status == 4 ? 'upcomingRoutes' : 'currentRoutes');

                        data[key].push({
                            name : route.name,
                            points: route.points
                        });
                    }

                    return data;
                }
            };
        }])

        .controller('MainController', [
            '$window', '$scope', '$translate', '$translatePartialLoader', '$mdDialog', '$state', '$http', '$httpParamSerializer', 'Common', 'Socket', 'MainService', '$timeout', '$cookies', 'AuthService', 'DemoData',
            function ($window, $scope, $translate, $translatePartialLoader, $mdDialog, $state, $http, $httpParamSerializer, Common, Socket, MainService, $timeout, $cookies, AuthService, DemoData) {

                $scope.isAdmin = [11, 1].indexOf(AuthService.getUserId()) != -1;
                $scope.demoStarted = false;

                $scope.bmapOptions = MainService.bmapOpts();
                $scope.bmapOptions.convertMode = 'local';

                var center = $cookies.get('bmap_center'),
                    zoom = $cookies.get('bmap_zoom');

                if(center) {
                    center = center.split(';');

                    $scope.bmapOptions.center = {lat: center[0], long: center[1]};
                }

                if(zoom) $scope.bmapOptions.zoom = zoom;

                $scope.startDemo = function() {
                    $scope.demoStarted = true;
                    //Sample data;
                    var starts = DemoData.getStart();
                    var sm = 600;

                    $scope.bmapOptions.currentRoutes.routes = [
                        {name: 'Area A1 AM', isDemo: true, points: [{"point":[starts._1.long,starts._1.lat],"title":"10:00 AM"}]},
                        {name: 'Area A2 PM', isDemo: true, points: [{"point":[starts._2.long,starts._2.lat],"title":"10:00 AM"}]},
                        {name: 'Area A3 PM', isDemo: false, points: [{"point":[starts._3.long,starts._3.lat],"title":"10:00 AM"}]},
                        {name: 'Area A4 PM', isDemo: false, points: [{"point":[starts._4.long,starts._4.lat],"title":"10:00 AM"}]},
                        {name: 'Area A5 PM', isDemo: false, points: [{"point":[starts._5.long,starts._5.lat],"title":"10:00 AM"}]}
                    ];
                    $scope.bmapOptions.markers = {
                        _1: {
                            long: starts._1.long,
                            lat: starts._1.lat,
                            icon: '/public/images/bmap-marker.png',
                            width: 30,
                            height: 30,
                            title: "KX4204",
                            label: "KX4204",
                            uif: {
                                name: "Ben Wong",
                                avatar: '/public/images/default-avatar.gif'
                            },
                            assistant: "Bert",
                            isDemo: true
                        },
                        _2: {
                            long: starts._2.long,
                            lat: starts._2.lat,
                            icon: '/public/images/bmap-marker.png',
                            width: 30,
                            height: 30,
                            title: "KH4152",
                            label: "KH4152",
                            uif: {
                                name: "Corrine",
                                avatar: '/public/images/default-avatar.gif'
                            },
                            assistant: "Sandy",
                            isDemo: true
                        },
                        _3: {
                            long: starts._3.long,
                            lat: starts._3.lat,
                            icon: '/public/images/bmap-marker.png',
                            width: 30,
                            height: 30,
                            title: "KX4502",
                            label: "KX4502",
                            uif: {
                                name: "Anthony",
                                avatar: '/public/images/default-avatar.gif'
                            },
                            assistant: "Carol",
                            isDemo: false
                        },
                        _4: {
                            long: starts._4.long,
                            lat: starts._4.lat,
                            icon: '/public/images/bmap-marker.png',
                            width: 30,
                            height: 30,
                            title: "KX5832",
                            label: "KX5832",
                            uif: {
                                name: "Derrick",
                                avatar: '/public/images/default-avatar.gif'
                            },
                            assistant: "Helen",
                            isDemo: false
                        },
                        _5: {
                            long: starts._5.long,
                            lat: starts._5.lat,
                            icon: '/public/images/bmap-marker.png',
                            width: 30,
                            height: 30,
                            title: "KX4302",
                            label: "KX4302",
                            uif: {
                                name: "John",
                                avatar: '/public/images/default-avatar.gif'
                            },
                            assistant: "Esther",
                            isDemo: false
                        }
                    };

                    setInterval(function() {
                        sm += 5;

                        ['_1', '_2', '_3', '_4', '_5'].forEach(function(v, i) {
                            var p = DemoData.getNext(v);

                            if(p) {
                                var h = Math.floor(sm / 60), m = sm % 60, a = h >= 12 ? 'PM' : 'AM';
                                if(h > 12) h -= 12;

                                $scope.bmapOptions.markers[v].lat = p.lat;
                                $scope.bmapOptions.markers[v].long = p.long;
                                $scope.bmapOptions.markers[v].changed = true;

                                $scope.bmapOptions.currentRoutes.routes[i].points.push({
                                    "point": [p.long, p.lat],
                                    "title": [(h < 10 ? '0' + h : h), ':', (m < 10 ? '0' + m : m), ' ', a].join('')
                                });

                                $timeout(function () {
                                    $scope.$apply();
                                });
                            } else {
                                $scope.demoStarted = false;
                            }
                        });
                    }, 5000);
                };

                //End sample;
                if (Socket.get()) {
                    Socket.get().on('driver:location:pull', function (data) {
                        var markersData = MainService.compareMarkers($scope.bmapOptions.markers, data);

                        $scope.bmapOptions.markers = markersData.markers;

                        if (markersData.changed) {
                            $timeout(function () {
                                $scope.$apply();
                            });
                        }
                    });
                }

                setInterval(function () {
                    console.log('Queue running...');
                    var markersData = MainService.cleanMarkersQueue($scope.bmapOptions.markers);

                    $scope.bmapOptions.markers = markersData.markers;

                    console.log(markersData.markers);
                    if (markersData.changed) {
                        $timeout(function () {
                            $scope.$apply();
                        });
                    }
                }, MainService.frequency);

                $scope.bcMapFilter = function () {
                    if (!this.past_hour || !this.next_hour) return false;

                    $scope.isLoading = true;
                    $http.get('/company/current-route?' + $httpParamSerializer({
                            past_hour: this.past_hour,
                            next_hour: this.next_hour
                        })).then(
                        function (res) {
                            $scope.isLoading = false;

                            var data = res.data;
                            if (data.success) {
                                var routesData = MainService.buildRoutesLineData(data.routes);

                                if (!routesData) {
                                    $scope.bmapOptions.pastRoutes.routes = [];
                                    $scope.bmapOptions.currentRoutes.routes = [];
                                    $scope.bmapOptions.upcomingRoutes.routes = [];
                                } else {
                                    $scope.bmapOptions.pastRoutes.routes = routesData.pastRoutes || [];
                                    $scope.bmapOptions.currentRoutes.routes = routesData.currentRoutes || [];
                                    $scope.bmapOptions.upcomingRoutes.routes = routesData.upcomingRoutes || [];
                                }

                                $timeout(function () {
                                    $scope.$apply();
                                });

                            } else Common.pushNoty(data.message, 'error');
                        },
                        function () {
                            $scope.isLoading = false;
                            Common.pushNoty($translate.instant('err_anonymous'), 'error');
                        }
                    );
                };
                $translate.refresh();
            }
        ])

        .controller('MasterMapController', [
            '$window', '$scope', '$translate', '$translatePartialLoader', '$mdDialog', '$timeout', '$cookies', 'Common', 'Socket', 'MainService',
            function ($window, $scope, $translate, $translatePartialLoader, $mdDialog, $timeout, $cookies, Common, Socket, MainService) {

                $scope.$parent.hiddenEdge = true;
                $scope.bmapOptions = MainService.bmapOpts();

                var center = $cookies.get('bmap_center'),
                    zoom = $cookies.get('bmap_zoom');

                if(center) {
                    center = center.split(';');

                    $scope.bmapOptions.center = {lat: center[0], long: center[1]};
                }

                if(zoom) $scope.bmapOptions.zoom = zoom;

                if (Socket.get()) {
                    Socket.get().on('driver:location:pull', function (data) {
                        var markersData = MainService.compareMarkers($scope.bmapOptions.markers, data);

                        $scope.bmapOptions.markers = markersData.markers;

                        if (markersData.changed) {
                            $timeout(function () {
                                $scope.$apply();
                            });
                        }
                    });
                }

                setInterval(function () {
                    var markersData = MainService.cleanMarkersQueue($scope.bmapOptions.markers);

                    $scope.bmapOptions.markers = markersData.markers;

                    if (markersData.changed) {
                        $timeout(function () {
                            $scope.$apply();
                        });
                    }
                }, MainService.frequency);

                $translate.refresh();
            }
        ])

        .controller('ProfileController', [
            '$window', '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'AuthService', '$cookies', 'UserProfile', '$state',
            function ($window, $scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, AuthService, $cookies, UserProfile, $state) {

                $scope._csrf = $cookies.get('xsrf-token');
                $scope.secInfo = {};
                $scope.accInfo = {};
                $scope.role = AuthService.getRole();

                $scope.cancel = function () {
                    $state.go('main');
                };

                $scope.submit = function (form, model, type) {

                    if (form.$valid) {

                        $scope['isLoading' + type] = true;

                        var url = '';
                        switch (type) {
                            case 'Company':
                                url = '/profile/company';
                                break;
                            case 'Security':
                                url = '/profile/password';
                                break;
                            default :
                                url = '/profile';
                        }
                        $http
                            .post(url, $scope[model])
                            .then(
                                function (res) {

                                    var data = res.data;
                                    $scope['isLoading' + type] = false;

                                    if (data.success) {
                                        new validationService().resetForm(form);

                                    }

                                    Common.pushNoty(
                                        data.message,
                                        (data.success) ? 'success' : 'error'
                                    );

                                    if (type == 'Basic') {
                                        AuthService._uData.avatar = $scope.accInfo.image;
                                    } else if (type == 'Security') {
                                        $scope.secInfo = {};
                                    }
                                    if (data.success) {

                                        if (url == '/profile') {
                                            $scope[model].locale = $scope[model].locale ? $scope[model].locale : 'en-US';
                                            if ($translate.use() != $scope[model].locale) {
                                                setTimeout(function(){
                                                    $window.location.href ='/?locale='+$scope[model].locale;
                                                },1000);
                                            }

                                        }
                                    }
                                },
                                function (res) {

                                    $scope['isLoading' + type] = false;
                                    Common.pushNoty($translate.instant('err_anonymous'), 'error');
                                }
                            );

                    } else {
                        new validationService().checkFormValidity(form);
                    }
                };

                if (UserProfile.data && !UserProfile.data.error) {

                    var data = UserProfile.data;
                    $scope.accInfo = data.user;

                    if (AuthService.getRole() == 'bus-company' && data.comp) {
                        $scope.comInfo = data.comp;
                    }
                }

                $translatePartialLoader.addPart('app/main');
                $translate.refresh();
            }
        ]);

})();
