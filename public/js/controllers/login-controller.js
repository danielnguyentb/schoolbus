/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('controllers/login-controller', [])

        .controller('LoginController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'Socket', 'validationService', '$http', 'AuthService', '$state', '$rootScope', '$cookies',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, Socket, validationService, $http, AuthService, $state, $rootScope, $cookies) {
                $scope.user = {};

                $scope.blockBrowse = function(){
                    // if login fail > 10 => block
                    var count = parseInt($cookies.get('__LI'));
                    return (count >= 10);
                };

                $scope.register = function (ev) {

                    $mdDialog.show({
                        controller         : 'RegisterController',
                        templateUrl        : 'public/views/auth/register.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true
                    });

                };

                $scope.changeLanguage = function (langKey) {
                    $translate.use(langKey);
                };

                $scope.submit = function () {

                    if (this.loginForm.$valid) {

                        this.isLoading = true;
                        $http
                            .post('/', this.user)
                            .then(
                            function(res) {

                                var data         = res.data;
                                $scope.isLoading = false;
                                Common.pushNoty(
                                    data.message,
                                    (!data.success) ? 'error' : 'success'
                                );
                                if (data.success && data.uData) {

                                    AuthService.init(data.uData);
                                    if ($translate.use() != data.uData.locale) {
                                        $translate.use(data.uData.locale);
                                    }

                                    $rootScope.$broadcast('AuthServiceUpdate', {});
                                    Socket.connect();
                                    $state.go('main');
                                }else{

                                    //Set cookie count login invalid
                                    var count = parseInt($cookies.get('__LI'));
                                    count = count ? ++count : 1;
                                    if(data.lock) count = 10;
                                    Common.setCookie('__LI', count, 1);


                                }
                            },
                            function() {

                                $scope.isLoading = false;
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else {
                        new validationService().checkFormValidity($scope.loginForm);
                    }
                };

                $scope.forgot = function (ev) {

                    $mdDialog.show({
                        controller         : 'ForgotController',
                        templateUrl        : 'public/views/auth/forgot.html',
                        parent             : angular.element(document.body),
                        targetEvent        : ev,
                        clickOutsideToClose: true
                    });
                };

                $translatePartialLoader.addPart('app/auth');
                $translate.refresh();
            }
        ])

        .controller('RegisterController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', 'vcRecaptchaService', '$http',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, recaptcha, $http) {

                $scope.user      = {};
                $scope.recaptcha = false;

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.submit = function (form, ev) {

                    //$scope.isLoading = true;
                    if (form.$valid) {

                        this.isLoading = true;
                        $http
                            .post('/register', this.user)
                            .then(
                            function (res) {

                                var data         = res.data;
                                $scope.isLoading = false;
                                recaptcha.reload($scope.recaptcha);

                                if (data.success) {
                                    $scope.user = {};
                                    new validationService().resetForm($scope.register);
                                    Common.confirm($translate.instant('register_success'))
                                        .then(function() {
                                            $mdDialog.cancel();
                                        }, function() {

                                        });
                                    $mdDialog.cancel();
                                }
                                if(!data.success)
                                    Common.pushNoty(
                                        data.message,
                                        (!data.success) ? 'error' : 'success'
                                    );
                            },
                            function (res) {
                                $scope.isLoading = false;
                                recaptcha.reload($scope.recaptcha);
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else {
                        new validationService().checkFormValidity($scope.register);
                    }
                };


                $scope.setWidgetId = function (widgetId) {
                    $scope.recaptcha = widgetId;
                };

                $scope.setResponse = function (response) {
                    $scope.user.captcha = response;
                };

            }
        ])

        .controller('ResetPassController', [
            '$state', '$stateParams', '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'vcRecaptchaService',
            function ($state, $stateParams, $scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, recaptcha) {

                $scope.user = {
                    email: $stateParams.email,
                    token: $stateParams.token
                };

                $scope.cancel = function() {
                    $state.go('auth');
                };

                $scope.submit = function() {

                    if(!this.forgot.$valid) {
                        new validationService().checkFormValidity($scope.forgot);
                        return false;
                    }

                    $scope.isLoading = true;
                    $http
                        .post('/forgot/change', this.user)
                        .then(
                        function(res) {

                            var data = res.data;
                            $scope.isLoading = false;

                            Common.pushNoty(
                                data.message,
                                (!data.success) ? 'error' : 'success'
                            );

                            if(data.success) $state.go('auth');
                        },
                        function() {

                            $scope.isLoading = false;
                            Common.pushNoty($translate.instant('err_anonymous'), 'error');
                        }
                    );
                };

                $translatePartialLoader.addPart('app/auth');
                $translate.refresh();
            }
        ])

        .controller('ForgotController', [
            '$scope', '$translate', '$translatePartialLoader', '$mdDialog', 'Common', 'validationService', '$http', 'vcRecaptchaService',
            function ($scope, $translate, $translatePartialLoader, $mdDialog, Common, validationService, $http, recaptcha) {

                $scope.user      = {};
                $scope.recaptcha = false;

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.submit = function () {

                    if (this.forgot.$valid) {

                        $scope.isLoading = true;
                        $http
                            .post('/forgot', this.user)
                            .then(
                            function (res) {

                                var data         = res.data;
                                $scope.isLoading = false;
                                recaptcha.reload($scope.recaptcha);

                                if (data.success) {
                                    $scope.user = {};
                                    new validationService().resetForm($scope.forgot);
                                }

                                Common.pushNoty(
                                    data.message,
                                    (!data.success) ? 'error' : 'success'
                                );
                            },
                            function (res) {

                                $scope.isLoading = false;
                                recaptcha.reload($scope.recaptcha);
                                Common.pushNoty($translate.instant('err_anonymous'), 'error');
                            }
                        );
                    } else {

                        new validationService().checkFormValidity($scope.forgot);
                        if($scope.forgot.forgot_email.$valid) {
                            Common.pushNoty($translate.instant('check_errors_captcha'), 'error');
                        }
                    }
                };

                $scope.setWidgetId = function (widgetId) {
                    $scope.recaptcha = widgetId;
                };

                $scope.setResponse = function (response) {
                    $scope.user.captcha = response;
                };

            }
        ]);

})();
