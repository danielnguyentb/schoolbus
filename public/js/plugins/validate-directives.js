
(function () {
    "use strict";

    var app = angular.module('plugins/validate-directives', [])


		var stringPart = function(str, num){
			var arrPart = [], len = str.length;
			var arrStr = str.split('');
			arrStr.forEach(function(c, i) {
				if ((i + num) < (len+1) ) arrPart.push(str.substr(i, num));
				
			});
			return arrPart;
		}


		app.directive('passcomp', function() {
		  return {
		    require: 'ngModel',
		    link: function(scope, elm, attrs, ctrl) {
		      ctrl.$validators.passcomp = function(modelValue, viewValue) {
		      	if (ctrl.$isEmpty(modelValue)) {
		          // consider empty models to be valid
		          return true;
		        }
        		var regx = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[!@#\$%\^&\*\)\(,.\{\}\[\]\|`-_+=*/]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[!@#\$%\^&\*\)\(,.\{\}\[\]\|`-_+=*/]))|((?=.*[0-9])(?=.*[!@#\$%\^&\*\)\(,.\{\}\[\]\|`-_+=*/])))(?=.{8,})");
				var cond1 =  regx.test(modelValue);

				var cond2 = true;
				var letter = 'abcdefghijklmnopqrstuvwxyz',
					letterRv = letter.split("").reverse().join(""),
					digit = '0123456789',
					digitRv = digit.split("").reverse().join("");
				var arrPart = stringPart(modelValue, 3);
				for(var i = 0; i < arrPart.length; i++){
					var tmp = arrPart[i].toLowerCase();
					if(letter.indexOf(tmp) != -1 || letterRv.indexOf(tmp) != -1
						|| digit.indexOf(tmp) != -1 || digitRv.indexOf(tmp) != -1){
						cond2 = false;
					}
				}
				return cond1 && cond2;
		      };
		    }
		  };
		});
        

        
})();
