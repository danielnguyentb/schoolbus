/**
 * Created by AnhNguyen
 */

(function() {

    angular.module('plugins/wcl-commons', [])

        .service('Common', [
            '$mdDialog',
            '$translate',
            '$mdToast',
            '$cookies',
            function($mdDialog, $translate, $mdToast, $cookies) {
                var instance = {

                    alert: function(content) {

                        var alert = $mdDialog.alert({
                            title  : $translate.instant('attention'),
                            content: content,
                            ok     : $translate.instant('ok')
                        });

                        $mdDialog.show(alert).finally(function() {
                            alert = undefined;
                        });
                    },

                    confirm: function(content) {

                        var confirm = $mdDialog.confirm({
                            title  : $translate.instant('attention'),
                            content: content,
                            ok     : $translate.instant('ok'),
                            cancel : $translate.instant('cancel')
                        });

                        return $mdDialog.show(confirm);
                    },

                    pushNoty: function(message, type) {

                        type = type || 'success';

                        var toast = $mdToast.simple()
                            .content(message)
                            .action('x')
                            .highlightAction(false)
                            .position('top right')
                            .theme(type + '-toast');

                        $mdToast.show(toast).then(function(res) {});
                    },

                    f: function(str, data, trans) {
                        if(!trans) trans = [];

                        var i, tokens = {};
                        for(i = 0; i < trans.length; i++) {
                            tokens["t|" + trans[i]] = $translate.instant(trans[i]);
                        }

                        angular.extend(tokens, data);

                        return str.replace(/{([^{}]*)}/g,
                            function (a, b) {

                                var r = tokens[b];
                                return typeof r === 'string' ? r : "" + r;
                            }
                        );
                    },
                    setCookie: function(name, value, hours){
                        if (hours){
                            var date = new Date();
                            date.setTime(date.getTime()+(hours*60*60*1000));
                            var expires = "; expires="+date.toGMTString();
                        }else var expires = "";
                        $cookies.put(name, value, {expiry: expires});

                    }
                        
                };

                return instance;
            }
        ]);

})();
