/**
 * Created by AnhNguyen
 */

(function() {

    angular.module('plugins/wcl-gpsconverter', [])

        .service('GPSConverter', [
            function() {
                var Converter = function () {
                    this.PI = Math.PI;
                    //
                    // Krasovsky 1940
                    //
                    // a = 6378245.0, 1/f = 298.3
                    // b = a * (1 - f)
                    // ee = (a^2 - b^2) / a^2;
                    this.a = 6378245.0;
                    this.ee = 0.00669342162296594323;
                };
                Converter.prototype = {
                    /**
                     * World Geodetic System ==> Mars Geodetic System
                     * WGS-84 ==> GCJ-02
                     * 直接纠偏转换，部分地区，如较高海拔，可能会造成较大的误差
                     * @param worldCoords include{latitude:value,longitude: value}
                     * @returns marsCoords include{latitude: value,longitude: value}
                     */
                    toMars      : function (worldCoords) {
                        var marsCoords = {
//                latitude: 0,
//                longitude: 0
                        };
                        if (this.isOnEarth(worldCoords)) {
                            marsCoords.latitude = worldCoords.latitude;
                            marsCoords.longitude = worldCoords.longitude;
                            return marsCoords;
                        }

                        var dLat = this.transformLat(worldCoords.longitude - 105.0, worldCoords.latitude - 35.0);
                        var dLon = this.transformLon(worldCoords.longitude - 105.0, worldCoords.latitude - 35.0);
                        var radLat = worldCoords.latitude / 180.0 * this.PI;
                        var magic = Math.sin(radLat);
                        magic = 1 - this.ee * magic * magic;

                        var sqrtMagic = Math.sqrt(magic);

                        dLat = (dLat * 180.0) / ((this.a * (1 - this.ee)) / (magic * sqrtMagic) * this.PI);
                        dLon = (dLon * 180.0) / (this.a / sqrtMagic * Math.cos(radLat) * this.PI);

                        marsCoords.latitude = worldCoords.latitude + dLat;
                        marsCoords.longitude = worldCoords.longitude + dLon;


                        return marsCoords;
                    },
                    /**
                     * GCJ-02 ==> WGS-84
                     * 反推，假设 GCJ-02 就是 WGS-84，计算大概差值，精度不高，
                     * 二分法查表内插可能会好点，但是太多数据，前端不好做
                     * @param marsCoords include{latitude:value,longitude: value}
                     * @returns worldCoords include{latitude: value,longitude: value}
                     */
                    toEarth     : function (marsCoords) {
                        var worldCoords = {
//                latitude: 0,
//                longitude: 0
                        }, marsTemp, dLat, dLon;

                        marsTemp = this.toMars(marsCoords);

                        dLat = marsTemp.latitude - marsCoords.latitude;
                        dLon = marsTemp.longitude - marsCoords.longitude;

                        worldCoords.latitude = marsCoords.latitude - dLat;
                        worldCoords.longitude = marsCoords.longitude - dLon;

                        return worldCoords;
                    },
                    isOnEarth   : function (worldCoords) {
                        return (worldCoords.longitude < 72.004 || worldCoords.longitude > 137.8347 || worldCoords.latitude < 0.8293 || worldCoords.latitude > 55.8271);
                    },
                    transformLat: function (x, y) {
                        var ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * Math.sqrt(Math.abs(x));
                        ret += (20.0 * Math.sin(6.0 * x * this.PI) + 20.0 * Math.sin(2.0 * x * this.PI)) * 2.0 / 3.0;
                        ret += (20.0 * Math.sin(y * this.PI) + 40.0 * Math.sin(y / 3.0 * this.PI)) * 2.0 / 3.0;
                        ret += (160.0 * Math.sin(y / 12.0 * this.PI) + 320 * Math.sin(y * this.PI / 30.0)) * 2.0 / 3.0;
                        return ret;
                    },
                    transformLon: function (x, y) {
                        var ret = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * Math.sqrt(Math.abs(x));
                        ret += (20.0 * Math.sin(6.0 * x * this.PI) + 20.0 * Math.sin(2.0 * x * this.PI)) * 2.0 / 3.0;
                        ret += (20.0 * Math.sin(x * this.PI) + 40.0 * Math.sin(x / 3.0 * this.PI)) * 2.0 / 3.0;
                        ret += (150.0 * Math.sin(x / 12.0 * this.PI) + 300.0 * Math.sin(x / 30.0 * this.PI)) * 2.0 / 3.0;
                        return ret;
                    },
                    /**
                     * GCJ-02 ==> BD-09
                     * 木有验证过 三角函数转换后计算误差本来就有点蛋疼
                     * @param marsCoords include{latitude:value,longitude: value}
                     * @returns baiduCoords include{latitude: value,longitude: value}
                     */
                    marsToBaidu : function (marsCoords) {
                        var baiduCoords = {
//                latitude: 0,
//                longitude: 0
                        };

                        var x = marsCoords.longitude,
                            y = marsCoords.latitude,
                            z, theta, x_pi;

                        x_pi = this.PI * 3000.0 / 180.0;
                        z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi);
                        theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi);

                        baiduCoords.latitude = z * Math.sin(theta) + 0.006;
                        baiduCoords.longitude = z * Math.cos(theta) + 0.0065;

                        return baiduCoords;

                    },
                    /**
                     * BD-09 ==> GCJ-02
                     * 木有验证过 三角函数转换后计算误差本来就有点蛋疼
                     * @param baiduCoords include{latitude:value,longitude: value}
                     * @returns marsCoords include{latitude: value,longitude: value}
                     */
                    baiduToMars : function (baiduCoords) {
                        var marsCoords = {
//                latitude: 0,
//                longitude: 0
                        };
                        var x = baiduCoords.longitude - 0.0065, y = baiduCoords.latitude - 0.006,
                            z, theta, x_pi;

                        x_pi = this.PI * 3000.0 / 180.0;
                        z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
                        theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);

                        marsCoords.latitude = z * Math.sin(theta);
                        marsCoords.longitude = z * Math.cos(theta);

                        return marsCoords;
                    },
                    earthToBaidu: function (earthCoords) {
                        return this.marsToBaidu(this.toMars(earthCoords));
                    },
                    baiduToEarth: function (baiduCoords){
                        return this.toEarth(this.baiduToMars(baiduCoords));
                    }


                };
                return new Converter();
            }
        ]);

})();
