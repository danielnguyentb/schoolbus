/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('plugins/wcl-directives', ['ngCookies'])

        .directive("passwordMatch", function () {

            return {
                require: 'ngModel',
                scope  : {
                    passwordMatch: '='
                },
                link   : function (scope, element, attrs, ctrl) {
                    scope.$watch(function () {
                        var combined;

                        if (scope.passwordMatch || ctrl.$viewValue) {
                            combined = scope.passwordMatch + '_' + ctrl.$viewValue;
                        }

                        return combined;
                    }, function (value) {
                        if (value) {
                            ctrl.$parsers.unshift(function (viewValue) {
                                var origin = scope.passwordMatch;

                                if (origin !== viewValue) {
                                    ctrl.$setValidity("passwordMatch", false);
                                    return undefined;
                                } else {
                                    ctrl.$setValidity("passwordMatch", true);
                                    return viewValue;
                                }
                            });
                        }
                    })
                }
            }
        })
        .directive("fineUploader", ['Common', function(Common) {

            return {
                restrict  : "A",
                scope: {
                    model: '=model',
                    csrf : '=csrf'
                },
                link      : function (scope, element, attrs, ctrl) {

                    var obj = element[0];
                    var uploader = new qq.FineUploader({
                        element    : obj,
                        multiple   : false,
                        text       : {
                            uploadButton: "SELECT IMAGE"
                        },
                        dragAndDrop: {
                            disableDefaultDropzone: true
                        },
                        template: '<div class="qq-uploader">' +
                                        '<div class="qq-upload-button md-raised md-primary md-button md-ink-ripple"><div>{uploadButtonText}</div></div>' +
                                        '<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>' +
                                        '<ul class="qq-upload-list"></ul>' +
                                "</div>",
                        validation : {
                            allowedExtensions: ["jpg", "jpeg", "png", "gif"],
                            sizeLimit        : 5000 * 1024
                        },
                        request    : {
                            endpoint    : attrs.endpoint,
                            paramsInBody: true,
                            params      : {
                                _csrf : scope.csrf
                            }
                        },
                        callbacks  : {
                            onSubmit  : function () {
                                var qqProgress = $(obj).find(".qq-progress-bar");
                                qqProgress.hide();
                            },
                            onProgress: function (qqid, fileName) {
                                var qqProgress = $(obj).find(".qq-progress-bar");
                                qqProgress.hide();
                            },
                            onUpload  : function (qqid, fileName) {

                            },
                            onComplete: function(qqid, fileName, objData) {

                                if(objData.success) {

                                    scope.model = objData.link;
                                    $(obj).find(".qq-upload-list").hide();
                                    Common.pushNoty(objData.message, 'success');
                                }
                            }
                        }
                    });
                }
            }
        }])

        .directive("bmap", ['$window', '$timeout', '$cookies', 'Common', 'GPSConverter', function($window, $timeout, $cookies, Common, GPSConverter) {
            return {
                restrict: 'E',
                scope: {
                    'options': '='
                },
                link: function($scope, element, attrs) {
                    var fixHeight = function(element, attrs) {
                        //Fix height for BMAP;
                        if (attrs.parentTarget && attrs.parentTarget != '') {
                            var $parent = element.parent();

                            if(!$parent.length || !$parent[0] || !$parent[0].tagName) return false;

                            while ($parent[0].tagName != attrs.parentTarget || $parent[0].tagName == 'BODY') {
                                $parent = $parent.parent();
                            }

                            if ($parent[0].tagName != attrs.parentTarget && $parent[0].tagName == 'BODY') $parent = null;

                            if ($parent) {
                                var height = $parent.prop('offsetHeight') - 16; //Minus padding;

                                if (attrs.edgeHeight && attrs.edgeHeight != 0) height -= parseInt(attrs.edgeHeight, 10);

                                element.css({
                                    height: height + 'px',
                                    width: '100%'
                                }).parent().css('height', height + 'px');
                            }
                        }
                    };

                    fixHeight(element, attrs);
                    $timeout(function() {
                        fixHeight(element, attrs);
                    }, 500);

                    var timer;
                    angular.element($window).on('resize', function() {
                        if(timer) $timeout.cancel(timer);

                        timer = $timeout(function() {
                            fixHeight(element, attrs);
                            $scope.$apply();
                        }, 500);
                    });

                    var ui = {
                        infoWindow: '<div class="ifw-wrap" style="width: 320px;">' +
                                    '<div class="ifw-title"><p>{title}</p></div>' +
                                    '<div class="ifw-content" style="font-size: 12px;">' +
                                        '<div class="ifw-ava" style="float: left;width: 60px;margin-right: 5px;"><img src="{avatar}" style="width: 100%;"/></div>' +
                                        '<p style="overflow: hidden;">' +
                                            '<label>{t|name}:&nbsp;</label>{name}<br/>' +
                                            '<label>{t|assistant}:&nbsp;</label>{assistant}<br/>' +
                                            '<label>{t|location}:&nbsp;</label>{address}<br/>' +
                                            '<label>Lat - Long:&nbsp;</label>{lat} - {long}<br/>' +
                                        '</p>' +
                                    '</div>' +
                                    '</div>'
                    };

                    var defaultOpts = {
                        navCtrl: true,
                        scaleCtrl: true,
                        overviewCtrl: true,
                        enableScrollWheelZoom: true,
                        gpsSource: 1
                    };

                    var opts = angular.extend({}, defaultOpts, $scope.options);

                    // create map instance
                    var map = new BMap.Map(element.find('div')[0]);

                     //init map, set central location and zoom level
                    map.centerAndZoom(new BMap.Point(opts.center.long, opts.center.lat), opts.zoom);

                    if (opts.navCtrl) {
                        // add navigation control
                        map.addControl(new BMap.NavigationControl());
                    }

                    if (opts.scaleCtrl) {
                        // add scale control
                        map.addControl(new BMap.ScaleControl());
                    }

                    if (opts.overviewCtrl) {
                        //add overview map control
                        map.addControl(new BMap.OverviewMapControl());
                    }

                    if (opts.enableScrollWheelZoom) {
                        //enable scroll wheel zoom
                        map.enableScrollWheelZoom();
                    }
                    // set the city name
                    map.setCurrentCity(opts.city);

                    var currentRouteLines = [];
                    //Driving Route Service;
                    var getDrivingRoute = function(map, color) {
                        var drv = new BMap.DrivingRoute(map, {
                            onSearchComplete: function(res) {
                                if (drv.getStatus() == BMAP_STATUS_SUCCESS) {
                                    var plan = res.getPlan(0), points = [], i, route;
                                    for(i = 0; i < plan.getNumRoutes(); i++){
                                        route = plan.getRoute(i);
                                        points = points.concat(route.getPath());
                                    }

                                    var line = new BMap.Polyline(points, {strokeColor: color});
                                    currentRouteLines.push(line);
                                    map.addOverlay(line);
                                    //map.setViewport(points);
                                }
                            }
                        });

                        return drv;
                    };

                    var pointsConvertor = function(type, points, cb) {
                        if(opts.convertMode && opts.convertMode == 'local') {
                            var func = type == 3 ? 'marsToBaidu' : 'earthToBaidu';

                            var convertedPoints = [];
                            points.forEach(function(v) {
                                var point = GPSConverter[func]({longitude: v.lng, latitude: v.lat});

                                convertedPoints.push({lat: point.latitude, lng: point.longitude});
                            });

                            cb({status: 0, points: convertedPoints});
                        } else {
                            var convertor = new BMap.Convertor();
                            convertor.translate(points, type, 5, cb);
                        }
                    };

                    var routeETAs = {};
                    var markPlace = function(key, place, placeType, title, type, style) {//Type = 1 current route, 2 upcoming route, 3 past route
                        if(routeETAs[key]) return;

                        style = style || {};

                        pointsConvertor(placeType, [place], function(res) {
                            if(res.status == 0 && res.points.length) {
                                var point = res.points[0];
                                var types = {1: 'blue', 2: 'orange', 3: 'red'},
                                    colors = {1: '#6CAECA', 2: '#FF9625', 3: '#EE5D5B'};

                                var _marker = new BMap.Marker(new BMap.Point(point.lng, point.lat), {
                                    icon: new BMap.Icon('/public/images/dot-' + types[type] + '.png', new BMap.Size(10, 10)),
                                    title: title
                                });

                                //var suffix = '';//'<br>' + [point.lng, point.lat].join(',');
                                //var label = new BMap.Label(title + suffix, {"offset": new BMap.Size(11, -3)});
                                //label.setStyle(angular.extend({borderColor: colors[type], fontSize: '10px'}, style));
                                //
                                //_marker.setLabel(label);

                                //if(routeETAs[key]) map.removeOverlay(routeETAs[key]);

                                routeETAs[key] = _marker;

                                map.addOverlay(_marker);
                            }
                        });
                    };

                    var drawRoutesLine = function() {
                        //map.clearOverlays();
                        //currentRouteLines.forEach(function(v) {
                        //    map.removeOverlay(v);
                        //});
                        //
                        //currentRouteLines = [];

                        var start, end, length;
                        if (opts.pastRoutes && opts.pastRoutes.routes) {
                            var ps = opts.pastRoutes.settings,
                                pDrv = getDrivingRoute(map, (ps.lineColor || '#EE5D5B'));

                            opts.pastRoutes.routes.forEach(function (v, i) {
                                if(!v.points) return true;

                                length = v.points.length;
                                start = end = null;
                                var type = v.isDemo ? 3 : 1;
                                v.points.forEach(function(_v, _i) {
                                    var key = ['pastRoutes', i, _i].join(':');

                                    if(!start) {
                                        if(_v.drawed) return true;

                                        _v.drawed = true;
                                        start = new BMap.Point(_v.point[0], _v.point[1]);
                                        markPlace(key, start, type, _v.title, 3, {backgroundColor: 'green', borderColor: 'green'});

                                        return true;
                                    }

                                    end = new BMap.Point(_v.point[0], _v.point[1]);

                                    if(_i + 1 < length) markPlace(key, end, type, _v.title, 3, (_i + 1 == length ? {backgroundColor: 'red', borderColor: 'red'} : {}));

                                    if(_v.drawed) {
                                        start = end;
                                        return true;
                                    }

                                    //pointsConvertor(type, [start, end], function(res) {
                                    //    if (res.status == 0 && res.points.length > 1) {
                                    //        _v.drawed = true;
                                    //
                                    //        pDrv.search(new BMap.Point(res.points[0].lng, res.points[0].lat), new BMap.Point(res.points[1].lng, res.points[1].lat));
                                    //    }
                                    //});

                                    start = end;
                                });

                                //pDrv.search(start, end);
                            });
                        }

                        if (opts.upcomingRoutes && opts.upcomingRoutes.routes) {
                            var ups = opts.upcomingRoutes.settings,
                                upDrv = getDrivingRoute(map, (ups.lineColor || '#FF9625'));

                            opts.upcomingRoutes.routes.forEach(function (v, i) {
                                if(!v.points) return true;

                                length = v.points.length;
                                start = end = null;
                                var type = v.isDemo ? 3 : 1;
                                v.points.forEach(function(_v, _i) {
                                    var key = ['upcomingRoutes', i, _i].join(':');

                                    if(!start) {
                                        start = new BMap.Point(_v.point[0], _v.point[1]);
                                        if(_v.drawed) return true;

                                        _v.drawed = true;
                                        markPlace(key, start, type, _v.title, 2, {backgroundColor: 'green', borderColor: 'green'});

                                        return true;
                                    }

                                    end = new BMap.Point(_v.point[0], _v.point[1]);

                                    if (_i + 1 < length) markPlace(key, end, type, _v.title, 2, (_i + 1 == length ? {backgroundColor: 'red', borderColor: 'red'} : {}));

                                    if(_v.drawed) {
                                        start = end;
                                        return true;
                                    }

                                    //upDrv.search(start, end);
                                    //pointsConvertor(type, [start, end], function (res) {
                                    //    if (res.status == 0 && res.points.length > 1) {
                                    //        _v.drawed = true;
                                    //
                                    //        upDrv.search(new BMap.Point(res.points[0].lng, res.points[0].lat), new BMap.Point(res.points[1].lng, res.points[1].lat));
                                    //    }
                                    //});

                                    start = end;
                                });
                                //upDrv.search(start, end);
                            });
                        }

                        if (opts.currentRoutes && opts.currentRoutes.routes) {
                            var cs = opts.currentRoutes.settings,
                                cDrv = getDrivingRoute(map, (cs.lineColor || '#6CAECA'));

                            opts.currentRoutes.routes.forEach(function (v, i) {
                                if(!v.points) return true;

                                length = v.points.length;
                                start = end = null;
                                var type = v.isDemo ? 3 : 1;
                                v.points.forEach(function(_v, _i) {
                                    var key = ['currentRoutes', i, _i].join(':');
                                    if(!start) {
                                        start = new BMap.Point(_v.point[0], _v.point[1]);
                                        if(_v.drawed) return true;

                                        _v.drawed = true;
                                        markPlace(key, start, type, _v.title, 1, {backgroundColor: 'green', borderColor: 'green'});

                                        return true;
                                    }

                                    end = new BMap.Point(_v.point[0], _v.point[1]);

                                    if(_i + 1 < length) markPlace(key, end, type, _v.title, 1, (_i + 1 == length ? {backgroundColor: 'red', borderColor: 'red'} : {}));

                                    if(_v.drawed) {
                                        start = end;
                                        return true;
                                    }

                                    //cDrv.search(start, end);
                                    //pointsConvertor(type, [start, end], function(res) {
                                    //    if (res.status == 0 && res.points.length > 1) {
                                    //        _v.drawed = true;
                                    //
                                    //        cDrv.search(new BMap.Point(res.points[0].lng, res.points[0].lat), new BMap.Point(res.points[1].lng, res.points[1].lat));
                                    //    }
                                    //});

                                    start = end;
                                });

                                //cDrv.search(start, end);
                            });
                        }
                    };

                    //Draw markers;
                    var previousMarkers = {};

                    var getInfoWindow = function(marker) {
                        var template = Common.f(ui.infoWindow, {
                            title: marker.title,
                            name: marker.uif.name,
                            avatar: marker.uif.avatar,
                            address: marker.address || 'N/A',
                            assistant: marker.assistant,
                            long: marker.long,
                            lat: marker.lat
                        }, ['name', 'location', 'assistant']);

                        return new BMap.InfoWindow(template, {
                            enableMessage: !!marker.enableMessage
                        });
                    };

                    var markerRender = function(marker, point) {
                        var _marker;

                        if (marker.icon) {
                            var icon = new BMap.Icon(marker.icon, new BMap.Size(marker.width, marker.height));
                            _marker = new BMap.Marker(point, {
                                icon: icon,
                                title: marker.title
                            });
                        } else {
                            _marker = new BMap.Marker(point);
                        }

                        if(marker.label) {
                            var label = new BMap.Label(marker.label + (marker.labelSuffix || ''), {"offset": new BMap.Size(27, 6)});
                            label.setStyle({borderColor: 'green', fontWeight: 'bold'});

                            _marker.setLabel(label);
                        }

                        if(previousMarkers[marker.id]) {
                            previousMarkers[marker.id].removeEventListener('click');
                            map.removeOverlay(previousMarkers[marker.id]);
                        }

                        previousMarkers[marker.id] = _marker;

                        // add marker to the map
                        map.addOverlay(_marker);

                        if (!marker.title && !marker.address) return;

                        _marker.addEventListener('click', function() {
                            var self = this;
                            if(!marker.address) {
                                var geocoder = new BMap.Geocoder();
                                geocoder.getLocation(point, function (result) {
                                    if (result && result.address) marker.address = result.address;

                                    self.openInfoWindow(getInfoWindow(marker));
                                });
                            } else {
                                self.openInfoWindow(getInfoWindow(marker));
                            }
                        });

                        return _marker;
                    };

                    var mark = function() {
                        if(!opts.markers) return;

                        angular.forEach(opts.markers, function(v, i) {
                            if(!v.changed && !v.deleted && previousMarkers[v.id]) return true;

                            v.changed = false;
                            v.id = v.id || i;

                            if(previousMarkers[v.id]) {
                                previousMarkers[v.id].removeEventListener('click');
                                map.removeOverlay(previousMarkers[v.id]);
                            }

                            if(v.deleted) {
                                delete opts.markers[i];

                                return true;
                            }

                            var type = v.isDemo ? 3 : 1;
                            pointsConvertor(type, [new BMap.Point(v.long, v.lat)], function(res) {
                                if(res.status == 0 && res.points.length) {
                                    markerRender(v, new BMap.Point(res.points[0].lng, res.points[0].lat));
                                }
                            });
                        });
                    };

                    mark();
                    drawRoutesLine();

                    //var points = [];
                    //map.addEventListener("click", function(e) {
                    //    alert(e.point.lat + ',' + e.point.lng);
                    //    points.push({point: [e.point.lng, e.point.lat], title: '08:00 AM'});
                    //
                    //    console.log(JSON.stringify(points));
                    //});

                    map.addEventListener("zoomend", function(e) {
                        var center = map.getCenter();
                        $cookies.put('bmap_center', [center.lat, center.lng].join(';'));
                        $cookies.put('bmap_zoom', map.getZoom());
                    });

                    map.addEventListener("moveend", function(e) {
                        var center = map.getCenter();
                        $cookies.put('bmap_center', [center.lat, center.lng].join(';'));
                    });

                    $scope.$watch('options.center', function(nv, ov) {
                        opts = $scope.options;
                        map.centerAndZoom(new BMap.Point(opts.center.long, opts.center.lat), opts.zoom);
                        mark();

                    }, true);

                    $scope.$watch('options.markers', function(nv, ov) {
                        mark();
                    }, true);

                    $scope.$watch('options.pastRoutes.routes', function(nv, ov) {
                        drawRoutesLine();
                    }, true);

                    $scope.$watch('options.currentRoutes.routes', function(nv, ov) {
                        drawRoutesLine();
                    }, true);

                    $scope.$watch('options.upcomingRoutes.routes', function(nv, ov) {
                        drawRoutesLine();
                    }, true);
                },

                template: '<div style="width: 100%; height: 100%;"></div>'
            };
        }]);
})();
