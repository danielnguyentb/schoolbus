/**
 * Created by AnhNguyen
 */

(function() {
    "use strict";

    angular.module('services/child-services', [])

        .factory('ChildService', ['$http', function($http) {

            return {

                getChildById : function(id) {

                    return $http.get('/child/' + id);
                }
            }
        }]);

})();
