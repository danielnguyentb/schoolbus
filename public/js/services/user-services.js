/**
 * Created by AnhNguyen
 */

(function () {
    "use strict";

    angular.module('services/user-services', ['ngCookies'])

        .factory('User' , ['$http', function($http) {

            return {

                getMe : function() {

                    return $http.get('/profile');
                }
            };
        }])

        .factory('AuthService', ['$cookies', '$rootScope','$translate', function ($cookies, $rootScope,$translate) {

            return {

                isLogged : false,
                _uData   : {},
                _perms   : {},
                _resource: {},
                _role    : false,
                _uiKey   : 'wcl-UI',
                locale   : 'en-US',
                modeFuncs: {
                    1: ['driver', 'assistant', 'bus', 'child', 'group', 'location', 'notification']
                },

                init: function (uData) {

                    if (!uData.uId || !uData.perms) return false;
                    this._resource = angular.extend({}, uData.perms);
                    delete uData.perms;
                    this._uData = angular.extend({}, uData);

                    //if(!$cookies.get(this._uiKey)) {
                    this.setUI(Object.keys(this._resource)[0]);
                    //} else this.setUI($cookies.get(this._uiKey));
                    this.locale = uData.locale;

                    this.isLogged = true;
                },

                isAllowed: function (resource, permission) {

                    return this._perms[resource] && this._perms[resource].indexOf(permission) != -1;
                },

                isAdmin: function() {
                    return this._role == "admin";
                },

                getUserId: function() {
                    return this._uData.uId ? parseInt(this._uData.uId, 10) : 0;
                },

                inMode: function(resource) {
                    if(['admin'].indexOf(this.getRole()) != -1) return true;

                    var self = this, modes = self.getBcMode(), valid = false;
                    modes.forEach(function(v) {
                        if(self.modeFuncs[v] && self.modeFuncs[v].indexOf(resource) != -1) {
                            valid = true;

                            return false;
                        }
                    });

                    return valid;
                },

                setUI : function(key) {
                    this._role = key;
                    this._perms = this._resource[this._role];
                    $cookies.put(this._uiKey, this._role);
                    $rootScope.$broadcast('AuthServiceUpdate', {});

                    return this;
                },

                setLocale : function(val) {
                    $translate.use(val);
                    this.locale = val;
                },

                getRole : function() {

                    return this._role;
                },

                getBcMode: function() {
                    return this._uData.bc_mode || ["1"];
                }
            };
        }])

        .factory('AppInterceptor', ['$q', '$window', '$location', function ($q, $window, $location) {
            return {
                request: function (config) {

                    return config;
                }
            }
        }]);

})();
