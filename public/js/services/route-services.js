/**
 * Created by AnhNguyen
 */

(function() {
    "use strict";

    angular.module('services/route-services', [])

        .factory('RouteService', ['$http', function($http) {
            return {
                getRouteById : function(id) {
                    return $http.get('/route/' + id);
                }
            }
        }]);

})();
