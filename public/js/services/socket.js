(function() {
    "use strict";

    angular.module('services/socket', [])

        .service('Socket', [function () {
            var self = {

                socket: null,

                connect: function () {
                    this.socket = new io();

                    this.socket.on('connect', function () {
                        self.initEvent();
                    });

                    return this;
                },

                reconnect: function () {
                    this.socket.io.disconnect();
                    this.socket.io.connect();
                },

                get: function () {

                    return this.socket;
                },

                initEvent: function () {
                }

            };

            return self;

        }]);
})();