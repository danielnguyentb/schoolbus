/**
 * Created by AnhNguyen
 */

(function (window) {
    "use strict";

    angular.module('app-routes', [])

        .config([
            '$stateProvider', '$urlRouterProvider', '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                var version = '?v=' + window._APP_CONFIG.version;

                $stateProvider
                    .state('auth', {
                        url          : '/login',
                        templateUrl: 'public/views/auth/login.html' + version,
                        controller : 'LoginController',
                        authenticate: false,
                        requireLogout: true,
                        resolve      : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.login");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('forgot', {
                        url          : '/forgot?email&token',
                        templateUrl: 'public/views/auth/forgot.html' + version,
                        controller : 'ResetPassController',
                        authenticate: false,
                        requireLogout: true,
                        resolve      : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.reset-password");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('main', {
                        url         : '/lobby',
                        templateUrl: 'public/views/app/main.html' + version,
                        controller : 'MainController',
                        abstract   : false,
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.home");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('root', {
                        url         : '/',
                        templateUrl: 'public/views/app/main.html' + version,
                        controller : 'MainController',
                        abstract   : false,
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.home");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('main.my-account', {
                        url         : '/my-account',
                        views: {
                            "@": {
                                templateUrl: 'public/views/app/my-account.html' + version,
                                controller : 'ProfileController'
                            }
                        },
                        authenticate: true,
                        resolve     : {
                            trans      : ['$translate', function ($translate) {
                                return $translate("title.my-account");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }],
                            UserProfile: ['User', function (User) {
                                return User.getMe();
                            }]
                        }
                    })
                    .state('master-map', {
                        url         : '/master-map',
                        templateUrl: 'public/views/app/main.html' + version,
                        controller : 'MasterMapController',
                        abstract   : false,
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.master-map");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('group', {
                        url         : '/group',
                        templateUrl: 'public/views/group/index.html' + version,
                        controller : 'GroupController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.group");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('driver', {
                        url         : '/driver',
                        templateUrl : 'public/views/driver/index.html' + version,
                        controller  : 'DriverController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.driver");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('assistant', {
                        url         : '/assistant',
                        templateUrl : 'public/views/assistant/index.html' + version,
                        controller  : 'AssistantController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.assistant");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('location', {
                        url         : '/location',
                        templateUrl : 'public/views/location/index.html' + version,
                        controller  : 'LocationController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.location");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('notification', {
                        url         : '/notification',
                        templateUrl : 'public/views/notification/index.html' + version,
                        controller  : 'NotificationController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.notification");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('log', {
                        url         : '/log',
                        templateUrl : 'public/views/log/index.html' + version,
                        controller  : 'LogController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.log");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('bus', {
                        url         : '/bus',
                        templateUrl : 'public/views/bus/index.html' + version,
                        controller  : 'BusController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.bus");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('company', {
                        url         : '/company',
                        templateUrl : 'public/views/company/index.html' + version,
                        controller  : 'CompanyController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.company");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('staff-attendance', {
                        url         : '/staff-attendance',
                        templateUrl : 'public/views/company/staff-attendance.html' + version,
                        controller  : 'StaffAttendanceController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.staff-attendance");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('staff-attendance-date', {
                        url         : '/staff-attendance-date?date',
                        templateUrl : 'public/views/company/staff-attendance-date.html' + version,
                        controller  : 'StaffAttendanceDateController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.staff-attendance");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('config', {
                        url         : '/config',
                        templateUrl : 'public/views/config/index.html' + version,
                        controller  : 'ConfigController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.config");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('bc-config', {
                        url         : '/bc-config',
                        templateUrl : 'public/views/config/bc.html' + version,
                        controller  : 'BCConfigController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.config");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('card', {
                        url         : '/card',
                        templateUrl : 'public/views/card/index.html' + version,
                        controller  : 'CardController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.card");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('route', {
                        url         : '/route',
                        templateUrl : 'public/views/route/index.html' + version,
                        controller  : 'RouteController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.route");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('route.detail', {
                        url         : '/:route',
                        views: {
                            "@": {
                                templateUrl: 'public/views/route/detail.html' + version,
                                controller : 'RouteDetailController'
                            }
                        },
                        authenticate: true,
                        resolve     : {
                            routeDetail: ['$stateParams', 'RouteService', function ($stateParams, RouteService) {
                                return RouteService.getRouteById($stateParams.route);
                            }],
                            CompanyResource : ['$http', function($http) {
                                return $http.get('/companyResource');
                            }],
                            trans      : ['$translate', function ($translate) {
                                return $translate("title.route-detail");
                            }],
                            $title     : ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('calendar', {
                        url         : '/calendar',
                        templateUrl : 'public/views/calendar/index.html' + version,
                        controller  : 'CalendarController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.calendar");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('calendar.detail', {
                        url         : '/:calendarId',
                        views: {
                            "@": {
                                templateUrl: 'public/views/calendar/detail.html' + version,
                                controller: 'CalendarDetailController'
                            }
                        },
                        authenticate: true,
                        resolve     : {
                            CalendarDetail: ['$http', '$stateParams', function ($http, $stateParams) {
                                return $http.get('/calendar/' + $stateParams.calendarId);
                            }],
                            trans : ['$translate', function ($translate) {
                                return $translate("title.calendar-detail");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('pending-approval', {
                        url         : '/pending-approval',
                        templateUrl: 'public/views/pending/index.html' + version,
                        controller : 'PendingController',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.pending-approval");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('child', {
                        url         : '/child',
                        templateUrl: 'public/views/child/index.html' + version,
                        controller : 'ChildManage',
                        authenticate: true,
                        resolve     : {
                            trans : ['$translate', function ($translate) {
                                return $translate("title.child");
                            }],
                            $title: ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('child.detail', {
                        url         : '/:child',
                        views: {
                            "@": {
                                templateUrl: 'public/views/child/detail.html' + version,
                                controller : 'ChildDetailController'
                            }
                        },
                        authenticate: true,
                        resolve     : {
                            ChildDetail: ['$stateParams', 'ChildService', function ($stateParams, ChildService) {

                                return ChildService.getChildById($stateParams.child);
                            }],
                            trans      : ['$translate', function ($translate) {
                                return $translate("title.child-detail");
                            }],
                            $title     : ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                    .state('welcome', {
                        url         : '/welcome?uif',
                        views: {
                            "@": {
                                templateUrl: 'public/views/app/welcome.html' + version,
                                controller: ['$scope', '$translate', '$state', '$stateParams', 'Common', function($scope, $translate, $state, $stateParams, Common) {
                                    //if(!$stateParams.uif || $stateParams.uif == '') $state.go('app');

                                    var Base64 = {
                                        _keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
                                        decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r,c1,c2,c3;r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}
                                    };

                                    $scope.message = Common.f($translate.instant('welcome_message'), {name: Base64.decode($stateParams.uif)});
                                }]
                            }
                        },
                        authenticate: false,
                        resolve     : {
                            trans      : ['$translate', function ($translate) {
                                return $translate("title.welcome");
                            }],
                            $title     : ['trans', function (trans) {
                                return trans;
                            }]
                        }
                    })
                ;

                $urlRouterProvider.otherwise('/login');
                $locationProvider.hashPrefix('!');
            }
        ]);
})(window);
