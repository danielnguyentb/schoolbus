/**
 * Created by AnhNguyen
 */

(function (window) {
    "use strict";

    var app = angular.module('WCLApp', [
        'ui.router',
        'ngMaterial',
        'ngProgress',
        'ngMessages',
        'md.data.table',
        'vcRecaptcha',
        'angular-flash.service',
        'angular-flash.flash-alert-directive',
        'ui.router.title',
        'pascalprecht.translate',
        'ghiscoding.validation',
        'ui.sortable',

        'app-routes',

        'plugins/wcl-commons',
        'plugins/wcl-gpsconverter',
        'plugins/wcl-directives',
        'plugins/validate-directives',
        'services/user-services',
        'services/child-services',
        'services/route-services',
        'services/demo-data',
        'services/socket',

        'controllers/app-controller',
        'controllers/login-controller',
        'controllers/main-controller',
        'controllers/group-controller',
        'controllers/pending-controller',
        'controllers/driver-controller',
        'controllers/assistant-controller',
        'controllers/log-controller',
        'controllers/location-controller',
        'controllers/notification-controller',
        'controllers/child-controller',
        'controllers/bus-controller',
        'controllers/config-controller',
        'controllers/card-controller',
        'controllers/route-controller',
        'controllers/calendar-controller',
        'controllers/company-controller'
    ]);

    app.config([
        '$mdThemingProvider',
        '$translateProvider',
        '$translatePartialLoaderProvider',
        '$httpProvider',
        function ($mdThemingProvider, $translateProvider, $translatePartialLoaderProvider, $httpProvider) {

            var customPurple = $mdThemingProvider.extendPalette('purple', {
                'A100': 'd0a0ff'
            });
            var customYellow = $mdThemingProvider.extendPalette('yellow', {
                '200': 'ffffe0'
            });
            // Register the new color palette;
            $mdThemingProvider.definePalette('customPurple', customPurple);
            $mdThemingProvider.definePalette('customYellow', customYellow);

            $mdThemingProvider.theme('default')
                .primaryPalette('customPurple', {
                    default: 'A100'
                })
                .accentPalette('orange')
                .warnPalette('red')
                .backgroundPalette('customYellow', {
                    default: '200'
                });

            $translateProvider
                .useLoader('$translatePartialLoader', {
                    urlTemplate: 'public/locales/{lang}/{part}.json?v=' + (new Date()).getTime()//(window._APP_CONFIG.version || '1.0.0')
                })
                .preferredLanguage('en-US')
                .fallbackLanguage('en-US')
                .useSanitizeValueStrategy('escaped');

            $httpProvider.interceptors.push('AppInterceptor');
            $httpProvider.defaults.xsrfCookieName = 'xsrf-token';
            $httpProvider.defaults.xsrfHeaderName = 'x-csrf-token';

            $translatePartialLoaderProvider.addPart('main');
            $translatePartialLoaderProvider.addPart('validation');
        }
    ]);

    app.run(function ($rootScope, $window, $state, $translate, ngProgressFactory, AuthService, Common, Socket) {

        $rootScope.dataLoaded = false;

        var ngProgress = ngProgressFactory.createInstance();
        //Check logged in
        if ($window._APP_CONFIG && $window._APP_CONFIG.uId) {

            AuthService.init($window._APP_CONFIG.uData);

            Socket.connect($window._APP_CONFIG.base_url);
        }

        AuthService.setLocale(_APP_CONFIG.locale);


        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            ngProgress.setColor('#d33');
            ngProgress.setHeight('4px');
            ngProgress.start();

            if (toState.authenticate && !AuthService.isLogged) {

                Common.pushNoty($translate.instant('err_permission'), 'error');
                $state.go("auth");
                ngProgress.complete();
                event.preventDefault();
            }

            if (toState.requireLogout && AuthService.isLogged) {
                $state.go("main");
                event.preventDefault();
                //ngProgress.complete();
                //event.preventDefault();
            }
        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            ngProgress.complete();
            $rootScope.dataLoaded = true;
        });

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams) {
            ngProgress.complete();
        });
    });

})(window);
