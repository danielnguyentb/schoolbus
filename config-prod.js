var config = {};

config.LANG = 'en';
config.MODE = 'production';
config.PORT = 3000;

// enter domain of the app server
config.ROOTDOMAIN = '172.20.8.254';

// http | https
config.PROTOCOL = 'http';

//Pretty-print jade templates
config.VIEWPRETTY = true;

//Dust debug level  'DEBUG' | 'INFO' | 'WARN' | 'ERROR'
config.DUSTDEBUGLVL = 'ERROR';

//Pretty-print jade templates
config.SHUTDOWNTIMEOUT = 3000;

config.STATIC_DOMAIN = config.PROTOCOL + '://' + config.ROOTDOMAIN + ':' + config.PORT + '/';


config.UPLOAD_TMP = __dirname + '/uploads/tmp/';

// HTTPOPTS used for https only
// a key and cert file is used for https, not needed for http
// config.HTTPOPTS = {
//     key: require('fs').readFileSync('localhost-key.pem'),
//     cert: require('fs').readFileSync('localhost-cert.pem')
// }

// 0 = Don't use clustering, single process
// otherwise, enter the number of processes (aka workers) to spawn
config.CLUSTERWORKERS = 0;

//Level of express.logger() - use null in production
config.LOGGERLVL = "dev";

//Whether or not to pretty-print 500 or 403 type errors (only use for development)
config.ERRLOGGER = true;

// Show all requests (reported by express) on console log
config.LOGREQUESTS = false;

// File uploads path
// config.FILEUPLOADPATH = 'public/uploads';

// We can hook stdout and write to alternate transports
// var hook = require('./routes/syshooks');
//hook.stdout(hook.tcpclient(8888), true);
//hook.stdout(hook.tlsclient(8000, 'localhost-key.pem', 'localhost-cert.pem'), true);
//hook.stdout(hook.file('somelog.txt'), true);

// Handle errors, all errors are bubbled up to here and logged
config.errLogger = null;  // set in audit module after it has loaded

// This function can be uncommented and will handle all uncaught errors in the app
// Leave commented for development, but handling uncaught errors should prevent the app from ever crashing
/*
 hook.uncaught(function(s) {
 console.log(s);
 })
 */

// var logr = require('tracer').colorConsole({ format : "{{timestamp}} {{message}}", dateformat : "HH:MM" });

var logr = require('tracer').colorConsole({
	transport: function (data) {
		console.log(data.output);
		// fs.open('./file.log', 'a', 0666, function(e, id) {
		//     fs.write(id, data.output+"\n", null, 'utf8', function() {
		//         fs.close(id, function() {
		//         });
		//     });
		// });
	},
	format: "{{timestamp}} {{message}}", dateformat: "HH:MM"
});

// Custom logging, use config.log('...') in your code
config.log = function (args) {
	var args = Array.prototype.slice.call(arguments);
	// if( Array.isArray(args) ) args = args.join(' ');
	logr.info.apply(null, args);
}
config.error = function (args) {
	var args = Array.prototype.slice.call(arguments);
	logr.error.apply(null, args);
}

// Database configuration.  Use db.use('L1") in your code
config.dbs = [
	{
		name: 'L1',
		host: '127.0.0.1',
		port: 6379,
		auth: '2GnIo6dJCC8OZ0MKfTLoVxdo6wgOAt3iPNbzXQ9TuSXmDMzO4ibjEjF5KSseZ8t',
		ready: config.log,
		error: config.error,
		type: 'Redis'
	},
	{
		name: 'L2',
		host: '127.0.0.1',
		port: 6379,
		auth: '2GnIo6dJCC8OZ0MKfTLoVxdo6wgOAt3iPNbzXQ9TuSXmDMzO4ibjEjF5KSseZ8t',
		ready: config.log,
		error: config.error,
		type: 'Redis'
	},
	{
		name: 'DataDB',
		host: '127.0.0.1:3306',
		user: 'sint',
		pass: 'sint1234!@#$',
		multi: true,
		db: 'sint',
		ready: config.log,
		error: config.error,
		type: 'MySQL',
		prefix: 'wump_',
		prefixDt: 'wump__'

	}
];

// config.hashItr = 126;

// Outgoing SMTP email configuration
config.Email = {
	systemName: 'WCL Mail',
	systemAddress: 'noreply@wwcom.com',
	securityAlertFROMEmail: 'noreply@wwcom.com',
	securityAlertTOEmail: 'duong@wwcom.com',
	//To set some other custom host - change settings in routes/utils/email/
	SMTPHost: 'smtp.gmail.com',
	SMTPPort: 587,
	SMTPMyDomain: 'wwcom.com',
	SMTPUser: 'noreply@wwcom.com',
	SMTPPass: 'zA78sCIyDLrB'
};


//Only for DEV / debugging purposes
config.dbug = config.log;

//Config for session
config.session = {
	key: "WCLProductSession", //For security, renamed from connect.sid
	secret: 'EdKIbjoouxZfLy8sQ!mBz#TTnr'  //and make secret Hard to guess
}

//
//exports.db = {
//	host     : 'localhost',
//	user     : 'root',
//	password : '',
//	database : 'sint',
//	table	   : 'wump_log',
//	prefix   : 'wump_',
//	prefixDt : 'wump__',
//}

config.msdbA = [];
config.msdbB = [];
//List Import Station
config.stn = [];

config.remotesyslog = {
	host: '127.0.0.1',
	port: '514'
}

config.service = {
	name: 'SINT Jupiter',
	description: 'SINT Jupiter.',
	script: 'D:\\ProjectNode\\WCL\\sint\\app.js'
}


module.exports = config;

