(function (r) {

    // Load config file
    r('config-node')({default: 'config-dev'});

    var dbs       = r("./middleware/db.js");
    var Acl       = r('acl'),
        Sequelize = r('sequelize'),
        AclSeq    = r('acl-sequelize');

    dbs.connect(config.dbs, function (db) {

        //Clear L1 and L2 caches
        var num = 0, res = {};
        db.use('L2').keys('*', function (e, r) {
            r.forEach(function (key) {
                db.use('L2').del(key);
            });
        });

        var connectCfg = db.use('DataDB').config.connectionConfig;
        var acl        = new Acl(
            new AclSeq(
                new Sequelize(
                    connectCfg.database,
                    connectCfg.user,
                    connectCfg.password,
                    {
                        host   : connectCfg.host,
                        dialect: 'mysql'
                    }
                ),
                {prefix: 'acl_'}
            )
        );

        acl.allow([
            {
                roles : [config.ROLE.ADMIN],
                allows: [
                    {
                        resources  : 'notifications',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'my-account',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'company-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'group-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'driver-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'assistant-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'bus-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'pending-approval',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'calendar-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'master-map',
                        permissions: ['get']
                    }
                ]
            },
            {
                roles : [config.ROLE.SCHOOL_ADMIN],
                allows: [
                    {
                        resources  : 'class-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'my-account',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'users-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'calendar-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    }
                ]
            },
            {
                roles : [config.ROLE.DRIVER],
                allows: [
                    {
                        resources  : 'notifications',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'my-account',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'route-manage',
                        permissions: ['get', 'post', 'delete']
                    },
                    {
                        resources  : 'calendar-manage',
                        permissions: ['get', 'post', 'delete']
                    },
                    {
                        resources  : 'company-manage',
                        permissions: ['get', 'post', 'put']
                    },
                    {
                        resources  : 'card-manage',
                        permissions: ['post']
                    },
                    {
                        resources  : 'child-manage',
                        permissions: ['post']
                    }
                ]
            },
            {
                roles : [config.ROLE.ASSISTANT],
                allows: [
                    {
                        resources  : 'notifications',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'my-account',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'route-manage',
                        permissions: ['get', 'post', 'delete']
                    },
                    {
                        resources  : 'calendar-manage',
                        permissions: ['get', 'post', 'delete']
                    },
                    {
                        resources  : 'company-manage',
                        permissions: ['get', 'post', 'put']
                    },
                    {
                        resources  : 'card-manage',
                        permissions: ['post']
                    },
                    {
                        resources  : 'child-manage',
                        permissions: ['post']
                    }
                ]
            },
            {
                roles : [config.ROLE.PARENT],
                allows: [
                    {
                        resources  : 'notifications',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'child-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'calendar-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    }
                ]
            },
            {
                roles : [config.ROLE.BUS_COMPANY],
                allows: [
                    {
                        resources  : 'driver-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'assistant-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'bus-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'child-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'route-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'calendar-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'group-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'my-account',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'master-map',
                        permissions: ['get']
                    },
                    {
                        resources  : 'location-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    }
                ]
            },
            {
                roles : [config.ROLE.BUS],
                allows: [
                    {
                        resources  : 'bus-manage',
                        permissions: ['get', 'post']
                    },
                    {
                        resources  : 'company-manage',
                        permissions: ['get', 'post', 'put']
                    },
                    {
                        resources  : 'route-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'calendar-manage',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'my-account',
                        permissions: ['get', 'post', 'put', 'delete']
                    },
                    {
                        resources  : 'card-manage',
                        permissions: ['post']
                    },
                    {
                        resources  : 'child-manage',
                        permissions: ['post']
                    }
                ]
            }
        ], function() {
            console.log('Add roles success!');

            acl.addUserRoles('1', 'admin', function(e, r) {
                console.log('Add admin success!');
            });
        });

    });

})(require);