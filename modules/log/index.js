/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'log';

    var Step = r("step");

    ns.init = function(app, _db, fn, mn) {
        db = _db;
        _fn = fn;
        managers = mn;

        app.route('/log')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.get, _fn.auth.permission, routes.getLog);

        app.route('/routebydate')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.get, _fn.auth.permission, routes.getRoute);
    };

    var routes = {

        getLog : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            var date = req.query.date ? (_fn.sanitize(_fn.trim(req.query.date)).xss()) : '';
            var route_trip = req.query.route_trip ? (_fn.sanitize(_fn.trim(req.query.route_trip)).xss()) : 0;

            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;
                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");
                    managers.log.eventByTripDate(route_trip, date, 0, page, numrow, order, desc, '', this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            logs  : r[0]
                        });
                    }
                }
            )
        },

        getRoute : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var date = req.query.date ? (_fn.sanitize(_fn.trim(req.query.date)).xss()) : 10;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;
                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");
                    managers.log.routeTripByDate(0, 0, '', '', '', date, '', '', '', '', '', 123, 123, '', '', 1, 100, '', '', '', this);
                },
                function response(e, r) {
                    console.log(r);

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            routes  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        }

    };

})(module.exports, require);
