/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'configs';

    var Step = r("step");

    ns.init = function(app, _db, fn, mn) {
        db = _db;
        _fn = fn;
        managers = mn;

        app.route('/config')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.configManage, _fn.auth.act.get, _fn.auth.permission, routes.getConfig)
            .post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.configManage, _fn.auth.act.post, _fn.auth.permission, routes.updateConfig);
    };

    var routes = {

        getConfig : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;

            Step (
                function getConfig() {
                    managers.config.getConfigAll(this);
                },
                function response(e, r) {

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            configs  : r[0]
                        });
                    }
                }
            )
        },

        updateConfig : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var auto_alight_after           = req.body.auto_alight_after ? _fn.sanitize(_fn.trim(req.body.auto_alight_after.toString())).xss() : '';
            var new_index                   = req.body.new_index ? _fn.sanitize(_fn.trim(req.body.new_index.toString())).xss() : '';
            var increase                    = req.body.increase ? _fn.sanitize(_fn.trim(req.body.increase.toString())).xss() : '';
            var decrease                    = req.body.decrease ? _fn.sanitize(_fn.trim(req.body.decrease.toString())).xss() : '';
            var max_index                   = req.body.max_index ? _fn.sanitize(_fn.trim(req.body.max_index.toString())).xss() : '';
            var manual_index                = req.body.manual_index ? _fn.sanitize(_fn.trim(req.body.manual_index.toString())).xss() : '';
            var gps_report                  = req.body.gps_report ? _fn.sanitize(_fn.trim(req.body.gps_report.toString())).xss() : '';
            var location_range              = req.body.location_range ? _fn.sanitize(_fn.trim(req.body.location_range.toString())).xss() : '';
            Step (
                function getConfig() {
                    managers.config.updateConfigAll(auto_alight_after, 1, 1, new_index, increase, decrease, max_index, manual_index, gps_report, location_range,this);
                },
                function response(e, r) {

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            message: lingo.get('update_config_success'),
                            configs  : r[0]
                        });
                    }
                }
            )
        }

    };

})(module.exports, require);
