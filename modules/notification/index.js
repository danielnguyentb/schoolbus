/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'notification';

    var Step = r("step");

    ns.init = function(app, _db, fn, mn) {
        db = _db;
        _fn = fn;
        managers = mn;

        app.route('/notification')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.notificationManage, _fn.auth.act.get, _fn.auth.permission, routes.getNotification);
    };

    var routes = {

        getNotification : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;

            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");
                    managers.notification.getListNotificationPaged(uid, 123, this);
                },
                function response(e, r) {

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            notifications  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        },

        removeNotification : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).xss() : '';

            Step (
                function removeNotification() {
                    if(e) throw e;
                    _fn.check(id, lingo.get('notification_not_exists')).notEmpty().isInt();
                    managers.notification.removeNotification(id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('remove_notification_success')});
                }
            )
        }

    };

})(module.exports, require);
