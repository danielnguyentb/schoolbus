/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn;        //Utility functions

    var locale = 'route';

    var request = r("request");
    var Step = r("step");
    var qr = r('qr-image');
    var moment = r('moment');
    var gcm = r('node-gcm');
    var jwt = r('jsonwebtoken');

    ns.init = function(app, _db, fn, mn) {

        db       = _db;
        _fn      = fn;
        managers = mn;

        var apiRoutes = app.get('wcl:api:routes');

        apiRoutes.route('/route')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.getRoutes);

        apiRoutes.route('/route/smart')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.smartScan);

        apiRoutes.route('/route/smartBasic')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.smartBasicScan);

        apiRoutes.route('/route/gpsLog')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.gpsLog);

        apiRoutes.route('/route/busSearch')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.busSearch);

        apiRoutes.route('/route/busUpdate')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.busUpdate);

        apiRoutes.route('/route/busMerge')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.busMerge);

        apiRoutes.route('/route/locationUpdate')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.locationUpdate);

        apiRoutes.route('/route/check-child-on-bus').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.checkChildOnBus);

        apiRoutes.route('/route/:id')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.startRouteDaily)
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.getRouteDetail);

        apiRoutes.route('/routeBasic/:id')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.getRouteBasicDetail);

        apiRoutes.route('/routePassive/:id')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.getRoutePassiveDetail);

        apiRoutes.route('/route/:id/alight-all')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.updateAlightAll);

        apiRoutes.route('/route/:id/alight-all-basic')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.updateAlightAllBasic);

        apiRoutes.route('/route/update/:id')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.updateRoutePoint);

        apiRoutes.route('/route/sync/:id')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.syncRoutePoint);

        apiRoutes.route('/route/location/push').post(_fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.workerLocation);

        apiRoutes.route('/manual/check-io').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.workerManualCheckInOut);
        apiRoutes.route('/manual/basic-check-out').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.workerManualCheckOutBasic);

        apiRoutes.route('/driverNotifications').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.getDriverNotifications);

        app.route('/route')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.getRoute)
            .post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.insertRoute);

        app.route('/route/point')
            .post(_fn.getBundle(locale),_fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.updateRoutePointDetail);

        app.route('/route/updatePointOrder')
            .post(_fn.getBundle(locale),_fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.updatePointOrder);

        app.route('/route/addPoint')
            .post(_fn.getBundle(locale),_fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.addPointInPair);

        app.route('/route/point/:id')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.getRoutePoint);

        app.route('/route/listPoint/:id')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.getRoutePoints);

        app.route('/route/:id')
            .get( _fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.getRouteDetailPage)
            .post(_fn.getBundle(locale),_fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.post, _fn.auth.permission, routes.changeRouteDetail)
            .delete(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.delete, _fn.auth.permission, routes.removeRoute);

        app.route('/companyResource')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.routeManage, _fn.auth.act.get, _fn.auth.permission, routes.companyResource);

        //app.route('/notification/push').get(_fn.getBundle(locale), routes.pushNotificationTest)
    };

    var routes = {

        getRoutes : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var company_id = req.session.company_id;
            var role = req.session._role;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            var type = req.query.type ? _fn.sanitize(_fn.trim(req.query.type)).xss() : '';
            numrow = 100;

            Step (
                function checkInput() {
                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var allowType = ['today', 'history', 'upcoming'];
                    if(allowType.indexOf(type) == -1) type = allowType[0];

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    if(type == 'history') {

                        var yesterday = new Date();
                        yesterday.setDate(yesterday.getDate() - 1);
                        order = 'route_trip_date';
                        desc = 'DESC';
                        managers.route.routeTripDailySearchPaged(0, company_id, uid, 0, 0, '', '', '', '', '', 0, 123, 123, '', _fn.date.dateString(yesterday), page, numrow, order, desc, '', this);
                    } else {

                        var time = new Date();
                        if(type == 'upcoming') time.setDate(time.getDate() + 1);
                        managers.route.routeTripSearchPaged(0, company_id, uid, 0, 0, _fn.date.dateString(time), '', '', '', '', 0, 123, 123, page, numrow, order, desc, '', this);
                    }
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        routes : r[0]
                    });
                }
            )
        },
        companyResource: function(req, res) {
            var company_id      = req.session.company_id;
            Step (
                function checkInput() {
                    managers.route.companyResource(company_id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true,
                            drivers: r[0],
                            assistants: r[1],
                            buses: r[2]
                        });
                    }
                }
            )
        },
        getRoute : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var company_id = req.session.company_id;
            var role = req.session._role;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");
                    managers.route.routeTripPaged(uid, page, numrow, order, desc, this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true,
                            routes : r[0],
                            drivers: r[2],
                            assistants: r[3],
                            buses: r[4]
                        });
                    }
                }
            )
        },
        insertRoute : function(req, res){
            var lingo           = req.lingo;
            var uid             = req.session.uId;
            var company_id      = req.session.company_id;
            var routeid         = req.body.routeid ? _fn.sanitize(_fn.trim(req.body.routeid.toString())).xss() : '';
            var name            = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var driver_id       = req.body.driver_id ? _fn.sanitize(_fn.trim(req.body.driver_id.toString())).xss() : '';
            var assistant_id    = req.body.assistant_id ? _fn.sanitize(_fn.trim(req.body.assistant_id.toString())).xss() : '';
            var bus_id          = req.body.bus_id ? _fn.sanitize(_fn.trim(req.body.bus_id.toString())).xss() : '';
            var plan_date       = req.body.plan_date ? _fn.sanitize(_fn.trim(req.body.plan_date)).xss() : '';
            var estimate_from   = req.body.estimate_from ? _fn.sanitize(_fn.trim(req.body.estimate_from)).xss() : '';
            var estimate_to     = req.body.estimate_to ? _fn.sanitize(_fn.trim(req.body.estimate_to)).xss() : '';
            var description     = req.body.description ? _fn.sanitize(_fn.trim(req.body.description)).xss() : '';
            var edit            = false;

            Step (
                function checkDrirver() {
                    managers.route.checkDriverAvaliable(driver_id, 0, plan_date, estimate_from, estimate_to, this);
                },
                function checkBus(e, r) {
                    if(e) throw e;
                    if(r[0][0].used == 0) throw Error(lingo.get('driver_not_avaliable'));
                    managers.route.checkBusAvaliable(bus_id, 0, plan_date, estimate_from, estimate_to, this);
                },
                function updateRoute(e, r) {
                    if(e) throw e;
                    if(r[0][0].used == 0) throw Error(lingo.get('bus_not_avaliable'));
                    var status = 1;
                    managers.route.insertRoute(uid, company_id, name, driver_id, assistant_id, bus_id, plan_date, estimate_from, estimate_to, description, status, this);
                },
                function response(e, r) {
                    if(routeid != 0 && r.length){
                        managers.route.clonePoints(routeid, r[0][0].id, uid, this);
                    }
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('create_route_success')
                    })
                }
            )
        },
        changeRouteDetail : function(req, res){
            var lingo           = req.lingo;
            var uid             = req.session.uId;
            var company_id      = req.session.company_id;
            var id              = req.body.id ? req.body.id.toString() : false;
            var name            = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var driver_id       = req.body.driver_id ? _fn.sanitize(_fn.trim(req.body.driver_id.toString())).xss() : '';
            var assistant_id    = req.body.assistant_id ? _fn.sanitize(_fn.trim(req.body.assistant_id.toString())).xss() : '';
            var bus_id          = req.body.bus_id ? _fn.sanitize(_fn.trim(req.body.bus_id.toString())).xss() : '';
            var schedule_id     = req.body.schedule_id ? _fn.sanitize(_fn.trim(req.body.schedule_id.toString())).xss() : '';
            var plan_date       = req.body.plan_date ? _fn.sanitize(_fn.trim(req.body.plan_date)).xss() : '';
            var start_date      = req.body.start_date ? _fn.sanitize(_fn.trim(req.body.start_date)).xss() : '';
            var end_date        = req.body.end_date ? _fn.sanitize(_fn.trim(req.body.end_date)).xss() : '';
            var estimate_from   = req.body.estimate_from ? _fn.sanitize(_fn.trim(req.body.estimate_from)).xss() : '';
            var estimate_to     = req.body.estimate_to ? _fn.sanitize(_fn.trim(req.body.estimate_to)).xss() : '';
            var description     = req.body.description ? _fn.sanitize(_fn.trim(req.body.description)).xss() : '';
            var schedule        = req.body.schedule ? 1:0;
            var scheduledt      = {};
            scheduledt.day2     = req.body.day2 ? 1:0;
            scheduledt.day3     = req.body.day3 ? 1:0;
            scheduledt.day4     = req.body.day4 ? 1:0;
            scheduledt.day5     = req.body.day5 ? 1:0;
            scheduledt.day6     = req.body.day6 ? 1:0;
            scheduledt.day7     = req.body.day7 ? 1:0;
            scheduledt.day8     = req.body.day8 ? 1:0;
            var edit            = false;
            Step (
                function checkInput() {
                    managers.route.routeTripGetInfoById(id, this);
                },
                function updateSchedule(e, r){
                    if(e) throw e;
                    if(edit && (!r.length || !r[0].length) ) throw Error(lingo.get('route_not_exists'));
                    if(schedule) {
                        if(schedule_id) managers.route.routeScheduleUpdate(schedule_id,'Schedule', start_date, end_date, estimate_from, estimate_to, this);
                        else managers.route.routeScheduleCreate('Schedule', start_date, end_date, estimate_from, estimate_to, uid, this);
                    } else _fn.r([], this);
                },
                function updateSchedule(e, r){
                    if(e) throw e;

                    if(r.length) {
                        var weekid = [];
                        for(var i = 2; i < 9; i ++) {
                            if(scheduledt['day'+i]) weekid.push(i);
                        }

                        weekid = weekid.join();
                        managers.route.updateScheduleWeek(r[0][0].id, weekid, this.parallel());
                        _fn.r(r[0][0].id, this.parallel());
                    } else _fn.r([], this);
                },
                function updateRoute(e, r) {
                    if(e) throw e;
                    var status = 1;
                    if(schedule_id) managers.route.updateRoute(id, company_id, name, driver_id, assistant_id, bus_id, schedule_id, plan_date, estimate_from, estimate_to, description, status, this.parallel());
                    else managers.route.updateRoute(id, company_id, name, driver_id, assistant_id, bus_id, r, plan_date, estimate_from, estimate_to, description, status, this.parallel());

                    //Push LBA to update data to driver/assistant;
                    utils.pushRouteTripDailyChanges(id, 'info', this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('update_route_success')
                    })
                }
            )
        },
        removeRoute: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).xss() : '';

            Step (
                function getAssistant() {

                    _fn.check(id, lingo.get('routet_not_exists')).notEmpty().isInt();
                    managers.route.getRouteById(id, this);
                },
                function removeRoute(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('routet_not_exists'));

                    r = r[0][0];
                    managers.route.removeRoute(id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('remove_route_success')});
                }
            )
        },

        updateAlightAll : function(req, res) {

            var lingo       = req.lingo;
            var uid         = req.session.uId;
            var company_id  = req.session.company_id;
            var role        = req.session._role;
            var rid         = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var now         = _fn.date.format(new Date(), 'YYYY-0MM-0DD');

            Step (
                function checkInput() {

                    _fn.check(rid, lingo.get('route_not_exists')).notEmpty().isInt();

                    managers.route.routeTripGetInfoById(rid, this.parallel());
                    managers.route.routeTripDailyGetInfoById(rid, now, this.parallel());
                },
                function updateAlight(e, r, rd) {
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));
                    r = r[0][0];
                    rd = rd[0][0];

                    _fn.r(r, this.parallel());
                    //Update all child alight
                    managers.route.routeTripDailyDetailUpdateChildStatus(rid, 0, '', '', 0, 3, this.parallel());
                },
                function getPoint(e, r, l) {
                    if(e) throw e;

                    utils.routeTripFinishLBA(r);

                    if(l && l.length && l[1]) utils.pushLBAParent(l[1], lingo);

                    managers.route.routeTripDailyGetSplitByTripId(rid, now, 1, 10, '', '', '', this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        points : r[0],
                        message: lingo.get('update_alight_all_success')
                    });
                }
            );
        },

        updateAlightAllBasic : function(req, res) {

            var lingo       = req.lingo;
            var uid         = req.session.uId;
            var company_id  = req.session.company_id;
            var role        = req.session._role;
            var rid         = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var now         = _fn.date.format(new Date(), 'YYYY-0MM-0DD');

            Step (
                function checkInput() {

                    _fn.check(rid, lingo.get('route_not_exists')).notEmpty().isInt();

                    managers.route.routeTripGetInfoById(rid, this.parallel());
                },
                function updateAlight(e, r) {
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));
                    r = r[0][0];


                    _fn.r(r, this.parallel());
                    //Update all child alight
                    managers.route.routeTripDailyDetailUpdateChildStatusBasic(rid, 0, '', '', 0, 3, this.parallel());
                },
                function getPoint(e, r, l) {
                    if(e) throw e;
                    utils.routeTripFinishLBA(r);

                    if(l && l.length && l[1]) utils.pushLBAParent(l[1], lingo);

                    managers.route.routeTripBasicGetSplitByTripId(rid, 12, 1, 200, this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        points : r[0],
                        message: lingo.get('update_alight_all_success')
                    });
                }
            );
        },

        busSearch: function(req, res ){
            var lingo = req.lingo;
            var uid         = req.session.uId;
            var company_id  = req.session.company_id;
            var bus_license_plate_no = req.body.bus_license_plate_no ? _fn.sanitize(_fn.trim(req.body.bus_license_plate_no.toString())).xss() : null;

            Step (
                function searchBus(e, r) {
                    if(!bus_license_plate_no) throw Error(lingo.get('invalid_data'));
                    managers.route.busSearch(company_id, bus_license_plate_no, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        bus: r
                    })
                }
            )
        },

        busUpdate: function(req, res ){
            var lingo = req.lingo;
            var uid         = req.session.uId;
            var company_id  = req.session.company_id;
            var bus_id = req.body.bus_id ? _fn.sanitize(_fn.trim(req.body.bus_id.toString())).xss() : null;
            var bus_license_plate_no = req.body.bus_license_plate_no ? _fn.sanitize(_fn.trim(req.body.bus_license_plate_no.toString())).xss() : null;
            var bus_registered_country = req.body.bus_registered_country ? _fn.sanitize(_fn.trim(req.body.bus_registered_country.toString())).xss() : null;
            var bus_snd_license_plate_no = req.body.bus_snd_license_plate_no ? _fn.sanitize(_fn.trim(req.body.bus_snd_license_plate_no.toString())).xss() : null;
            var bus_snd_registered_country = req.body.bus_snd_registered_country ? _fn.sanitize(_fn.trim(req.body.bus_snd_registered_country.toString())).xss() : null;

            Step (
                function searchBus(e, r) {
                    if(!bus_name && !bus_license_plate_no) throw Error(lingo.get('invalid_data'));
                    managers.route.busUpdate(bus_id, '', bus_license_plate_no, bus_registered_country, bus_snd_license_plate_no, bus_snd_registered_country, company_id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        bus: r
                    })
                }
            )
        },

        busMerge: function(req, res ){
            var lingo = req.lingo;
            var uid         = req.session.uId;
            var company_id  = req.session.company_id;
            var pfrom_bus_id = req.body.id_from ? _fn.sanitize(_fn.trim(req.body.id_from.toString())).xss() : null;
            var pto_bus_id = req.body.id_to ? _fn.sanitize(_fn.trim(req.body.id_to.toString())).xss() : null;

            Step (
                function searchBus(e, r) {
                    if(!pfrom_bus_id || !pto_bus_id) throw Error(lingo.get('invalid_data'));
                    managers.route.busMerge(pfrom_bus_id,pto_bus_id, this)
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        bus: r
                    })
                }
            )
        },



        gpsLog: function(req, res ){
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var route_id = req.body.route_id ? _fn.sanitize(_fn.trim(req.body.route_id.toString())).xss() : null;
            var lat = req.body.lat ? _fn.sanitize(_fn.trim(req.body.lat.toString())).xss() : null;
            var long = req.body.long ? _fn.sanitize(_fn.trim(req.body.long.toString())).xss() : null;
            var starttime = data.starttime||new Date().getTime(), roundtrip = Math.abs((new Date().getTime()) - starttime);

            Step (
                function gpsLog(e, r) {
                    var ploc_point = lat+','+long;
                    managers.route.gpsLog(route_id, ploc_point, uid, '',roundtrip, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true
                    })
                }
            )
        },

        locationUpdate: function(req, res ){
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var location = req.body.location ? _fn.sanitize(_fn.trim(req.body.location.toString())).xss() : null;
            var location_name = req.body.location_name ? _fn.sanitize(_fn.trim(req.body.location_name.toString())).xss() : null;
            var location_ad = req.body.location_ad ? _fn.sanitize(_fn.trim(req.body.location_ad.toString())).xss() : null;

            Step (
                function locationRouteUpdate(e, r) {
                    managers.route.locationRouteUpdate(location, location_name, location_ad, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true
                    })
                }
            )
        },

        smartScan : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var company_id = req.session.company_id;
            var route_id = req.body.route_id ? _fn.sanitize(_fn.trim(req.body.route_id.toString())).xss() : 0;
            var id = req.body.id ? _fn.sanitize(_fn.trim(req.body.id.toString())).xss() : '';
            var nfc = req.body.nfc ? true : false;
            var lat = req.body.lat ? _fn.sanitize(_fn.trim(req.body.lat.toString())).xss() : '';
            var long = req.body.long ? _fn.sanitize(_fn.trim(req.body.long.toString())).xss() : '';

            var now         = _fn.date.dateString(new Date());
            var type, card_code = id, card_type = nfc ? 1 : 2, action_repeated = false;

            Step (
                function checkInput() {
                    _fn.check(id, lingo.get('card_not_exists')).notEmpty();

                    managers.card.getCardInfoByCard(card_code, card_type, this.parallel());
                },
                function getChild(e, c) {
                    if(e) throw e;

                    if(!c.length || !c[0].length) throw Error(lingo.get('card_not_exists'));

                    c = c[0][0];
                    if(c.company_id != 0 && c.company_id != company_id){
                        throw Error(lingo.get('this_card_belong_to_another_company'));
                    }
                    _fn.r(c, this.parallel());

                    id = c.owner_id;
                    type = c.ower_type == 1 ? 'child' : 'guardian';

                    if(c.ower_type == 2) managers.child.getListChildByGuardianChild(id, this.parallel());
                    else if(c.ower_type == 1) _fn.r([ [{child_id: id}] ], this.parallel());
                    else if(c.ower_type == 3) _fn.r([{bus_type : c.bus_type}], this.parallel());

                    utils.saveRouteLocation(req, lat, long, this.parallel());
                },
                function getRoute(e, c, r, g) {
                    if(e) throw e;

                    if(!r.length || ([1, 2].indexOf(c.ower_type) != -1 && !r[0].length)) throw Error(lingo.get('card_not_exists'));

                    _fn.r(c, this.parallel());
                    _fn.r(r, this.parallel());
                    //_fn.r(g, this.parallel());
                    managers.route.getLocationInfo(g[0][0].id, this.parallel());
                    if(c.ower_type == 3)  managers.route.getCardLastAction(company_id, g[0][0].id, 1, uid, 3, 0, this.parallel());
                    else {
                        if(r.length && r[0].length) managers.route.getCardLastAction(company_id, g[0][0].id, 1, uid, 2, r[0][0].child_id, this.parallel());
                        else _fn.r([[], []], this.parallel());
                    }
                },
                function getRoute(e, c, r, g, ce) {
                    if(e) throw e;

                    if(!ce) ce = [ [], [] ];//Set default value;
                    _fn.r(c, this.parallel());
                    _fn.r(r, this.parallel());

                    var card_type;
                    if(ce[1].length && utils.routeTimeValid(new Date(ce[1][0].last_updated))) {
                        action_repeated = true;

                        _fn.r([], this.parallel());
                        _fn.r(g, this.parallel());
                        _fn.r([], this.parallel());
                    } else {
                        card_type = nfc ? 1 : 2;
                        /*1: driver, 2: Child, 3: Guardian, 4: Bus, 5: assitant*/
                        if(c.ower_type == 3) {
                            if(role == 'driver')
                                managers.route.insertEventHistory(route_id, g[0][0].id, '', 1, uid, 3, 0, card_type, card_code, company_id, this.parallel());
                            else
                                managers.route.insertEventHistory(route_id, g[0][0].id, '', 5, uid, 3, 0, card_type, card_code, company_id, this.parallel());
                        }
                        else {
                            var childIds = [], group = this.group();
                            r[0].forEach(function (v) {
                                managers.route.insertEventHistory(route_id, g[0][0].id, '', 1, uid, 2, v.child_id, card_type, card_code, company_id, group());
                                childIds.push(v.child_id);
                            });
                        }

                        _fn.r(g, this.parallel());

                        if(c.ower_type != 3) managers.child.getListChildByIDs(childIds.join(','), this.parallel());
                        else _fn.r([], this.parallel());
                    }

                    _fn.r(ce, this.parallel());
                },
                function getRouteDetail(e, g, c, r, l, cl, ce) {
                    if(e) throw e;

                    if(r.length && g.ower_type != 3) r = r[0];

                    _fn.r(g, this.parallel());
                    _fn.r(c, this.parallel());
                    _fn.r(r, this.parallel());
                    _fn.r(l, this.parallel());

                    if(ce[1].length && utils.routeTimeValid(new Date(ce[1][0].last_updated))) _fn.r([], this.parallel());
                    else {
                        if(r.length && r[0].length) managers.route.routeTripGetInfoById(r[0][0].route_trip_id, this.parallel());
                        else _fn.r([], this.parallel());
                    }

                    _fn.r(cl, this.parallel());
                    _fn.r(ce, this.parallel());
                },
                function getRouteData(e, g, c, r, l, ri, cl, ce) {
                    if(e) throw e;

                    _fn.r(g, this.parallel());
                    _fn.r(c, this.parallel());
                    _fn.r(r, this.parallel());
                    _fn.r(l, this.parallel());
                    _fn.r(cl, this.parallel());
                    _fn.r(ce, this.parallel());

                    ri = (ri.length && ri[0].length) ? ri[0][0] : null;

                    _fn.r(ri, this.parallel());

                    if(ri && g.bus_type == 1 || (r.length && r[0].length && r[0][0].action == 1)) {
                        managers.route.routeTripDailyGetSplitByTripId(ri.id, now, 1, 100, '', '', '', this.parallel());
                        managers.user.getUserById(ri.driver_id, this.parallel());
                        managers.route.getRouteTripDailyData(ri.id, now, this.parallel());
                    }

                },
                function updateChildStatus(e, g, c, r, l, cl, ce, ri, rip, di, rid) {
                    if(e) throw e;
                    _fn.r(r, this.parallel());
                    _fn.r(g, this.parallel());
                    _fn.r(cl, this.parallel());
                    _fn.r(ce, this.parallel());
                    _fn.r(l, this.parallel());

                    var date = _fn.date.format(new Date(), 'YYYY-0MM-0DD');
                    managers.route.routeTripDailyGetInfoById(r[0][0].route_trip_id, date, this.parallel());

                    var time = new Date();
                    time.setHours(time.getHours() + 1);

                    if(ce[1].length && utils.routeTimeValid(new Date(ce[1][0].last_updated), time)) {

                        _fn.r(false, this.parallel());
                    } else {
                        _fn.r(true, this.parallel());

                        if(g.bus_type == 0) {

                            var group = this.group(), action = (r[0][0].action == 1) ? 1 : 2;

                            c[0].forEach(function(v) {
                                managers.route.routeTripDailyDetailUpdateChildStatus(r[0][0].route_trip_id, v.child_id, card_type, card_code, l[0][0].id, action, group());
                            });

                        } else _fn.r([], this.parallel());

                        if(ri && ri.status != 3 && g.bus_type != 2) utils.startRouteDailyLBA(ri, rip, rid, di, now, this.parallel());
                        if(ri && ri.status == 3) utils.routeTripFinishLBA(ri);
                    }
                },
                function response(e, r, g, cl, ce, lct, ri, valid, l) {
                    if(e) res.json({success: false, message: e.message, repeated: action_repeated});
                    else {
                        var message = '', action = (g.bus_type == 2) ? 2 : 1, workers = [];
                        if(ri[0][0].assitant_id)
                            workers = ri[0][0].assitant_id.toString().split(',');
                        workers.push(ri[0][0].driver_id);
                        if(!valid) {
                            if(g.ower_type == 3) message = ce[1][0].action == 1 ? lingo.get('already_checkin') : lingo.get('already_checkout');
                            else message = ce[1][0].action == 1 ? lingo.get('already_onboard') : lingo.get('already_alight');
                        } else {
                            if(g.bus_type == 0) {
                                message = r[0][0].action == 1 ? lingo.get('pickup_success') : lingo.get('dropoff_success');
                                utils.routeTripScanPush(message, cl, lct, workers);
                            } else {
                                message = g.bus_type == 1 ? lingo.get('checkin_success') : lingo.get('checkout_success');
                                utils.routeTripScanPush(message, cl, lct, workers);
                            }
                        }

                        var data = {
                            success: true,
                            message: message,
                            repeated: action_repeated
                        };

                        if(valid) _fn.extend(data, {action: action, routes: r[0], card: g, childList: cl});

                        if(l && l.length) {
                            l.forEach(function(v) {
                                if(v[1]) utils.pushLBAParent(v[1], lingo);
                            });
                        }

                        res.json(data);
                    }
                }
            )
        },

        smartBasicScan : function(req, res) {
            var lingo           = req.lingo;
            var uid             = req.session.uId;
            var role            = req.session._role;
            var company_id      = req.session.company_id;
            var route_id        = req.body.route_id ? _fn.sanitize(_fn.trim(req.body.route_id.toString())).xss() : 0;
            var passive_mode    = req.body.passive_mode ? _fn.sanitize(_fn.trim(req.body.passive_mode.toString())).xss() : 0;
            var bus_id          = req.body.bus_id ? _fn.sanitize(_fn.trim(req.body.bus_id.toString())).xss() : 0;
            var id              = req.body.id ? _fn.sanitize(_fn.trim(req.body.id.toString())).xss() : '';
            var nfc             = req.body.nfc ? true : false;
            var now             = _fn.date.dateString(new Date());
            var type, card_code = id, card_type = nfc ? 1 : 2, action_repeated = false;

            Step (
                function checkInput() {
                    _fn.check(id, lingo.get('card_not_exists')).notEmpty();

                    managers.card.getCardInfoByCard(card_code, card_type, this.parallel());
                },
                function getChild(e, c) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('card_not_exists'));
                    c = c[0][0];

                    if((passive_mode && c.ower_type == 3) || (!passive_mode && (c.ower_type == 4 || c.ower_type == 5))){
                        throw Error(lingo.get('cant_use_this_card_in_current_mode'));
                    }
                    if(c.company_id != 0 && c.company_id != company_id){
                        throw Error(lingo.get('this_card_belong_to_another_company'));
                    }
                    if((c.ower_type == 1 || c.ower_type == 2) && route_id == 0)
                        throw Error(lingo.get('please_checkin_first'));
                    if(c.ower_type == 3 && c.bus_type == 1 && route_id != 0) {
                        throw Error(lingo.get('already_checkin'));
                    }
                    if(c.ower_type == 3 && c.bus_type == 2 && route_id == 0){
                        throw Error(lingo.get('already_checkout'));
                    }

                    _fn.r(c, this.parallel());

                    id = c.owner_id;
                    type = c.ower_type == 1 ? 'child' : 'guardian';

                    if(c.ower_type == 2) managers.child.getListChildByGuardianChild(id, this.parallel());
                    else if(c.ower_type == 1) _fn.r([ [{child_id: id}] ], this.parallel());
                    else if(c.ower_type == 3) _fn.r([{bus_type : c.bus_type}], this.parallel());
                    else _fn.r([{id : c.id, ower_type : c.ower_type}], this.parallel());
                },
                function getRoute(e, c, r) {
                    if(e) throw e;

                    if(!r.length || ([1, 2].indexOf(c.ower_type) != -1 && !r[0].length)) throw Error(lingo.get('card_not_exists'));

                    _fn.r(c, this.parallel());
                    _fn.r(r, this.parallel());
                    if(c.ower_type == 3){
                        //managers.route.getPassiveLastAction(company_id, 0, 1, uid, 3, 0, this.parallel());
                        managers.route.getActiveLastAction(uid, this.parallel());
                        _fn.r([[]], this.parallel());
                    }
                    else if(c.ower_type == 1 || c.ower_type == 2) {
                        if(r.length && r[0].length){
                              //managers.route.getPassiveLastAction(company_id, 0, 1, uid, 2, r[0][0].child_id, this.parallel());
                            managers.route.getPassiveLastAction(2,r[0][0].child_id, this.parallel());
                            _fn.r([[]], this.parallel());
                        }
                        else{
                            _fn.r([[]], this.parallel());
                            _fn.r([[]], this.parallel());
                        }
                    }
                    else{
                        managers.route.getPassiveLastAction(r[0].ower_type, r[0].id, this.parallel());
                        managers.route.getBusLastAction(bus_id, this.parallel());
                    }
                },
                function getRoute(e, c, r, ce, psmr) {
                    if(e) throw e;
                    if(ce[0].length && utils.routeTimeValid(new Date(ce[0][0].created_at_time))) {
                        action_repeated = true;
                        throw Error(lingo.get('repeat_tapping'));
                    }

                    _fn.r(c, this.parallel());
                    _fn.r(r, this.parallel());
                    if(psmr.length && psmr[0].length){
                        psmr = psmr[0][0];
                        route_id = psmr.route_trip_id;
                    }
                    if(c.ower_type == 3 && route_id != 0)
                        route_id = 0;
                    card_type = nfc ? 1 : 2;
                    /*1: driver, 2: Child, 3: Guardian, 4: Bus, 5: assitant*/
                    if(c.ower_type == 3){
                        if(role == 'driver')
                            managers.route.insertEventHistoryBasic(route_id, 0, '', 1, uid, 4, c.owner_id, card_type, card_code, company_id, this.parallel());
                        else
                            managers.route.insertEventHistoryBasic(route_id, 0, '', 5, uid, 4, c.owner_id, card_type, card_code, company_id, this.parallel());
                    }
                    else if(c.ower_type == 1 || c.ower_type == 2){
                        var childIds = [], group = this.parallel();
                        r[0].forEach(function (v) {
                            if(!passive_mode)
                                managers.route.insertEventHistoryBasic(route_id, 0, '', 1, uid, 2, v.child_id, card_type, card_code, company_id, group);
                            else
                                managers.route.insertEventHistoryBasic(route_id, 0, '', 4, bus_id, 2, v.child_id, card_type, card_code, company_id, group);
                            childIds.push(v.child_id);
                        });
                    }
                    else{
                        var passive_type = (c.ower_type == 4) ? 1 : 5
                        managers.route.insertEventHistoryBasic(route_id, 0, '', 4, bus_id, passive_type, c.owner_id, card_type, card_code, company_id, this.parallel());
                    }


                    if(c.ower_type == 1 || c.ower_type == 2) managers.child.getListChildByIDs(childIds.join(','), this.parallel());
                    else if(c.ower_type == 3) managers.bus.getBusById(c.owner_id, this.parallel());
                    else managers.bus.getBusById(bus_id, this.parallel());

                },

                function getRouteDetail(e, g, c, r, cl) {
                    if(e) throw e;

                    if(r.length && r[0].length) r = r[0];
                    _fn.r(g, this.parallel());
                    _fn.r(c, this.parallel());
                    _fn.r(r, this.parallel());
                    _fn.r(cl, this.parallel());
                    managers.route.routeTripBasicGetSplitByTripId(r[0].route_trip_id, 100000, 1, 200000, this.parallel());
                },

                function updateChildStatus(e, g, c, r, cl, hs) {
                    if(e) throw e;
                    _fn.r(r, this.parallel());
                    _fn.r(g, this.parallel());
                    _fn.r(cl, this.parallel());
                    var date = _fn.date.format(new Date(), 'YYYY-0MM-0DD');
                    managers.route.routeTripDailyGetInfoById(r[0].route_trip_id, date, this.parallel());
                    if(g.bus_type == 0 && (c.ower_type == 1 || c.ower_type == 2)) {
                        var group = this.group(), action = (r[0].action == 1) ? 1 : 2;
                        c[0].forEach(function(v) {
                            managers.route.routeTripDailyDetailUpdateChildStatusBasic(r[0].route_trip_id, v.child_id, card_type, card_code, 0, action, group());
                        });

                    } else _fn.r([], this.parallel());
                    var arr = [];
                    if(hs.length){
                        for(var i = 0; i < hs[0].length; i ++){
                            var point = hs[0][i];
                            if(point.passive_party_type == 4){
                                var index = arr.indexOf(point.active_party_id);
                                if(index > -1){
                                    arr.splice(index, 1);
                                }
                                else{
                                    arr.push(point.active_party_id);
                                }
                            }
                        }
                    }
                    _fn.r(arr, this.parallel());

                },
                function response(e, r, g, cl, ri, l, ws) {

                    if(e) res.json({success: false, message: e.message, repeated: action_repeated});
                    else {
                        var message = '', action = (g.bus_type == 2) ? 2 : 1, workers = ws.length ? ws : [uid];
                        if(g.bus_type == 0) {
                            message = r[0].action == 1 ? lingo.get('pickup_success') : lingo.get('dropoff_success');
                            utils.routeTripScanPushBasic(message, cl, workers);
                        } else {
                            message = g.bus_type == 1 ? lingo.get('checkin_success') : lingo.get('checkout_success');
                        }

                        var data = {
                            success: true,
                            message: message,
                            repeated: action_repeated
                        };

                        _fn.extend(data, {action: action, routes: r[0], card: g, childList: cl, workers: workers});

                        if(l && l.length) {
                            l.forEach(function(v) {
                                if(v[1]) utils.pushLBAParent(v[1], lingo);
                            });
                        }

                        res.json(data);
                    }
                }
            )
        },

        updateRoutePoint : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var company_id = req.session.company_id;
            var rid = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var child = req.body.child ? _fn.sanitize(_fn.trim(req.body.child)).toInt() : 0;
            var action = req.body.action ? _fn.sanitize(_fn.trim(req.body.action)).toInt() : 1;
            var location = req.body.location ? _fn.sanitize(_fn.trim(req.body.location)).toInt() : 0;

            var now = _fn.date.format(new Date(), 'YYYY-0MM-0DD');

            Step (
                function valid() {

                    _fn.check(rid, lingo.get('route_not_exists')).notEmpty().isInt();
                    _fn.check(child, lingo.get('card_not_exists')).notEmpty();
                    _fn.check(action, lingo.get('route_point_action_invalid')).notEmpty().isInt();

                    managers.route.routeTripGetInfoById(rid, this.parallel());
                    managers.route.routeTripDailyGetInfoById(rid, now, this.parallel());
                    managers.child.getChildById(child, 1, this.parallel());
                },
                function update(e, r, rd, c) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));
                    if(!rd.length || !rd[0].length) throw Error(lingo.get('route_trip_not_start_daily'));
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));

                    r = r[0][0];
                    rd = rd[0][0];
                    c = c[0][0];

                    if(rd.driver_id != uid && rd.assistant_id != uid) throw Error(lingo.get('err_permission_route'));

                    _fn.r(rd, this.parallel());
                    _fn.r(c, this.parallel());

                    //proute_trip_id, plocation_id, pdevice_code, pactive_party_type, pactive_party_id, ppassive_party_type, ppassive_party_id, pcard_type, pcard_code, pcompany_id
                    managers.route.insertEventHistory(rid, location, '', 1, uid, 2, child, 0, '', company_id, this.parallel());
                },
                function getPoint(e, rd, c) {
                    if(e) throw e;

                    managers.route.routeTripDailyDetailUpdateChildStatus(rid, child, 0, '', location, action, this.parallel());
                },
                function remakeTripDaily(e, r) {
                    if(e) throw e;

                    if(r && r.length && r[1]) utils.pushLBAParent(r[1], lingo);
                    var nextLocation = null;
                    if(r && r.length && r[0].length) {
                        nextLocation = r[0][0];
                    }

                    managers.route.routeTripDailyGetInfoById(rid, now, this.parallel());
                    managers.route.routeTripDailyGetSplitByTripId(rid, now, 1, 10, '', '', '', this.parallel());
                },
                function response(e, r, rd) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        r = r.length && r[0].length ? r[0][0] : {};

                        if(r.status == 3) utils.routeTripFinishLBA(r);

                        res.json({
                            success: true,
                            points : rd[0],
                            message: lingo.get('update_point_success')
                        });
                    }
                }
            )
        },

        syncRoutePoint : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var rid = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var date = req.body.date ? _fn.sanitize(_fn.trim(req.body.date.toString())).xss() : null;
            var child = req.body.child ? _fn.sanitize(_fn.trim(req.body.child.toString())).xss() : '';

            date = date || _fn.date.format(new Date(), 'YYYY-0MM-0DD');
            Step (
                function checkInput() {

                    _fn.check(rid, lingo.get('route_not_exists')).notEmpty().isInt();
                    _fn.check(child, lingo.get('err_anonymous')).notEmpty();

                    managers.route.routeTripGetInfoById(rid, this.parallel());
                    managers.route.routeTripDailyGetInfoById(rid, date, this.parallel());
                },
                function syncPoint(e, r, rd) {
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));
                    if(!rd.length || !rd[0].length) throw Error(lingo.get('route_trip_not_start_daily'));

                    r = r[0][0];
                    rd = rd[0][0];

                    if(rd.driver_id != uid && rd.assistant_id != uid) throw Error(lingo.get('err_permission_route'));

                    child = JSON.parse(child);

                    if(!child.length) return this;

                    var ongoing = false, finish = true, group = this.group();
                    child.forEach(function(v) {
                        if(v.ob_done_status == 1) ongoing = true;
                        if(v.al_done_status == 0) finish = false;

                        utils.syncRoutePoint(lingo, uid, v, rd, group());
                    });

                    if(finish) {
                        //Push LBA;
                        utils.routeTripFinishLBA(r);
                    }
                    //Update status for route daily;
                    if(ongoing || finish) managers.route.updateRouteTripDaily(rd.id, rd.route_trip_id, rd.route_trip_date, (finish ? 3 : 2), rd.driver_id, rd.assitant_id, rd.bus_id, rd.plan_date, rd.estimate_from, rd.estimate_to, this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, msg: e.message});
                    else res.json({success: true});
                }
            )
        },

        getRoutePoints : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var rid = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            Step (
                function getRoutePoint(e, r) {
                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");
                    managers.route.routeTripGetPointById(rid, '', '', '', page, numrow, order, desc, '', this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        points : r.length ? r[0] : []
                    })
                }
            )
        },

        getRoutePoint : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var pid = _fn.sanitize(_fn.trim(req.params.id)).xss();
            Step (
                function getRoutePoint(e, r) {
                    managers.route.getRoutePointByID(pid, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        point : r[0][0]
                    })
                }
            )
        },

        updateRoutePointDetail : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var id              = req.body.id ? req.body.id : 0;
            var routeId         = req.body.route_trip_id ? _fn.sanitize(_fn.trim(req.body.route_trip_id)).toInt() : 0;
            var location_ad     = req.body.location_ad ? _fn.sanitize(_fn.trim(req.body.location_ad.toString())).xss() : '';
            var location_name   = req.body.location_name ? _fn.sanitize(_fn.trim(req.body.location_name.toString())).xss() : '';
            var location_x      = req.body.location_x ? _fn.sanitize(_fn.trim(req.body.location_x.toString())).xss() : '';
            var location_y      = req.body.location_y ? _fn.sanitize(_fn.trim(req.body.location_y.toString())).xss() : '';
            var isFutureSche    = req.body.futrSche ? '1' : '0';
            Step (
                function updateRoute(e, r) {
                    managers.route.updateRoutePointDetail(id, location_name, location_ad, location_x, location_y, isFutureSche, this.parallel());

                    //Push LBA to update data to driver/assistant;
                    utils.pushRouteTripDailyChanges(routeId, 'points', this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('update_point_success')
                    })
                }
            )
        },

        updatePointOrder : function(req, res) {
            var lingo       = req.lingo;
            var rid         = req.body.rid ? _fn.sanitize(_fn.trim(req.body.rid.toString())).xss() : 0;
            var point_id    = req.body.point_id ? _fn.sanitize(_fn.trim(req.body.point_id.toString())).xss() : '';
            var prefid      = req.body.prefid ? _fn.sanitize(_fn.trim(req.body.prefid.toString())).xss() : '';

            Step (
                function updateRoutePointOrder(e, r) {
                    managers.route.updateRoutePointOrder(point_id, prefid, this);

                    //Push LBA to update data to driver/assistant;
                    utils.pushRouteTripDailyChanges(rid, 'points', this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('update_point_order_success')
                    })
                }
            )
        },

        addPointInPair : function(req, res) {
            var lingo = req.lingo,
                uid = req.session.uId;
            var route_id                = req.body.route_id ? _fn.sanitize(_fn.trim(req.body.route_id.toString())).xss() : '',
                child_id                = req.body.child_id ? _fn.sanitize(_fn.trim(req.body.child_id.toString())).xss() : '',
                futrSche                = req.body.futrSche ? '1': '0',
                pickup_location_ad      = req.body.pickup_location_ad ? _fn.sanitize(_fn.trim(req.body.pickup_location_ad.toString())).xss() : '',
                pickup_location_name    = req.body.pickup_location_name ? _fn.sanitize(_fn.trim(req.body.pickup_location_name.toString())).xss() : '',
                pickup_location_x       = req.body.pickup_location_x ? _fn.sanitize(_fn.trim(req.body.pickup_location_x.toString())).xss() : '',
                pickup_location_y       = req.body.pickup_location_y ? _fn.sanitize(_fn.trim(req.body.pickup_location_y.toString())).xss() : '',
                drop_location_ad        = req.body.drop_location_ad ? _fn.sanitize(_fn.trim(req.body.drop_location_ad.toString())).xss() : '',
                drop_location_name      = req.body.drop_location_name ? _fn.sanitize(_fn.trim(req.body.drop_location_name.toString())).xss() : '',
                drop_location_x         = req.body.drop_location_x ? _fn.sanitize(_fn.trim(req.body.drop_location_x.toString())).xss() : '',
                drop_location_y         = req.body.drop_location_y ? _fn.sanitize(_fn.trim(req.body.drop_location_y.toString())).xss() : '';
                
            Step (
                function updateRoute(e, r) {
                    managers.route.insertRoutePointInPair(route_id, child_id, pickup_location_name, pickup_location_ad, pickup_location_x, pickup_location_y, drop_location_name, drop_location_ad, drop_location_x, drop_location_y, uid, futrSche, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('update_point_success')
                    })
                }
            )
        },

        startRouteDaily : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var rid = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var now = _fn.date.format(new Date(), 'YYYY-0MM-0DD');

            Step (
                function checkInput() {

                    _fn.check(rid, lingo.get('route_not_exists')).notEmpty().isInt();

                    managers.route.routeTripGetInfoById(rid, this.parallel());
                    managers.route.routeTripDailyGetInfoById(rid, now, this.parallel());
                },
                function startRoute(e, r, rd) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));
                    if(rd.length && rd[0].length) throw Error(lingo.get('route_has_started'));

                    r = r[0][0];
                    if(r.driver_id != uid && r.assitant_id != uid) throw Error(lingo.get('err_permission_route'));

                    _fn.r(r, this.parallel());
                    managers.route.routeTripDailyGenById(r.id, now, 0, this.parallel());
                },
                function getPoints(e, r, rdg) {
                    if(e) throw e;

                    _fn.r(r, this.parallel());
                    managers.route.routeTripDailyGetSplitByTripId(r.id, now, 1, 10, '', '', '', this.parallel());
                    managers.user.getUserById(r.driver_id, this.parallel());
                    managers.route.getRouteTripDailyData(r.id, now, this.parallel());
                },
                function pushLBA(e, r, rd, di, rdd) {
                    if(e) throw e;

                    _fn.r(rd, this.parallel());
                    utils.startRouteDailyLBA(r, rd, rdd, di, now, this.parallel());
                },
                function response(e, r) {
                    if (e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('start_route_daily_success'), points: r[0]});
                }
            )
        },

        getRouteDetail : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var rid = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            var date = req.query.date ? _fn.sanitize(_fn.trim(req.query.date)).xss() : _fn.date.dateString(new Date());

            Step (
                function checkInput() {

                    _fn.check(rid, lingo.get('route_not_exists')).notEmpty().isInt();
                    _fn.check(date, lingo.get('date_invalid')).notEmpty().isDate();

                    if(isNaN(page)) page = 1;
                    numrow = 100;

                    managers.route.routeTripGetInfoById(rid, this);
                },
                function getChild(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));

                    r = r[0][0];
                    if(r.driver_id != uid && r.assitant_id != uid) throw Error(lingo.get('err_permission_route'));

                    var desc = '';
                    order = '';

                    _fn.r(r, this.parallel());
                    managers.route.routeTripDailyGetSplitByTripId(r.id, date, page, numrow, order, desc, '', this.parallel());
                },
                function getPointsPlan(e, r, c) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) {
                        r.inactive = true;
                        managers.route.routeTripGetPointById(r.id, 0, 0, '', page, numrow, '', '', '', this.parallel());
                    }
                    else _fn.r(c, this.parallel());

                    _fn.r(r, this.parallel());
                },
                function response(e, c, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            route  : r,
                            points : c.length ? c[0] : []
                        });
                    }
                }
            )
        },

        getRouteBasicDetail : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var rid = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            var time = req.query.time ? parseInt(req.query.time, 10) : 12;
            Step (
                function checkLastAction() {
                    if(rid == 0){
                        managers.route.getActiveLastAction(uid, this);
                    }
                    else
                        _fn.r([[]], this);
                },
                function checkInput(e, la) {
                    if(e) throw e;
                    if(la.length && la[0].length){
                        la = la[0][0];
                        if(la.action == 1){
                            rid = la.route_trip_id;
                        }
                    }
                    _fn.check(rid, lingo.get('route_not_exists')).notEmpty().isInt();

                    if(isNaN(page)) page = 1;
                    numrow = 100;
                    managers.route.routeTripGetInfoById(rid, this);
                },
                function getChild(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));

                    r = r[0][0];
                    var desc = '';

                    _fn.r(r, this.parallel());
                    managers.route.routeTripBasicGetSplitByTripId(r.id, time, 1, 2000, this.parallel());
                    managers.route.routeTripGetCurrPassByTripId(r.id, this.parallel());
                },
                function response(e, r, c, cp) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        if(c.length){
                            var points = c[0], point, arrTmp = [], lstOnboard = [];
                            if(cp[0] && cp[0].length)
                                cp[0].forEach(function(v){
                                    lstOnboard.push(v.ob_id);
                                });
                            points.sort(function(a, b) {
                                var timeA = new Date(a.at).getTime();
                                var timeB = new Date(b.at).getTime();
                                return timeB - timeA;
                            });

                            for(var i = 0; i < points.length; i++){
                                point = points[i];
                                points[i].onboard = 0;
                                if(point.action == 1 && lstOnboard.indexOf(point.party_id) != -1 && arrTmp.indexOf(point.party_id) == -1){
                                    arrTmp.push(point.party_id);
                                    points[i].onboard = 1;
                                }
                            }
                            points.reverse();

                        }
                        res.json({
                            success: true,
                            route  : r,
                            points : c.length ? points : [],
                            points_order : c.length ? points : []
                        });
                    }
                }
            )
        },

        getRoutePassiveDetail : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var rid = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            var time = req.query.time ? parseInt(req.query.time, 10) : 12;
            Step (
                function checkLastAction() {
                    managers.route.getBusLastAction(rid, this);
                },
                function checkInput(e, la) {
                    if(e) throw e;
                    if(la.length && la[0].length){
                        la = la[0][0];
                        rid = la.route_trip_id;
                    }
                    _fn.check(rid, lingo.get('route_not_exists')).notEmpty().isInt();

                    if(isNaN(page)) page = 1;
                    numrow = 100;
                    managers.route.routeTripGetInfoById(rid, this);
                },
                function getChild(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));

                    r = r[0][0];
                    var desc = '';

                    _fn.r(r, this.parallel());
                    managers.route.routeTripBasicGetSplitByTripId(r.id, time, 1, 2000, this.parallel());
                    managers.route.routeTripGetCurrPassByTripId(r.id, this.parallel());
                },
                function response(e, r, c, cp) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        if(c.length){
                            var points = c[0], point, arrTmp = [], lstOnboard = [];
                            if(cp[0] && cp[0].length)
                                cp[0].forEach(function(v){
                                    lstOnboard.push(v.ob_id);
                                });
                            points.sort(function(a, b) {
                                var timeA = new Date(a.at).getTime();
                                var timeB = new Date(b.at).getTime();
                                return timeB - timeA;
                            });

                            for(var i = 0; i < points.length; i++){
                                point = points[i];
                                points[i].onboard = 0;
                                if(point.action == 1 && lstOnboard.indexOf(point.party_id) != -1 && arrTmp.indexOf(point.party_id) == -1){
                                    arrTmp.push(point.party_id);
                                    points[i].onboard = 1;
                                }
                            }
                            points.reverse();

                        }
                        res.json({
                            success: true,
                            route  : r,
                            points : c.length ? points : [],
                            points_order : c.length ? points : []
                        });
                    }
                }
            )
        },

        getRouteDetailPage : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var rid = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;

            Step (
                function checkInput() {
                    //_fn.check(rid, lingo.get('route_not_exists')).notEmpty().isInt();
                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;
                    managers.route.routeTripGetInfoById(rid, this);
                },
                function getChild(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    _fn.r(r, this.parallel());
                    managers.route.routeTripGetPointById(r[0][0].id, '', '', '', page, numrow, order, desc, '', this.parallel());
                    if(r[0][0].schedule_id){
                        managers.route.routeScheduleGetById(r[0][0].schedule_id, this.parallel());
                    }
                    else{
                        _fn.r([], this.parallel());
                    }

                },
                function getSchedule(e, r, c, s){
                    if(e) throw e;
                    _fn.r(r, this.parallel());
                    _fn.r(c, this.parallel());
                    _fn.r(s, this.parallel());
                    if(s.length){
                        if(s[0][0].schedule_type == 'Weekly')
                            managers.route.routeScheduleWeekGetById(s[0][0].id, this.parallel());
                        else
                            managers.route.routeScheduleDateGetById(s[0][0].id, this.parallel());
                    }
                    else{
                        _fn.r([], this.parallel());
                    }
                },
                function response(e, r, c , s, sdt) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            route  : r[0][0],
                            points : c.length ? c[0] : [],
                            schedule: s.length ? s[0] : [],
                            schedule_detail: sdt.length ? sdt[0] : []
                        });
                    }
                }
            )
        },

        workerLocation: function(req, res) {
            var uid = req.session.uId;
            var companyId = req.session.company_id;
            var location = req.body.location || {};
            var route = req.body.route ? _fn.sanitize(_fn.trim(req.body.route)).xss() : '{}';
            var date = _fn.date.format(new Date(), 'YYYY-0MM-0DD');

            Step(
                function valid() {
                    try {
                        route = JSON.parse(route);
                    } catch (e) {
                        throw e;
                    }

                    if(!location.longitude || !location.latitude) throw Error('Invalid location info.');

                    managers.config.getBcConfig(companyId, this.parallel());
                    if(!route || !route.id || !route.driverInfo) managers.user.getUserById(uid, this.parallel());
                },
                function getParents(e, cfg, u) {
                    if(e) throw e;

                    cfg = cfg || {};

                    if(u && u.length && u[0].length) {
                        u = u[0][0];

                        if(!route) route = {};

                        route.driverInfo = {
                            id: u.id,
                            name: u.fullname || u.username,
                            email: u.email,
                            avatar: u.image
                        };
                    }

                    if(!route || !route.id || cfg.parent_map == 0) return [];

                    managers.child.getListParentByRouteTrip(route.id, date, this);
                },
                function pushLBA(e, r) {
                    if(e) throw e;

                    var data = {
                        route: route,
                        pos: {
                            long: location.longitude,
                            lat: location.latitude
                        }
                    };

                    if(r && r.length && r[0].length) {
                        var parentMap = {};
                        r[0].forEach(function(v) {
                            if(!v.parent_id) return true;

                            if(!parentMap[v.parent_id]) parentMap[v.parent_id] = [];

                            parentMap[v.parent_id].push({
                                child_id: v.child_id,
                                child_name: v.child_name
                            });
                        });

                        var i;
                        for(i in parentMap) {
                            if(!parentMap.hasOwnProperty(i)) continue;

                            managers.LBA.pushRoom('wcl#user#' + i, 'driver:location:pull', _fn.extend({}, data, {childs: parentMap[i]}));
                        }
                    }

                    var companyId = route.company_id || req.session.company_id;
                    if(companyId) managers.LBA.pushRoom('wcl#comp#' + companyId, 'driver:location:pull', data);

                    return this;
                },
                function response(e, r) {
                    res.json({success: e ? false : true});
                }
            );
        },

        workerManualCheckInOut: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var company_id = req.session.company_id;
            var lat = req.body.lat ? _fn.sanitize(_fn.trim(req.body.lat.toString())).xss() : '';
            var long = req.body.long ? _fn.sanitize(_fn.trim(req.body.long.toString())).xss() : '';

            Step(
                function saveLocation() {
                    utils.saveRouteLocation(req, lat, long, this);
                },
                function insert(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('err_anonymous'));

                    var partyType = (role == config.ROLE.DRIVER ? 1 : 5);
                    managers.route.insertEventHistory(0, r[0][0].id, '', partyType, uid, partyType, uid, 0, '', company_id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, action: r[0][0].action, message: (r[0][0].action == 1 ? lingo.get('checkin_success') : lingo.get('checkout_success'))})
                }
            );
        },

        workerManualCheckOutBasic: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var company_id = req.session.company_id;
            var rid = req.body.rid ? _fn.sanitize(_fn.trim(req.body.rid)).toInt() : 0;

            Step(
                function saveLocation() {
                    _fn.check(rid, lingo.get('please_checkin_first')).notEmpty().isInt();
                    managers.route.routeTripGetInfoById(rid, this);
                },
                function insert(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('route_not_exists'));

                    r = r[0][0];

                    managers.route.insertEventHistoryBasic(r.id, 0, '', 1, uid, 4, r.bus_id, 0, '', company_id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, action: 2, message: lingo.get('checkout_success')})
                }
            );
        },

        checkChildOnBus: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var company_id = req.session.company_id;
            var rid = req.body.rid ? _fn.sanitize(_fn.trim(req.body.rid)).toInt() : 0;

            Step(
                function count() {
                    if(rid) managers.route.countCurrentChildOnRouteTrip(rid, this);
                    else _fn.r(null, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        var count = 0;
                        if(r && r.length && r[0].length) count = r[0][0].num_child;

                        res.json({success: true, count: count})
                    }
                }
            );
        },

        getDriverNotifications: function(req, res) {
            var uid = req.session.uId;
            Step(
                function saveLocation() {
                    managers.route.getNotifications(uid, 123, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, notifications: r})
                }
            );
        },

        pushNotificationTest: function (req, res) {
            var lingo = req.lingo;
            var parentId = req.query.pid ? req.query.pid : 0;

            Step(
                function pushNotification() {
                    var notification = {
                        to: parentId || 42,
                        message: 'SBA test notification'
                    };

                    utils.pushNotification(notification, lingo, this);
                },
                function response(e, r) {
                    if(e) res.send(e.message);
                    else res.send(r);
                }
            );
        }

    };

    var utils = {
        updateRoutePoint : function(lingo, uid, point, routeDaily, date, cb) {

            Step (
                function getPoint() {

                    if(!point.childData) managers.child.getChildById(point.child_id, 1, this.parallel());
                    else _fn.r([[point.childData]], this.parallel());

                    managers.route.routeTripDailyDetailGetInfo(routeDaily.id, point.child_id, this.parallel());
                    managers.child.getListParentChildByType(uid, point.child_id, '1,2', this.parallel());
                },
                function updatePoint(e, c, td, pc) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));
                    if(!td.length || !td[0].length) throw Error(lingo.get('route_trip_dont_have_children'));

                    c = c[0][0];
                    td = td[0][0];

                    if(td.ob_done_status == 1 && td.al_done_status == 1) throw Error(lingo.get('children_has_been_done_route_trip_today'));
                    var act = td.ob_done_status == 0 ? 1 : 2;

                    utils.routeTripDailyUpdateStatusDone(lingo, td, act, c, pc, this.parallel());
                    utils.autoUpdateAlightStatusForPrevBus(lingo, routeDaily.route_trip_id, c, pc, date, routeDaily.id, this.parallel());
                },
                cb
            )
        },

        startRouteDailyLBA: function(r, rd, rdd, di, date, cb) {
            Step(
                function getBusInfo() {
                    managers.bus.getBusById(r.bus_id, this.parallel());

                    if(r.assitant_id && r.assitant_id != '') managers.user.getUserById(r.assitant_id, this.parallel());
                    else _fn.r([], this.parallel());

                    if(!di || !di.length || !di[0].length) managers.user.getUserById(r.driver_id, this.parallel());
                },
                function pushLBA(e, bi, ai, _di) {
                    if(e) throw e;

                    if(!di || !di.length || !di[0].length) di = _di;

                    r.busInfo = {name: r.name};
                    if(bi.length && bi[0].length) {
                        bi = bi[0][0];
                        r.busInfo = {id: bi.id, name: bi.license_plate_no};
                    }

                    //Push LBA;
                    var routeData = {child: [], guardian: [], points: (rd && rd.length ? rd[0] : []), date: date};

                    if(rdd && rdd.length) {
                        routeData.child = rdd[3] || []; //Child list;

                        if(rdd[4].length) { //Guardian list;
                            rdd[4].forEach(function(v) {
                                v.child_ids = v.child_ids.split(',');

                                routeData.guardian.push(v);
                            });
                        }
                    }

                    var uids = [r.driver_id];
                    if(r.assitant_id && r.assitant_id != '') uids.push(r.assitant_id);

                    if(di && di.length && di[0].length) {
                        di = di[0][0];

                        r.driverInfo = {
                            id: di.id,
                            name: di.fullname || di.username,
                            email: di.email,
                            avatar: di.image
                        };
                    }

                    if(ai.length && ai[0].length) {
                        ai = ai[0][0];

                        r.assistant = ai.fullname || ai.username;
                    }

                    uids.forEach(function(v) {
                        managers.LBA.pushRoom('wcl#user#' + v, 'route:trip:start', r, routeData);
                    });

                    return this;
                },
                cb
            );
        },

        routeTripScanPush : function(message, child, location, uids){
            uids.forEach(function(v) {
                managers.LBA.pushRoom('wcl#user#' + v, 'route:trip:scan', message, child, location);
            });
        },

        routeTripScanPushBasic : function(message, child, uids){
            uids.forEach(function(v) {
                managers.LBA.pushRoom('wcl#user#' + v, 'route:trip:scan:basic', message, child);
            });
        },

        routeTripFinishLBA: function(r) {
            if(!r) return false;

            var uids = [r.driver_id];
            if(r.assitant_id && r.assitant_id != '') {
                var aids = r.assitant_id.split(',');

                aids.forEach(function(v) {
                    if(v && v != 0) uids.push(v);
                });
            }

            uids.forEach(function(v) {
                managers.LBA.pushRoom('wcl#user#' + v, 'route:trip:finish', r);
            });

            return true;
        },

        pushLBAParent: function(notifications, lingo) {
            if(!notifications || !notifications.length) return false;

            notifications.forEach(function(v) {
                var data = {
                    child: v.for_id,
                    id: v.id,
                    status: v.status,
                    message: v.message,
                    time: moment(v.at).fromNow()
                };

                if(v.to && v.to != '') managers.LBA.pushRoom('wcl#user#' + v.to, 'child:notifications', data);

                utils.pushNotification(v, lingo, _fn.noop);
            });
        },

        pushNotification: function(notification, lingo, cb) {
            var deviceTokens = [];
            var retry_times = 4;

            var sender = new gcm.Sender(config.GOOGLE_API_KEY);
            var message = new gcm.Message();

            message.addData('title', lingo.get('notification_title'));
            message.addData('message', notification.message);
            message.addData('sound', 'notification');

            message.collapseKey = 'sba_notification'; //grouping messages
            message.delayWhileIdle = true; //delay sending while receiving device is offline
            message.timeToLive = 12 * 60 * 60; //the number of seconds to keep the message on the server if the device is offline

            Step(
                function getToken() {
                    if(!notification.to) return [];

                    managers.user.getUserDeviceTokens(notification.to, this);
                },
                function sendMessage(e, r) {
                    if(e || !r.length || !r[0].length) throw Error('Could not find Device Token ID.');

                    r[0].forEach(function(v) {
                        deviceTokens.push(v.key);
                    });

                    var self = this;
                    sender.send(message, deviceTokens, retry_times, function (e, result) {

                        _fn.r({error: e, code: result, id: r}, self.parallel());
                    });
                },
                cb
            );
        },

        syncRoutePoint : function(lingo, uid, child, route, cb) {

            Step (
                function getPoint() {

                    managers.child.getChildById(child.id, 1, this.parallel());
                    managers.route.routeTripDailyDetailGetInfo(route.id, child.id, this.parallel());
                    managers.child.getListParentChildByType(uid, child.id, '1,2', this.parallel());
                },
                function updatePoint(e, c, td, pc) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) return this;
                    if(!td.length || !td[0].length) return this;

                    c = c[0][0];
                    td = td[0][0];

                    if(td.ob_done_status == 1 && td.al_done_status == 1) return this;

                    if(td.ob_done_status == 0 && child.ob_done_status == 1) {
                        utils.routeTripDailyUpdateStatusDone(lingo, td, 1, c, pc, this.parallel());
                    }

                    if(td.al_done_status == 0 && child.al_done_status == 1) {
                        utils.routeTripDailyUpdateStatusDone(lingo, td, 2, c, pc, this.parallel());
                    }
                },
                cb
            )
        },

        pushRouteTripDailyChanges: function(rid, type, cb) {
            var date = _fn.date.format(new Date(), 'YYYY-0MM-0DD');

            Step(
                function getRoute() {
                    managers.route.routeTripGetInfoById(rid, this);
                },
                function validate(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length)  throw Error('Break');

                    r = r[0][0];

                    _fn.r(r, this.parallel());
                    if(type == 'points') managers.route.routeTripDailyGetInfoById(rid, date, this.parallel());
                },
                function getTripDailyData(e, r, rd) {
                    if(e) throw e;

                    _fn.r(r, this.parallel());

                    if(rd && rd.length && rd[0].length) {
                        rd = rd[0][0];

                        _fn.r(rd, this.parallel());
                        if(rd.status != 3) managers.route.routeTripDailyGetDetailByTripId(r.id, date, 1, 1000, '', '', '', this.parallel());
                    }
                },
                function regenerateTripDailyData(e, r, rd, rdd) {
                    if(e) throw e;

                    _fn.r(r, this.parallel());
                    if(rd) managers.route.routeTripDailyGenById(r.id, date, 1, this.parallel());
                    if(rdd && rdd.length && rdd[0].length) _fn.r(rdd[0], this.parallel());
                },
                function updateRouteDailyData(e, r, rd, rdd) {
                    if(e) throw e;

                    _fn.r(r, this.parallel());

                    if(rd && rd.length && rd[0].length) {
                        _fn.r(rd[0][0], this.parallel());

                        var rdId = rd[0][0].id, group = this.group();

                        if(rdd && rdd.length) {
                            rdd.forEach(function(v) { //Push old status to new generate items;
                                managers.route.routeTripDailyDetailManualUpdateStatus(rdId, v.child_id, v.ob_at, v.ob_remind_status, v.ob_remind_notify_id, v.ob_ack_status, v.ob_done_status, v.ob_done_notify_id, v.al_at, v.al_remind_status, v.al_remind_notify_id, v.al_ack_status, v.al_done_status, v.al_done_notify_id, v.status, group());
                            });
                        }
                    }
                },
                function reloadData(e, r, rd) {
                    if(e) throw e;

                    _fn.r(r, this.parallel());

                    if(rd) {
                        managers.route.routeTripDailyGetSplitByTripId(r.id, date, 1, 10, '', '', '', this.parallel());
                        managers.user.getUserById(r.driver_id, this.parallel());
                        managers.route.getRouteTripDailyData(r.id, date, this.parallel());
                    } else {
                        r.inactive = true;
                        managers.route.routeTripGetPointById(r.id, 0, 0, '', 1, 10, '', '', '', this.parallel());
                    }
                },
                function pushLBA(e, r, rd, di, rdd) {
                    if(e) throw e;

                    var uids = [r.driver_id];
                    if(r.assitant_id && r.assitant_id != '') {
                        var aids = r.assitant_id.split(',');

                        aids.forEach(function(v) {
                            if(v && v != 0) uids.push(v);
                        });
                    }

                    var data = {route  : r, points: rd.length ? rd[0] : []};
                    uids.forEach(function(v) {
                        managers.LBA.pushRoom('wcl#user#' + v, 'route:trip:sync', data);
                    });

                    if(di && rdd) utils.startRouteDailyLBA(r, rd, rdd, di, date, this.parallel());

                    return this;
                },
                function response(e, r) {
                    return this;
                },
                cb
            );
        },

        autoUpdateAlightStatusForPrevBus: function(lingo, routeId, c, pc, date, currentRouteId, cb) {
            Step(
                function getDailyRoute() {
                    managers.route.getRouteTripDailyForChild(routeId, c.id, date, this.parallel());

                    //Get config;
                    managers.config.getConfigAll(this.parallel());
                },
                function update(e, r, cfg) {
                    if(e) throw e;

                    cfg = cfg[0][0];
                    if(r.length && r[0].length > 1) {
                        var time = parseInt(cfg.auto_alight_after || 30, 10) * 60 * 1000;

                        r[0].forEach(function(v) {
                            if(v.route_trip_daily_id != currentRouteId && v.ob_done_status == 1 && v.al_done_status == 0) {
                                setTimeout(function(td, action, c, pc) {
                                    utils.routeTripDailyUpdateStatusDone(lingo, td, action, c, pc, _fn.noop);
                                }, time, v, 2, c, pc);
                            }
                        });
                    }

                    return this;
                },
                function response(e, r) {//Do not show error;
                    return this;
                },
                cb
            );
        },

        routeTripDailyUpdateStatusDone: function(lingo, td, action, c, pc, cb) {
            Step(
                function update() {
                    managers.route.routeTripDailyDetailUpdateDoneStatus(td.id, 1, '', 0, action, this);
                },
                function pushLBA(e, r) {
                    if(e) throw e;

                    //if(pc.length) {
                    //
                    //    var msg = _fn.str.f(lingo.get('child_notification_act_' + action), {
                    //        child: c.name,
                    //        location: action == 1 ? td.ob_location_name : td.al_location_name,
                    //        time: _fn.date.format(new Date(), 'hh:mm'),
                    //        action: action
                    //    });
                    //
                    //    pc[0].forEach(function(v) {
                    //
                    //        managers.LBA.pushRoom('wcl#user#' + v.parent_id, 'child:notifications', msg);
                    //    });
                    //}

                    return this;
                },
                cb
            );
        },

        saveRouteLocation: function(req, lat, long, cb) {
            var uid = req.session.uId;
            var company_id = req.session.company_id;

            Step(
                function getLocationsAround() {
                    managers.card.getLocationAround(lat, long, 0.001, 0, this);
                },
                function getAdress(e, r) {
                    if(e) throw e;
                    _fn.r(r, this.parallel());
                    var min_distance = 11, locationId = 0;
                    var locations = r[0] || [];

                    if (locations.length) {
                        locations.forEach(function(v) {
                            var lat1 = v.X;
                            var long1 = v.Y;
                            var R = 6371000; // metres
                            var φ1 = lat * Math.PI / 180;
                            var φ2 = lat1 * Math.PI / 180;
                            var Δφ = (lat1 - lat) * Math.PI / 180;
                            var Δλ = (long1 - long) * Math.PI / 180;

                            var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
                                Math.cos(φ1) * Math.cos(φ2) *
                                    Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
                            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                            var d = R * c;

                            if (d < min_distance) {
                                min_distance = d;
                                locationId = v.id;
                            }
                        });

                        if (locationId)
                            _fn.r({address:'',name:''}, this.parallel());
                        else
                            utils.getLocationAddress(lat, long, this.parallel());

                    } else {
                        utils.getLocationAddress(lat, long, this.parallel());
                    }
                    _fn.r(locationId, this.parallel());
                },
                function save(e, r, l, lid) {
                    if(e) throw e;
                    var locations = r[0] || [];
                    if(!l.name || l.name == ''){l.name = 'TEMP_LOCATION';l.address = 'TEMP_ADDRESS';}

                    if (locations.length) {
                        if (lid) _fn.r([[{id: lid}]], this);
                        else managers.card.insertLocation(company_id, uid, lat, long, l.name, l.address, this);

                    } else {

                        managers.card.insertLocation(company_id, uid, lat, long, l.name, l.address, this);
                    }
                },
                cb
            );
        },

        routeTimeValid: function(lastUpdated, now) {
            if(!now) now = new Date();

            if(now.getTime() < lastUpdated.getTime()) now.setHours(now.getHours() + 1);

            if((now.getTime() - lastUpdated.getTime()) < 60000) return true;

            return false;
        },

        getLocationAddress : function(lat, long, cb) {
            Step(
                function getLocationsAround() {
                    request(config.BAIDU_ADDRESS_API + '&location='+lat+','+long+'&radius=2000&ak='+config.BAIDU_API_KEY, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            body = JSON.parse(body);
                            if(body.message == 'ok'){
                                if(body.results.length){
                                    _fn.r({address:body.results[0].address,name:body.results[0].name}, cb);
                                }
                                _fn.r({address:'',name:''}, cb);
                            }
                            _fn.r({address:'',name:''}, cb);
                        }
                        _fn.r({address:'',name:''}, cb);
                    });
                },cb
            )
        }
    };

})(module.exports, require);
