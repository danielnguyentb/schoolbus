/**
 * Created by AnhNguyen
 */

(function (ns, r) {
    "use strict";


    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'auth';

    var Step = r("step");
    var crypto = r("crypto");
    var reCaptcha;
    var fs = require('fs');
    ns.init = function (app, _db, fn, mn, i18n) {

        db = _db;
        _fn = fn;
        managers = mn;

        reCaptcha = new _fn.reCaptcha(config.reCaptcha.PUBLIC, config.reCaptcha.PRIVATE);

        var apiRoutes = app.get('wcl:api:routes');

        app.route('/').get(routes.index);
        app.route('/').post(_fn.getBundle(locale), _fn.auth.mustLoggedOut, routes.login);
        app.route('/logout').get(_fn.getBundle(locale), _fn.auth.check, routes.logout);
        app.route('/register').post(_fn.getBundle(locale), _fn.auth.mustLoggedOut, routes.register);
        app.route('/register/active').get(_fn.getBundle(locale), routes.active);
        app.route('/forgot').post(_fn.getBundle(locale), _fn.auth.mustLoggedOut, routes.forgot);
        app.route('/forgot/change').post(_fn.getBundle(locale), _fn.auth.mustLoggedOut, routes.changePass);
        app.route('/profile').get(_fn.getBundle(locale), _fn.auth.check, routes.getProfile);
        app.route('/profile').post(_fn.getBundle(locale), _fn.auth.check, routes.updateProfile);
        app.route('/profile/password').post(_fn.getBundle(locale), _fn.auth.check, routes.updatePassword);
        app.route('/profile/company').post(_fn.getBundle(locale), _fn.auth.check, routes.updateCompany);
        app.route('/profile/upl').post(
            _fn.getBundle(locale),
            _fn.auth.check,
            _fn.uploads.processImg,
            managers.privateData.avatarImgUpload,
            routes.handleUploadAvatar
        );

        apiRoutes.route('/profile/password').post(_fn.getBundle(locale), _fn.auth.apiAuth, routes.updatePassword);
    };

    var routes = {

        index: function (req, res) {
            var ses = req.session;


            if (req.query.locale) {
                var locale = req.query.locale;
                if (config.LOCALES.indexOf(locale) === -1){

                    locale = 'en-US';
                }

                res.cookie('locale', locale);
                return res.redirect('/');
            }

            res.render('app/index', {
                title: 'School Bus',
                uData: {
                    uId   : ses.uId,
                    uname : ses.uname,
                    email : ses.email,
                    perms : ses.roles,
                    _role : ses._role,
                    avatar: (ses.avatar && ses.avatar != '') ? ses.avatar : config.DEFAULT_AVATAR,
                    company_id: ses.company_id,
                    bc_mode: ses.bc_mode || ["1"]
                }
            });
        },

        handleUploadAvatar: function (req, res) {

            var lingo = req.lingo;
            var file = req.uplFile;
            delete(file.tmpfile);

            var data = {
                success : true,
                fileinfo: file,
                link    : '/avatar/' + req.session.uId + '.' + file.hash,
                message : lingo.get('upload_success')
            };
            res.send(JSON.stringify(data), {
                'Content-Type': 'text/plain'
            }, 200);
        },

        forgot: function (req, res) {

            var lingo = req.lingo;
            var email = _fn.sanitize(_fn.trim(req.body.email)).xss();

            Step(
                function checkEmail() {

                    managers.user.checkUserExist(0, 2, email, this.parallel());
                    crypto.randomBytes(48, this.parallel());
                    reCaptcha.verify({
                        response: req.body.captcha
                    }, this.parallel());
                },
                function createToken(e, r, g) {
                    if (e) throw e;
                    if (!r.length || !r[0].length) throw Error(lingo.get('err_invalid_email'));

                    var token = g.toString('base64').substring(0, 8);
                    var cipher = crypto.createCipher('aes-256-cbc', email);
                    var encrypted = cipher.update(token, 'utf8', 'base64');
                    encrypted = cipher.final('base64');

                    r = r[0][0];
                    _fn.r(_fn.sanitize(encrypted).entityEncode(), this.parallel());
                    managers.user.insertResetToken(r.email, crypto.createHash('sha256').update(token).digest("hex"), this.parallel());
                },
                function emailToUser(e, r, g) {
                    if (e) res.send({success: false, message: e.message});
                    else {

                        _fn.email.systemMail(email, lingo.get('reset_email_title'), _fn.str.f(lingo.get('reset_email'), {
                            reset_url: req.protocol + '://' + req.headers.host + '/#!/forgot?email=' + email + '&token=' + r
                        }));

                        res.send({success: true, message: lingo.get('reset_success')});
                    }
                }
            )
        },

        updateCompany: function (req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var name = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var addr = req.body.address ? _fn.sanitize(_fn.trim(req.body.address)).xss() : '';

            Step(
                function validInput() {

                    _fn.check(name, lingo.get('invalid_company_name')).notEmpty().len(3);

                    managers.user.getUserCompanyByType(uid, 3, this);
                },
                function updateComp(e, r) {
                    if (e) throw e;
                    if (!r.length || !r[0].length) throw Error(lingo.get('company_invalid'));

                    r = r[0][0];
                    managers.comp.updateCompany(r.id, name, r.city_id, r.district_id, addr, r.status, r.mode, this);
                },
                function response(e, r) {
                    if (e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('update_company_success')});
                }
            )
        },

        updatePassword: function (req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var old_pass = req.body.old_password ? _fn.sanitize(_fn.trim(req.body.old_password)).xss() : '';
            var new_pass = req.body.new_password ? _fn.sanitize(_fn.trim(req.body.new_password)).xss() : '';
            var renew_pass = req.body.renew_password ? _fn.sanitize(_fn.trim(req.body.renew_password)).xss() : '';

            Step(
                function checkInput() {

                    _fn.check(old_pass, lingo.get('err_old_password')).notEmpty().len(3);
                    _fn.check(new_pass, lingo.get('err_new_password')).notEmpty().len(3);

                    old_pass = crypto.createHash('sha256').update(old_pass).digest("hex");
                    new_pass = crypto.createHash('sha256').update(new_pass).digest("hex");

                    managers.user.getUserLogIn(req.session.uname, old_pass, this);
                },
                function updatePass(e, r) {
                    if (e) throw e;
                    if (!r.length || !r[0].length) throw Error(lingo.get('err_old_password'));

                    managers.user.updatePassword(uid, new_pass, this);
                },
                function response(e, r) {
                    if (e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('change_password_success')});
                }
            )
        },

        updateProfile: function (req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var email = req.body.email ? _fn.sanitize(_fn.trim(req.body.email)).xss() : '';
            var mobile = req.body.mobile_phone ? _fn.sanitize(_fn.trim(req.body.mobile_phone)).xss() : '';
            var home = req.body.home_phone ? _fn.sanitize(_fn.trim(req.body.home_phone)).xss() : '';
            var office = req.body.office_phone ? _fn.sanitize(_fn.trim(req.body.office_phone)).xss() : '';
            var image = req.body.image ? _fn.sanitize(_fn.trim(req.body.image)).xss() : '';
            var fullname = req.body.fullname ? _fn.sanitize(_fn.trim(req.body.fullname)).xss() : '';
            var locale = req.body.locale ? _fn.sanitize(_fn.trim(req.body.locale)).xss() : '';

            if (config.LOCALES.indexOf(locale) === -1)
                locale = 'en-US';

            Step(
                function valid() {
                    _fn.check(email, lingo.get('err_invalid_email')).notEmpty().isEmail();

                    _fn.doAsync(this);
                },
                function checkInput(e, r) {
                    if(e) throw e;

                    managers.user.getUserById(uid, this.parallel());
                    managers.user.checkUserExist(uid, 2, email, this.parallel());

                    if (mobile != '') managers.user.checkUserExist(uid, 3, mobile, this.parallel());
                    else _fn.r([[]], this.parallel());
                },
                function updateProfile(e, u, r, me) {
                    if (e) throw e;

                    if (r.length && r[0].length) throw Error(lingo.get('err_exists_email'));
                    if (me.length && me[0].length) throw Error(lingo.get('err_mobile_phone_exists'));

                    u = u[0][0];

                    managers.user.updateProfile(u.id, u.username, email, mobile, home, office, image, fullname, locale, 12, this);
                },
                function response(e, r) {
                    if (e) res.json({success: false, message: e.message});
                    else {

                        req.session.email = email;
                        req.session.avatar = (image != '') ? image : config.DEFAULT_AVATAR;
                        req.session.save();

                        res.json({success: true, message: lingo.get('update_profile_success')});
                    }
                }
            )
        },

        getProfile: function (req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;

            Step(
                function getUser() {

                    managers.user.getUserById(uid, this.parallel());
                    managers.user.getUserCompanyByType(uid, 3, this.parallel());
                },
                function getCompany(e, u, r) {
                    if (e) throw e;
                    if (!u.length || !u[0].length) throw Error(lingo.get('err_anonymous'));

                    u = u[0][0];
                    if (u.image == '') u.image = config.DEFAULT_AVATAR;
                    if(!u.locale||u.locale=='') u.locale='en-US';
                    _fn.r(u, this.parallel());
                    if (r.length && r[0].length) _fn.r(r[0][0], this.parallel());
                    else _fn.r({}, this.parallel());
                },
                function response(e, u, c) {
                    if (e) res.json({error: true, message: e.message});
                    else res.json({
                        error: false,
                        user : u,
                        comp : c
                    });
                }
            )
        },

        changePass: function (req, res) {

            var lingo = req.lingo;
            var token = _fn.sanitize(_fn.trim(req.body.token)).xss();
            var email = _fn.sanitize(_fn.trim(req.body.email)).xss();
            var pass = _fn.sanitize(_fn.trim(req.body.password)).xss();
            var rpass = _fn.sanitize(_fn.trim(req.body.repassword)).xss();

            Step(
                function checkInput() {

                    _fn.check(token, lingo.get('err_valid_token')).notEmpty().len(3, 255);
                    _fn.check(email, lingo.get('err_invalid_email')).notEmpty().isEmail();
                    if (pass != rpass) {
                        throw Error(lingo.get('err_rpass_missmatch'));
                    }

                    var decipher = crypto.createDecipher('aes-256-cbc', email);
                    var decrypted = decipher.update(token, 'base64', 'utf8');
                    decrypted = decipher.final('utf8');

                    token = crypto.createHash('sha256').update(decrypted).digest("hex");
                    return this;
                },
                function checkToken(e, r) {
                    if (e) throw e;

                    managers.user.checkUserExist(0, 2, email, this.parallel());
                    managers.user.getUserByToken(email, token, this.parallel());
                },
                function updatePassword(e, u, r) {
                    if (e) throw e;
                    if (!u.length || !u[0].length) throw Error(lingo.get('err_invalid_email'));
                    if (!r.length || !r[0].length) throw Error(lingo.get('err_valid_token'));

                    u = u[0][0];
                    r = r[0][0];

                    var now = new Date();
                    if (now - (new Date(r.created_at)) > 3 * 60 * 60 * 1000) throw Error(lingo.get('err_sesion_token'));

                    var newPass = crypto.createHash('sha256').update(pass).digest("hex");
                    managers.user.updateUserPasswordWithToken(u.id, email, newPass, this);
                },
                function response(e, r) {
                    if (e) res.send({success: false, message: e.message});
                    else res.send({success: true, message: lingo.get('change_password_success')});
                }
            );
        },

        active: function (req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var email = req.query.email ? req.query.email : '';
            var token = req.query.token ? req.query.token : '';

            email = _fn.sanitize(_fn.trim(email)).xss();
            token = _fn.sanitize(_fn.trim(token)).entityDecode();

            Step(
                function getResetToken() {

                    managers.user.getUserByToken(email, crypto.createHash('sha256').update(token).digest("hex"), this);
                },
                function getUser(e, r) {
                    if (e) throw e;
                    if (!r.length || !r[0].length) throw Error(lingo.get('err_valid_token'));

                    r = r[0][0];
                    managers.user.userGetByEmail(r.key, this);
                },
                function activeUsers(e, r) {
                    if (e) throw e;
                    if (!r.length || !r[0].length) throw Error(lingo.get('err_invalid_email'));

                    r = r[0][0];

                    _fn.r(r, this.parallel());
                    managers.user.updateActiveUser(r.id, this.parallel());
                },
                function response(e, u, r) {
                    if (e) {
                        req.flash('error_msg', e.message);
                        res.redirect('/');
                    } else {
                        req.flash('success_msg', lingo.get('active_success'));
                        if (req.session.uId) {
                            req.session.destroy();
                            res.clearCookie(config.session.key, {
                                path: '/'
                            });
                        }

                        if (u.type == 1) res.redirect('/#!/welcome?uif=' + encodeURIComponent(new Buffer(u.username).toString('base64')));
                        else res.redirect('/');
                    }
                }
            )
        },

        login: function (req, res) {

            var lingo = req.lingo;
            var uname = req.body.username ? _fn.sanitize(req.body.username).xss() : '';
            var upass = req.body.password ? _fn.sanitize(req.body.password).xss() : '';
            var remember = req.body.remember, lockU = false;
            var error_msg = "";
            Step(
                function checkInput() {

                    _fn.check(uname, lingo.get('invalid_username')).notEmpty();
                    _fn.check(upass, lingo.get('invalid_user_pass')).notEmpty().len(3, 255);

                    upass = crypto.createHash('sha256').update(upass).digest("hex");
                    managers.user.getUserLogIn(uname, upass, this);
                },
                function getAcl(e, r) {
                    if (e) throw e;

                    if(r[1][0] && r[1][0].nLogWrong >= 30){
                        lockU = true;
                        throw Error(lingo.get('lock_use_one_hour'));
                    }
                    if (!r.length || !r[0].length) throw Error(lingo.get('invalid_login'));

                    r = r[0][0];
                    if (r.status != 1) throw Error(lingo.get('err_active'));
                    
                    _fn.r(r, this.parallel());
                    managers.acl.userRoles(r.id.toString(), this.parallel());
                },
                function getResource(e, r, _r) {
                    if (e) throw e;
                    if (_r.indexOf(config.ROLE.ADMIN) == -1 && _r.indexOf(config.ROLE.SCHOOL_ADMIN) == -1 && _r.indexOf(config.ROLE.BUS_COMPANY) == -1) throw Error(lingo.get('invalid_login'));

                    _fn.r(r, this.parallel());
                    _fn.r(_r, this.parallel());
                    managers.acl.whatResources(_r, this.parallel());

                    if(_r.indexOf(config.ROLE.ADMIN) == -1) managers.user.getUserCompany(r.id, this.parallel());
                    else _fn.r([[]], this.parallel());

                    var group = this.group();
                    _r.forEach(function (v) {
                        managers.acl.whatResources(v, group());
                    });
                },
                function prepare(e, u, role, resource, uc, _r) {
                    if(e) throw e;

                    if (_r.indexOf(config.ROLE.BUS_COMPANY) != -1) {
                        if (!uc.length || !uc[0].length) throw Error(lingo.get('err_active'));
                        if (uc[0][0].status != 1) throw Error(lingo.get('err_company_active'));
                    }

                    _fn.r(u, this.parallel());
                    _fn.r(role, this.parallel());
                    _fn.r(resource, this.parallel());
                    _fn.r(uc[0], this.parallel());
                    _fn.r(_r, this.parallel());
                },
                function store(e, u, role, resource, uc, _r) {
                    if (e) {

                        setTimeout(function(){
                            res.send({success: false, message: e.message, lock: lockU});
                        }, 3000);
                        
                    }
                    else {

                        var _res = {}, comps = [];
                        _r.forEach(function (v, i) {
                            if (role[i]) {
                                _res[role[i]] = v;
                            }
                        });

                        uc.forEach(function (v) {
                            if (v.status == 1) comps.push(v);
                        });

                        var company = comps.length ? comps[0] : {};
                        req.session.uId = u.id;
                        req.session.uname = u.username;
                        req.session.company_id = company.company_id || 0;
                        req.session.bc_mode = company.mode ? company.mode.split(',') : ["1"];
                        req.session.email = u.email;
                        req.session.avatar = u.image;
                        req.session.perms = resource;
                        req.session.roles = _res;
                        req.session._role = role[0];
                        req.session.save();

                        if (remember) {

                            var maxAge = 1000 * 60 * 60 * 24 * 30;// 30 days
                            req.session.cookie.expires = new Date(Date.now() + maxAge);
                            req.session.cookie.maxAge = maxAge;
                        }

                        u.locale = u.locale ? u.locale : 'en-US';

                        res.cookie('locale', u.locale);

                        res.send({
                            success: true,
                            message: lingo.get('login_success'),
                            uData  : {
                                uId   : u.id,
                                uname : u.username,
                                email : u.email,
                                avatar: (u.image && u.image != '') ? u.image : config.DEFAULT_AVATAR,
                                perms : _res,
                                role  : role[0],
                                locale: u.locale,
                                company_id: company.company_id || 0,
                                bc_mode: company.mode ? company.mode.split(',') : ["1"]
                            }
                        });
                    }
                }
            );
        },

        register: function (req, res) {

            var lingo = req.lingo;
            var uname = req.body.username ? _fn.sanitize(req.body.username).xss() : '';
            var upass = req.body.password ? _fn.sanitize(req.body.password).xss() : '';
            var email = req.body.email ? _fn.sanitize(req.body.email).xss() : '';
            var comp = req.body.company ? _fn.sanitize(req.body.company).xss() : '';
            var addr = req.body.address ? _fn.sanitize(req.body.address).xss() : '';
            var obj = {};
            Step(
                function valid() {

                    _fn.check(uname, lingo.get('invalid_username')).notEmpty().len(2);
                    _fn.check(email, lingo.get('invalid_email')).notEmpty().isEmail();
                    _fn.check(upass, lingo.get('invalid_user_pass')).notEmpty().len(3);
                    _fn.check(comp, lingo.get('invalid_company_name')).notEmpty().len(3);
                    //_fn.check(addr, lingo.get('invalid_company_address')).notEmpty().len(5);
                    reCaptcha.verify({
                        response: req.body.captcha
                    }, this.parallel());

                    _fn.checkPass(obj, upass, this.parallel());
                },
                function checkUser(e){
                    if(e) throw e;
                    if(obj.invalid) {
                        throw Error(lingo.get('invalid_user_pass_'+obj.code));
                    }
                    managers.user.checkUserExist(0, 1, uname, this.parallel());
                    managers.user.checkUserExist(0, 2, email, this.parallel());
                },
                function saveUser(e, un, em) {
                    if (e) throw e;

                    if (un[0].length) throw Error(lingo.get('err_exists_uname'));
                    if (em[0].length) throw Error(lingo.get('err_exists_email'));

                    upass = crypto.createHash('sha256').update(upass).digest("hex");
                    managers.user.insertUser(uname, upass, 0, 4, email, '', '', '', 0, '', _fn.date.mysqlDate(new Date()), config.DEFAULT_AVATAR, uname, this.parallel());
                    managers.comp.insertCompany(comp, 0, 0, addr, 0, 1, this.parallel());
                },
                function createToken(e, u, c) {
                    if (e) throw e;

                    u = u[0][0].userid;
                    c = c[0][0].id;

                    _fn.r(u, this.parallel());
                    _fn.r(c, this.parallel());
                    managers.user.getUserByType(7, this.parallel()); //Get admin
                    managers.user.insertUserCompany(u, c, 3, 0, '', '', '', this.parallel());
                    managers.acl.addUsertoRole(u.toString(), [config.ROLE.BUS_COMPANY], this.parallel());
                },
                function sendMail(e, u, c, a, g) {
                    if (e) throw e;

                    var ems = [];
                    if (a.length) {
                        a[0].forEach(function (v) {
                            ems.push(v.email);
                        });
                    }

                    _fn.email.systemMail(
                        ems,
                        lingo.get('active_pending_email_title'),
                        _fn.str.f(lingo.get('active_email_pending'), {
                            uname: uname,
                            cname: comp,
                            link : req.protocol + '://' + req.headers.host + '/#!/users/approval'
                        }),
                        this
                    );
                },
                function response(e, r) {
                    if (e) res.send({success: false, message: e.message});
                    else res.send({success: true, message: lingo.get('register_success_bc')});
                }
            );
        },

        logout: function (req, res) {

            req.session.destroy();
            res.clearCookie(config.session.key, {
                path: '/'
            });

            res.redirect('/');
        }
    };


    


})(module.exports, require);
