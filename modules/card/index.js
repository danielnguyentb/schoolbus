/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'card';

    var Step = r("step");

    ns.init = function(app, _db, fn, mn) {
        db = _db;
        _fn = fn;
        managers = mn;

        var apiRoutes = app.get('wcl:api:routes');

        apiRoutes.route('/card/checkCard').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.cardManage, _fn.auth.act.get, _fn.auth.permission, routes.checkCard);

        apiRoutes.route('/card/pairCard').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.cardManage, _fn.auth.act.get, _fn.auth.permission, routes.pairCard);

        apiRoutes.route('/card/scanCard').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.cardManage, _fn.auth.act.get, _fn.auth.permission, routes.cardPull);

        apiRoutes.route('/card/cardType').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.cardManage, _fn.auth.act.get, _fn.auth.permission, routes.cardType);

        apiRoutes.route('/card/check').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.cardManage, _fn.auth.act.post, _fn.auth.permission, routes.getCardInfo);
    };

    var routes = {

        cardPull: function(req, res) {
            managers.LBA.pushRoom('wcl#user#' + req.session.uId, 'scan:card:pull', {test: 'aaaa'});
            res.json({
                success: true
            });
        },

        cardType: function(req, res) {

            var card_code     = req.body.qr_code ? _fn.sanitize(_fn.trim(req.body.qr_code)).xss() : '';
            Step (
                function cardType() {
                    managers.card.checkCardType(card_code, this.parallel());
                },
                function response(e, q) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true,
                            card  : q
                        });
                    }
                }
            )
        },
        checkCard : function(req, res) {
            var lingo       = req.lingo;
            var uid         = req.session.uId;
            var qr_code     = req.body.qr_code ? _fn.sanitize(_fn.trim(req.body.qr_code)).xss() : '';
            var nfc_code    = req.body.nfc_code ? _fn.sanitize(_fn.trim(req.body.nfc_code)).xss() : '';
            var role = req.session._role;
            Step (
                function checkCard() {
                    managers.card.checkCardInPair(qr_code, '', this.parallel());
                    managers.card.checkCardInPair('', nfc_code, this.parallel());
                },
                function response(e, q, n) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        var exists = 0;
                        if(q[0].length || n[0].length)
                            exists = 1;
                        res.json({
                            success: true,
                            exists  : exists
                        });
                    }
                }
            )
        },

        pairCard : function(req, res) {
            var lingo       = req.lingo;
            var uid         = req.session.uId;
            var company_id  = req.session.company_id;
            var qr_code     = req.body.qr_code ? _fn.sanitize(_fn.trim(req.body.qr_code)).xss() : '';
            var nfc_code    = req.body.nfc_code ? _fn.sanitize(_fn.trim(req.body.nfc_code)).xss() : '';
            var role = req.session._role;
            Step (
                function checkCard() {
                    managers.card.checkCardInPair(qr_code, '', this.parallel());
                    managers.card.checkCardInPair('', nfc_code, this.parallel());
                },
                function pairCard(e, q, n) {
                    if(q[0].length){
                        for(var i = 0; i < q[0].length; i ++ )
                            managers.card.updateCardInPairStatus(q[0][i].id, 2, function(){});
                    }
                    if(n[0].length){
                        for(var i = 0; i < q[0].length; i ++ )
                            managers.card.updateCardInPairStatus(q[0][i].id, 2, function(){});
                    }
                    managers.card.insertCardInPair(qr_code, nfc_code, uid, this.parallel());
                },
                function createTempChild(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    _fn.r(r, this.parallel());
                    if(r[0].length){
                        managers.child.insertChild('TEMP_CHILD', '', '', nfc_code, qr_code, company_id, '', 2, uid, '', function(){});
                    }
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        card  : r
                    });
                }
            )
        },

        getCardInfo : function(req, res) {
            var lingo   = req.lingo;
            var uid     = req.session.uId;
            var nfc     = req.body.nfc ? true : false;
            var card    = req.body.card ? _fn.sanitize(_fn.trim(req.body.card)).xss() : '';

            Step (
                function getCardInfo() {
                    _fn.check(card, lingo.get('card_not_exists')).notEmpty();

                    managers.card.getCardInfoByCard(card, (nfc ? 1 : 2), this);
                },
                function getAuthorInfo(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('card_not_exists'));

                    r = r[0][0];
                    r.ower_type = parseInt(r.ower_type, 10);

                    if(r.status != 1 || [config.CARD_TYPE.CHILD, config.CARD_TYPE.GUARDIAN, config.CARD_TYPE.BUS_CHECKIN, config.CARD_TYPE.BUS_CHECKOUT, config.CARD_TYPE.BUS].indexOf(r.ower_type) == -1) throw Error(lingo.get('card_invalid'));

                    _fn.r(r, this.parallel());

                    if(r.ower_type == config.CARD_TYPE.CHILD) managers.child.getChildById(r.owner_id, 1, this.parallel());
                    else if(r.ower_type == config.CARD_TYPE.GUARDIAN) managers.child.getGuardianById(r.owner_id, this.parallel());
                    else managers.bus.getBusById(r.owner_id, this.parallel());
                },
                function prepare(e, c, a) {
                    if(e) throw e;
                    if([config.CARD_TYPE.CHILD, config.CARD_TYPE.GUARDIAN].indexOf(r.ower_type) != -1 && (!a.length || !a[0].length)) throw Error(lingo.get('card_invalid'));

                    a = a ? a[0][0] : {};

                    var typeMap = {
                        1: 'Child Card',
                        2: 'Guardian Card',
                        6: 'Bus Checkin Card',
                        7: 'Bus Checkout Card'
                    };

                    return {
                        type: typeMap[c.ower_type] || '',
                        org_type: c.ower_type,
                        author_id: a.id || '',
                        author_name: a.name || '',
                        author_license_no: a.license_plate_no || '',
                        author_image: a.image || config.DEFAULT_AVATAR,
                        author_status: a.status || 0
                    };
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, card: r});
                }
            )
        }
    };

})(module.exports, require);
