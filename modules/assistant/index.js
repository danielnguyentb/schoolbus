/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'assistants';

    var Step = r("step");
    var crypto = r("crypto");

    ns.init = function(app, _db, fn, mn) {
        db = _db;
        _fn = fn;
        managers = mn;

        app.route('/assistant')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.assistantManage, _fn.auth.act.get, _fn.auth.permission, routes.getAssistant)
            .post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.assistantManage, _fn.auth.act.post, _fn.auth.permission, routes.updateAssistant);

        app.route('/assistant/:id')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.assistantManage, _fn.auth.act.get, _fn.auth.permission, routes.getAssistantDetail)
            .delete(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.assistantManage, _fn.auth.act.delete, _fn.auth.permission, routes.removeAssistant);
    };

    var routes = {

        getAssistant : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;

            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");
                    managers.assistant.getListAssistantPaged(companyId, page, numrow, order, desc, '', this);
                },
                function prepare(e, r) {
                    if(e) throw e;

                    var data = [];
                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            data.push({
                                id: v.id,
                                fullname: v.fullname,
                                username: v.username,
                                mobile_phone: v.mobile_phone,
                                home_phone: v.home_phone,
                                office_phone: v.office_phone,
                                email: v.email,
                                status: v.ub_status, //User company status;
                                avatar: (v.image && v.image != '') ? v.image : config.DEFAULT_AVATAR
                            });
                        });
                    }

                    return [data, r[1]]
                },
                function response(e, r) {

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            assistants  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        },

        updateAssistant : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var company_id      = req.session.company_id;
            var id              = req.body.id ? _fn.sanitize(_fn.trim(req.body.id)).toInt() : 0;
            var fullname        = req.body.fullname ? _fn.sanitize(_fn.trim(req.body.fullname)).xss() : '';
            var username        = req.body.username ? _fn.sanitize(_fn.trim(req.body.username)).xss() : '';
            var email           = req.body.email ? _fn.sanitize(_fn.trim(req.body.email)).xss() : '';
            var mobile_phone    = req.body.mobile_phone ? _fn.sanitize(_fn.trim(req.body.mobile_phone)).xss() : '';
            var home_phone      = req.body.home_phone ? _fn.sanitize(_fn.trim(req.body.home_phone)).xss() : '';
            var office_phone    = req.body.office_phone ? _fn.sanitize(_fn.trim(req.body.office_phone)).xss() : '';
            var status          = req.body.status ? _fn.sanitize(_fn.trim(req.body.status)).toInt() : 0;
            var password          = req.body.password ? _fn.sanitize(_fn.trim(req.body.password)).xss() : '';
            var repassword          = req.body.repassword ? _fn.sanitize(_fn.trim(req.body.repassword)).xss() : '';

            Step (
                function checkInput() {
                    _fn.check(username, lingo.get('assistant_name_invalid')).notEmpty().len(5);

                    if(role == config.ROLE.ADMIN && id && password != '' && password != repassword) throw Error(lingo.get('password_miss_match'));

                    if(id) managers.user.getUserById(id, this.parallel());
                    else _fn.r(null, this.parallel());
                    managers.user.checkUserExist(0, 1, username, this.parallel());
                    managers.user.checkUserExist(0, 2, email, this.parallel());
                },
                function updateAssistant(e, r, un, um) {
                    if(e) throw e;
                    if(id && (!r.length || !r[0].length) ) throw Error(lingo.get('assistant_not_exists'));
                    if(un.length && un[0].length && un[0][0].id != id) throw Error(lingo.get('assistant_username_exists'));
                    if(um.length && um[0].length && um[0][0].id != id) throw Error(lingo.get('assistant_email_exists'));

                    if(id) r = r[0][0];

                    if(id) managers.assistant.updateAssistant(id, email, mobile_phone, home_phone, office_phone, r.image, fullname, username, r.status, this);
                    else managers.assistant.insertAssistant(company_id, email, mobile_phone, home_phone, office_phone, config.DEFAULT_AVATAR, fullname, username, 1, this);
                },
                function updateRelate(e, r) {
                    if(e) throw e;

                    if(id) {
                        managers.user.userCompanyApprove(id.toString(), company_id, status, this.parallel());

                        if(role == config.ROLE.ADMIN && password != '') {
                            var hashPassword = crypto.createHash('sha256').update(password).digest("hex");
                            managers.user.updateUserPassword(id, hashPassword, this.parallel());
                        }
                    }
                    else return this;
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: id ? lingo.get('update_assistant_success') : lingo.get('create_assistant_success')
                    })
                }
            )
        },

        getAssistantDetail : function(req, res) {

            var lingo = req.lingo;
            var companyId = req.session.company_id;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).toInt() : 0;

            Step (
                function getDetail() {

                    _fn.check(id, lingo.get('assistant_not_exists')).notEmpty().isInt();

                    managers.user.getUserByIdWithCompany(id, companyId, 2, this);
                },
                function prepare(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('assistant_not_exists'));

                    r = r[0][0];

                    return {
                        id    : r.id,
                        email  : r.email,
                        fullname  : r.fullname || '',
                        username  : r.username,
                        home_phone  : r.home_phone || '',
                        mobile_phone  : r.mobile_phone || '',
                        office_phone  : r.office_phone || '',
                        status: r.user_com_status
                    };
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: lingo.get('assistant_not_exists')});
                    else res.json({success: true, assistant: r});
                }
            )
        },

        removeAssistant : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).toInt() : 0;

            Step (
                function getAssistant() {

                    _fn.check(id, lingo.get('assistant_not_exists')).notEmpty().isInt();
                    managers.user.getUserById(id, this);
                },
                function removeAssistant(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('assistant_not_exists'));

                    r = r[0][0];
                    managers.user.userCompanyApprove(r.id.toString(), companyId, 2, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('remove_assistant_success')});
                }
            )
        }
    };

})(module.exports, require);
