(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn;        //Utility functions

    var locale = 'calendar';
    var DATE_FORMAT = 'YYYY-0MM-0DD';
    var Step = r("step");

    ns.init = function(app, _db, fn, mn) {
        db = _db;
        _fn = fn;
        managers = mn;

        var apiRoutes = app.get('wcl:api:routes');

        apiRoutes.route('/calendar/locations').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.calendarManage, _fn.auth.act.get, _fn.auth.permission, routes.getLocations);
        apiRoutes.route('/get/calendars').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.calendarManage, _fn.auth.act.get, _fn.auth.permission, routes.getCalendars);
        apiRoutes.route('/calendar/:id').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.calendarManage, _fn.auth.act.get, _fn.auth.permission, routes.getCalendarDetail);
        apiRoutes.route('/calendar/:id/schedules').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.calendarManage, _fn.auth.act.get, _fn.auth.permission, routes.getCalendarSchedules);
        apiRoutes.route('/calendar/schedule').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.calendarManage, _fn.auth.act.post, _fn.auth.permission, routes.saveCalendarSchedule);
        apiRoutes.route('/calendar/schedule/:id')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.calendarManage, _fn.auth.act.post, _fn.auth.permission, routes.getCalendarSchedule)
            .delete(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.calendarManage, _fn.auth.act.delete, _fn.auth.permission, routes.deleteCalendarSchedule);

        app.route('/calendar/locations').get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.calendarManage, _fn.auth.act.get, _fn.auth.permission, routes.getLocations);
        app.route('/get/calendars').get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.calendarManage, _fn.auth.act.get, _fn.auth.permission, routes.getCalendars);
        app.route('/calendar').post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.calendarManage, _fn.auth.act.post, _fn.auth.permission, routes.saveCalendar);
        app.route('/calendar/:id').get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.calendarManage, _fn.auth.act.get, _fn.auth.permission, routes.getCalendarDetail);
        app.route('/calendar/:id/schedules').get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.calendarManage, _fn.auth.act.get, _fn.auth.permission, routes.getCalendarSchedules);
        app.route('/calendar/schedule').post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.calendarManage, _fn.auth.act.post, _fn.auth.permission, routes.saveCalendarSchedule);
        app.route('/calendar/schedule/:id')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.calendarManage, _fn.auth.act.post, _fn.auth.permission, routes.getCalendarSchedule)
            .delete(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.calendarManage, _fn.auth.act.delete, _fn.auth.permission, routes.deleteCalendarSchedule);
    };

    var routes = {
        getCalendarDetail: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).toInt() : 0;

            Step(
                function valid() {
                    _fn.check(id, lingo.get('calendar_invalid')).notEmpty();

                    managers.calendar.getCalendarById(id, this);
                },
                function getSchedule(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('calendar_not_exists'));

                    r = r[0][0];

                    _fn.r(r, this.parallel());
                    if(r.owner_id != uid) {
                        if(r.owner_type == 1) managers.user.getUserById(r.owner_id, this.parallel());
                        else managers.comp.getCompanyById(r.owner_id, this.parallel());
                    } else _fn.r([], this.parallel());

                    managers.calendar.getCalendarSharedUsers(1, r.id, this.parallel());
                },
                function prepare(e, r, u, su) {
                    if(e) throw e;

                    if(r.owner_id == uid) r.owner_type = 0;
                    r.inherits = r.inherits ? r.inherits.split(',') : [];

                    if(u.length && u[0].length) {
                        u = u[0][0];

                        r.owner = {
                            id: u.id,
                            text: u.name ? u.name : v.email
                        };
                    }

                    if(su.length && su[0].length) {
                        r.shared_users = [];

                        su[0].forEach(function(v) {
                            r.shared_users.push({
                                email: v.email,
                                uid: v.for_id,
                                role: v.role
                            });
                        });
                    }

                    return r;
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success : true,
                            calendar: r
                        });
                    }
                }
            );
        },

        getCalendars: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var ownerType = (role == config.ROLE.ADMIN) ? 1 : (role == config.ROLE.PARENT ? 3 : 2);

            var limit = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            var currentId = req.query.id ? _fn.sanitize(_fn.trim(req.query.id)).xss() : '';
            var child_id = req.query.ownerId ? _fn.sanitize(_fn.trim(req.query.ownerId)).toInt() : 0;

            Step (
                function getData() {
                    managers.calendar.getCalendars(0, ownerType, (child_id || uid), '', '', 1, currentId, page, limit, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            calendars  : r[0],
                            total  : r[1] ? r[1][0].total : 0
                        });
                    }
                }
            )
        },

        saveCalendar: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var userOwnerType = (role == config.ROLE.ADMIN) ? 1 : (role == config.ROLE.PARENT ? 3 : 2);

            var id = req.body.id ? _fn.sanitize(_fn.trim(req.body.id)).toInt() : 0;
            var name = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var inherits = req.body.inherits ? _fn.sanitize(_fn.trim(req.body.inherits)).xss() : '';
            var status = req.body.status ? _fn.sanitize(_fn.trim(req.body.status)).toInt() : 1;
            var shared_type = req.body.shared_type ? _fn.sanitize(_fn.trim(req.body.shared_type)).toInt() : 1;
            var description = req.body.description ? _fn.sanitize(_fn.trim(req.body.description)).xss() : '';
            var shared_users = req.body.shared_users ? req.body.shared_users : [];
            var owner_type = req.body.owner_type ? _fn.sanitize(_fn.trim(req.body.owner_type)).toInt() : 0;
            var owner_id = req.body.owner_id ? _fn.sanitize(_fn.trim(req.body.owner_id)).toInt() : uid;

            Step (
                function valid() {
                    _fn.check(name, lingo.get('calendar_name_empty')).notEmpty();

                    if(id) managers.calendar.getCalendarById(id, this);
                    else _fn.r(null, this);
                },
                function save(e, r) {
                    if(e) throw e;
                    if(id) {
                        if(!r.length || !r[0].length) throw Error(lingo.get('calendar_invalid'));

                        r = r[0][0];
                        if(r.owner_id != uid && role != config.ROLE.ADMIN) throw Error(lingo.get('err_permission'));
                    }

                    if(owner_type == 0) {
                        owner_type = userOwnerType;
                        owner_id = uid;
                    }

                    if(id) managers.calendar.updateCalendar(r.id, owner_type, owner_id, name, description, status, shared_type, inherits, this);
                    else managers.calendar.insertCalendar(owner_type, owner_id, name, description, status, shared_type, uid, inherits, this);
                },
                function saveRelation(e, r) {
                    if(e) throw e;

                    if(!id && r.length) id = r[0][0].id;

                    if(id && Array.isArray(shared_users) && shared_users.length) {
                        var group = this.group();

                        shared_users.forEach(function(v) {
                            managers.calendar.insertCalendarSharedUser(1, id, 0, v.uid, v.role, group());
                        });
                    } else return this;
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: id ? lingo.get('save_calender_success') : lingo.get('add_calender_success')});
                }
            )
        },

        saveCalendarSchedule: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id;
            var role = req.session._role;
            var ownerType = (role == config.ROLE.ADMIN) ? 1 : (role == config.ROLE.PARENT ? 3 : 2);

            var schedule = {
                id: req.body.id ? _fn.sanitize(_fn.trim(req.body.id)).toInt() : 0,
                inherit: req.body.inherit_id ? _fn.sanitize(_fn.trim(req.body.inherit_id)).toInt() : 0,
                date_from: req.body.date_from ? _fn.sanitize(_fn.trim(req.body.date_from)).xss() : '',
                date_to: req.body.date_to ? _fn.sanitize(_fn.trim(req.body.date_to)).xss() : null,
                time_from: req.body.time_from ? _fn.sanitize(_fn.trim(req.body.time_from)).xss() : null,
                time_to: req.body.time_to ? _fn.sanitize(_fn.trim(req.body.time_to)).xss() : null,
                recurring: req.body.recurring ? _fn.sanitize(_fn.trim(req.body.recurring)).xss() : '',
                location_id: req.body.location_id ? _fn.sanitize(_fn.trim(req.body.location_id)).xss() : '',
                description: req.body.description ? _fn.sanitize(_fn.trim(req.body.description)).xss() : '',
                mode: req.body.mode ? 1 : 0,
                specify_dates: req.body.specify_dates || [],
                calendarId: req.body.calendar_id ? _fn.sanitize(_fn.trim(req.body.calendar_id)).toInt() : 0,
                child_id: req.body.child_id ? _fn.sanitize(_fn.trim(req.body.child_id)).toInt() : 0,
                company_id: req.body.company_id ? _fn.sanitize(_fn.trim(req.body.company_id)).toInt() : companyId
            };

            Step(
                function valid() {
                    _fn.check(schedule.calendarId, lingo.get('schedule_calendar_required')).notEmpty().isInt();
                    _fn.check(schedule.description, lingo.get('schedule_title_required')).notEmpty();
                    _fn.check(schedule.date_from, lingo.get('schedule_date_from_required')).notEmpty().isDate();
                    _fn.check(schedule.recurring, lingo.get('schedule_repeat_mode_invalid')).notEmpty().isIn([config.CALENDAR_REPEAT_TYPES.ONE, config.CALENDAR_REPEAT_TYPES.SPECIFY_DATES, config.CALENDAR_REPEAT_TYPES.WEEKLY, config.CALENDAR_REPEAT_TYPES.YEARLY]);

                    if(schedule.recurring == config.CALENDAR_REPEAT_TYPES.SPECIFY_DATES) {

                        if(!Array.isArray(schedule.specify_dates) || !schedule.specify_dates.length) throw Error(lingo.get('schedule_special_dates_required'));
                    } else if(schedule.recurring == config.CALENDAR_REPEAT_TYPES.WEEKLY) {
                        var valid = false;
                        for(var i = 1; i <= 7; i++) {
                            if(req.body['wd' + i]) {
                                valid = true;
                                break;
                            }
                        }

                        if(!valid) throw Error(lingo.get('schedule_daily_week_required'));
                    }

                    managers.calendar.getCalendarById(schedule.calendarId, this.parallel());

                    if(schedule.id) managers.calendar.getCalendarScheduleInfo(schedule.id, this.parallel());
                },
                function save(e, c, s) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('calendar_not_exists'));

                    c = c[0][0];

                    //if(c.created_by != uid && (c.shared_type != 2 && c.status != 2)) throw Error(lingo.get('err_permission'));

                    if(schedule.id) {
                        if(!s.length || !s[0].length) throw Error(lingo.get('schedule_not_exists'));

                        s = s[0][0];
                        if((role == config.ROLE.BUS_COMPANY && s.bus_company_id != companyId) || (role == config.ROLE.PARENT && s.created_by != uid)) throw Error(lingo.get('err_permission'));
                    }

                   if(schedule.id) managers.calendar.updateCalendarSchedule(schedule.id, schedule.calendarId, schedule.inherit, schedule.mode, schedule.child_id, schedule.description, schedule.date_from, schedule.date_to, schedule.time_from, schedule.time_to, schedule.location_id, 0, schedule.recurring, schedule.company_id, this);
                   else managers.calendar.insertCalendarSchedule(schedule.calendarId, schedule.inherit, schedule.mode, schedule.child_id, schedule.description, schedule.date_from, schedule.date_to, schedule.time_from, schedule.time_to, schedule.location_id, 0, schedule.recurring, schedule.company_id, uid, this);
                },
                function saveConfig(e, r) {
                    if(e) throw e;

                    var scheduleId = schedule.id || r[0][0].id;

                    if(schedule.recurring == config.CALENDAR_REPEAT_TYPES.SPECIFY_DATES && schedule.specify_dates.length) {

                        managers.calendar.saveCalendarScheduleSpecifyDates(scheduleId, '"' + schedule.specify_dates.join('","') + '"', this.parallel());
                    } else if(schedule.recurring == config.CALENDAR_REPEAT_TYPES.WEEKLY) {
                        var i, days = [];

                        for(i = 1; i <= 7; i++) {
                            if(req.body['wd' + i]) days.push(i);
                        }

                        managers.calendar.saveCalendarScheduleDailyWeek(scheduleId, days.join(','), this.parallel());
                    } else if(schedule.recurring == config.CALENDAR_REPEAT_TYPES.YEARLY) {

                        var order = req.body.yearly_order ? _fn.sanitize(_fn.trim(req.body.yearly_order)).toInt() : 1;
                        var weekId = req.body.yearly_weekid ? _fn.sanitize(_fn.trim(req.body.yearly_weekid)).toInt() : 1;
                        var monthId = req.body.yearly_monthid ? _fn.sanitize(_fn.trim(req.body.yearly_monthid)).toInt() : 1;

                        managers.calendar.saveCalendarScheduleYearly(scheduleId, order, weekId, monthId, this.parallel());
                    } else _fn.r(null, this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: schedule.id ? lingo.get('save_schedule_success') : lingo.get('add_schedule_success')});
                }
            );
        },

        getCalendarSchedule: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).toInt() : 0;

            Step(
                function valid() {
                    _fn.check(id, lingo.get('calendar_schedule_invalid')).notEmpty();

                    managers.calendar.getCalendarScheduleById(id, this.parallel());
                },
                function getSchedule(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('schedule_not_exists'));

                    r = r[0][0];

                    _fn.r(r, this.parallel());
                    managers.calendar.getCalendarSchedulesByCalendarId(r.id, this.parallel());

                    if(r.home_location_id) managers.calendar.getLocationInfo(r.home_location_id, this.parallel());
                    else _fn.r([], this.parallel());

                    if(r.recurring == config.CALENDAR_REPEAT_TYPES.WEEKLY) managers.calendar.getCalendarScheduleDailyWeek(r.id, this.parallel());
                    else if(r.recurring == config.CALENDAR_REPEAT_TYPES.SPECIFY_DATES) managers.calendar.getCalendarScheduleSpecifyDates(r.id, this.parallel());
                    else if(r.recurring == config.CALENDAR_REPEAT_TYPES.YEARLY) managers.calendar.getCalendarScheduleYearly(r.id, this.parallel());
                },
                function prepare(e, r, sc, lc, sd) {
                    if(e) throw e;

                    r.inherits = sc[0] || [];
                    r.shared = r.created_by != uid;
                    r.from_date = r.from_date ? _fn.date.dateString(new Date(r.from_date)) : '';
                    r.to_date = r.to_date ? _fn.date.dateString(new Date(r.to_date)) : '';

                    if(lc.length && lc[0].length) {
                        lc = lc[0][0];

                        r.location = {id: lc.id, name: lc.name, address: lc.address};
                    }

                    sd = (sd && sd.length) ? sd[0] : [];

                    if(r.recurring == config.CALENDAR_REPEAT_TYPES.WEEKLY) {

                        r.weekly_days = [];

                        sd.forEach(function(v) {
                            r.weekly_days.push(v.weekid);
                        });
                    } else if(r.recurring == config.CALENDAR_REPEAT_TYPES.SPECIFY_DATES && sd.length) {

                        r.specify_dates = [];

                        sd.forEach(function(v) {
                            r.specify_dates.push(_fn.date.dateString(new Date(v.dt)));
                        });
                    } else if(r.recurring == config.CALENDAR_REPEAT_TYPES.YEARLY && sd.length) {

                        r.yearly = sd[0] || {};
                    }

                    return r;
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success : true,
                            schedule: r
                        });
                    }
                }
            );
        },

        deleteCalendarSchedule: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id;
            var role = req.session._role;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).toInt() : 0;

            Step(
                function valid() {
                    _fn.check(id, lingo.get('calendar_schedule_invalid')).notEmpty();

                    managers.calendar.getCalendarScheduleInfo(id, this);
                },
                function save(e, r) {
                    if(e) throw e;

                    if(!r.length || !r[0].length) throw Error(lingo.get('schedule_not_exists'));

                    r = r[0][0];

                    if((role == config.ROLE.BUS_COMPANY && r.bus_company_id != companyId) || (role == config.ROLE.PARENT && r.created_by != uid)) throw Error(lingo.get('err_permission'));

                    managers.calendar.deleteCalendarSchedule(id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('delete_schedule_success')});
                }
            );
        },

        getCalendarSchedules: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).toInt() : 0;
            var raw = req.query.raw == 1;

            Step(
                function valid() {
                    _fn.check(id, lingo.get('calendar_invalid')).notEmpty();

                    managers.calendar.getCalendarById(id, this);
                },
                function getSchedule(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('calendar_not_exists'));

                    r = r[0][0];

                    _fn.r(r, this.parallel());
                    if(raw) managers.calendar.getCalendarSchedulesByCalendarId(r.id, this.parallel());
                    else {
                        var now = new Date();
                        now.setFullYear(now.getFullYear() - (config.CALENDAR_MAX_YEAR || 5));

                        managers.calendar.getCalendarScheduleByType(1, r.id, _fn.date.dateString(now),  _fn.date.dateString(new Date()), this.parallel());
                    }
                },
                function prepare(e, c, sc) {
                    if(e) throw e;

                    if(raw) return sc[0] || [];

                    var phs = utils.getPHDates(true);
                    _fn.r([phs], this.parallel());

                    if(sc.length) {
                        utils.buildSchedules(sc[0], {}, this.parallel());
                        utils.buildHistorySchedules(sc[1], this.parallel());
                    }
                },
                function response(e, r, fs, hs) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        console.log(hs);
                        r = _fn.merge(r, (fs || []), (hs || []));

                        res.json({
                            success : true,
                            schedules: r
                        });
                    }
                }
            );
        },

        getLocations: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id || 0;
            var role = req.session._role;
            var ownerType = (role == config.ROLE.ADMIN) ? 1 : (role == config.ROLE.PARENT ? 3 : 2);

            var keyword = req.query.q ? _fn.sanitize(_fn.trim(req.query.q)).xss() : '';

            Step(
                function getData() {
                    managers.calendar.searchLocation(0, companyId, '', keyword, '', '', '', '', 1, 10, this);
                },
                function prepare(e, r) {
                    if(e) throw e;

                    var data = [];
                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            data.push({
                                id: v.id,
                                name: v.name,
                                address: v.address
                            });
                        });
                    }

                    return data;
                },
                function response(e, r) {
                    if(e) res.json({success: false, locations: []});
                    else res.json({success: true, locations: r});
                }
            );
        }
    };

    var utils = {
        buildHistorySchedules: function(items, cb) {
            Step(
                function prepare() {
                    var schedules = {};
                    items.forEach(function(v) {
                        v.plan_date = new Date(v.plan_date);

                        if(!schedules[v.calendar_schedule_id]) {
                            v.id = v.calendar_schedule_id;
                            v.dates = {};
                            v.history = true;

                            schedules[v.calendar_schedule_id] = v;

                            return true;
                        }

                        var schedule = schedules[v.calendar_schedule_id];
                        schedule.dates[_fn.date.dateString(v.plan_date)] = v.plan_date;
                    });

                    utils.buildSchedules(_fn.array.toArray(schedules), {className: 'his-day', is_his: true}, this);
                },
                cb
            );
        },

        buildSchedules: function(schedules, ext, cb) {
            ext = ext || {};

            Step(
                function getDates() {
                    var group = this.group();
                    schedules.forEach(function(v) {
                        utils.getCalendarScheduleData(v, group());
                    });
                },
                function mergeInherit(e, r) {
                    if(e) throw e;

                    var scheduleMap = {}, groups = {};
                    schedules.forEach(function(v) {
                        scheduleMap[v.id] = v;

                        if(!groups[v.id] && !v.inherit_schedule) groups[v.id] = [v.id];

                        if(v.inherit_schedule) {
                            if(groups[v.inherit_schedule]) groups[v.inherit_schedule].push(v.id);
                            else groups[v.inherit_schedule] = [v.inherit_schedule, v.id];
                        }
                    });

                    var i, g, schedule;
                    for(i in groups) {
                        if(!groups.hasOwnProperty(i) || !scheduleMap[i]) continue;

                        schedule = scheduleMap[i];
                        g = groups[i];

                        if(g.length > 1) {
                            g.forEach(function(v) {
                                if(v == schedule.id || !scheduleMap[v]) return true;

                                schedule.dates = utils.mergeDates(schedule, (scheduleMap[v].dates || {}));
                            });
                        }
                    }

                    return scheduleMap;
                },
                function prepare(e, r) {
                    if(e) throw e;

                    var i, data = [];

                    for(i in r) {
                        if(!r.hasOwnProperty(i)) continue;

                        data.push(utils.getScheduleEvents(r[i],  ext));
                    }

                    return data;
                },
                cb
            );
        },

        getCalendarScheduleData: function(schedule, cb) {
            Step(
                function getData() {
                    if(schedule.dates) return [];

                    if(schedule.recurring == config.CALENDAR_REPEAT_TYPES.SPECIFY_DATES) managers.calendar.getCalendarScheduleSpecifyDates(schedule.id, this.parallel());
                    else if(schedule.recurring == config.CALENDAR_REPEAT_TYPES.WEEKLY) managers.calendar.getCalendarScheduleDailyWeek(schedule.id, this.parallel());
                    else if(schedule.recurring == config.CALENDAR_REPEAT_TYPES.YEARLY) managers.calendar.getCalendarScheduleYearly(schedule.id, this.parallel());
                    else _fn.r([], this.parallel());
                },
                function prepare(e, r) {
                    if(e) throw e;

                    if(schedule.dates) return this;

                    var phDays = [], dates = {};
                    if(config.PH_DAYS) {
                        config.PH_DAYS.forEach(function(v) {
                            phDays.push(v.start);
                        });
                    }

                    switch (schedule.recurring) {
                        case config.CALENDAR_REPEAT_TYPES.ONE:
                            dates = utils.scheduleGetDatesRange(schedule, phDays);
                            break;

                        case config.CALENDAR_REPEAT_TYPES.SPECIFY_DATES:
                            if(r.length && r[0].length) {
                                r[0].forEach(function (v) {
                                    var date = new Date(v.dt), now = new Date();

                                    if ((schedule.history || (!schedule.history && date >= now)) && (schedule.mode == 0 || phDays.indexOf(_fn.date.dateString(date, true)) == -1)) {
                                        dates[_fn.date.dateString(date)] = date;
                                    }
                                });
                            }
                            break;

                        case config.CALENDAR_REPEAT_TYPES.WEEKLY:
                            var dow = [];
                            if(r.length && r[0].length) {
                                r[0].forEach(function (v) {
                                    dow.push(v.weekid);
                                });
                            }

                            dates = utils.scheduleGetWeeklyDates(schedule, dow, (schedule.mode ? phDays : []));
                            break;

                        case config.CALENDAR_REPEAT_TYPES.YEARLY:
                            if(r.length && r[0].length) {
                                r = r[0][0];

                                dates = utils.scheduleGetYearlyDates(schedule, r);
                            }
                            break;

                        default:
                            break;
                    }

                    schedule.dates = dates;
                    return this;
                },
                cb
            );
        },

        mergeDates: function(schedule, dates) {
            var i;

            for(i in dates) {
                if(!dates.hasOwnProperty(i)) continue;

                if(schedule.dates[i]) delete schedule.dates[i];
            }

            return schedule.dates;
        },

        getScheduleEvents: function(schedule, ext) {
            var data = [], groups = utils.groupDates((schedule.dates || {}));

            groups.forEach(function(v) {
                var event = _fn.extend({
                    id: schedule.id,
                    title: schedule.description,
                    start: _fn.date.dateString(v.start) + (schedule.checkin_time ? ' ' + schedule.checkin_time : ''),
                    end: _fn.date.dateString(v.end) + (schedule.checkout_time ? ' ' + schedule.checkout_time : ''),
                    allDay: schedule.checkin_time ? false : true
                }, (ext || {}));

                data.push(event);
            });

            return data;
        },

        scheduleGetDatesRange: function(schedule, excludeDates) {
            var dates = {}, now = new Date(), start = new Date(schedule.from_date), end = schedule.to_date ? new Date(schedule.to_date) : start;

            if(!schedule.history && start < now) start = new Date(now);

            while (start <= end) {
                if(excludeDates.indexOf(_fn.date.dateString(start, true)) == -1) {
                    dates[_fn.date.dateString(start)] = new Date(start);
                }

                start.setDate(start.getDate() + 1);
            }

            return dates;
        },

        scheduleGetWeeklyDates: function(schedule, dow, excludeDates) {
            var start = new Date(schedule.from_date), end, dates = {}, now = new Date();

            if(!schedule.history && start < now) start = new Date(now);

            if(!schedule.to_date) {
                now.setFullYear(now.getFullYear() + config.CALENDAR_MAX_YEAR);
                end = new Date(now);
            } else end = new Date(schedule.to_date);

            while (start <= end) {
                if(dow.indexOf(start.getDay() + 1) !== -1 && excludeDates.indexOf(_fn.date.dateString(start, true)) == -1) {
                    dates[_fn.date.dateString(start)] = new Date(start);
                }

                start.setDate(start.getDate() + 1);
            }

            return dates;
        },

        scheduleGetYearlyDates: function(schedule, dom) {
            var start = new Date(schedule.from_date), end, dates = {}, now = new Date();

            if(!schedule.history && start < now) start = new Date(now);

            if(!schedule.to_date) {
                now.setFullYear(now.getFullYear() + config.CALENDAR_MAX_YEAR);
                end = new Date(now);
            } else end = new Date(schedule.to_date);

            while (start <= end) {
                var _start = new Date(start.getFullYear(), dom.monthid - 1, 1), _end = new Date(start.getFullYear(), dom.monthid, 0);

                if(dom.the_order == 10) {
                    while (_end > _start) {
                        if(dom.weekid == (_end.getDay() + 1)) {
                            dates[_fn.date.dateString(_end)] = new Date(_end);

                            break;
                        }

                        _end.setDate(_end.getDate() - 1);
                    }
                } else {
                    var count = 0;

                    while (_start <= _end) {
                        if(dom.weekid == (_start.getDay() + 1)) {
                            count++;

                            if(count == dom.the_order) {
                                dates[_fn.date.dateString(_start)] = new Date(_start);

                                break;
                            }
                        }

                        _start.setDate(_start.getDate() + 1);
                    }
                }

                start.setFullYear(start.getFullYear() + 1);
            }

            return dates;
        },

        groupDates: function(dates) {
            if(!dates) return [];

            var groups = [], groupStart = null, groupEnd = null;

            var i, v;
            for(i in dates) {
                if(!dates.hasOwnProperty(i)) continue;

                v = dates[i];
                if(!groupStart || !groupEnd) {
                    groupStart = v;
                    groupEnd = new Date(v);

                    continue;
                }

                groupEnd.setDate(groupEnd.getDate() + 1);
                if (_fn.date.dateString(groupEnd) != _fn.date.dateString(v)) {
                    groupEnd.setDate(groupEnd.getDate() - 1);

                    groups.push({
                        start: new Date(groupStart),
                        end: new Date(groupEnd)
                    });

                    groupStart = v;
                    groupEnd = new Date(v);
                } else groupEnd = v;
            }

            groups.push({
                start: new Date(groupStart),
                end: new Date(groupEnd)
            });

            return groups;
        },

        getPHDates: function(future) {
            var phs = [], year = (new Date()).getFullYear();

            if(config.PH_DAYS) {
                if(!future) {
                    config.PH_DAYS.forEach(function(v) {

                        phs.push(_fn.extend({}, v, {start: [year, v.start].join('-')}));
                    });
                } else {
                    var max = year + (config.CALENDAR_MAX_YEAR || 5), min = year - (config.CALENDAR_MAX_YEAR || 5);

                    while (min <= max) {
                        config.PH_DAYS.forEach(function(v) {
                            var cls = 'ph-day', start = [min, v.start].join('-');

                            if(new Date(start) < new Date()) cls += ' his-day';

                            phs.push(_fn.extend({}, v, {start: start, className: cls}));
                        });

                        min += 1;
                    }
                }
            }

            return phs;
        }
    };

})(module.exports, require);
