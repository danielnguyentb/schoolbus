/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'users';

    var Step = r("step");

    ns.init = function(app, _db, fn, mn) {

        db = _db;
        _fn = fn;
        managers = mn;

        app.route('/users/pending-approval')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.pendingApproval, _fn.auth.act.get, _fn.auth.permission, routes.getPending)
            .post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.pendingApproval, _fn.auth.act.post, _fn.auth.permission, routes.approvePending);

        app.route('/users/search')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.pendingApproval, _fn.auth.act.get, _fn.auth.permission, routes.searchUsers)
    };

    var routes = {

        getPending : function(req, res) {

            var lingo = req.lingo;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;

            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    managers.user.getUserPendingApproval(page, numrow, order, desc, '', this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            users  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        },

        approvePending : function(req, res) {

            var lingo = req.lingo;
            var users = req.body.u;
            var approve = (req.body.t == 'approve') ? 1 : 0;

            Step (
                function checkValid() {

                    if(!Array.isArray(users) || !users.length) throw Error(lingo.get('users_not_exists'));

                    var group = this.group();
                    users.forEach(function(v) {
                        managers.user.getUserCompanyByType(v, 3, group());
                    });
                },
                function disabledUser(e, r) {
                    if(e) throw e;

                    var activeUser = this.group(),
                        activeUserCom = this.group(),
                        activeCom = this.group(),
                        activeGroup = this.group();
                    r.forEach(function(v) {
                        v = v[0][0];

                        if(v.user_status == 0) {

                            managers.user.userApprove(v.user_id, approve, activeUser());
                            managers.user.userCompanyApprove(v.user_id, v.id, approve, activeUserCom());
                            managers.user.userGroupUpdate(v.user_id, 4, 1, activeGroup());
                            managers.comp.companyApprove(v.id, approve, activeCom());
                        }
                    });
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: approve ? lingo.get('approve_success') : lingo.get('cancel_success')
                    });
                }
            );
        },

        searchUsers: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id || 0;
            var role = req.session._role;
            var ownerType = (role == config.ROLE.ADMIN) ? 1 : (role == config.ROLE.PARENT ? 3 : 2);

            var searchType = req.query.t ? _fn.sanitize(_fn.trim(req.query.t)).toInt() : 0;
            var keyword = req.query.q ? _fn.sanitize(_fn.trim(req.query.q)).xss() : '';
            var limit = 11;

            Step(
                function getData() {
                    //preg_from, pkeyword, pcompany_name, pcompany_role, page, num

                    if(searchType == 4) managers.user.searchUsers(searchType, '', keyword, 0, 1, limit, this);
                    else managers.user.searchUsers(searchType, keyword, '', 0, 1, limit, this);
                },
                function prepare(e, r) {
                    if(e) throw e;

                    var data = [], count = 0;
                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            if(v.id == uid || count >= (limit - 1)) return true;

                            count++;
                            data.push({
                                id: v.id,
                                email: v.email,
                                text: (searchType == 4 ? v.company_name : v.email),
                                t: v.type
                            });
                        });
                    }

                    return data;
                },
                function response(e, r) {
                    if(e) res.json({success: false, users: []});
                    else res.json({success: true, users: r});
                }
            );
        }
    };

})(module.exports, require);
