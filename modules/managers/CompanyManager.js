/**
 * Created by AnhNguyen
 */

(function(r, ns) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var comps;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        comps = db.use('DataDB');

        return pub;
    };

    pub.getCompanies = function(pstatus, keyword, page, limit, order, desc, cb) {

        comps.query("CALL sb_company_search_paged(?, ?, ?, ?, ?, ?)", [pstatus, keyword, page, limit, order, desc], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertCompany = function(pname, pcity_id, pdistrict_id, paddress, pstatus, pmode, cb) {

        comps.query("CALL sb_company_insert(?, ?, ?, ?, ?, ?)", [pname, pcity_id, pdistrict_id, paddress, pstatus, pmode], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.companyApprove = function(pid, ptype, cb) {

        comps.query("CALL sb_company_approval(?, ?)", [pid, ptype], function(e, r) {

            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateCompany = function(pid, pname, pcity_id, pdistrict_id, paddress, pstatus, pmode, cb) {

        comps.query("CALL sb_company_update(?, ?, ?, ?, ?, ?, ?)", [pid, pname, pcity_id, pdistrict_id, paddress, pstatus, pmode], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCompanyById = function(pid, cb) {

        comps.query("CALL sb_company_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertStaffAbsent = function(puser_id, pcompany_id, pcreated_by, cb) {
        comps.query("CALL sb_company_staff_absent_insert(?, ?, ?)", [puser_id, pcompany_id, pcreated_by], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCompanyStaffAttendance = function(pcompany_id, pfromdt, ptodt, page, limit, _order, _desc, cb) {
        comps.query("CALL sb_event_index_report_by_user(?, ?, ?, ?, ?, ?, ?)", [pcompany_id, pfromdt, ptodt, page, limit, _order, _desc], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCompanyStaffAttendanceLogByDate = function(pcompany_id, pdate, page, limit, _order, _desc, keyword, cb) {
        comps.query("CALL sb_event_history_by_company_date_paged(?, ?, ?, ?, ?, ?, ?)", [pcompany_id, pdate, page, limit, _order, _desc, keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCompanyAbsentsByDate = function(pcompany_id, pdt, cb) {
        comps.query("CALL sb_company_absent_by_date(?, ?)", [pcompany_id, pdt], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.countCompanyWorkersPending = function(pcompany_id, cb) {
        comps.query("CALL sb_company_count_workers(?)", [pcompany_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCompanyWorkersWithDate = function(pcompany_id, pstatus, pdate, cb) {
        comps.query("CALL sb_company_get_workers_by_id_with_date(?, ?, ?)", [pcompany_id, pstatus, pdate], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCompanyCurrentRoute = function(pprev_h, pnext_h, pcompany_id, cb) {
        comps.query("CALL sb_route_trip_get_by_range_time(?, ?, ?);", [pprev_h, pnext_h, pcompany_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCompanyByParent = function(pid, cb) {
        comps.query("CALL sb_company_by_parent_id(?);", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

})(require, module);
