(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;
    var resource;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        resource = db.use('DataDB');

        return pub;
    };

    pub.getCalendars = function(pcalendarid, powner_type, powner_id, pname, pdescription, pstatus, pexcludeIds, ppage, pnum, cb) {
        resource.query("CALL sb_calendar_search(?, ?, ?, ?, ?, ?, ?, ?, ?)", [pcalendarid, powner_type, powner_id, pname, pdescription, pstatus, pexcludeIds, ppage, pnum], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getSharedCalendars = function(pobject_type, pobject_id, pfor_type, pfor_id, pshared_type, cb) {
        resource.query("CALL sb_calendar_shared_search(?, ?, ?, ?, ?)", [pobject_type, pobject_id, pfor_type, pfor_id, pshared_type], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCalendarById = function(pid, cb) {
        resource.query("CALL sb_calendar_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertCalendar = function(powner_type, powner_id, pname, pdescription, pstatus, pshared_type, pcreated_by, pinherits, cb) {
        resource.query("CALL sb_calendar_insert(?, ?, ?, ?, ?, ?, ?, ?)", [powner_type, powner_id, pname, pdescription, pstatus, pshared_type, pcreated_by, pinherits], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateCalendar = function(pid, powner_type, powner_id, pname, pdescription, pstatus, pshared_type, pinherits, cb) {
        resource.query("CALL sb_calendar_update(?, ?, ?, ?, ?, ?, ?, ?)", [pid, powner_type, powner_id, pname, pdescription, pstatus, pshared_type, pinherits], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCalendarScheduleByType = function(pobject_type, pobject_id, pfrom_dt, pto_dt, cb) {
        resource.query("CALL sb_calendar_schedule_get_by_user(?, ?, ?, ?)", [pobject_type, pobject_id, pfrom_dt, pto_dt], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCalendarSchedulesByCalendarId = function(pcalendar_id, cb) {
        resource.query("CALL sb_calendar_schedule_get_by_calendar_id(?)", [pcalendar_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCalendarScheduleInfo = function(pid, cb) {
        resource.query("CALL sb_calendar_schedule_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCalendarScheduleSpecifyDates = function(pcalendar_schedule_id, cb) {
        resource.query("CALL sb_calendar_schedule_specify_date_get_by_schedule_id_v2(?)", [pcalendar_schedule_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCalendarScheduleDailyWeek = function(pcalendar_schedule_id, cb) {
        resource.query("CALL sb_calendar_schedule_week_get_by_schedule_id(?)", [pcalendar_schedule_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCalendarScheduleYearly = function(pcalendar_schedule_id, cb) {
        resource.query("CALL sb_calendar_schedule_year_get_by_schedule_id(?)", [pcalendar_schedule_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCalendarScheduleById = function(pschedule_id, cb) {
        resource.query("CALL sb_calendar_schedule_get_info(?)", [pschedule_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertCalendarSchedule = function(pcalendar_id, pinherit_schedule, pmode, pchild_id, pdescription, pfrom_date, pto_date, pcheckin_time, pcheckout_time, phome_location_id, pevent_location_id, precurring, pbus_company_id, pcreated_by, cb) {
        resource.query("CALL sb_calendar_schedule_insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pcalendar_id, pinherit_schedule, pmode, pchild_id, pdescription, pfrom_date, pto_date, pcheckin_time, pcheckout_time, phome_location_id, pevent_location_id, precurring, pbus_company_id, pcreated_by], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateCalendarSchedule = function(pid, pcalendar_id, pinherit_schedule, pmode, pchild_id, pdescription, pfrom_date, pto_date, pcheckin_time, pcheckout_time, phome_location_id, pevent_location_id, precurring, pbus_company_id, cb) {
        resource.query("CALL sb_calendar_schedule_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pcalendar_id, pinherit_schedule, pmode, pchild_id, pdescription, pfrom_date, pto_date, pcheckin_time, pcheckout_time, phome_location_id, pevent_location_id, precurring, pbus_company_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.deleteCalendarSchedule = function(pschedule_id, cb) {
        resource.query("CALL sb_calendar_schedule_delete(?)", [pschedule_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.saveCalendarScheduleSpecifyDates = function(pcalendar_schedule_id, plstdt, cb) {
        resource.query("CALL sb_calendar_schedule_specify_date_update(?, ?)", [pcalendar_schedule_id, plstdt], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.saveCalendarScheduleDailyWeek = function(pcalendar_schedule_id, plstweekid, cb) {
        resource.query("CALL sb_calendar_schedule_week_update(?, ?)", [pcalendar_schedule_id, plstweekid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.saveCalendarScheduleYearly = function(pcalendar_schedule_id, pthe_order, pweekid, pmonthid, cb) {
        resource.query("CALL sb_calendar_schedule_year_update(?, ?, ?, ?)", [pcalendar_schedule_id, pthe_order, pweekid, pmonthid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.searchLocation = function(pid, pcompany_id, pname, paddress, pX, pY, pcreated_by_trip, pcreated_by_user, ppage, plimit, cb) {
        resource.query("CALL sb_location_dic_search(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pcompany_id, pname, paddress, pX, pY, pcreated_by_trip, pcreated_by_user, ppage, plimit], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getLocationInfo = function(pid, cb) {
        resource.query("CALL sb_location_dic_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertCalendarSharedUser = function(pobject_type, pobject_id, pfor_type, pfor_id, pshared_type, cb) {
        resource.query("CALL sb_calendar_shared_update(?, ?, ?, ?, ?)", [pobject_type, pobject_id, pfor_type, pfor_id, pshared_type], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCalendarSharedUsers = function(pobject_type, pobject_id, cb) {
        resource.query("CALL sb_calendar_get_shared_user(?, ?)", [pobject_type, pobject_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

})(module, require);
