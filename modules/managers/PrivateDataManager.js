/* Copyright (c) 2014 WCL , Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

 /*
  * Private Data Manager
  *
  * Setups up routes that'll authenticate and process private data
  */

(function(r){

	var fs = r("fs");
	var Step = r("step");
	var mime = r("mime");

	var pub = {};
	var _fn;
	var basepath = "uploads";

 	//Init function - will return "pub" for public funcs
 	module.exports = function(app, utils){

 		_fn = utils;

 		return pub;
 	};

 	//NOTE: Doing this here because when the module is inited (above) _fn.auth doesn't exist
 	pub.setupRoutes = function(app){

	    app.route("/pvf/*").get(
 			_fn.auth.check,
            // _fn.auth.checkReferral,
 			routes.getPrivateFile);

 		app.route("/avatar/:id").get(
			//_fn.auth.check,
            // _fn.auth.checkReferral,
 			routes.getUserAvatar);

		app.route("/pvi/*").get(
			//_fn.auth.check,
			// _fn.auth.checkReferral,
			routes.getPrivateImg);

	    app.route("/docs/*").get(
		    _fn.auth.check,
		    routes.getUserDocs
	    );
 	};

 	//============ Controllers ======================
 	var routes = {

	    //Gets user docs file
	    getUserDocs : function(req, res, next) {

		    var path = req.url.replace(/\.\.\//g,"").replace("/docs/", "/docs/" + req.session.uId + "/");

		    var etag = req.headers["if-none-match"];
		    if (etag) return res.send(304);

		    getFile(path, function(e,file,stat, mime){
			    if (e) {
				    res.send(404);
				    return config.error("File not found : " + path)
			    };

			    //Far future cache the user docs
			    var headers = {
				    'Content-Type': mime,
				    "Content-Disposition" : 'attachment',
				    "Accept-Ranges" : "bytes",
				    "Last-Modified" : stat.mtime.toUTCString(),
				    "etag" : '"' + (stat.size + '-' + Number(stat.mtime)) + '"',
				    'Cache-Control': 'public, max-age: ' + (360 * 24 * 60 * 60)
			    };

			    res.writeHead(200, headers);
			    res.end(file);
		    });
	    },

		//Gets private image
		getPrivateImg : function(req,res, next) {

			var path = req.url.replace(/\.\.\//g,"").replace("/pvi/", "/upl/");

			var etag = req.headers["if-none-match"];
			if (etag) return res.send(304);

			getFile(path, function(e,file,stat){

				if (e) {
					res.send(404);
					return config.error("File not found : " + path)
				};

				//Far future cache the user avatar - all new avatars would be a new image
				var headers = {
					'Content-Type': 'image/png',
					"Accept-Ranges" : "bytes",
					"Last-Modified" : stat.mtime.toUTCString(),
					"etag" : '"' + (stat.size + '-' + Number(stat.mtime)) + '"',
					'Cache-Control': 'public, max-age: ' + (360 * 24 * 60 * 60)
				};

				res.writeHead(200, headers);
				res.end(file);
			});
		},

	 	//Gets private file
	 	getPrivateFile : function(req,res, next) {

	 		var path = req.url.replace(/\.\.\//g,"").replace("/pvf/", "/cs/" + req.session.cid + "/");

 		    var etag = req.headers["if-none-match"];
 		    if (etag) return res.send(304);

	 		getFile(path, function(e,file,stat){

	 			if (e) {
				    res.send(404);
				    return config.error("File not found : " + path)
			    };

	 			//Far future cache the user avatar - all new avatars would be a new image
				var headers = {
			        'Content-Type': 'image/png',
			        "Accept-Ranges" : "bytes",
			        "Last-Modified" : stat.mtime.toUTCString(),
			        "etag" : '"' + (stat.size + '-' + Number(stat.mtime)) + '"',
			      	'Cache-Control': 'public, max-age: ' + (360 * 24 * 60 * 60)
			    };

			    res.writeHead(200, headers);
				res.end(file);
	 		});
	 	},

	 	//Gets User Avatar photo
	 	//Rules: Not authenticated ; Far-futures expiry
	 	getUserAvatar : function(req, res) {

		    var size = req.query.size;
		    var size = isNaN(_fn.sanitize(size).toInt()) ? 100 : _fn.sanitize(size).toInt();

	 		//Sanitize filename to prevent hacks
	 		var filename = ("" + req.params.id).replace(/\.\.\//g,"");

 		    var etag = req.headers["if-none-match"];

		    var response = function(e, file, stat) {

			    if (etag && (etag == ('"' + stat.size + '-' + Number(stat.mtime) + '"'))){
				    return res.send(304);
			    }

			    //Far future cache the user avatar - all new avatars would be a new image
			    var headers = {
				    'Content-Type': 'image/png',
				    "Accept-Ranges" : "bytes",
				    "Last-Modified" : stat.mtime.toUTCString(),
				    "etag" : '"' + (stat.size + '-' + Number(stat.mtime)) + '"',
				    'Cache-Control': 'public, max-age: ' + (360 * 24 * 60 * 60)
			    };

			    res.writeHead(200, headers);
			    res.end(file);
		    };

	 		getFile("/upl/" + filename + '.' + size, function(e, file, stat){

			    //Note: Dont break here pm errpr or that'll bring down whole server
			    if (e) {

				    getFile(config.DEFAULT_AVATAR.replace(/^\/(.*)$/,'$1') , response, true);
				    return config.error("File not found : " + filename);
			    }

			    response(e, file, stat);
	 		});
	 	}
 	};

 	//Used by all to grab the file
 	//TODO - this should change to something like Amazon S3 or some Cloud files storage
 	var getFile = function(path, callback, append) {

	    if(!append) path = basepath + path;

 		Step(
 			function readit(){
	 			fs.readFile(path, this.parallel());
	 			fs.stat(path, this.parallel());
			    this.parallel()(null, mime.lookup(path));
 			},
 			callback

		)
 	};

 	//================= Public interface (for othe modules to use) ===============
 	//Mainly has post-processing middleware functions for uploading various types of files

 	//NOTE: When these called - we've successfully uploaded file to temp, but it hasn't reached
 	//controller yet, which is next
 	//ALSO: req.uplFile would've been set with the file that got uploaded



 	pub.avatarImgUpload = function(req, res, next){
 		makeThumbsAndMove([100], false, "/uploads/upl/", req, res, next, req.session.uId + ".");
 	};

	pub.libImgUpload = function(req, res, next){
		makeThumbsAndMove([req.uplFile.w], false, "/uploads/upl/", req, res, next, req.session.uId + ".");
	};

 	//Common code used by various image upload post-processing handlers (above)
 	var makeThumbsAndMove = function(sizes, doFlash, destPath, req, res, next, pfix){


		var name = req.uplFile.hash;
	    var file = __dirname+'/../../uploads/tmp/'+name;
		if (!pfix) pfix = "";
		if (!req.socket.writable && !req.socket.readable) {
			res.send(JSON.stringify({success: false, preventRetry: true, error: "cancelled"}), {'Content-Type': 'text/plain'}, 404);
			return false;
		}
		Step(
			function makeThumbs(){
				_fn.img.makeThumb(file, sizes, doFlash, this.parallel());

				//TODO - File save should be on Amazon S3 instead
				_fn.file.ensureDir(destPath, this.parallel());
			},

			function moveFiles(e, files, basedir){
				if (e) throw e;

				config.log("Thumbs made : " + name);
				_fn.file.moveVariants(file, sizes, destPath + "/" + pfix + name, this);
			},

			next
		);
 	};

})(require);
