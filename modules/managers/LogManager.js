(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var log;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        log = db.use('DataDB');

        return pub;
    };

    pub.routeTripByDate = function(pid, pcompany_id, pdriver_id, passitant_id, pbus_id, pplan_date, pestimate_from, pestimate_to, pname, pdescription, pschedule_id, pstatus, pdaily_status, pfromdate, ptodate, page, num, _order, _desc, _keyword, cb) {
        log.query("CALL sb_route_trip_daily_search_paged(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pcompany_id, pdriver_id, passitant_id, pbus_id, pplan_date, pestimate_from, pestimate_to, pname, pdescription, pschedule_id, pstatus, pdaily_status, pfromdate, ptodate, page, num, _order, _desc, _keyword], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };
    //
    pub.eventByTripDate = function(proute_trip_id, proute_trip_date, pevent_index_id, page, num, _order, _desc, _keyword, cb){
        log.query("CALL sb_event_history_by_trip_date_paged(?, ?, ?, ?, ?, ?, ?, ?)", [proute_trip_id, proute_trip_date, pevent_index_id, page, num, _order, _desc, _keyword ], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };



})(module, require);
