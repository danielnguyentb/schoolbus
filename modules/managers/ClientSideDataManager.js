/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

/*
 * Client-side data manager
 */


(function(r) {

    var pub = {};		//public
    var _fn;			//utils
    var managers;
    var Step = r('step');

    //Init function - will return "pub" for public funcs
    module.exports = function(app, utils, mn) {

        _fn = utils;
        managers = mn;

        app.route("/dd/:hash").get(returnData);

        return pub;
    };

    //Handler of the /dd/ route
    //Returns the client-side data for that hash
    var returnData = function(req, res, next) {
        //if (!req.session.clientData)
        //    return res.send(404);

        var headers = req.headers;
        var ref = req.query._h || '';
        var hash = req.params.hash;

        Step (
            function getData() {

                ref = ref.split('|||');
                ref.push(headers.referer);

                for(var i in ref) {
                    if(!ref.hasOwnProperty(i)) break;
                    if(ref[i] == undefined) continue;

                    if(ref[i].indexOf(headers.host) == -1) ref[i] = headers.host + ref[i];

                    ref[i] = ref[i].replace("http://", "").replace("https://", "").split("?")[0];
                    if(req.session.bid != '') ref[i] = ref[i].replace('/' + req.session.bid + '/', '/');
                    if(req.session.gid != '') ref[i] = ref[i].replace('/_' + req.session.gid + '/', '/');
                }

                managers.L2.hget('clientData##data', hash, this.parallel());
                managers.L2.hget('clientData##pages', hash, this.parallel());
            },
            function valid(e, d, p) {
                if(e || !p) throw Error(404);
                if(!d) throw Error(304);

                if (!ref.length || ref.indexOf(p) == -1) throw Error(404);
                d = "$dd.s('" + d + "');";

                _fn.r(d, this.parallel());
                managers.L2.hdel('clientData##data', hash, this.parallel());
                managers.L2.hdel('clientData##pages', hash, this.parallel());
            },
            function response(e, r) {
                if(e) res.send(e.message);
                else res.send(r);
            }
        );

        //Check referring URL
        //var headers = req.headers;
        //var ref = req.query._h || '';
        //var hash = req.params.hash;
        //ref = ref.split('|||');
        //ref.push(headers.referer);
        //
        //for(var i in ref) {
        //    if(!ref.hasOwnProperty(i)) break;
        //    if(ref[i] == undefined) continue;
        //
        //    if(ref[i].indexOf(headers.host) == -1) ref[i] = headers.host + ref[i];
        //
        //    ref[i] = ref[i].replace("http://", "").replace("https://", "").split("?")[0];
        //    if(req.session.bid != '') ref[i] = ref[i].replace('/' + req.session.bid + '/', '/');
        //    if(req.session.gid != '') ref[i] = ref[i].replace('/_' + req.session.gid + '/', '/');
        //}
        //
        //if (!ref.length || !req.session.clientData.pages[hash] || ref.indexOf(req.session.clientData.pages[hash]) == -1) {
        //    return res.send(404);
        //}
        //
        ////send the data
        //var dd = req.session.clientData[hash];
        //if (dd) {
        //    if (dd == 304)
        //        return res.send(304);
        //    else
        //        res.send("$dd.s('" + JSON.stringify(dd) + "');");
        //}
        //else
        //    return res.send(404);
        //
        ////clear that data item
        //req.session.clientData[hash] = null;
        //req.session.clientData.pages[hash] = null;
        //delete req.session.clientData.pages[hash];
        //delete req.session.clientData[hash];
        //
        //req.session.save();
    };

    //---

    //This is the function called by others to set the data
    pub.store = function(req, key, data) {

        //if (!req.session.clientData)
        //    setup(req);

        var hash = _fn.sec.md5(key + Math.random().toString());

        managers.L2.hset('clientData##data', hash, JSON.stringify(data), function() {});
        managers.L2.hset('clientData##pages', hash, req.headers.host + req.path, function() {});

        //req.session.clientData[hash] = data;
        //
        ////Quick fix for ref check return 404
        //if(!req.session.clientData.pages) req.session.clientData.pages = {};
        //req.session.clientData.pages[hash] = req.headers.host + req.path;

        return hash;
    };

    pub.setCSRF = function(req, res) {
        return pub.store(req, req.sessionID + "##wcl", {csrf: res.locals._csrf});
    };

    var setup = function(req) {
        req.session.clientData = null;
        req.session.clientData = {
            pages  : {},
            numItms: 0
        };
    };


})(require);