/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var groups;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        groups = db.use('DataDB');

        return pub;
    };

    pub.getListGroupPaged = function(page, num, order, desc, keyword, cb) {

        groups.query("CALL sb_get_list_group_paged(?, ?, ?, ?, ?)", [page, num, order, desc, keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getGroupById = function(pid, cb) {

        groups.query("CALL sb_group_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertGroup = function(name, type, status, owner_type, owner_id, pdesc, porder, cb) {

        groups.query("CALL sb_group_insert(?, ?, ?, ?, ?, ?, ?)", [name, type, status, owner_type, owner_id, pdesc, porder], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getGroupByUser = function(puid, ptype, cb) {

        groups.query("CALL sb_group_get_by_user_id_user_type(?, ?)", [puid, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getGroupByListId = function(plistid, cb) {

        groups.query("CALL sb_group_get_info_by_listid(?)", [plistid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.addUserToMultiGroup = function(puid, pgroupids, pstatus, premark, ptype, cb) {

        groups.query("CALL sb_user_group_add_to_multi_group(?, ?, ?, ?, ?)", [puid, pgroupids, pstatus, premark, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.removeUserMultiGroup = function(puid, pgroupids, ptype, cb) {

        groups.query("CALL sb_user_group_remove_multi_group(?, ?, ?)", [puid, pgroupids, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        })
    };

    pub.updateGroup = function(id, name, type, status, owner_type, owner_id, desc, porder, cb) {

        groups.query("CALL sb_group_update(?, ?, ?, ?, ?, ?, ?, ?)", [id, name, type, status, owner_type, owner_id, desc, porder], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.removeGroup = function(puid, pid, cb) {

        groups.query("CALL sb_group_delete(?, ?, ?)", [pid, puid, 2], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

})(module, require);
