/**
 * Created by AnhNguyen
 */

(function(r, ns) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var users;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        users = db.use('DataDB');

        return pub;
    };

    pub.checkUserExist = function(pid, ptype, pvalue, cb) {

        users.query("CALL sb_users_check_exist(?, ?, ?);", [pid, ptype, pvalue], function(e, r) {

            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.checkCompanyExist = function(company_name, cb) {

        users.query("CALL sb_company_check_exist(?);", [company_name], function(e, r) {

            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateProfile = function(pid, pusername, pemail, pmobile, phome, poffice, pimage, pfullname, locale, past_task_time, cb) {
        users.query("CALL sb_users_update_info(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pusername, pemail, pmobile, phome, poffice, pimage, pfullname, locale, past_task_time], function(e, r) {

            if(e) config.error(e);

            cb(e, r);
        });
    };

    //type: Registed from App/UI:
    // 1: Parent
    // 2: Driver
    // 3: Assistant
    // 4: Company bus
    // 5: IT company
    // 6: Event Organizer
    // 7: Admin
    // 9: Bus account
    pub.insertUser = function(pusername, ppassword, pmanager_id, ptype, pemail, pmobile_phone, phome_phone, poffice_phone, pstatus, pactivate_code, pregistration_at, pimage, pfullname, cb) {

        users.query("CALL sb_users_insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
            pusername, ppassword, pmanager_id, ptype, pemail, pmobile_phone, phome_phone, poffice_phone, pstatus, pactivate_code, pregistration_at, pimage, pfullname
        ], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        })
    };

    pub.updateUserPasswordWithToken = function(pid, email, pass, cb) {

        Step (
            function() {
                users.query("DELETE FROM `sb_tokens` WHERE `key` = ?;", [email], this);
            },
            function(e, r) {
                if(e) throw e;

                users.query("CALL sb_users_update_password(?, ?)", [pid, pass], this);
            },
            function(e, r) {
                if(e) config.error(e);

                cb(e, r);
            }
        )
    };

    pub.updateUserPassword = function(pid, pass, cb) {
        users.query("CALL sb_users_update_password(?, ?)", [pid, pass], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertResetToken = function(pemail, ptoken, cb) {

        users.query("CALL sb_user_insert_token(?, ?)", [pemail, ptoken], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getUserById = function(pid, cb) {

        users.query("CALL sb_user_get_info_by_id(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getUserCompanyByType = function(pid, ptype, cb) {

        users.query("CALL sb_users_company_get_companyinfo_by_type(?, ?)", [pid, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getUserByToken = function(pemail, ptoken, cb) {
        users.query("CALL sb_get_user_by_token(?, ?)", [pemail, ptoken], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.userGroupUpdate = function(puid, pgroupid, pstatus, cb) {

        users.query("CALL sb_user_group_update(?, ?, ?, ?, ?)", [puid, pgroupid, pstatus, null, null], function(e, r) {

            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.userGetByEmail = function(pemail, cb) {

        users.query("CALL sb_user_get_info_by_email(?)", [pemail], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.userApprove = function(puid, ptype, cb) {

        users.query("CALL sb_user_approval(?, ?)", [puid, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getUserCompany = function(puid, cb) {

        users.query("CALL sb_get_user_company(?)", [puid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.userCompanyApprove = function(puid, pcompid, ptype, cb) {

        users.query("CALL sb_user_company_approval(?, ?, ?)", [puid, pcompid, ptype],  function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertUserCompany = function(puid, pcompany_id, ptype, pstatus, pNFC, pQR, pimage, cb) {

        users.query("CALL sb_user_company_insert(?, ?, ?, ?, ?, ?, ?)", [puid, pcompany_id, ptype, pstatus, pNFC, pQR, pimage], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getUserByType = function(ptype, cb) {

        users.query("CALL sb_user_get_by_type(?)", [ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updatePassword = function(pid, ppass, cb) {

        users.query("CALL sb_users_update_password(?, ?)", [pid, ppass], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getUserLogIn = function(uname, upass, cb) {
        
        users.query("CALL sb_user_login(?, ?)", [uname, upass], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateActiveUser = function (puid, cb) {

        users.query("UPDATE `sb_users` SET `status` = 1 WHERE `id` = ?", [puid], function (e, r) {

            if (e) config.error(e);
            else users.query("UPDATE `sb_user_group` SET `status` = 1 WHERE `user_id` = ?", [puid], function(e, r) {

                if (e) config.error(e);

                cb(e, r);
            });

        })
    };

    pub.getUserPendingApproval = function(page, numrow, order, desc, keyword, cb) {

        users.query("CALL sb_get_list_user_company_approval(?, ?, ?, ?, ?)", [page, numrow, order, desc, keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getUserByGroup = function(pid, cb) {

        users.query("CALL sb_users_list_by_group(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.searchUsers = function(preg_from, pkeyword, pcompany_name, pcompany_role, page, num, cb) {

        users.query("CALL sb_user_search(?, ?, ?, ?, ?, ?)", [preg_from, pkeyword, pcompany_name, pcompany_role, page, num], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getUserByIdWithCompany = function(pid, pcompany_id, role, cb) {
        users.query("CALL sb_users_company_get_by_company_role_status(?, ?, ?, ?)", [pid, pcompany_id, role, '0,1'], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.saveUserDeviceToken = function(puser_id, pkey, cb) {
        users.query("CALL sb_user_notification_key_update(?, ?)", [puser_id, pkey], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.removeUserDeviceToken = function(puser_id, pkey, cb) {
        users.query("CALL sb_user_notification_key_remove(?, ?)", [puser_id, pkey], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getUserDeviceTokens = function(puser_id, cb) {
        users.query("CALL sb_user_notification_key_get_by_user_id(?)", [puser_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

})(require, module);
