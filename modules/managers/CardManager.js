(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var card;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        card = db.use('DataDB');

        return pub;
    };

    pub.updateCardInfoByOwner = function(powner_type, powner_id, pcard_code, pcard_type, prequest_by, papprove_by, pstatus, pdescription, cb) {

        card.query("CALL sb_update_card_info_by_owner(?, ?, ?, ?, ?, ?, ?, ?)", [powner_type, powner_id, pcard_code, pcard_type, prequest_by, papprove_by, pstatus, pdescription], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCardInfoByOwner = function(powner_type, powner_id, cb) {

        card.query("CALL sb_get_card_info_by_owner(?, ?)", [powner_type, powner_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCardInfoByCard = function(pcard_code, pcard_type, cb) {

        card.query("CALL sb_get_card_info_by_card(?, ?)", [pcard_code, pcard_type], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };


    pub.insertLocation = function(company_id, uid, lat, long, name, address, cb) {
        card.query("CALL sb_location_dic_insert(?, ?, ?, ?, ?, ?, ?)", [company_id, name, address, lat, long, 0, uid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getLocationAround = function(pX, pY, prange, pcompany_id, cb) {
        card.query("CALL sb_location_dic_get_location_around(?, ?, ?, ?)", [pX, pY, prange, pcompany_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getLocationInfo = function (pid, cb){
        card.query("CALL sb_location_dic_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.getCardInPair = function(pid, pqr_code, pnfc_code, cb) {
        card.query("CALL sb_card_in_pair_search_paged(?, ?, ?, ?, ?, ?)", [pid, pqr_code, pnfc_code, 123, 1, 1], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateCardInPair = function(pid, powner_id, pstatus, cb) {
        card.query("CALL sb_card_in_pair_update_v2(?, ?, ?)", [pid, powner_id, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertBusCard = function(pbus_id, ppair_card_id, pNFC, pQRCode, pcard_type, pcreaded_by, pstatus, cb) {
        card.query("CALL sb_bus_card_insert(?, ?, ?, ?, ?, ?, ?)", [pbus_id, ppair_card_id, pNFC, pQRCode, pcard_type, pcreaded_by, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getBusCard = function(pbus_id, pcard_type, cb) {
        card.query("CALL sb_bus_card_get_by_busid(?, ?)", [pbus_id, pcard_type], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
})(module, require);
