/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var childs;

    ns.exports = function (_db, utils, mn) {
        db       = _db;
        _fn      = utils;
        managers = mn;

        childs = db.use('DataDB');

        return pub;
    };

    pub.getListChildPaged = function(companyid, page, num, order, desc, keyword, cb) {

        childs.query("CALL sb_child_get_by_company_paged(?, ?, ?, ?, ?, ?)", [companyid, page, num, order, desc, keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        })
    };

    pub.getListChildByParentPaged = function(parent_id, page, num, order, desc, keyword, cb) {

        childs.query("CALL sb_child_get_by_parent_id(?, ?, ?, ?, ?, ?)", [parent_id, page, num, order, desc, keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        })
    };

    pub.getParentChildByType = function(parent_id, child_id, type, cb) {

        childs.query("CALL sb_parent_child_get_by_type(?, ?, ?)", [parent_id, child_id, type], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertChild = function(pname, pid_no, pmobile, pNFC, pQRCode, pcompany_id, pimage, pstatus, pcreate_by, pOTP, cb) {

        childs.query("CALL sb_child_insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pname, pid_no, pmobile, pNFC, pQRCode, pcompany_id, pimage, pstatus, pcreate_by, pOTP], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getChildById = function(pid, ptype, cb) {

        childs.query("CALL sb_child_get_info(?, ?)", [pid, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getChildByCode = function(pid, ptype, pnfccode, pqrcode, cb) {

        childs.query("CALL sb_child_get_child_by_card_code(?, ?, ?, ?)", [pid, ptype, pnfccode, pqrcode], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getGuardianByCode = function(pid, pnfccode, pqrcode, cb) {

        childs.query("CALL sb_guardia_get_guardian_by_card_code(?, ?, ?)", [pid, pnfccode, pqrcode], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertGuardianChild = function(pguardian_id, pchild_id, preg_by, pstatus, group_id, cb) {

        childs.query("CALL sb_guardian_child_insert(?, ?, ?, ?, ?)", [pguardian_id, pchild_id, preg_by, pstatus, group_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.syncChildCard = function(temp_profile, child_id, cb) {

        childs.query("CALL sb_guardian_child_insert(?, ?)", [temp_profile, child_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateTempProfile = function(pid, pname, pcompany_id, pstatus, cb) {

        childs.query("CALL sb_child_temp_update(?, ?, ?, ?)", [pid, pname, pcompany_id, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateChildOtp = function(pchild_id, potp, cb) {

        childs.query("CALL sb_child_update_otp(?, ?)", [pchild_id, potp], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateChild = function(pid, pname, pid_no, pmobile, pNFC, pQRCode, pcompany_id, pimage, pstatus, pcreate_by, cb) {

        childs.query("CALL sb_child_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pname, pid_no, pmobile, pNFC, pQRCode, pcompany_id, pimage, pstatus, pcreate_by], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.removeChild = function(pid, cb) {

        childs.query("CALL sb_child_delete(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateCardInfoByOwner = function(powner_type, powner_id, pcard_code, pcard_type, prequest_by, papprove_by, pstatus, pdescription, cb) {

        childs.query("CALL sb_update_card_info_by_owner(?, ?, ?, ?, ?, ?, ?, ?)", [powner_type, powner_id, pcard_code, pcard_type, prequest_by, papprove_by, pstatus, pdescription], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCardInfoByOwner = function(powner_type, powner_id, cb) {

        childs.query("CALL sb_get_card_info_by_owner(?, ?)", [powner_type, powner_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCardInfoByCard = function(pcard_code, pcard_type, cb) {

        childs.query("CALL addGroup(?, ?)", [pcard_code, pcard_type], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };



    pub.createGuardian = function(pname, pimage, pcard_code, pcard_type, pcreaded_by, pstatus, pdescription, cb) {

        childs.query("CALL sb_guardian_create(?, ?, ?, ?, ?, ?, ?)", [pname, pimage, pcard_code, pcard_type, pcreaded_by, pstatus, pdescription], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertGuardian = function(pname, pimage, pcard_code, pnfc_code, pcreaded_by, pcparent_id, pstatus, cb) {

        childs.query("CALL sb_guardian_insert(?, ?, ?, ?, ?, ?, ?)", [pname, pimage, pcard_code, pnfc_code, pcreaded_by, pcparent_id, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.parentChildInsert = function(pparent_id, pchild_id, prelationship, potp, pstatus, cb) {

        childs.query("CALL sb_parent_child_insert(?, ?, ?, ?, ?)", [pparent_id, pchild_id, prelationship, potp, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        })
    };

    pub.parentChildUpdate = function(pparent_id, pchild_id, prelationship, potp, pstatus, cb) {

        childs.query("CALL sb_parent_child_update(?, ?, ?, ?, ?)", [pparent_id, pchild_id, prelationship, potp, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.parentChildUpdateByType = function(pparent_id, pchild_id, prelationship, potp, pstatus, cb) {

        childs.query("CALL sb_parent_child_update_by_type(?, ?, ?, ?, ?)", [pparent_id, pchild_id, prelationship, potp, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.guardianChildUpdate = function(pguardian_id, pchild_id, pstatus, cb) {

        childs.query("CALL sb_guardian_child_update(?, ?, ?)", [pguardian_id, pchild_id, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.parentChildUpdateById = function(pid, pparent_id, pchild_id, prelationship, potp, pstatus, cb) {

        childs.query("CALL sb_parent_child_update_by_id(?, ?, ?, ?, ?, ?)", [pid, pparent_id, pchild_id, prelationship, potp, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getParentChildByChild = function(pchild_id, prelation, cb) {

        childs.query("CALL sb_parent_child_get_parent_by_type(?, ?)", [pchild_id, prelation], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getListParentChildByType = function(puid, pchildid, ptype, cb) {

        childs.query("CALL sb_parent_child_get_list_by_type(?, ?, ?)", [puid, pchildid, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getListParentByRouteTrip = function(proute_trip_id, pdate, cb) {

        childs.query("CALL sb_route_trip_get_list_parent(?, ?)", [proute_trip_id, pdate], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getListGuardianByChild = function(pchildId, pstatus, cb) {

        childs.query("CALL sb_guardian_get_by_child_id_v2(?, ?)", [pchildId, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getParentChildByOtp = function(pchild_id, potp, cb) {

        childs.query("CALL sb_parent_child_get_by_otp(?, ?)", [pchild_id, potp], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getListChildByParentChild = function(parent_id, prelation, cb) {

        childs.query("CALL sb_get_list_child_by_parent_child(?, ?)", [parent_id, prelation], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getListChildByGuardianChild = function(guardian_id, cb) {

        childs.query("CALL sb_get_list_child_by_guardian_child(?)", [guardian_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getListChildByIDs = function(ids, cb) {

        childs.query("CALL sb_child_get_info_list(" + ids + ")", [], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getListChildByGuardian = function(pguardian_id, cb) {

        childs.query("CALL sb_get_list_child_by_guardian(?)", [pguardian_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.checkCardInPair = function(qr_code, nfc_code, cb) {
        childs.query("CALL sb_card_in_pair_search_paged(?, ?, ?, ?, ?, ?)", [0, qr_code, nfc_code, 123, 1, 10], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.insertCardInPair = function(qr_code, nfc_code, owner_type, powner_id, uid, group_id, bus_type, pair_id, cb) {
        childs.query("CALL sb_card_in_pair_insert(?, ?, ?, ?, ?, ?, ?, ?, ?)", [nfc_code, qr_code, owner_type, powner_id, 1, uid, group_id, bus_type, pair_id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.insertCardInfo = function(power_type, pbus_type, powner_id, pcard_code, pcard_type, prequest_by, papproval_by, pstatus, pdescription, cb){
        childs.query("CALL sb_card_info_insert(?, ?, ?, ?, ?, ?, ?, ?, ?)", [power_type, pbus_type, powner_id, pcard_code, pcard_type, prequest_by, papproval_by, pstatus, pdescription], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.updateCardInPairStatus = function(qrcode, nfccode, status, cb) {
        childs.query("CALL sb_card_in_pair_update_status(?, ?, ?)", [qrcode, nfccode, status], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.checkCardType = function(code, cb) {
        childs.query("CALL sb_card_dic_get_by_card_code(?)", [code], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.addCardGroup = function(pname, cb) {
        childs.query("CALL sb_group_paired_card_insert(?)", [pname], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.getCardGroup = function(cb) {
        childs.query("CALL sb_group_paired_incomplete_paged(?, ?)", [1, 100], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.groupSetCount = function(id, cb) {
        childs.query("CALL sb_group_paired_card_card_in_group(?)", [id], function(e, r) {
      //  childs.query("CALL sb_group_paired_card_count_card_in_group(?)", [id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.groupComplete = function(id, cb) {
        childs.query("CALL sb_group_paired_complete(?)", [id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.groupGetInfo = function(id, cb) {
        childs.query("CALL sb_group_paired_getinfo(?)", [id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.groupType = function(id, type, cb) {
        childs.query("CALL sb_group_paired_update_type(?, ?)", [id, type], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.getChildByCardCode = function(pid, ptype, pnfccode, pqrcode, cb) {

        childs.query("CALL sb_child_get_child_by_card_code(?, ?, ?, ?)", [pid, ptype, pnfccode, pqrcode], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getGuardianByCardCode = function(pid, pnfccode, pqrcode, cb) {

        childs.query("CALL sb_guardian_get_by_card_code(?, ?, ?)", [pid, pnfccode, pqrcode], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getGuardianById = function(pid, cb) {

        childs.query("CALL sb_guardian_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateGuardianInfo = function(pid, pname, pimage, pstatus, pparent_id, cb) {

        childs.query("CALL sb_guardian_udate_info_v2(?, ?, ?, ?, ?)", [pid, pname, pimage, pstatus, pparent_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.guardianChildInsert = function(pguardian_id, pchild_id, preg_by, pstatus, cb) {

        childs.query("CALL sb_guardian_child_insert(?, ?, ?, ?, ?)", [pguardian_id, pchild_id, preg_by, pstatus, ''], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getNotifications = function(pfor_id, pstatus, cb) {
        childs.query("CALL sb_notifications_search_paged(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [123, 0, 0, pfor_id, '', pstatus, 0, 0, 0, '', ''], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateNotificationsStatus = function(pid, pstatus, cb) {
        childs.query("CALL sb_notifications_update_status(?, ?)", [pid, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getPassiveLastAction = function(ppassive_party_type, ppassive_party_id, cb){
        childs.query("CALL sb_event_history_get_by_passive(?, ?)", [ppassive_party_type, ppassive_party_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getWorkeronRoute = function(route_trip_id, cb){
        childs.query("CALL sb_event_history_get_worker_onroute(?)", [route_trip_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getCompanyAdmin = function(company_id, cb){
        childs.query("CALL sb_get_company_admin(?)", [company_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.notificationInsert = function(pis_ack, pfrom, pto, pfor_id, pmessage, pstatus, pgluoon_id, preply_for, cb){
        childs.query("CALL sb_notifications_insert(?, ?, ?, ?, ?, ?, ?, ?)", [pis_ack, pfrom, pto, pfor_id, pmessage, pstatus, pgluoon_id, preply_for], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

})(module, require);
