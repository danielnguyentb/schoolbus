/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var notification;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        notification = db.use('DataDB');

        return pub;
    };
    pub.getListNotificationPaged = function(pto, pstatus, cb) {
        notification.query("CALL sb_notifications_search_paged(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [123, 0, pto, 0, '', pstatus, 0, 0, 0, '', ''], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
})(module, require);
