/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var configs;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        configs = db.use('DataDB');

        return pub;
    };

    pub.getConfigAll = function(cb) {

        configs.query("CALL sb_sys_config_get_all()", [], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateConfigAll = function(pauto_alight_after, pmap_tracking, pparent_map, pnew_index, pincrease, pdecrease, pmax_index, pmanual_index, pgps_report, plocation_range, cb) {

        configs.query("CALL sb_sys_config_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pauto_alight_after, pmap_tracking, pparent_map, pnew_index, pincrease, pdecrease, pmax_index, pmanual_index, pgps_report, plocation_range], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getBcConfig = function(pcompany_id, cb) {
        configs.query("CALL sb_bc_config_get_by_company_id(?)", [pcompany_id], function(e, r) {
            if(e) config.error(e);

            var cfg = {
                map_tracking: 1,
                parent_map: 1
            };

            if(r.length && r[0].length) cfg = _fn.extend(cfg, r[0][0]);

            cb(e, cfg);
        });
    };

    pub.saveBcConfig = function(pcompany_id, pmap_tracking, pparent_map, cb) {
        configs.query("CALL sb_bc_config_update(?, ?, ?)", [pcompany_id, pmap_tracking, pparent_map], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

})(module, require);
