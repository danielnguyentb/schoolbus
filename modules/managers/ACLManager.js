/**
 * Created by AnhNguyen
 */

(function (r) {

    var pub       = {};
    var _fn, db, managers, acl;
    var Step      = r('step'),
        Acl       = r('acl'),
        Sequelize = r('sequelize'),
        AclSeq    = r('acl-sequelize'),
        res;

    module.exports = function (_db, utils, mn) {

        _fn      = utils;
        db       = _db;
        managers = mn;

        var connectCfg = db.use('DataDB').config.connectionConfig;
        acl = new Acl(
            new AclSeq(
                new Sequelize(
                    connectCfg.database,
                    connectCfg.user,
                    connectCfg.password,
                    {
                        host   : connectCfg.host,
                        dialect: 'mysql'
                    }
                ),
                {prefix: 'acl_'}
            )
        );

        return pub;
    };

    pub.getUsersInRoles = function(roles, cb) {
        if(!Array.isArray(roles)) roles = [roles];
        var criteria = {
            _bucketname : "users",
            $or : []
        }, tmp;

        roles.forEach(function(v) {
            tmp = {};
            tmp[v] = true;
            criteria['$or'].push(tmp);
        });

        res.distinct('key', criteria, cb);
    };

    pub.addUsertoRole = function(uid, roles, cb) {
        acl.addUserRoles(uid, roles, cb);
    };

    pub.allow = function(role, resources, permissions, cb) {
        acl.allow(role, resources, permissions, cb);
    };

    pub.removeAllow = function(role, resources, permissions, cb) {
        acl.removeAllow(role, resources, permissions, cb);
    };

    pub.whatResources = function(role, cb) {
        acl.whatResources(role, cb);
    };

    pub.areAnyRolesAllowed = function(roles, resource, permissions, cb) {
        acl.areAnyRolesAllowed(roles, resource, permissions, cb);
    };

    pub.userRoles = function(uid, cb) {
        acl.userRoles(uid, cb);
    };

    pub.userRoleInRes = function(uid, resource, cb) {

        var $this = this;
        Step (
            function getRoles() {
                $this.userRoles(uid, this);
            },
            function findResource(e, r) {
                if(e) throw e;

                var role = '';
                for(var i = 0, len = r.length; i < len; i++) {
                    if(r[i].indexOf(resource) != -1) {
                        role = r[i];
                        break;
                    }
                }

                return {
                    u: uid,
                    r: role.replace('-' + resource, '')
                };
            },
            cb
        );
    };

    pub.allowedPermissions = function(uid, resources, cb) {
        acl.allowedPermissions(uid, resources, cb);
    };

    pub.isAllowed = function(uId, resource, permissions, cb) {
        acl.isAllowed(uId, resource, permissions, cb);
    };

    //Get role permissions in resource
    pub.resourcePermissions = function(role, resource, cb) {
        acl._resourcePermissions(role, resource).nodeify(cb);
    };

    pub.removeUserRoles = function(userId, roles, cb) {
        acl.removeUserRoles(userId, roles, cb);
    };

})(require);
