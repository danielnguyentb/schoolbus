//Loader for all managers

(function (r) {

	//init
	module.exports = function (app, db, utils) {

		var managers = {};

		managers.prefix = 'wump_';
		managers.prefixDt = 'wump__';

		managers.clientData = r("./ClientSideDataManager.js")(app, utils, managers);
		managers.privateData = r("./PrivateDataManager.js")(app, utils);
		managers.L2 = r("./L2.js")(db, utils, managers);
		managers.LBA = r("./LBAManager.js")(db, utils, managers);
		managers.acl = r("./ACLManager.js")(db, utils, managers);
		managers.user = r("./UserManager.js")(db, utils, managers);
		managers.comp = r("./CompanyManager.js")(db, utils, managers);
		managers.group = r("./GroupManager.js")(db, utils, managers);
		managers.child = r("./ChildManager.js")(db, utils, managers);
        managers.driver = r("./DriverManager.js")(db, utils, managers);
        managers.assistant = r("./AssistantManager.js")(db, utils, managers);
        managers.bus = r("./BusManager.js")(db, utils, managers);
        managers.card = r("./CardManager.js")(db, utils, managers);
        managers.log = r("./LogManager.js")(db, utils, managers);
        managers.location = r("./LocationManager.js")(db, utils, managers);
        managers.notification = r("./NotificationManager.js")(db, utils, managers);
        managers.route = r("./RouteManager.js")(db, utils, managers);
        managers.calendar = r("./CalendarManager.js")(db, utils, managers);
        managers.config = r("./ConfigManager.js")(db, utils, managers);

		return managers;

	};

})(require);

