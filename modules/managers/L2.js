/* Copyright (c) 2014 WCL, Inc. All rights reserved.
 * 
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

/* 
 * L2 Cache Manager
 *
 * The L2 cache is a REDIS cache that is used like memcache - to
 * cache query results or rendered pages
 *
 * Controllers call L2 directly so its clear when reading the code 
 * and L2 cache - if it misses - calls the other managers to run the full query
 */

(function (r) {

	var _fn, db, managers;

	var Step = r("step");

	var redis = require("redis");

	var L2;

	//public funcs
	var pub = {};
	var async = r('async');

	module.exports = function (a, u, mn) {
		db = a;

		L2 = db.use("L2");

		_fn = u;
		managers = mn;

		return pub;
	}


	pub.getSiteConfig = function (callback) {
		L2.del(managers.prefix + "##config", function (e, r) {});
		L2.get(managers.prefix + "##config", function (e, r) {
			if (e) return callback(e);
			if (r) return callback(null, JSON.parse(r));		//if cached, done here

			//otherwise cache miss
			managers.user.getSiteConfig(

				function (e, r) {
					if (e) return callback(e);

					r = r[0];
					var t, tmp;

					r.tree = JSON.parse(r.mod);

					//Company module/functions
					if (r && r.mod && r.mod.length) {
						t = {};
						tmp = JSON.parse(r.mod);
						tmp.forEach(function (m) {
							t[m.id] = m.value;
						});
						r.mod = t;
					}

					//Company action
					if (r && r.act && r.act.length > 0) {
						t = {};
						tmp = JSON.parse(r.act);
						tmp.forEach(function (m) {
							t[m.id] = {"code": m.code, "name": m.value};
						});
						r.act = t;
					}

					//Group module/functions
					if(r && r.gmod && r.gmod.length) {
						t = {};
						tmp = JSON.parse(r.gmod);
						tmp.forEach(function(m) {
							t[m.id] = m.value;
						});
						r.gmod = t;
					}

					//Group action
					if(r && r.gact && r.gact.length) {
						t = {};
						tmp = JSON.parse(r.gact);
						tmp.forEach(function (m) {
							t[m.id] = {"code": m.code, "name": m.value};
						});
						r.gact = t;
					}

					//Set in cache but dont wait for it - return immediately
					L2.set(managers.prefix + "##config", JSON.stringify(r), function () {});

					callback(null, r);
				});
		});
	};
	pub.getUserSessionList = function (callback) {
		L2.smembers(managers.prefix + "##usersessionlist", function (e, r) {
			console.log(r);
			return callback(e, r);
		});
	};
	pub.checkUserSessionList = function (id, callback) {
		L2.sismember(managers.prefix + "##usersessionlist", id, function (e, r) {
			return callback(e, r);
		});
	};

	pub.updateUserSessionList = function (lst, callback) {
		try {
			lst = lst.split(',');
		} catch (e) {

		}

		L2.sadd(managers.prefix + "##usersessionlist", lst, function (e, r) {
			callback(e, r);
		});

	};
	pub.removeUserSessionList = function (lst, callback) {
		try {
			lst = lst.split(',');
		} catch (e) {

		}
		L2.srem(managers.prefix + "##usersessionlist", lst, function (e, r) {
			callback(e, r);
		});

	};

	pub.removeKeyWithPrefix = function(prefix, cb) {

		Step (
			function getKeys() {
				L2.keys(prefix, this);
			},
			function delKeys(e, r) {
				if(e) throw e;

				var group = this.group();
				r.forEach(function(v) {
					L2.del(v, group());
				});
			},
			function res(e, r){
				cb(e, r);
			}
		);
	};

    pub.sadd = function (key, val, cb) {
        L2.sadd(key, val, function(e,r){
            cb(e,r);
        });
    };

	pub.smembers = function(key, cb) {
		L2.smembers(key, cb);
	};

	pub.set = function (key, val, cb) {
		L2.set(key, JSON.stringify(val), cb);
	};

	pub.get = function (c, cb) {
		L2.get(c, function (err, d) {
			cb(err, d);
		});
	};

	pub.del = function (c, cb) {
		L2.del(c, function (err, d) {
			cb(err, d);
		});
	};

	pub.hset = function (key, field, val,cb) {
		L2.hset(key, field, val, function(e,r){
			cb(e,r);
		});
	};

	pub.hget = function (key, field,cb) {
		L2.hget(key, field, function(err, d){
			cb(err,d);
		});
	};

	pub.hdel = function(key, field, cb) {

		L2.hdel(key, field, function(err, d){
			cb(err,d);
		});
	};

	pub.hkeys = function (key,cb) {
		L2.hgetall(key , function (e, r) {
		  cb(e,r);
		});
	};

	pub.setex = function(key, value, ttl, cb) {
		L2.setex(key, ttl, value, function(e, r) {
			if(e) config.error(e);

			cb(e, r);
		});
	};

	
})(require);
