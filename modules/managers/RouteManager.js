/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var routes;

    ns.exports = function (_db, utils, mn) {
        db       = _db;
        _fn      = utils;
        managers = mn;

        routes = db.use('DataDB');

        return pub;
    };

    pub.routeTripSearchPaged = function(pid, pcompany_id, pdriver_id, passitant_id, pbus_id, pplan_date, pestimate_from, pestimate_to, pname, pdescription, pschedule_id, pstatus, pdaily_status, page, num, _order, _desc, _keyword, cb) {
        routes.query("CALL sb_route_trip_search_paged(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pcompany_id, pdriver_id, passitant_id, pbus_id, pplan_date, pestimate_from, pestimate_to, pname, pdescription, pschedule_id, pstatus, pdaily_status, page, num, _order, _desc, _keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.checkDriverAvaliable = function(pdriver_id, ptrip_id, pplan_date, pestimate_from, pestimate_to, cb){
        routes.query("CALL sb_route_trip_check_driver_isused(?, ?, ?, ?, ?)", [pdriver_id, ptrip_id, pplan_date, pestimate_from, pestimate_to], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };
    pub.checkBusAvaliable = function(pbus_id, ptrip_id, pplan_date, pestimate_from, pestimate_to, cb){
        routes.query("CALL sb_route_trip_check_bus_isused(?, ?, ?, ?, ?)", [pbus_id, ptrip_id, pplan_date, pestimate_from, pestimate_to], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };
    pub.clonePoints = function(fptrip_id, sptrip_id, uid, cb){
        routes.query("CALL sb_route_point_plan_clone(?, ?, ?)", [fptrip_id, sptrip_id, uid], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };
    pub.routeTripPaged = function(puid, ppage, pnumrow,  _order, _desc, cb) {

        routes.query("CALL sb_route_trip_by_bc_paged(?, ?, ?, ?, ?)", [puid, ppage, pnumrow, _order, _desc], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeScheduleUpdate = function (pid, pname, pstart_date, pend_date, pestimate_from, pestimate_to, cb) {
        routes.query("CALL sb_schedule_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pname, 'Weekly', pstart_date, pend_date, pestimate_from, pestimate_to, 1, 1, 1], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };
    pub.routeScheduleCreate = function(pname, pstart_date, pend_date, pestimate_from, pestimate_to, pcreated_by, cb) {
        routes.query("CALL sb_schedule_insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pname, 'Weekly', pstart_date, pend_date, pestimate_from, pestimate_to, 1, 1, 1, pcreated_by], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.routeScheduleRouteCreate = function(proute_id, pname, pstart_date, pend_date, pestimate_from, pestimate_to, pcreated_by, cb) {
        routes.query("CALL sb_schedule_route_insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [proute_id, pname, 'Weekly', pstart_date, pend_date, pestimate_from, pestimate_to, 1, 1, 1, pcreated_by], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.routeScheduleGetById = function(pcid, cb) {
        routes.query("CALL sb_schedule_get_by_id(?)", [pcid], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.routeScheduleDateGetById = function(pcid, cb) {
        routes.query("CALL sb_schedule_specify_date_get_by_schedule_id(?)", [pcid], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.routeScheduleWeekGetById = function(pcid, cb) {
        routes.query("CALL sb_schedule_week_get_by_schedule_id(?)", [pcid], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.companyResource = function(puid, cb) {

        routes.query("CALL sb_bc_resource(?)", [puid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeTripGetInfoById = function(pid, cb) {
        routes.query("CALL sb_route_trip_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.busCheckInOut = function(pactive_party_id, plocation_id, paction, cb){
        routes.query("CALL sb_event_index_insert(?, ?, ?, ?, ?, ?)", [1, pactive_party_id, plocation_id, paction, 0, 1], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertEventHistory = function(proute_trip_id, plocation_id, pdevice_code, pactive_party_type, pactive_party_id, ppassive_party_type, ppassive_party_id, pcard_type, pcard_code, pcompany_id, cb){
        routes.query("CALL sb_event_history_insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [proute_trip_id, plocation_id, pdevice_code, pactive_party_type, pactive_party_id, ppassive_party_type, ppassive_party_id, pcard_type, pcard_code, pcompany_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertEventHistoryBasic = function(proute_trip_id, plocation_id, pdevice_code, pactive_party_type, pactive_party_id, ppassive_party_type, ppassive_party_id, pcard_type, pcard_code, pcompany_id, cb){
        console.log('route_id:'+proute_trip_id);
        console.log('pactive_party_id:'+pactive_party_id);
        console.log('ppassive_party_id:'+ppassive_party_id);

        routes.query("CALL sb_event_history_basic_insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [proute_trip_id, plocation_id, pdevice_code, pactive_party_type, pactive_party_id, ppassive_party_type, ppassive_party_id, pcard_type, pcard_code, pcompany_id], function(e, r) {
            if(e) config.error(e);
            console.log('sb_event_history_basic_insert');
            console.log(r);
            cb(e, r);
        });
    };

    pub.getCardLastAction = function(pcompany_id, plocation_id, pactive_party_type, pactive_party_id, ppassive_party_type, ppassive_party_id, cb){
        routes.query("CALL sb_event_index_get_last_action_by_child_v2(?, ?, ?, ?, ?, ?)", [pcompany_id, plocation_id, pactive_party_type, pactive_party_id, ppassive_party_type, ppassive_party_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getActiveLastAction = function(pactive_party_id, cb){
        routes.query("CALL sb_event_history_get_by_active(?)", [pactive_party_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getBusLastAction = function(pactive_party_id, cb){
        routes.query("CALL sb_event_history_get_by_bus(?)", [pactive_party_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getNotifications = function(pto, pstatus, cb) {
        routes.query("CALL sb_notifications_search_paged(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [123, 0, pto, 0, '', pstatus, 0, 0, 0, '', ''], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getPassiveLastAction = function(ppassive_party_type, ppassive_party_id, cb){
        routes.query("CALL sb_event_history_get_by_passive(?, ?)", [ppassive_party_type, ppassive_party_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
    //sb_event_history_get_by_active

    pub.getLocationInfo = function(pid, cb) {
        routes.query("CALL sb_location_dic_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);
            console.log(r);
            cb(e, r);
        });
    };


    pub.updateRoute = function(id, company_id, name, driver_id, assistant_id, bus_id, schedule_id, plan_date, estimate_from, estimate_to, description, status, cb){
        routes.query("CALL sb_route_trip_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [id, company_id, driver_id, assistant_id, bus_id, plan_date, estimate_from, estimate_to, name, description, schedule_id, status ], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };
    pub.updateRoutePointDetail = function(id, location_name, location_ad, location_x, location_y, futureSche, cb){
        routes.query("CALL sb_route_point_plan_detail_update(?, ?, ?, ?, ?, ?)", [id, location_name, location_ad, location_x, location_y, futureSche], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.insertRoutePointInPair = function(route_id, chid_id, plocation_name, plocation_ad, plocation_x, plocation_y, dlocation_name, dlocation_ad, dlocation_x, dlocation_y, pcreated_by, futureSche, cb){
        routes.query("CALL sb_route_point_plan_insert_in_pair(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [route_id, chid_id, plocation_name, plocation_ad, plocation_x, plocation_y, dlocation_name, dlocation_ad, dlocation_x, dlocation_y, pcreated_by, futureSche], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.insertRoutePoint = function(route_id, chid_id, paction, plocation_name, plocation_ad, plocation_x, plocation_y, pfor_order, pcreated_by, cb){
        routes.query("CALL sb_route_point_plan_insert(?, ?, ?, ?, ?, ?, ?, ?, ?)", [route_id, chid_id, paction, plocation_name, plocation_ad, plocation_x, plocation_y, pfor_order, pcreated_by], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.updateRoutePointOrder = function(point_id, prefid, cb){
        routes.query("CALL sb_route_point_plan_update_order(?, ?)", [point_id, prefid], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.getRoutePointByID = function(id, cb){
        routes.query("CALL sb_route_point_plan_get_infor(?)", [id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.updateScheduleWeek = function(pschedule_id, plstweekid, cb){
        routes.query("CALL sb_schedule_week_update(?, ?)", [pschedule_id, plstweekid], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.insertRoute = function(uid, company_id, name, driver_id, assistant_id, bus_id, plan_date, estimate_from, estimate_to, description, status, cb){
        routes.query("CALL sb_route_trip_insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [company_id, driver_id, assistant_id, bus_id, plan_date, estimate_from, estimate_to, name, description, 0, uid, status ], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };
    pub.removeRoute = function(id, cb){
        routes.query("CALL sb_route_trip_delete(?)", [id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.routeTripGetPointById = function(proute_trip_id, pchild_id, paction, plocation_name, page, num, _order, _desc, _keyword, cb) {

        routes.query("CALL sb_route_point_plan_get_by_route_trip_paged(?, ?, ?, ?, ?, ?, ?, ?, ?)", [proute_trip_id, pchild_id, paction, plocation_name, page, num, _order, _desc, _keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.ins = function(pid, proute_trip_id, pchild_id, paction, plocation_name, plocation_ad, plocation_x, plocation_y, pfor_order, cb) {

        routes.query("CALL sb_route_trip_insert(?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, proute_trip_id, pchild_id, paction, plocation_name, plocation_ad, plocation_x, plocation_y, pfor_order], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routePointPlanUpdate = function(pid, proute_trip_id, pchild_id, paction, plocation_name, plocation_ad, plocation_x, plocation_y, pfor_order, cb) {

        routes.query("CALL sb_route_point_plan_update(?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, proute_trip_id, pchild_id, paction, plocation_name, plocation_ad, plocation_x, plocation_y, pfor_order], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getRoutePointByChildId = function(proute_trip_id, pchild_id, cb) {

        routes.query("CALL sb_route_point_get_by_child(?, ?)", [proute_trip_id, pchild_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeTripDailyGetDetailByTripId = function(proute_trip_id, pdate, page, num, _order, _desc, _keyword, cb) {

        routes.query("CALL sb_route_trip_daily_detail_get_by_route_trip_id(?, ?, ?, ?, ?, ?, ?)", [proute_trip_id, pdate, page, num, _order, _desc, _keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeTripDailySearchPaged = function(pid, pcompany_id, pdriver_id, passitant_id, pbus_id, pplan_date, pestimate_from, pestimate_to, pname, pdescription, pschedule_id, pstatus, pdaily_status, pfromdate, ptodate, page, num, _order, _desc, _keyword, cb) {
        routes.query("CALL sb_route_trip_daily_search_paged(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pcompany_id, pdriver_id, passitant_id, pbus_id, pplan_date, pestimate_from, pestimate_to, pname, pdescription, pschedule_id, pstatus, pdaily_status, pfromdate, ptodate, page, num, _order, _desc, _keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeTripDailyGetInfoById = function(proute_trip_id, pdate, cb) {

        routes.query("CALL sb_route_trip_daily_get_info_by_id(?, ?)", [proute_trip_id, pdate], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.routeTripDailyGetSplitByTripId = function(proute_trip_id, pdate, page, num, _order, _desc, _keyword, cb) {
        routes.query("CALL sb_route_trip_daily_detail_split_get_by_route_trip_id(?, ?, ?, ?, ?, ?, ?)", [proute_trip_id, pdate, page, num, _order, _desc, _keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeTripBasicGetSplitByTripId = function(proute_trip_id, ptime, page, num, cb) {
        routes.query("CALL sb_route_trip_history_get_by_time(?, ?, ?, ?)", [proute_trip_id, ptime, page, num], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.routeTripGetCurrPassByTripId= function(proute_trip_id, cb) {
        routes.query("CALL sb_route_trip_get_current_passenger(?)", [proute_trip_id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.busSearch = function(pcompanyid, plicense_plate_no, cb) {
        routes.query("CALL sb_bus_get_by_name_license_v2(?, ?, ?)", [pcompanyid, '', plicense_plate_no], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.busUpdate = function(pid, pname, plicense_plate_no, pregistered_country, psnd_license_plate_no, psnd_registered_country, pcompany_id, cb) {
        routes.query("CALL sb_bus_temp_update(?, ?, ?, ?, ?, ?, ?)", [pid, pname, plicense_plate_no, pregistered_country, psnd_license_plate_no, psnd_registered_country, pcompany_id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.busMerge = function(pfrom_bus_id, pto_bus_id, cb) {
        routes.query("CALL sb_bus_merge_bus(?, ?)", [pfrom_bus_id, pto_bus_id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.routeTripGetWorkerOnboard = function(proute_trip_id, cb) {
        routes.query("CALL sb_event_history_get_worker_onboard(?)", [proute_trip_id], function(e, r) {
            if(e) config.error(e);
            cb(e, r);
        });
    };

    pub.routeTripDailyDetailGetInfo = function(proute_trip_daily_detail, pchild_id, cb) {

        routes.query("CALL sb_route_trip_daily_detail_get_info(?, ?)", [proute_trip_daily_detail, pchild_id], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeTripDailyDetailManualUpdateInfo = function(proute_trip_daily_id, pchild_id, pob_location_name, pob_location_ad, pob_location_x, pob_location_y, pob_user_created, pob_done_notify_id, pob_fororder, pal_location_name, pal_location_ad, pal_location_x, pal_location_y, pal_user_created, pal_fororder, pstatus, cb) {

        routes.query("CALL sb_route_trip_daily_detail_manual_update_info(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [proute_trip_daily_id, pchild_id, pob_location_name, pob_location_ad, pob_location_x, pob_location_y, pob_user_created, pob_done_notify_id, pob_fororder, pal_location_name, pal_location_ad, pal_location_x, pal_location_y, pal_user_created, pal_fororder, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
    pub.routeTripDailyDetailManualUpdateStatus = function(proute_trip_daily_id, pchild_id, pob_at, pob_remind_status, pob_remind_notify_id, pob_ack_status, pob_done_status, pob_done_notify_id, pal_at, pal_remind_status, pal_remind_notify_id, pal_ack_status, pal_done_status, pal_done_notify_id, pstatus, cb) {

        routes.query("CALL sb_route_trip_daily_detail_manual_update_status(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [proute_trip_daily_id, pchild_id, pob_at, pob_remind_status, pob_remind_notify_id, pob_ack_status, pob_done_status, pob_done_notify_id, pal_at, pal_remind_status, pal_remind_notify_id, pal_ack_status, pal_done_status, pal_done_notify_id, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeTripDailyDetailUpdateDoneStatus = function(pid, pdone_status, pcard_code, pdone_noty_id, ptype, cb) {
        routes.query("CALL sb_route_trip_daily_detail_update_done_status(?, ?, ?, ?, ?)", [pid, pdone_status, pcard_code, pdone_noty_id, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    //
    pub.routeTripDailyDetailUpdateChildStatus = function(proute_trip_id, pchild_id, pcard_type, pcard_code, plocation_id, ptype, cb) {
        routes.query("CALL sb_route_trip_daily_detail_update_child_status(?, ?, ?, ?, ?, ?)", [proute_trip_id, pchild_id, pcard_type, pcard_code, plocation_id, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeTripDailyDetailUpdateChildStatusBasic = function(proute_trip_id, pchild_id, pcard_type, pcard_code, plocation_id, ptype, cb) {

        //routes.query("CALL sb_route_trip_daily_detail_basic_update_child_status(?, ?, ?, ?, ?, ?)", [proute_trip_id, pchild_id, pcard_type, pcard_code, plocation_id, ptype], function(e, r) {
        routes.query("CALL sb_route_trip_daily_detail_basic_update_child_status_v2(?, ?, ?, ?, ?, ?)", [proute_trip_id, pchild_id, pcard_type, pcard_code, plocation_id, ptype], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };


    pub.gpsLog = function(proute_trip_id, ploc_point, presponse_by, pdevice_code, roundtrip, cb) {

        routes.query("CALL sb_gps_history_insert(?, ?, ?, ?, ?)", [proute_trip_id, ploc_point, presponse_by, pdevice_code, roundtrip], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.locationRouteUpdate = function(plocation, plocation_name, plocation_address, cb) {

        routes.query("CALL sb_location_dic_route_update(?, ?, ?)", [plocation, plocation_name, plocation_address], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
        //sb_route_trip_daily_detail_update_done_status

    pub.routeTripDailyGenById = function(proute_trip_id, pdate, poverride, cb) {

        routes.query("CALL sb_route_trip_daily_gen_from_route_trip_id(?, ?, ?, ?)", [proute_trip_id, pdate, poverride, 1], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.routeTripGetByChildId = function(pchild_id, pplan_date, cb) {

        routes.query("CALL sb_route_trip_get_by_child_id(?, ?)", [pchild_id, pplan_date], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        })
    };

    pub.updateRouteTripDaily = function(pid, proute_trip_id, proute_trip_date, pstatus, pdriver_id, passitant_id, pbus_id, pplan_date, pestimate_from, pestimate_to, cb) {
        routes.query("CALL sb_route_trip_daily_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, proute_trip_id, proute_trip_date, pstatus, pdriver_id, passitant_id, pbus_id, pplan_date, pestimate_from, pestimate_to], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getRouteTripDailyData = function(routeId, date, cb) {
        routes.query("CALL sb_route_trip_get_data_for_caching_v2(?, ?)", [routeId, date], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getRouteTripDailyForChild = function(routeId, childId, date, cb) {
        routes.query("CALL sb_route_trip_daily_detail_get(?, ?, ?)", [routeId, childId, date], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.countCurrentChildOnRouteTrip = function(routeId, cb) {
        routes.query("CALL sb_route_trip_history_get_num_child_onbus(?)", [routeId], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

})(module, require);
