/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var buses;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        buses = db.use('DataDB');

        return pub;
    };

    pub.getListBusPaged = function(puid, page, num, order, desc, keyword, cb) {

        buses.query("CALL sb_bus_get_by_company_paged(?, ?, ?, ?, ?, ?)", [puid, page, num, order, desc, keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getListBusInCompany = function(pcompanyId, cb) {

        buses.query("CALL sb_bus_get_by_company_paged(?, ?, ?, ?, ?, ?)", [pcompanyId,  1, 1000, '', '', ''], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getBusById = function(pid, cb) {
        buses.query("CALL sb_bus_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
    pub.insertBus = function(puid, pname, pdescription, plicense_plate_no, pregistered_country, psnd_license_plate_no, psnd_registered_country, pstatus, cb) {
        buses.query("CALL sb_bus_insert(?, ?, ?, ?, ?, ?, ?, ?, ?)", [
            puid, pname, pdescription, plicense_plate_no, pregistered_country, psnd_license_plate_no, psnd_registered_country, '', pstatus
        ], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
    pub.insertTempBus = function(pname, pdescription, plicense_plate_no, pregistered_country, pcreated_by, cb) {
        buses.query("CALL sb_bus_temp_insert(?, ?, ?, ?, ?)", [
            pname, pdescription, plicense_plate_no, pregistered_country, pcreated_by
        ], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
    //name, description, license_plate_no, registered_country, snd_license_plate_no, snd_registered_country, status,
    pub.updateBus = function(pid, pname, pdescription, plicense_plate_no, pregistered_country, psnd_license_plate_no, psnd_registered_country, pstatus, cb) {

        buses.query("CALL sb_bus_update(?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pname, pdescription, plicense_plate_no, pregistered_country, psnd_license_plate_no, psnd_registered_country, '', pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.removeBus = function(pid, cb) {

        buses.query("CALL sb_bus_delete(?, ?)", [pid, 2], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateBusAccount = function(puser_id, pbus_id, pcompany_id, pstatus, cb) {

        buses.query("CALL sb_bus_account_update(?, ?, ?, ?)", [puser_id, pbus_id, pcompany_id, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

})(module, require);
