/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var assistants;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        assistants = db.use('DataDB');

        return pub;
    };

    pub.getListAssistantPaged = function(pcompany_id, page, num, order, desc, keyword, cb) {

        assistants.query("CALL sb_da_get_by_type_paged(?, ?, ?, ?, ?, ?, ?)", [pcompany_id, 2, page, num, order, desc, keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.insertAssistant = function(puid, pemail, pmobile, phome, poffice, pimage, pfullname, pusername, pstatus, cb) {
        assistants.query("CALL sb_da_insert(? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
            puid, pusername, '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 0, 3, pemail, pmobile, phome, poffice, pstatus, '', pimage, pfullname
        ], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.updateAssistant = function(pid, pemail, pmobile, phome, poffice, pimage, pfullname, pusername, pstatus, cb) {

        assistants.query("CALL sb_driver_update_info(?, ?, ?, ?, ?, ?, ?, ?, ?)", [pid, pemail, pmobile, phome, poffice, pimage, pfullname, pusername, pstatus], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
})(module, require);
