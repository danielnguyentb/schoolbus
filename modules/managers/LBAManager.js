/* Copyright (c) 2014 WCL , Inc. All rights reserved.
 *
 * CONFIDENTIAL AND PROPRIETARY
 *--------------------------------------------------------------
 */

/*
 * Live Buffer Actions Manager
 */

(function (r) {
    "use strict";

    var pub = {};		//public
    var _fn;			//utils
    var db;
    var managers;
    var Step = r("step");
    var io;

    //Init function - will return "pub" for public funcs
    module.exports = function (_db, utils, mn) {

        _fn = utils;
        db = _db;
        managers = mn;

        io = r('socket.io-emitter')(db.use('PUB'), {
            key: 'wcl'
        });

        return pub;
    };

    pub.pushRoom = function (room, channel) {
        try {
            io.to(room).emit.apply(io, Array.prototype.slice.call(arguments, 1));
        } catch (e) {
            config.error(e);
        }
    };

    pub.emitClients = function(channel) {
        io.emit.apply(io, arguments);
    };

})(require);