/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var pub = {}; //public
    var _fn; //utils
    var db;
    var managers;

    var Step = r("step");

    var locations;

    ns.exports = function(_db, utils, mn) {
        db = _db;
        _fn = utils;
        managers = mn;

        locations = db.use('DataDB');

        return pub;
    };

    pub.getListLocationPaged = function(pcompany_id, page, num, order, desc, keyword, cb) {

        locations.query("CALL sb_location_get_by_company_paged(?, ?, ?, ?, ?, ?)", [pcompany_id, page, num, order, desc, keyword], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };

    pub.getLocationById = function(pid, cb) {

        locations.query("CALL sb_location_dic_get_info(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
    pub.insertLocation = function(pcompany_id, pname, paddress, pX, pY, pcreated_by_trip, pcreated_by_user, cb) {
        locations.query("CALL sb_location_dic_insert(?, ?, ?, ?, ?, ?, ?)", [
            pcompany_id, pname, paddress, pX, pY, pcreated_by_trip, pcreated_by_user
        ], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
    //name, description, license_plate_no, registered_country, snd_license_plate_no, snd_registered_country, status,
    pub.updateLocation = function(pid, pcompany_id, pname, paddress, pX, pY, pcreated_by_trip, pcreated_by_user, cb) {

        locations.query("CALL sb_location_dic_update(?, ?, ?, ?, ?, ?, ?, ?)", [pid, pcompany_id, pname, paddress, pX, pY, pcreated_by_trip, pcreated_by_user], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };
    pub.removeLocation = function(pid, cb) {

        locations.query("CALL sb_location_delete(?)", [pid], function(e, r) {
            if(e) config.error(e);

            cb(e, r);
        });
    };


})(module, require);
