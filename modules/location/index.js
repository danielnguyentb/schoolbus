/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'location';

    var Step = r("step");

    ns.init = function(app, _db, fn, mn) {
        db = _db;
        _fn = fn;
        managers = mn;

        app.route('/location')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.locationManage, _fn.auth.act.get, _fn.auth.permission, routes.getLocation)
            .post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.locationManage, _fn.auth.act.post, _fn.auth.permission, routes.updateLocation);

        app.route('/location/:id')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.locationManage, _fn.auth.act.get, _fn.auth.permission, routes.getLocationDetail)
            .delete(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.locationManage, _fn.auth.act.delete, _fn.auth.permission, routes.removeLocation);
    };

    var routes = {

        getLocation : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var company_id  = req.session.company_id;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;

            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    managers.location.getListLocationPaged(company_id, page, numrow, order, desc, '', this);
                },
                function response(e, r) {

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            locations  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        },

        updateLocation : function(req, res) {
            var lingo                   = req.lingo;
            var uid                     = req.session.uId;
            var companyId               = req.session.company_id;
            var id                      = req.body.id ? req.body.id : false;
            var name                    = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var address                 = req.body.address ? _fn.sanitize(_fn.trim(req.body.address)).xss() : '';
            var X                       = req.body.X ? _fn.sanitize(_fn.trim(req.body.X)).xss() : '';
            var Y                       = req.body.Y ? _fn.sanitize(_fn.trim(req.body.Y)).xss() : '';
            var edit                    = false;
            Step (
                function checkInput() {

                    //_fn.check(name, lingo.get('bus_name_invalid')).notEmpty().len(5);

                    if(id != '') edit = true;
                    if(edit) managers.location.getLocationById(id, this);
                    else return this;
                },
                function updateLocation(e, r) {
                    if(e) throw e;
                    if(edit && (!r.length || !r[0].length) ) throw Error(lingo.get('location_not_exists'));
                    if(edit) managers.location.updateLocation(id, companyId, name, address, X, Y, '', uid, this);
                    else managers.location.insertLocation(companyId, name, address, X, Y, '', uid, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: edit ? lingo.get('update_location_success') : lingo.get('create_location_success')
                    })
                }
            )
        },

        removeLocation : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).xss() : '';

            Step (
                function getLocation() {

                    _fn.check(id, lingo.get('location_not_exists')).notEmpty().isInt();
                    managers.location.getLocationById(id, this);
                },
                function removeLocation(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('location_not_exists'));

                    r = r[0][0];
                    managers.location.removeLocation(r.id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('remove_location_success')});
                }
            )
        },

        getLocationDetail : function(req, res) {

            var lingo = req.lingo;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).xss() : '';

            Step (
                function getDetail() {

                    _fn.check(id, lingo.get('location_not_exists')).notEmpty().isInt();

                    managers.location.getLocationById(id, this);
                },
                function response(e, r) {

                    if(e || !r.length || !r[0].length) res.json({success: false, message: lingo.get('location_not_exists')});
                    else res.json({success: true, location: r[0][0]});
                }
            )
        }
    };

})(module.exports, require);
