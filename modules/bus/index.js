/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'buses';

    var Step = r("step");

    ns.init = function(app, _db, fn, mn) {
        db = _db;
        _fn = fn;
        managers = mn;

        app.route('/bus')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.get, _fn.auth.permission, routes.getBus)
            .post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.post, _fn.auth.permission, routes.updateBus);

        app.route('/get/buses').get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.get, _fn.auth.permission, routes.getBuses);

        app.route('/bus/card/save').post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.post, _fn.auth.permission, routes.saveBusCard);

        app.route('/bus/:id')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.get, _fn.auth.permission, routes.getBusDetail)
            .delete(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.delete, _fn.auth.permission, routes.removeBus);

        var apiRoutes = app.get('wcl:api:routes');
        apiRoutes.route('/bus/card')
            .put(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.busManage, _fn.auth.act.post, _fn.auth.permission, routes.apiBusCard)
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.busManage, _fn.auth.act.post, _fn.auth.permission, routes.apiGetBusCardInfo);
        apiRoutes.route('/buses').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.busManage, _fn.auth.act.get, _fn.auth.permission, routes.getBuses)
        apiRoutes.route('/bus').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.busManage, _fn.auth.act.post, _fn.auth.permission, routes.setCurrentBus)
    };

    var routes = {

        getBus : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id || 0;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;

            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    managers.bus.getListBusPaged(companyId, page, numrow, order, desc, '', this);
                },
                function response(e, r) {

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            buses  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        },

        updateBus : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id                      = req.body.id ? req.body.id : false;
            var name                    = '';
            var description             = req.body.description ? _fn.sanitize(_fn.trim(req.body.description)).xss() : '';
            var license_plate_no        = req.body.license_plate_no ? _fn.sanitize(_fn.trim(req.body.license_plate_no)).xss() : '';
            var registered_country      = req.body.registered_country ? _fn.sanitize(_fn.trim(req.body.registered_country)).xss() : '';
            var snd_license_plate_no    = req.body.snd_license_plate_no ? _fn.sanitize(_fn.trim(req.body.snd_license_plate_no)).xss() : '';
            var snd_registered_country  = req.body.snd_registered_country ? _fn.sanitize(_fn.trim(req.body.snd_registered_country)).xss() : '';
            var edit            = false;

            Step (
                function checkInput() {

                    //_fn.check(name, lingo.get('bus_name_invalid')).notEmpty().len(5);

                    if(id != '') edit = true;

                    if(edit) managers.bus.getBusById(id, this);
                    else return this;
                },
                function updateBus(e, r) {
                    if(e) throw e;
                    if(edit && (!r.length || !r[0].length) ) throw Error(lingo.get('bus_not_exists'));

                    var status = 1;

                    if(edit) managers.bus.updateBus(id, name, description, license_plate_no, registered_country, snd_license_plate_no, snd_registered_country, status, this);
                    else managers.bus.insertBus(uid, name, description, license_plate_no, registered_country, snd_license_plate_no, snd_registered_country, status, this);
                },
                function response(e, r) {
                    console.log(e);
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: edit ? lingo.get('update_bus_success') : lingo.get('create_bus_success')
                    })
                }
            )
        },

        getBusDetail : function(req, res) {

            var lingo = req.lingo;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).xss() : '';

            Step (
                function getDetail() {

                    _fn.check(id, lingo.get('bus_not_exists')).notEmpty().isInt();

                    managers.bus.getBusById(id, this);
                },
                function response(e, r) {

                    if(e || !r.length || !r[0].length) res.json({success: false, message: lingo.get('bus_not_exists')});
                    else res.json({success: true, bus: r[0][0]});
                }
            )
        },

        removeBus : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).xss() : '';

            Step (
                function getBus() {

                    _fn.check(id, lingo.get('bus_not_exists')).notEmpty().isInt();
                    managers.bus.getBusById(id, this);
                },
                function removeBus(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('bus_not_exists'));

                    r = r[0][0];
                    managers.bus.removeBus(r.id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('remove_bus_success')});
                }
            )
        },

        saveBusCard : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var card  = req.body.card ? _fn.sanitize(_fn.trim(req.body.card)).xss() : '';
            var bus   = req.body.bus ? _fn.sanitize(_fn.trim(req.body.bus)).xss() : '';
            var busExists = false;

            Step (
                function checkInput() {

                    _fn.check(card, lingo.get('card_info_invalid')).notEmpty();
                    _fn.check(bus, lingo.get('bus_info_invalid')).notEmpty();

                    card = JSON.parse(card);
                    bus = JSON.parse(bus);

                    if(bus.id && bus.id != '') busExists = true;

                    managers.card.getCardInPair(card.cin.id, '', '', this.parallel());
                    managers.card.getCardInPair(card.cout.id, '', '', this.parallel());
                    if(busExists) managers.bus.getBusById(bus.id, this.parallel());
                },
                function insertBus(e, ci, co, r) {
                    if(e) throw e;
                    if(!ci.length || !ci[0].length) throw Error(lingo.get('bus_check_in_card_not_exists'));
                    if(!co.length || !co[0].length) throw Error(lingo.get('bus_check_out_card_not_exists'));
                    if(busExists && (!r.length || !r[0].length) ) throw Error(lingo.get('bus_not_exists'));

                    ci = ci[0][0];
                    co = co[0][0];

                    if(ci.status == 0 || ci.ower_type != 3) throw Error(lingo.get('bus_check_in_card_not_exists'));
                    if(co.status == 0 || co.ower_type != 3) throw Error(lingo.get('bus_check_out_card_not_exists'));

                    _fn.r(ci, this.parallel());
                    _fn.r(co, this.parallel());
                    if(!busExists) managers.bus.insertBus(uid, (bus.name || ""), bus.description, bus.license_plate_no, bus.registered_country, bus.snd_license_plate_no, bus.snd_registered_country, 1, this.parallel());
                    else _fn.r([[bus]], this.parallel());
                },
                function updateBusCard(e, ci, co, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('err_anonymous'));

                    r = r[0][0];

                    _fn.r(r, this.parallel());
                    _fn.r(ci, this.parallel());
                    _fn.r(co, this.parallel());
                    managers.card.insertBusCard(r.id, ci.id, ci.NFC, ci.QRCode, 1, uid, 1, this.parallel());
                    managers.card.insertBusCard(r.id, co.id, co.NFC, co.QRCode, 2, uid, 1, this.parallel());
                },
                function updateCardPair(e, b, ci, co, r, _r) {
                    if(e) throw e;

                    managers.card.updateCardInPair(ci.id, b.id, 1, this.parallel());
                    managers.card.updateCardInPair(co.id, b.id, 1, this.parallel());

                    //Update card info in card_info table;
                    //Check In card;
                    managers.card.updateCardInfoByOwner(6, b.id, ci.NFC, 1, uid, uid, 1, '', this.parallel());
                    managers.card.updateCardInfoByOwner(6, b.id, ci.QRCode, 2, uid, uid, 1, '', this.parallel());
                    //Check Out card;
                    managers.card.updateCardInfoByOwner(7, b.id, co.NFC, 1, uid, uid, 1, '', this.parallel());
                    managers.card.updateCardInfoByOwner(7, b.id, co.QRCode, 2, uid, uid, 1, '', this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('bus_register_card_success')
                    })
                }
            )
        },

        getBuses : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id || 0;

            Step (
                function checkInput() {
                    if(req.isApi && !companyId) throw Error('Company is required.');

                    managers.bus.getListBusInCompany(companyId, this);
                },
                function prepare(e, r) {
                    if(e) throw e;

                    var buses = [];

                    if(!req.isApi) buses.push({id: '', name: ''});

                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            buses.push({id: v.id, name: v.license_plate_no})
                        });
                    }

                    return buses;
                },
                function response(e, r) {
                    if(e) res.json({buses: []});
                    else res.json({buses  : r});
                }
            )
        },

        setCurrentBus : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id || 0;
            var busId    = req.body.busId ? _fn.sanitize(_fn.trim(req.body.busId)).toInt() : 0;

            Step (
                function valid() {
                    if(isNaN(busId)) busId = 0;

                    if(!busId || !companyId) throw Error(lingo.get('err_anonymous'));

                    managers.bus.updateBusAccount(uid, busId, companyId, 1, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true});
                }
            )
        },

        apiGetBusCardInfo: function(req, res) {
            var lingo   = req.lingo;
            var uid     = req.session.uId;
            var nfc     = req.body.nfc ? true : false;
            var card    = req.body.card ? _fn.sanitize(_fn.trim(req.body.card)).xss() : '';

            Step (
                function getCardInfo() {
                    _fn.check(card, lingo.get('card_not_exists')).notEmpty();

                    managers.card.getCardInfoByCard(card, (nfc ? 1 : 2), this);
                },
                function prepare(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('card_not_exists'));

                    r = r[0][0];
                    r.ower_type = parseInt(r.ower_type, 10);

                    if(r.status != 1 || [config.CARD_TYPE.BUS_CHECKIN, config.CARD_TYPE.BUS_CHECKOUT].indexOf(r.ower_type) == -1) throw Error(lingo.get('card_invalid'));

                    return {
                        id: r.card_code,
                        type: r.card_type,
                        owner_type: r.ower_type
                    };
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, card: r});
                }
            )
        },

        apiBusCard: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var role = req.session._role;
            var card = req.body.card ? _fn.sanitize(_fn.trim(req.body.card)).xss() : '';

            Step(
                function valid() {
                    _fn.check(card, lingo.get('bus_card_invalid')).notEmpty();

                    card = JSON.parse(card);

                    _fn.check(card.in.id, lingo.get('bus_check_in_card_invalid')).notEmpty();
                    _fn.check(card.out.id, lingo.get('bus_check_out_card_invalid')).notEmpty();

                    if(card.in.type == 1) managers.card.getCardInPair(0, '', card.in.id, this.parallel());
                    else managers.card.getCardInPair(0, card.in.id, '', this.parallel());

                    if(card.out.type == 1) managers.card.getCardInPair(0, '', card.out.id, this.parallel());
                    else managers.card.getCardInPair(0, card.out.id, '', this.parallel());
                },
                function pushLBA(e, ir, or) {
                    if(e) throw e;
                    if(!ir.length || !ir[0].length || ir[0][0].status == 0) throw Error(lingo.get('bus_check_in_card_not_exists'));
                    if(!or.length || !or[0].length || or[0][0].status == 0) throw Error(lingo.get('bus_check_out_card_not_exists'));

                    ir = ir[0][0];
                    or = or[0][0];

                    if(ir.ower_type != 3) throw Error(lingo.get('bus_check_in_card_invalid'));
                    if(or.ower_type != 3) throw Error(lingo.get('bus_check_in_card_invalid'));

                    managers.LBA.pushRoom('wcl#user#' + uid, 'bus:register:card', {
                        cin: {id: ir.id, card: ir.NFC},
                        cout: {id: or.id, card: or.NFC}
                    });

                    return this;
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('bus_register_card_success')});
                }
            );
        }

    };

})(module.exports, require);
