/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn;        //Utility functions

    var locale = 'childs';

    var Step = r("step");
    var crypto = r("crypto");
    var qr = r('qr-image');
    var jwt = r('jsonwebtoken');
    var moment = r('moment');
    var util = require( "util" );

    ns.init = function(app, _db, fn, mn) {

        db = _db;
        _fn = fn;
        managers = mn;

        var apiRoutes = app.get('wcl:api:routes');
        app.route('/child')
            .get(_fn.getBundle(locale), _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.getChilds)
            .post(_fn.getBundle(locale), _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.addChild);
        app.route('/child/getByCode')
            .post(_fn.getBundle(locale), _fn.auth.check,_fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.getChildByCode);

        app.route('/child/getGuardianByCode')
            .post(_fn.getBundle(locale), _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.getGuardianByCode);

        app.route('/child/insertGuardianChild')
            .post(_fn.getBundle(locale), _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.insertGuardianChild);

        app.route('/child/syncCard')
            .post(_fn.getBundle(locale), _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.syncCard);

        app.route('/child/updateTempProfile')
            .post(_fn.getBundle(locale), _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.updateTempProfile);

        app.route('/child/:id')
            .get(_fn.getBundle(locale), _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.getChildDetail)
            .post(_fn.getBundle(locale), _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.changeChildDetail)
            .delete(_fn.getBundle(locale), _fn.auth.mod.childManage, _fn.auth.act.delete, _fn.auth.permission, routes.removeChild);

        app.route('/child/qr/:id/:type').get(_fn.getBundle(locale), routes.genQR);

        apiRoutes.route('/child')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.apiGetChilds)
            .put(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.put, _fn.auth.permission, routes.apiAddChild);

        apiRoutes.route('/child/scan').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.scanChild);
        apiRoutes.route('/guardian/scan').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.scanGuardian);

        apiRoutes.route('/child/checkCard').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.checkCard);

        apiRoutes.route('/child/pairCard').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.pairCard);

        apiRoutes.route('/child/scanCard').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.cardPull);

        apiRoutes.route('/child/cardType').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.cardType);

        apiRoutes.route('/child/addCardGroup').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.addCardGroup);

        apiRoutes.route('/child/cardGroup').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.cardGroup);

        apiRoutes.route('/child/groupSetCount').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.groupSetCount);

        apiRoutes.route('/child/groupComplete').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.groupComplete);

        apiRoutes.route('/child/groupGetInfo').get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.groupGetInfo);

        apiRoutes.route('/child/groupType').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.groupType);

        apiRoutes.route('/child/guardian').post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.registerGuardian);



        apiRoutes.route('/child/notifications')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.apiGetChildNotifications)
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.apiChildNotificationsUpdate);


        apiRoutes.route('/child/absent')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.childAbsent);

        apiRoutes.route('/child/:id')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.apiGetChildDetail)
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.apiSaveChildDetail);

        apiRoutes.route('/child/list/:id/:type')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.get, _fn.auth.permission, routes.apiGetChildHolder);

        apiRoutes.route('/child/:id/:parent/:relation')
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.childManage, _fn.auth.act.post, _fn.auth.permission, routes.apiUpdateParentChild);
    };

    var routes = {
        cardPull: function(req, res) {
            //get data or do something;
            managers.LBA.pushRoom('wcl#user#' + req.session.uId, 'scan:card:pull', req.body);
            res.json({
                success: true
            })
        },

        cardGroup: function(req, res) {
            Step (
                function cardType() {
                    managers.child.getCardGroup(this.parallel());
                },
                function response(e, q) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true,
                            group  : q
                        });
                    }
                }
            )
        },

        groupSetCount: function(req, res) {
            var id = _fn.sanitize(_fn.trim(req.query.id.toString())).xss();
            Step (
                function cardType() {
                    managers.child.groupSetCount(id, this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true,
                            boxInfo  : r
                        });
                    }
                }
            )
        },

        groupComplete: function(req, res) {
            var id = _fn.sanitize(_fn.trim(req.query.id.toString())).xss();
            Step (
                function groupComplete() {
                    managers.child.groupComplete(id, this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true
                        });
                    }
                }
            )
        },

        groupGetInfo: function(req, res) {
            var id = _fn.sanitize(_fn.trim(req.query.id.toString())).xss();
            Step (
                function groupComplete() {
                    managers.child.groupGetInfo(id, this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true,
                            group: r
                        });
                    }
                }
            )
        },

        groupType: function(req, res) {
            var id = req.body.id ? _fn.sanitize(_fn.trim(req.body.id)).xss() : '';
            var type = req.body.type ? _fn.sanitize(_fn.trim(req.body.type)).xss() : '';
            Step (
                function groupType() {
                    managers.child.groupType(id, type, this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true
                        });
                    }
                }
            )
        },

        cardType: function(req, res) {

            var card_code     = req.body.qr_code ? _fn.sanitize(_fn.trim(req.body.qr_code)).xss() : '';
            Step (
                function cardType() {
                    managers.child.checkCardType(card_code, this.parallel());
                },
                function response(e, q) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        console.log(q);
                        res.json({
                            success: true,
                            card  : q
                        });
                    }
                }
            )
        },

        addCardGroup: function(req, res) {

            Step (
                function addCardGroup() {
                    var gname = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                    while(gname.length<8){
                        var r = Math.random();
                        gname+= possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    managers.child.addCardGroup(gname, this.parallel());
                },
                function response(e, r) {
                    console.log(r);
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true,
                            group  : r
                        });
                    }
                }
            )
        },

        childAbsent: function(req, res) {
            var lingo       = req.lingo;
            var uid         = req.session.uId;
            var child_id    = req.body.child_id ? _fn.sanitize(_fn.trim(req.body.child_id)).xss() : '';
            var from        = req.body.from ? _fn.sanitize(_fn.trim(req.body.from)).xss() : '';
            var to          = req.body.to ? _fn.sanitize(_fn.trim(req.body.to)).xss() : '';
            var all_day     = req.body.all_day ? _fn.sanitize(_fn.trim(req.body.all_day)).xss() : '';
            Step (
                function getLastAction() {
                    _fn.check(child_id, lingo.get('child_not_exists')).notEmpty().isInt();
                    managers.child.getChildById(child_id, 1, this.parallel());
                    managers.child.getPassiveLastAction(2, child_id, this.parallel());
                },
                function getWorkers(e, c, r) {
                    if(e) throw e;
                    _fn.r(c, this.parallel());
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));
                    if(!r.length || !r[0].length) throw Error(lingo.get('not_in_route'));
                    managers.child.getWorkeronRoute(r[0][0].route_trip_id, this.parallel());
                    managers.route.routeTripGetInfoById(r[0][0].route_trip_id, this.parallel());
                },
                function getCompanyAdmin(e, c, r, ri){
                    if(e) throw e;
                    _fn.r(c, this.parallel());
                    _fn.r(r, this.parallel());
                    var company_id = ri[0][0].company_id;

                    managers.child.getCompanyAdmin(company_id, this.parallel());
                },
                function insertNotification(e, c, r, ca){
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('not_found_any_worker'));
                    _fn.r(c, this.parallel());
                    _fn.r(r, this.parallel());
                    var messJson = {
                        messType: 3,
                        childName: c[0][0].name,
                        from: from,
                        to: to
                    };
                    var message = _fn.absentMess(req, messJson);
                    var strMess = JSON.stringify(messJson);
                    if(ca.length || ca[0].length){
                        ca[0].forEach(function(v) {
                            managers.child.notificationInsert(0, uid, v.user_id, child_id, strMess, 2, '', '', function(){});
                        });
                    }
                    r[0].forEach(function(v) {
                        managers.child.notificationInsert(0, uid, v.active_party_id, child_id, strMess, 2, '', '', function(){});
                        utils.pushNotificationtoWorker(message, v.active_party_id);
                    });

                },
                function response(e, c, r) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        res.json({
                            success: true,
                            message: lingo.get('notification_sent'),
                            child: c,
                            workers: r
                        });
                    }
                }
            )
        },



        genQR : function(req, res) {

            try {

                var id = _fn.sanitize(_fn.trim(req.params.id)).xss();
                var type = _fn.sanitize(_fn.trim(req.params.type)).xss();
                if(['child', 'guardian'].indexOf(type) == -1) throw Error();

                var data = {
                    i: id,
                    t: type
                };

                var img = qr.image(jwt.sign(data, config.session.secret, {}).toString());

                res.writeHead(200, {'Content-Type': 'image/png'});
                img.pipe(res);

            } catch (e) {
                res.render('errors/500', {
                    err: '414 Request-URI Too Large'
                });
            }
        },

        changeChildDetail : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id;
            var name = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var image = req.body.image ? _fn.sanitize(_fn.trim(req.body.image)).xss() : '';
            var groups = req.body.groups;
            var status = req.body.status ? 1 : 0;

            Step (
                function checkValid() {

                    _fn.check(id, lingo.get('child_not_exists')).notEmpty().isInt();
                    _fn.check(name, lingo.get('child_name_invalid')).notEmpty().len(5);

                    //Get company
                    managers.user.getUserCompanyByType(uid, 3, this.parallel());
                    managers.child.getChildById(id, 1, this.parallel());

                    if(Array.isArray(groups)) managers.group.getGroupByListId(groups.join(','), this.parallel());
                    else _fn.r([[]], this.parallel());
                },
                function updateChild(e, r, c, g) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));

                    r = r[0][0];
                    c = c[0][0];
                    g = g[0];

                    if(r.id != c.company_id) throw Error(lingo.get('err_permission'));

                    groups = [];
                    g.forEach(function(v) {
                        groups.push(v.id);
                    });

                    _fn.r(groups, this.parallel());
                    managers.child.updateChild(c.id, name, c.id_no, c.mobile, c.NFC, c.QRCode, c.company_id, image, status, c.create_by, this.parallel());
                    managers.group.removeUserMultiGroup(c.id, '', 2, this.parallel());
                },
                function addGroup(e, g, r, _r) {
                    if(e) throw e;

                    managers.group.addUserToMultiGroup(id, g.join(','), 1, '', 2, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('update_child_success')
                    })
                }
            )
        },

        getChildDetail : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id;

            Step (
                function checkValid() {

                    _fn.check(id, lingo.get('child_not_exists')).notEmpty().isInt();

                    //Get company
                    managers.user.getUserCompanyByType(uid, 3, this.parallel());
                    managers.child.getChildById(id, 1, this.parallel());
                },
                function getDetail(e, r, c) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));

                    r = r[0][0];
                    c = c[0][0];

                    if(r.id != c.company_id) throw Error(lingo.get('err_permission'));
                    if(c.image == '') c.image = config.DEFAULT_AVATAR;
                    _fn.r(c, this.parallel());
                    managers.group.getGroupByUser(id, 2, this.parallel());
                },
                function response(e, c, g) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        child : c,
                        group : g[0]
                    });
                }
            );
        },

        removeChild : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id;

            Step (
                function checkValid() {

                    _fn.check(id, lingo.get('child_not_exists')).notEmpty().isInt();

                    //Get company
                    managers.user.getUserCompanyByType(uid, 3, this.parallel());
                    managers.child.getChildById(id, 1, this.parallel());
                },
                function getDetail(e, r, c) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));

                    r = r[0][0];
                    c = c[0][0];

                    if(r.id != c.company_id) throw Error(lingo.get('err_permission'));
                    managers.child.removeChild(c.id, this);
                },
                function response(e, c, g) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('remove_child_success')
                    });
                }
            )
        },

        addChild : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var name = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';

            Step (
                function checkValid() {

                    _fn.check(name, lingo.get('child_name_invalid')).notEmpty().len(5);

                    //Get company
                    managers.user.getUserCompanyByType(uid, 3, this.parallel());
                    _fn.sec.randNum(6, this.parallel());
                },
                function addChild(e, r, g) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));

                    r = r[0][0];
                    managers.child.insertChild(name, 0, 0, '', '', r.id, '', 1, uid, g, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {

                        r = r[0][0];
                        res.json({
                            success: true,
                            cid    : r.id,
                            message: lingo.get('add_child_success')
                        })
                    }
                }
            )
        },

        apiAddChild : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var child = req.body.child ? _fn.sanitize(_fn.trim(req.body.child.toString())).xss() : '';

            Step (
                function checkInput() {

                    _fn.check(child, lingo.get('child_not_exists')).notEmpty();

                    _fn.doAsync(this);
                },
                function getData(e, r) {
                    if(e) throw e;

                    child = JSON.parse(child);

                    managers.child.getChildById(child.id, 1, this.parallel());
                    managers.child.getParentChildByType(uid, child.id, '1,2', this.parallel());
                },
                function addParentChild(e, c, pc) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));
                    if(pc.length && pc[0].length) throw Error(lingo.get('parent_child_is_exists'));

                    c = c[0][0];
                    _fn.r(c, this.parallel());

                    //Update child info;
                    c = _fn.extend(c, {name: child.name, image: child.image});
                    managers.child.updateChild(c.id, c.name, c.id_no, c.mobile, c.NFC, c.QRCode, c.company_id, c.image, 1, uid, this.parallel());
                    managers.child.parentChildInsert(uid, c.id, 1, '', 1, this.parallel());
                },
                function response(e, c) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: _fn.str.f(lingo.get('parent_add_child_success'), {
                            child: c.name
                        })
                    });
                }
            );
        },

        apiGetChilds : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            var incNotify = req.query.notify ? true : false;
            var role = req.session._role;

            Step (
                function getSelf() {

                    if(role == config.ROLE.BUS_COMPANY) {
                        //Get company
                        managers.user.getUserCompanyByType(uid, 3, this);
                    } else return this;
                },
                function getChilds(e, r) {
                    if(e) throw e;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    if(role == config.ROLE.BUS_COMPANY) {
                        if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));

                        r = r[0][0];

                        managers.child.getListChildPaged(r.id, page, numrow, order, desc, '', this);
                    } else {
                        managers.child.getListChildByParentPaged(uid, page, numrow, order, desc, '', this);
                    }
                },
                function getNotifications(e, r) {
                    if(e) throw e;

                    if(role == config.ROLE.BUS_COMPANY || !incNotify) return r;

                    _fn.r(r, this.parallel());

                    var group = this.group();
                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            managers.child.getNotifications(v.id, 1, group());
                        });
                    } else _fn.r([], this.parallel());
                },
                function prepare(e, r, n) {
                    if(e) throw e;

                    if(role == config.ROLE.BUS_COMPANY || !incNotify) return r;

                    _fn.r(r, this.parallel());

                    var notifications = {};
                    if(n.length) {
                        n.forEach(function(v) {
                            if(v.length > 2 && v[0].length) {
                                notifications[v[0][0].for_id] = v[1][0].total;
                            }
                        });
                    }

                    _fn.r(notifications, this.parallel());
                },
                function response(e, r, n) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        childs : r[0],
                        total  : r[1][0].total,
                        notifications: n || {}
                    })
                }
            )
        },

        scanChild : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.body.i ? _fn.sanitize(_fn.trim(req.body.i.toString())).xss() : '';
            var nfc = req.body.nfc ? true : false;
            var trigger = req.body.t ? true : false;
            var role = req.session._role;

            Step (
                function getChild() {

                    _fn.check(id, lingo.get('child_not_exists')).notEmpty();

                    if(nfc) managers.child.getChildByCardCode(0, 1, id, '', this);
                    else managers.child.getChildByCardCode(0, 1, '', id, this);
                },
                function getId(e, r) {
                    if(e) throw e;

                    if(!r.length || !r[0].length) throw Error(lingo.get('child_not_exists'));

                    if(r[0][0].status != 2) throw Error(lingo.get('child_already_exists'));

                    id = r[0][0].id;
                    _fn.r(r, this.parallel());

                    managers.child.getParentChildByType(uid, id, '1,2', this.parallel());

                    if(!trigger) {
                        if(role == config.ROLE.BUS_COMPANY) {
                            //Get company
                            managers.user.getUserCompanyByType(uid, 3, this.parallel());
                        }
                    }
                },
                function getInfo(e, c, pc, r) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));
                    if(pc.length && pc[0].length) throw Error(lingo.get('parent_child_is_exists'));

                    c = c[0][0];
                    if(c.image == '') c.image = config.DEFAULT_AVATAR;

                    if(!trigger) {

                        if(role == config.ROLE.BUS_COMPANY) {

                            if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                            r = r[0][0];

                            if(r.id != c.company_id) throw Error(lingo.get('err_permission'));
                        }
                    }

                    _fn.r(c, this.parallel());

                    if(trigger) managers.child.getParentChildByChild(c.id, '1', this.parallel());
                },
                function prepare(e, r, pc) {
                    if(e) throw e;

                    if(trigger) {

                        if(!pc || !pc.length || !pc[0].length) throw Error(lingo.get('err_anonymous'));
                    }

                    _fn.r(r, this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        child  : r
                    });
                }
            )
        },

        scanGuardian : function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.body.i ? _fn.sanitize(_fn.trim(req.body.i.toString())).xss() : '';
            var nfc = req.body.nfc ? true : false;
            var role = req.session._role;

            Step (
                function getChild() {

                    _fn.check(id, lingo.get('invalid_card')).notEmpty();

                    if(nfc) managers.child.getGuardianByCardCode(0, id, '', this);
                    else managers.child.getGuardianByCardCode(0, '', id, this);
                },
                function getId(e, g) {
                    if(e) throw e;

                    if(!g.length || !g[0].length) throw Error(lingo.get('guardian_not_exists'));

                    g = g[0][0];

                    return {
                        id: g.id,
                        name: g.name,
                        image: g.image != '' ? g.image : config.DEFAULT_AVATAR,
                        card: g.NFC,
                        qr_code: g.QRCode,
                        readOnly: g.status == 1 && g.parent_id != 0
                    };
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        guardian: r
                    });
                }
            )
        },

        checkCard : function(req, res) {

            var lingo       = req.lingo;
            var uid         = req.session.uId;
            var qr_code     = req.body.qr_code ? _fn.sanitize(_fn.trim(req.body.qr_code)).xss() : '';
            var nfc_code    = req.body.nfc_code ? _fn.sanitize(_fn.trim(req.body.nfc_code)).xss() : '';
            var qr_code2    = req.body.qr_code2 ? _fn.sanitize(_fn.trim(req.body.qr_code2)).xss() : '';
            var nfc_code2   = req.body.nfc_code2 ? _fn.sanitize(_fn.trim(req.body.nfc_code2)).xss() : '';
            var qr_code3    = req.body.qr_code3 ? _fn.sanitize(_fn.trim(req.body.qr_code3)).xss() : '';
            var nfc_code3   = req.body.nfc_code3 ? _fn.sanitize(_fn.trim(req.body.nfc_code3)).xss() : '';
            var qr_code4    = req.body.qr_code4 ? _fn.sanitize(_fn.trim(req.body.qr_code4)).xss() : '';
            var nfc_code4   = req.body.nfc_code4 ? _fn.sanitize(_fn.trim(req.body.nfc_code4)).xss() : '';
            var role = req.session._role;
            Step (
                function checkCard() {
                    managers.child.checkCardInPair(qr_code, '', this.parallel());
                    managers.child.checkCardInPair('', nfc_code, this.parallel());
                    managers.child.checkCardInPair(qr_code2, '', this.parallel());
                    managers.child.checkCardInPair('', nfc_code2, this.parallel());
                    managers.child.checkCardInPair(qr_code3, '', this.parallel());
                    managers.child.checkCardInPair('', nfc_code3, this.parallel());
                    managers.child.checkCardInPair(qr_code4, '', this.parallel());
                    managers.child.checkCardInPair('', nfc_code4, this.parallel());
                },
                function response(e, q, n, p, j, k, l, d, b) {
                    if(e) res.json({success: false, message: e.message});
                    else{
                        var exists = 0;
                        if(q[0].length || n[0].length || p[0].length || j[0].length || k[0].length || l[0].length || d[0].length || b[0].length)
                            exists = 1;
                        res.json({
                            success: true,
                            exists  : exists
                        });
                    }
                }
            )
        },

        pairCard : function(req, res) {
            var lingo       = req.lingo;
            var uid         = req.session.uId;
            var company_id  = req.session.company_id;
            var qr_code     = req.body.qr_code ? _fn.sanitize(_fn.trim(req.body.qr_code)).xss() : '';
            var nfc_code    = req.body.nfc_code ? _fn.sanitize(_fn.trim(req.body.nfc_code)).xss() : '';
            var qr_code2     = req.body.qr_code2 ? _fn.sanitize(_fn.trim(req.body.qr_code2)).xss() : '';
            var nfc_code2    = req.body.nfc_code2 ? _fn.sanitize(_fn.trim(req.body.nfc_code2)).xss() : '';
            var qr_code3     = req.body.qr_code3 ? _fn.sanitize(_fn.trim(req.body.qr_code3)).xss() : '';
            var nfc_code3    = req.body.nfc_code3 ? _fn.sanitize(_fn.trim(req.body.nfc_code3)).xss() : '';
            var qr_code4     = req.body.qr_code4 ? _fn.sanitize(_fn.trim(req.body.qr_code4)).xss() : '';
            var nfc_code4    = req.body.nfc_code4 ? _fn.sanitize(_fn.trim(req.body.nfc_code4)).xss() : '';
            var group_type  = req.body.group_type ? _fn.sanitize(_fn.trim(req.body.group_type)).xss() : '';
            var card_type  = req.body.card_type ? _fn.sanitize(_fn.trim(req.body.card_type)).xss() : '';
            var group_id    = req.body.group_id ? _fn.sanitize(_fn.trim(req.body.group_id)).xss() : '';
            var role = req.session._role;
            Step (
                function createTempChild() {
                    console.log('group_type:'+group_type);
                    console.log('card_type:'+card_type);
                    if(group_type == 1){
                        managers.child.insertChild('TEMP_CHILD', '', '', nfc_code, qr_code, company_id, '', 2, uid, '', this.parallel());
                        managers.child.insertGuardian('TEMP_GUARDIAN', '', qr_code2, nfc_code2, uid, 0, 2, this.parallel());
                    }
                    else if(group_type == 2){
                        managers.bus.insertTempBus('TEMP_BUS','','','',uid,this.parallel());
                        _fn.r([[{id:0}]], this.parallel());
                    }
                    else{
                        if(card_type == 4){
                            //Create Driver
                            var username = 'driver'+ (new Date()).getTime();
                            managers.driver.insertDriver(0, '', '', '', '', config.DEFAULT_AVATAR, username, username, 0, this.parallel());

                        }
                        else{
                            //Create Assistant
                            var username = 'assstant'+ (new Date()).getTime();
                            managers.assistant.insertAssistant(0, '', '', '', '', config.DEFAULT_AVATAR, username, username, 0, this.parallel());
                        }
                        _fn.r([[{id:0}]], this.parallel());
                    }
                    managers.child.updateCardInPairStatus(qr_code, '', 2, function(){});
                    managers.child.updateCardInPairStatus('', nfc_code, 2, function(){});
                    if(qr_code2 != '' && nfc_code2 != ''){
                        managers.child.updateCardInPairStatus(qr_code2, '', 2, function(){});
                        managers.child.updateCardInPairStatus('', nfc_code2, 2, function(){});
                    }
                    if(qr_code3 != '' && nfc_code3 != ''){
                        managers.child.updateCardInPairStatus(qr_code3, '', 2, function(){});
                        managers.child.updateCardInPairStatus('', nfc_code3, 2, function(){});
                    }
                    if(qr_code4 != '' && nfc_code4 != ''){
                        managers.child.updateCardInPairStatus(qr_code4, '', 2, function(){});
                        managers.child.updateCardInPairStatus('', nfc_code4, 2, function(){});
                    }
                },
                function addCardInfo(e, c, d){
                    if(e) throw e;
                    var pair_id = (new Date()).getTime();
                    if(group_type == 1){
                        var child_id = c[0][0].id;
                        managers.child.insertCardInfo(1, 0, child_id, nfc_code, 1, uid, uid, 1, '', function(){});
                        managers.child.insertCardInfo(1, 0, child_id, qr_code, 2, uid, uid, 1, '', function(){});
                        var guardian_id = d[0][0].id;
                        managers.child.insertCardInfo(2, 0, guardian_id, nfc_code2, 1, uid, uid, 1, '', function(){});
                        managers.child.insertCardInfo(2, 0,guardian_id, qr_code2, 2, uid, uid, 1, '', function(){});
                        managers.child.insertGuardianChild(guardian_id, child_id, uid, 1, group_id,  function(){});
                        managers.child.insertCardInPair(qr_code, nfc_code, 1, child_id, uid, group_id, 0, pair_id, this.parallel());
                        managers.child.insertCardInPair(qr_code2, nfc_code2, 2, guardian_id, uid, group_id, 0, pair_id, this.parallel());
                    }
                    else if(group_type == 2 ){
                        var bus_id = c[0][0].id;
                        managers.child.insertCardInfo(3, 1, bus_id, nfc_code, 1, uid, uid, 1, '', function(){});
                        managers.child.insertCardInfo(3, 1, bus_id, qr_code, 2, uid, uid, 1, '', function(){});
                        managers.child.insertCardInfo(3, 2, bus_id, nfc_code2, 1, uid, uid, 1, '', function(){});
                        managers.child.insertCardInfo(3, 2, bus_id, qr_code2, 2, uid, uid, 1, '', function(){});
                        managers.child.insertCardInfo(3, 1, bus_id, nfc_code3, 1, uid, uid, 1, '', function(){});
                        managers.child.insertCardInfo(3, 1, bus_id, qr_code3, 2, uid, uid, 1, '', function(){});
                        managers.child.insertCardInfo(3, 2, bus_id, nfc_code4, 1, uid, uid, 1, '', function(){});
                        managers.child.insertCardInfo(3, 2, bus_id, qr_code4, 2, uid, uid, 1, '', function(){});
                        managers.child.insertCardInPair(qr_code, nfc_code, 3, bus_id, uid, group_id, 1, pair_id, this.parallel());
                        managers.child.insertCardInPair(qr_code2, nfc_code2, 3, bus_id, uid, group_id, 2, pair_id, this.parallel());
                        managers.child.insertCardInPair(qr_code3, nfc_code3, 3, bus_id, uid, group_id, 1, pair_id, function(){});
                        managers.child.insertCardInPair(qr_code4, nfc_code4, 3, bus_id, uid, group_id, 2, pair_id, function(){});
                    }
                    else{
                        if(card_type == 4){
                            var driver_id = c[0][0].userid;
                            managers.child.insertCardInfo(4, 0, driver_id, nfc_code, 1, uid, uid, 1, '', function(){});
                            managers.child.insertCardInfo(4, 0, driver_id, qr_code, 2, uid, uid, 1, '', function(){});
                            managers.child.insertCardInPair(qr_code, nfc_code, 4, driver_id, uid, group_id, 2, pair_id, this.parallel());
                            _fn.r([], this.parallel());
                        }
                        else{
                            var assistant_id = c[0][0].userid;
                            managers.child.insertCardInfo(5, 0, assistant_id, nfc_code, 1, uid, uid, 1, '', function(){});
                            managers.child.insertCardInfo(5, 0, assistant_id, qr_code, 2, uid, uid, 1, '', function(){});
                            managers.child.insertCardInPair(qr_code, nfc_code, 5, assistant_id, uid, group_id, 2, pair_id, this.parallel());
                            _fn.r([], this.parallel());
                        }
                    }
                },
                function response(e, r, d) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        card  : r
                    });
                }
            )
        },

        apiUpdateParentChild : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var parent = _fn.sanitize(_fn.trim(req.params.parent)).xss();
            var relation = _fn.sanitize(_fn.trim(req.params.relation)).xss();
            var type = req.body.type ? _fn.sanitize(_fn.trim(req.body.type)).xss() : '1';
            var status = req.body.status ? _fn.sanitize(_fn.trim(req.body.status.toString())).xss() : '0';
            var role = req.session._role;

            Step (

                function checkValid() {

                    _fn.check(id, lingo.get('child_not_exists')).notEmpty().isInt();
                    _fn.check(parent, lingo.get('parent_not_exists')).notEmpty().isInt();
                    _fn.check(relation, lingo.get('err_anonymous')).notEmpty().isInt();
                    _fn.check(type, lingo.get('err_anonymous')).notEmpty();
                    _fn.check(status, lingo.get('err_anonymous')).notEmpty();
                    status = (status == '1') ? 1 : 0;

                    managers.child.getChildById(id, 1, this.parallel());

                    if(role == config.ROLE.BUS_COMPANY) {
                        //Get company
                        managers.user.getUserCompanyByType(uid, 3, this.parallel());
                    } else {
                        managers.child.getParentChildByType(uid, id, '1,2', this.parallel());
                    }
                },
                function getList(e, c, r) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));

                    c = c[0][0];

                    if(role == config.ROLE.BUS_COMPANY) {
                        if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                        r = r[0][0];

                        if(r.id != c.company_id) throw Error(lingo.get('err_permission'));
                    } else {
                        if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                    }

                    if(type == 1) managers.child.parentChildUpdateByType(parent, c.id, relation, '', status, this);
                    else managers.child.guardianChildUpdate(relation, c.id, status, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: (type == 1 ? lingo.get('update_parent_child_success') : lingo.get('update_guardian_child_success'))});
                }
            );
        },

        apiGetChildHolder : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var type = _fn.sanitize(_fn.trim(req.params.type)).xss();
            var role = req.session._role;

            Step (
                function checkValid() {

                    _fn.check(id, lingo.get('child_not_exists')).notEmpty().isInt();
                    type = (type == '1') ? 1 : 2;

                    managers.child.getChildById(id, 1, this.parallel());

                    if(role == config.ROLE.BUS_COMPANY) {
                        //Get company
                        managers.user.getUserCompanyByType(uid, 3, this.parallel());
                    } else {
                        managers.child.getParentChildByType(uid, id, '1,2', this.parallel());
                    }
                },
                function getList(e, c, r) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));

                    c = c[0][0];

                    if(role == config.ROLE.BUS_COMPANY) {
                        if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                        r = r[0][0];

                        if(r.id != c.company_id) throw Error(lingo.get('err_permission'));
                    } else {
                        if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                    }

                    if(type == 1) managers.child.getListParentChildByType(uid, c.id, type, this);
                    else managers.child.getListGuardianByChild(c.id, '0,1', this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        list   : r[0]
                    });
                }
            )
        },

        apiGetChildDetail : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var role = req.session._role;

            Step (
                function checkValid() {

                    _fn.check(id, lingo.get('child_not_exists')).notEmpty().isInt();

                    managers.child.getChildById(id, 1, this.parallel());

                    if(role == config.ROLE.BUS_COMPANY) {
                        //Get company
                        managers.user.getUserCompanyByType(uid, 3, this.parallel());
                    } else {
                        managers.child.getParentChildByType(uid, id, '1,2', this.parallel());
                    }
                },
                function getDetail(e, c, r) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));

                    c = c[0][0];

                    if(role == config.ROLE.BUS_COMPANY) {
                        if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                        r = r[0][0];

                        if(r.id != c.company_id) throw Error(lingo.get('err_permission'));
                    } else {
                        if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));
                    }

                    if(c.image == '') c.image = config.DEFAULT_AVATAR;

                    return c;
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        child  : r
                    });
                }
            );
        },

        registerGuardian : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var gid = req.body.id ? _fn.sanitize(_fn.trim(req.body.id)).xss() : '';
            var name = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var image = req.body.image ? _fn.sanitize(_fn.trim(req.body.image)).xss() : '';
            var child = req.body.child ? _fn.sanitize(_fn.trim(req.body.child.toString())).xss() : '';
            var childExsits = req.body.child_exists == 1;
            var role = req.session._role;

            Step (
                function checkValid() {

                    _fn.check(gid, lingo.get('guardian_card_invalid')).notEmpty();
                    _fn.check(child, lingo.get('child_not_exists')).notEmpty().isInt();
                    _fn.check(name, lingo.get('guardian_name_invalid')).notEmpty().len(5);

                    managers.child.getChildById(child, 1, this.parallel());
                    managers.child.getGuardianById(gid, this.parallel());

                    if(role == config.ROLE.BUS_COMPANY) {
                        //Get company
                        managers.user.getUserCompanyByType(uid, 3, this.parallel());
                    } else {
                        managers.child.getParentChildByType(uid, child, '1,2', this.parallel());
                    }
                },
                function makeGuardian(e, c, g, _r) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));
                    if(!g.length && !g[0].length) throw Error(lingo.get('guardian_not_exists'));

                    c = c[0][0];
                    g = g[0][0];

                    if(g.status == 1 && g.parent_id != 0) throw Error(lingo.get('err_permission'));

                    if(role == config.ROLE.BUS_COMPANY) {
                        if(!_r.length || !_r[0].length) throw Error(lingo.get('err_permission'));
                        _r = _r[0][0];

                        if(_r.id != c.company_id) throw Error(lingo.get('err_permission'));
                    } else {
                        if(!_r.length || !_r[0].length) throw Error(lingo.get('err_permission'));
                    }

                    g = _fn.extend(g, {name: name, image: image});

                    _fn.r(g, this.parallel());
                    managers.child.updateGuardianInfo(g.id, g.name, g.image, 1, uid, this.parallel());
                    managers.child.guardianChildInsert(g.id, c.id, uid, 1, this.parallel());
                    if(!childExsits) managers.child.parentChildInsert(g.id, c.id, 3, '', 1, this.parallel());
                },
                function response(e, g) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('create_guardian_success'),
                        guardian: {
                            id: g.id,
                            name: g.name,
                            image: g.image != '' ? g.image : config.DEFAULT_AVATAR,
                            card: g.NFC,
                            qr_code: g.QRCode,
                            readOnly: true
                        }
                    });
                }
            )
        },

        apiSaveChildDetail : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = _fn.sanitize(_fn.trim(req.params.id)).xss();
            var name = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var image = req.body.image ? _fn.sanitize(_fn.trim(req.body.image)).xss() : '';
            var nfc = req.body.NFC ? _fn.sanitize(_fn.trim(req.body.NFC)).xss() : '';
            var tempUpd = req.body.tempUpd ? true : false;// Temp Profile update by Worker;
            var role = req.session._role;

            Step (
                function checkValid() {

                    _fn.check(id, lingo.get('child_not_exists')).notEmpty().isInt();
                    _fn.check(name, lingo.get('child_name_invalid')).notEmpty().len(5);

                    managers.child.getChildById(id, 1, this.parallel());

                    if(role == config.ROLE.BUS_COMPANY) managers.user.getUserCompanyByType(uid, 3, this.parallel());
                    else if(!tempUpd) managers.child.getParentChildByType(uid, id, '1,2', this.parallel());
                    else _fn.r([], this.parallel());

                    if(nfc != '') managers.card.getCardInfoByCard(nfc, 0, this.parallel());
                    else _fn.r([], this.parallel());
                },
                function getDetail(e, c, r, ne) {
                    if(e) throw e;
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));
                    if(ne.length && ne[0].length) {
                        ne = ne[0][0];

                        if(ne.ower_type != 1 || ne.owner_id != id ) throw Error(lingo.get('card_has_been_added'));
                    }

                    c = c[0][0];

                    if(tempUpd) {
                        if((role != config.ROLE.DRIVER && role != config.ROLE.ASSISTANT) || c.status != 2) throw Error(lingo.get('err_permission'));
                    } else {
                        if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));

                        if(role == config.ROLE.BUS_COMPANY) {
                            r = r[0][0];

                            if(r.id != c.company_id) throw Error(lingo.get('err_permission'));
                        }
                    }

                    managers.child.updateChild(c.id, name, c.id_no, c.mobile, (nfc != '' ? nfc : c.NFC), c.QRCode, c.company_id, image, c.status, c.create_by, this.parallel());
                    if(nfc != '') managers.card.updateCardInfoByOwner(1, c.id, nfc, 1, uid, uid, 1, '', this.parallel());
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: lingo.get('update_child_success')
                    });
                }
            );
        },

        getChilds : function(req, res) {

            var lingo = req.lingo;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            var uid = req.session.uId;

            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    //Get company
                    managers.user.getUserCompanyByType(uid, 3, this);
                },
                function getChild(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('err_permission'));

                    r = r[0][0];

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    managers.child.getListChildPaged(r.id, page, numrow, order, desc, '', this);
                },
                function prepare(e, r) {
                    if(e) throw e;

                    var data = [];
                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            data.push({
                                id: v.id,
                                name: v.name,
                                id_no: v.id_no,
                                status: v.status,
                                avatar: (v.image && v.image != '') ? v.image : config.DEFAULT_AVATAR,
                                create_at: v.create_at
                            });
                        });
                    }

                    return [data, r[1]]
                },
                function response(e, r) {

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            childs  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        },

        getChildByCode : function(req, res) {

            var lingo = req.lingo;
            var qrcode = req.body.qrcode ? _fn.sanitize(_fn.trim(req.body.qrcode)).xss() : '';
            var nfccode = req.body.nfccode ? _fn.sanitize(_fn.trim(req.body.nfccode)).xss() : '';
            var uid = req.session.uId;

            Step (
                function getChild() {
                    managers.child.getChildByCode(0, 1, nfccode, qrcode, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            childs  : r[0]
                        });
                    }
                }
            )
        },

        getGuardianByCode : function(req, res) {

            var lingo = req.lingo;
            var qrcode = req.body.gr_qrcode ? _fn.sanitize(_fn.trim(req.body.gr_qrcode.toString())).xss() : '';
            var nfccode = req.body.gr_nfccode ? _fn.sanitize(_fn.trim(req.body.gr_nfccode.toString())).xss() : '';
            var uid = req.session.uId;
            console.log('aaaaaaaaaaaaaa');
            Step (
                function getGuardian() {
                    managers.child.getGuardianByCode(0, nfccode, qrcode, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            guardians  : r[0]
                        });
                    }
                }
            )
        },

        insertGuardianChild : function(req, res) {

            var lingo = req.lingo;
            var pguardian_id = req.body.guardian_id ? _fn.sanitize(_fn.trim(req.body.guardian_id.toString())).xss() : '';
            var pchild_id = req.body.child_id ? _fn.sanitize(_fn.trim(req.body.child_id.toString())).xss() : '';
            var uid = req.session.uId;
            Step (
                function insertGuardianChild() {
                    managers.child.insertGuardianChild(pguardian_id, pchild_id, uid, 1, 0, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            guardians  : r[0]
                        });
                    }
                }
            )
        },

        syncCard : function(req, res) {

            var lingo = req.lingo;
            var temp_profile = req.body.temp_profile ? _fn.sanitize(_fn.trim(req.body.temp_profile.toString())).xss() : '';
            var child_id = req.body.child_id ? _fn.sanitize(_fn.trim(req.body.child_id.toString())).xss() : '';
            var uid = req.session.uId;
            Step (
                function getChild() {
                   // managers.child.syncChildCard(temp_profile, child_id, this);
                    managers.child.getChildById(temp_profile, 1, this.parallel());
                    managers.child.getChildById(child_id, 1, this.parallel());
                },
                function syncCard(e, r, c) {
                    if(e) res.json({success: false, message: e.message});
                    if(!r.length || !r[0].length) throw Error(lingo.get('child_not_exists'));
                    if(!c.length || !c[0].length) throw Error(lingo.get('child_not_exists'));
                    var temp_data = r[0][0];
                    var child_data = c[0][0];
                    console.log(temp_data);
                    console.log(child_data);
                    managers.child.updateChild(child_data.id, child_data.name, child_data.id_no, child_data.mobile, temp_data.NFC, temp_data.QRCode, child_data.company_id, child_data.image, child_data.status, child_data.create_by, this.parallel());
                    managers.child.updateChild(temp_data.id, temp_data.name, temp_data.id_no, temp_data.mobile, temp_data.NFC, temp_data.QRCode, temp_data.company_id, temp_data.image, 3, temp_data.create_by, this.parallel());
                },
                function response(e, r , c) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true
                        });
                    }
                }
            )
        },

        updateTempProfile : function(req, res) {

            var lingo = req.lingo;
            var id = req.body.id ? _fn.sanitize(_fn.trim(req.body.id.toString())).xss() : '';
            var name = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var uid = req.session.uId;
            var company_id  = req.session.company_id;

            Step (
                function updateTempProfile() {
                    managers.child.updateTempProfile(id, name, company_id, 1, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            child  : r
                        });
                    }
                }
            )
        },

        apiGetChildNotifications: function(req, res) {
            var lingo = req.lingo;
            var id = req.query.id ? _fn.sanitize(_fn.trim(req.query.id)).toInt() : 0;

            Step(
                function valid() {
                    _fn.check(id, lingo.get('child_not_exists')).notEmpty().isInt();

                    managers.child.getNotifications(id, 123, this);
                },
                function prepare(e, r) {
                    if(e) throw e;

                    var data = [];
                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            data.push({
                                id: v.id,
                                status: v.status,
                                message: v.message,
                                time: moment(v.at).fromNow()
                            });
                        });
                    }

                    return data;
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, notifications: r});
                }
            );
        },


        apiChildNotificationsUpdate: function(req, res) {
            var lingo = req.lingo;
            var ids = req.body.ids ? req.body.ids : [];

            Step(
                function valid() {
                    if(!ids.length) throw Error('Break');

                    var group = this.group();
                    ids.forEach(function(v) {
                        managers.child.updateNotificationsStatus(v, 2, group());
                    });
                },
                function response(e, r) {
                    if(e) res.json({success: false});
                    else res.json({success: true});
                }
            );
        }
    };
    var utils = {
        pushNotificationtoWorker : function(message, uids){
            managers.LBA.pushRoom('wcl#user#' + uids, 'child:absent:notify', message);
        }
    };

})(module.exports, require);
