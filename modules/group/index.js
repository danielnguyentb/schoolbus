/**
 * Created by AnhNguyen
 */

(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn,        //Utility functions
        i18n;

    var locale = 'groups';

    var Step = r("step");

    ns.init = function(app, _db, fn, mn) {

        db = _db;
        _fn = fn;
        managers = mn;

        app.route('/group')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.groupManage, _fn.auth.act.get, _fn.auth.permission, routes.getGroup)
            .post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.groupManage, _fn.auth.act.post, _fn.auth.permission, routes.updateGroup);

        app.route('/group/:id')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.groupManage, _fn.auth.act.get, _fn.auth.permission, routes.getGroupDetail)
            .delete(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.groupManage, _fn.auth.act.delete, _fn.auth.permission, routes.removeGroup);
    };

    var routes = {

        getGroup : function(req, res) {

            var lingo = req.lingo;
            var numrow = req.query.limit ? parseInt(req.query.limit, 10) : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? parseInt(req.query.page, 10) : 1;
            var keyword = req.query.keyword ? _fn.sanitize(_fn.trim(req.query.keyword)).xss() : '';

            Step (
                function checkInput() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    managers.group.getListGroupPaged(page, numrow, order, desc, keyword, this);
                },
                function response(e, r) {

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            groups  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        },

        updateGroup : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.body.id ? req.body.id : false;
            var name = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var desc = req.body.desc ? _fn.sanitize(_fn.trim(req.body.desc)).xss() : '';
            var edit = false;

            Step (
                function checkInput() {

                    _fn.check(name, lingo.get('group_name_invalid')).notEmpty().len(5);

                    if(id != '') edit = true;

                    if(edit) managers.group.getGroupById(id, this);
                    else return this;
                },
                function updateGroup(e, r) {
                    if(e) throw e;
                    if(edit && (!r.length || !r[0].length) ) throw Error(lingo.get('group_not_exists'));

                    var status = req.body.status ? 1 : 0;

                    if(edit) managers.group.updateGroup(id, name, 1, status, r[0][0].owner_type, r[0][0].owner_id, desc, 0, this);
                    else managers.group.insertGroup(name, 1, status, 1, uid, desc, 0, this);
                },
                function response(e, r) {
                    console.log(e);
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: edit ? lingo.get('update_group_success') : lingo.get('create_group_success')
                    })
                }
            )
        },

        getGroupDetail : function(req, res) {

            var lingo = req.lingo;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).xss() : '';

            Step (
                function getDetail() {

                    _fn.check(id, lingo.get('group_not_exists')).notEmpty().isInt();

                    managers.group.getGroupById(id, this);
                },
                function response(e, r) {

                    if(e || !r.length || !r[0].length) res.json({success: false, message: lingo.get('group_not_exists')});
                    else res.json({success: true, group: r[0][0]});
                }
            )
        },

        removeGroup : function(req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).xss() : '';

            Step (
                function getGroup() {

                    _fn.check(id, lingo.get('group_not_exists')).notEmpty().isInt();
                    managers.group.getGroupById(id, this);
                },
                function checkUserExists(e, r) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('group_not_exists'));

                    r = r[0];
                    managers.user.getUserByGroup(r.id, this);
                },
                function removeGroup(e, r) {
                    if(e) throw e;
                    if(r.length && r[0].length) throw Error(lingo.get('group_contain_users'));

                    managers.group.removeGroup(uid, id, this);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('remove_group_success')});
                }
            )
        }
    };

})(module.exports, require);
