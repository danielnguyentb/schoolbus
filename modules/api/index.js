/**
 * Created by AnhNguyen
 */
(function(ns, r) {
    "use strict";

    var db,         //Database connections
        managers,   //Data layer
        _fn;        //Utility functions

    var auditr,
        Step = r('step'),
        jwt = r('jsonwebtoken');
    var crypto = r("crypto");
    var locale = 'auth';

    ns.init = function (app, _db, fn, mn, i18n) {
        db = _db;
        _fn = fn;
        managers = mn;

        auditr = fn.audit;
        var apiRoutes = app.get('wcl:api:routes');

        //============= Handlers for routes ============================
        apiRoutes.route('/').get(_fn.getBundle(locale), routes.index);
        apiRoutes.route('/authenticate').post(_fn.getBundle(locale), routes.auth);
        apiRoutes.route('/auth/role').post(_fn.getBundle(locale), routes.authRole);
        apiRoutes.route('/user/role').post(_fn.getBundle(locale), _fn.auth.apiAuth, routes.userRole);
        apiRoutes.route('/register').post(_fn.getBundle(locale), routes.register);
        apiRoutes.route('/auth/forgot').post(_fn.getBundle(locale), routes.forgot);
        apiRoutes.route('/profile')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, routes.getProfile)
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, routes.updateProfile);
        apiRoutes.route('/location').post(routes.location);
        //Upload handle
        apiRoutes.route('/file/upl').post(
            _fn.getBundle(locale),
            _fn.auth.apiAuth,
            _fn.uploads.processImg,
            managers.privateData.libImgUpload,
            routes.uploadFile
        );
        app.route('/file/upl').post(
            _fn.getBundle(locale),
            _fn.auth.check,
            _fn.uploads.processImg,
            managers.privateData.libImgUpload,
            routes.uploadFile
        );

        apiRoutes.route('/get/cfg')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, routes.getCfg);

        apiRoutes.route('/notification/token').post(_fn.auth.apiAuth, routes.registerDeviceToken);
        apiRoutes.route('/notification/token/:token').delete(routes.removeDeviceToken);
    };

    var routes = {
        index   : function (req, res) {
            res.send('Hello! Welcome to the WCL API v1');
        },
        location: function (req, res) {
            var path = require('path'), date = new Date();
            var filePath = path.join(__dirname, '../..', 'public', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + '-gps-logs.txt');
            var filePathPoints = path.join(__dirname, '../..', 'public', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + '-gps-points.txt');
            var fs = require('fs');
            var logStream = fs.createWriteStream(filePath, {'flags': 'a'});
            var pointsStream = fs.createWriteStream(filePathPoints, {'flags': 'a'});
            logStream.write(date.toString() + " - " + JSON.stringify(req.body));
            pointsStream.write(req.body.latlong);
            logStream.end('\n');
            pointsStream.end('\n');
            res.send('ok');
        },

        uploadFile: function (req, res) {

            var lingo = req.lingo;
            var file = req.uplFile;
            delete(file.tmpfile);

            var data = {
                success : true,
                fileinfo: file,
                link    : '/pvi/' + req.session.uId + '.' + file.hash,
                message : lingo.get('upload_success')
            };

            res.json(data);
        },

        forgot: function (req, res) {

            var lingo = req.lingo;
            var email = _fn.sanitize(_fn.trim(req.body.email)).xss();

            Step(
                function checkEmail() {

                    managers.user.checkUserExist(0, 2, email, this.parallel());
                    crypto.randomBytes(48, this.parallel());
                },
                function createToken(e, r, g) {
                    if (e) throw e;
                    if (!r.length || !r[0].length) throw Error(lingo.get('err_invalid_email'));

                    var token = g.toString('base64').substring(0, 8);
                    var cipher = crypto.createCipher('aes-256-cbc', email);
                    var encrypted = cipher.update(token, 'utf8', 'base64');
                    encrypted = cipher.final('base64');

                    r = r[0][0];
                    _fn.r(_fn.sanitize(encrypted).entityEncode(), this.parallel());
                    managers.user.insertResetToken(r.email, crypto.createHash('sha256').update(token).digest("hex"), this.parallel());
                },
                function emailToUser(e, r, g) {
                    if (e) res.send({success: false, message: e.message});
                    else {

                        _fn.email.systemMail(email, lingo.get('reset_email_title'), _fn.str.f(lingo.get('reset_email'), {
                            reset_url: config.SHARED_DOMAIN + '/#!/forgot?email=' + email + '&token=' + r
                        }));

                        res.send({success: true, message: lingo.get('reset_success')});
                    }
                }
            );
        },

        getProfile: function (req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;

            Step(
                function getUser() {

                    managers.user.getUserById(uid, this.parallel());
                },
                function getCompany(e, u, r) {
                    if (e) throw e;
                    if (!u.length || !u[0].length) throw Error(lingo.get('err_anonymous'));

                    u = u[0][0];
                    if (u.image == '') u.image = config.DEFAULT_AVATAR;
                    if (!u.locale || u.locale == '') u.locale = 'en-US';

                    _fn.r(u, this.parallel());
                },
                function response(e, u, c) {
                    if (e) res.json({error: true, message: e.message});
                    else res.json({
                        error: false,
                        user : u
                    });
                }
            )
        },

        updateProfile: function (req, res) {

            var lingo = req.lingo;
            var uid = req.session.uId;
            var email = req.body.email ? _fn.sanitize(_fn.trim(req.body.email)).xss() : '';
            var uname = req.body.username ? _fn.sanitize(_fn.trim(req.body.username)).xss() : '';
            var mobile = req.body.mobile_phone ? _fn.sanitize(_fn.trim(req.body.mobile_phone)).xss() : '';
            var home = req.body.home_phone ? _fn.sanitize(_fn.trim(req.body.home_phone)).xss() : '';
            var office = req.body.office_phone ? _fn.sanitize(_fn.trim(req.body.office_phone)).xss() : '';
            var image = req.body.image ? _fn.sanitize(_fn.trim(req.body.image)).xss() : '';
            var fullname = req.body.fullname ? _fn.sanitize(_fn.trim(req.body.fullname)).xss() : '';
            var past_task_time = req.body.past_task_time ? _fn.sanitize(_fn.trim(req.body.past_task_time)).xss() : '';
            var locale = req.body.locale ? _fn.sanitize(_fn.trim(req.body.locale)).xss() : '';

            Step(
                function valid() {
                    if (config.LOCALES.indexOf(locale) === -1) locale = 'en-US';

                    _fn.check(email, lingo.get('err_invalid_email')).notEmpty().isEmail();
                    _fn.check(uname, lingo.get('invalid_username')).notEmpty().len(2);

                    _fn.doAsync(this);
                },
                function checkInput(e, r) {
                    if(e) throw e;

                    managers.user.getUserById(uid, this.parallel());
                    managers.user.checkUserExist(uid, 2, email, this.parallel());
                    managers.user.checkUserExist(uid, 1, uname, this.parallel());

                    if (mobile != '') managers.user.checkUserExist(uid, 3, mobile, this.parallel());
                    else _fn.r([[]], this.parallel());
                },
                function updateProfile(e, u, r, ur, me) {
                    if (e) throw e;

                    if (r.length && r[0].length) throw Error(lingo.get('err_exists_email'));
                    if (ur.length && ur[0].length) throw Error(lingo.get('err_exists_uname'));
                    if (me.length && me[0].length) throw Error(lingo.get('err_mobile_phone_exists'));

                    u = u[0][0];

                    managers.user.updateProfile(uid, uname, email, mobile, home, office, image, fullname, locale, past_task_time, this.parallel());

                    if(u.status == 0) managers.user.userApprove(u.id, 1, this.parallel());
                },
                function response(e, r) {
                    if (e) res.json({success: false, message: e.message});
                    else {

                        req.session.email = email;
                        req.session.uname = uname;
                        req.session.avatar = (image != '') ? image : config.DEFAULT_AVATAR;
                        req.session.save();

                        res.json({success: true, message: lingo.get('update_profile_success')});
                    }
                }
            )
        },

        auth: function (req, res) {

            var lingo = req.lingo;
            var uname = req.body.username ? _fn.sanitize(_fn.trim(req.body.username)).xss() : '';
            var upass = req.body.password ? _fn.sanitize(_fn.trim(req.body.password)).xss() : '';
            var app = req.body.app ? _fn.sanitize(_fn.trim(req.body.app)).xss() : '';
            var switchMode = false, uid, token = '', tmpAcc = false;

            Step(
                function checkInput() {
                    _fn.check(uname, lingo.get('invalid_username')).notEmpty();
                    _fn.check(upass, lingo.get('invalid_user_pass')).notEmpty().len(3, 255);
                    _fn.check(app, lingo.get('err_permission')).notEmpty().isIn(['driver', 'parent', 'sba']);

                    upass = crypto.createHash('sha256').update(upass).digest("hex");
                    managers.user.getUserLogIn(uname, upass, this);
                },
                function getAcl(e, r) {
                    if (e) throw e;

                    if (r[1][0] && r[1][0].nLogWrong >= 30) throw Error(lingo.get('lock_use_one_hour'));
                    if (!r.length || !r[0].length) throw Error(lingo.get('invalid_login'));

                    r = r[0][0];

                    if(r.status == 0 && !r.registration_at) tmpAcc = true;

                    if (!tmpAcc && r.status != 1) throw Error(lingo.get('err_active'));

                    _fn.r(r, this.parallel());
                    managers.acl.userRoles(r.id.toString(), this.parallel());

                    managers.user.getUserCompany(r.id, this.parallel());
                    crypto.randomBytes(48, this.parallel());
                },
                function getRole(e, r, _r, uc, t) {
                    if (e) throw e;
                    if (!_r.length) throw Error(lingo.get('err_permission'));

                    var sysRole, company = null, companies = [], newRole = false;
                    if (uc.length && uc[0].length) {
                        uc[0].forEach(function (v) {
                            if (v.status == 1 && v.company_status == 1) {
                                companies.push({
                                    id  : v.company_id,
                                    name: v.name,
                                    type: v.type,
                                    mode: v.mode.split(',')
                                });
                            }
                        });
                    }

                    switch (app) {
                        case 'driver':
                            var role = req.body.role ? _fn.sanitize(_fn.trim(req.body.role)).toInt() : 1;
                            var companyId = req.body.cid ? _fn.sanitize(_fn.trim(req.body.cid)).toInt() : 0;
                            if (isNaN(role) || [1, 2, 9].indexOf(role) == -1) role = 1;
                            if (isNaN(companyId)) companyId = 0;

                            if (_r.indexOf(config.ROLE.BUS_COMPANY) != -1) throw Error(lingo.get('err_permission'));

                            if (_r.indexOf(config.ROLE.BUS) != -1) {
                                if (role != 9) throw Error(lingo.get('err_permission'));

                                companies.forEach(function (v) {
                                    if ((v.type == role && (companyId == 0 || companyId == v.id))) {
                                        company = v;

                                        return false;
                                    }

                                    if (!company && v.type == role) company = v;
                                });

                                sysRole = config.ROLE.BUS;
                            } else {
                                if (_r.indexOf(config.ROLE.DRIVER) == -1 && _r.indexOf(config.ROLE.ASSISTANT) == -1) {
                                    uid = r.id;
                                    switchMode = true;
                                    token = t.toString('base64').substring(0, 16);

                                    managers.L2.setex('userSwitchRoleToken:' + uid, token, 3600, _fn.noop);

                                    throw Error(lingo.get('err_permission'));
                                } else {
                                    companies.forEach(function (v) {
                                        if ((v.type == role && (companyId == 0 || companyId == v.id))) {
                                            company = v;

                                            return false;
                                        }

                                        if (!company && v.type == role) company = v;
                                    });

                                    sysRole = role == 1 ? config.ROLE.DRIVER : config.ROLE.ASSISTANT;

                                    if (_r.indexOf(sysRole) == -1) newRole = true;
                                }
                            }
                            break;
                        case 'parent':
                            if (_r.indexOf(config.ROLE.BUS_COMPANY) != -1 || _r.indexOf(config.ROLE.BUS) != -1) throw Error(lingo.get('err_permission'));

                            if (_r.indexOf(config.ROLE.PARENT) == -1) {
                                uid = r.id;
                                switchMode = true;
                                token = t.toString('base64').substring(0, 16);

                                managers.L2.setex('userSwitchRoleToken:' + uid, token, 3600, _fn.noop);

                                throw Error(lingo.get('err_permission'));
                            } else {
                                sysRole = config.ROLE.PARENT;
                                company = null;
                            }
                            break;
                        case 'sba':
                            if (_r.indexOf(config.ROLE.ADMIN) == -1 && _r.indexOf(config.ROLE.BUS_COMPANY) == -1) throw Error(lingo.get('err_permission'));
                            else {
                                sysRole = _r.indexOf(config.ROLE.ADMIN) != -1 ? config.ROLE.ADMIN : config.ROLE.BUS_COMPANY;

                                if (sysRole == config.ROLE.BUS_COMPANY) {
                                    companies.forEach(function (v) {
                                        if (v.type == 3) {
                                            company = v;

                                            return false;
                                        }
                                    });

                                    if (!company) throw Error(lingo.get('err_permission'));
                                }
                            }
                            break;
                        default:
                            break;
                    }

                    _fn.r(r, this.parallel());
                    _fn.r(sysRole, this.parallel());
                    _fn.r(company, this.parallel());

                    if (newRole) managers.acl.addUsertoRole(r.id.toString(), sysRole, this.parallel());
                },
                function getResource(e, u, ro, uc) {
                    if (e) throw e;

                    _fn.r(u, this.parallel());
                    _fn.r(ro, this.parallel());
                    managers.acl.whatResources(ro, this.parallel());
                    _fn.r(uc, this.parallel());
                },
                function store(e, u, role, ps, uc) {
                    if(e) res.json({success: false, message: e.message, switchMode: switchMode, uid: uid, token: token});
                    else {
                        uc = uc || {};

                        var user = {
                            uId       : u.id,
                            uname     : u.username,
                            company_id: uc.id || 0,
                            bc_mode   : uc.mode || ["1"],
                            email     : u.email,
                            avatar    : u.image != '' ? u.image : config.DEFAULT_AVATAR,
                            perms     : ps,
                            roles     : [role],
                            _role     : role,
                            locale    : u.locale || 'en-US',
                            tmpAcc    : tmpAcc
                        };
                        console.log('-----------------user----------------');
                        console.log(user);
                        var t = jwt.sign(user, config.session.secret, {
                            expiresIn: "1d"
                        });

                        res.json({
                            success: true,
                            message: lingo.get('auth_success'),
                            token  : t,
                            user   : _fn.extend({}, user, {compInfo: uc})
                        });
                    }

                }
            );
        },

        authRole: function (req, res) {

            var lingo = req.lingo;
            var uid = req.body.uid ? _fn.sanitize(_fn.trim(req.body.uid)).xss() : '';
            var role = req.body.role ? _fn.sanitize(_fn.trim(req.body.role)).xss() : "3";
            var token = req.body.token ? _fn.sanitize(_fn.trim(req.body.token)).xss() : '';

            Step(
                function valid() {
                    _fn.check(uid, lingo.get('user_invalid')).notEmpty();
                    _fn.check(role, lingo.get('please_select_role')).notEmpty().isIn(["1", "2", "3"]); //Role to switch: 1 - Driver, 2 - Assistant, 3 - Parent
                    _fn.check(token, lingo.get('err_anonymous')).notEmpty();

                    _fn.doAsync(this);
                },
                function getUser(e, r) {
                    if (e) throw e;

                    managers.user.getUserById(uid, this.parallel());
                    managers.L2.get('userSwitchRoleToken:' + uid, this.parallel());
                },
                function saveRole(e, r, t) {
                    if (e) throw e;
                    if (!r.length || !r[0].length) throw Error(lingo.get('user_invalid'));
                    if (!t || t != token) throw Error(lingo.get('err_permission'));

                    r = r[0][0];
                    if (r.status != 1) throw Error(lingo.get('err_active'));

                    var sysRole;
                    if (role == 1) sysRole = config.ROLE.DRIVER;
                    else if (role == 2) sysRole = config.ROLE.ASSISTANT;
                    else sysRole = config.ROLE.PARENT;

                    _fn.r(r, this.parallel());
                    _fn.r(sysRole, this.parallel());
                    managers.acl.addUsertoRole(r.id.toString(), sysRole, this.parallel());
                },
                function getResource(e, u, role, r) {
                    if (e) throw e;

                    _fn.r(u, this.parallel());
                    _fn.r(role, this.parallel());
                    managers.acl.whatResources(role, this.parallel());
                },
                function store(e, u, ro, ps) {
                    if (e) res.json({success: false, message: e.message});
                    else {
                        var user = {
                            uId       : u.id,
                            uname     : u.username,
                            company_id: 0,
                            bc_mode   : ["1"],
                            email     : u.email,
                            avatar    : u.image != '' ? u.image : config.DEFAULT_AVATAR,
                            perms     : ps,
                            roles     : [ro],
                            _role     : ro,
                            locale    : u.locale || 'en-US'
                        };
                        console.log('-----------------user----------------');
                        console.log(user);
                        var t = jwt.sign(user, config.session.secret, {
                            expiresIn: "1d"
                        });

                        res.json({
                            success: true,
                            token  : t,
                            user   : user
                        });
                    }

                }
            );
        },

        userRole: function (req, res) {

            var lingo = req.lingo;
            var role = req.session._role;
            var roleSwitch = role == config.ROLE.DRIVER ? 2 : 1;
            var uid = req.session.uId;
            var companyId = req.body.cid ? _fn.sanitize(_fn.trim(req.body.cid)).toInt() : 0;

            Step(
                function getUser() {
                    if (isNaN(companyId)) companyId = 0;

                    managers.user.getUserById(uid, this.parallel());
                    managers.user.getUserCompany(uid, this.parallel());
                    managers.acl.userRoles(uid.toString(), this.parallel());
                },
                function saveRole(e, u, uc, r) {
                    if (e) throw e;
                    if (!u.length || !u[0].length) throw Error(lingo.get('user_invalid'));

                    u = u[0][0];
                    if (u.status != 1) throw Error(lingo.get('err_active'));

                    var sysRole, company = null, companies = [];
                    if (uc.length && uc[0].length) {
                        uc[0].forEach(function (v) {
                            if (v.status == 1 && v.company_status == 1) {
                                companies.push({
                                    id  : v.company_id,
                                    name: v.name,
                                    type: v.type,
                                    mode: v.mode.split(',')
                                });
                            }
                        });
                    }

                    companies.forEach(function (v) {
                        if ((v.type == roleSwitch && (companyId == 0 || companyId == v.id))) {
                            company = v;

                            return false;
                        }

                        if (!company && v.type == roleSwitch) company = v;
                    });

                    sysRole = roleSwitch == 1 ? config.ROLE.DRIVER : config.ROLE.ASSISTANT;

                    _fn.r(u, this.parallel());
                    _fn.r(sysRole, this.parallel());
                    _fn.r(company, this.parallel());

                    if (r.indexOf(sysRole) == -1) managers.acl.addUsertoRole(uid.toString(), sysRole, this.parallel());
                },
                function getResource(e, u, role, uc, r) {
                    if (e) throw e;

                    _fn.r(u, this.parallel());
                    _fn.r(role, this.parallel());
                    _fn.r(uc, this.parallel());
                    managers.acl.whatResources(role, this.parallel());
                },
                function store(e, u, ro, uc, ps) {
                    if (e) res.json({success: false, message: e.message});
                    else {
                        uc = uc || {};

                        var user = {
                            uId       : u.id,
                            uname     : u.username,
                            company_id: uc.id || 0,
                            bc_mode   : uc.mode || ["1"],
                            email     : u.email,
                            avatar    : u.image != '' ? u.image : config.DEFAULT_AVATAR,
                            perms     : ps,
                            roles     : [ro],
                            _role     : ro,
                            locale    : u.locale || 'en-US'
                        };
                        console.log('-----------------user----------------');
                        console.log(user);
                        var t = jwt.sign(user, config.session.secret, {
                            expiresIn: "1d"
                        });

                        res.json({
                            success: true,
                            token  : t,
                            user   : user
                        });
                    }
                }
            );
        },

        register: function (req, res) {
            var lingo = req.lingo;
            var uname = req.body.username ? _fn.sanitize(req.body.username).xss() : '';
            var upass = req.body.password ? _fn.sanitize(req.body.password).xss() : '';
            var email = req.body.email ? _fn.sanitize(req.body.email).xss() : '';
            var company_name = req.body.company_name ? _fn.sanitize(req.body.company_name).xss() : '';
            var type = req.body.type ? _fn.sanitize(req.body.type).toInt() : 0;
            var obj = {};

            Step (
                function valid() {
                    if(isNaN(type)) type = 0;
                    if([1, 2, 3, 9].indexOf(type) == -1) throw Error(lingo.get('invalid_user_role'));

                    _fn.check(uname, lingo.get('invalid_username')).notEmpty().len(2);
                    _fn.check(email, lingo.get('invalid_email')).notEmpty().isEmail();
                    _fn.check(upass, lingo.get('invalid_user_pass')).notEmpty().len(3);

                    _fn.checkPass(obj, upass, this);
                },
                function checkUser(e) {
                    if (e) throw e;

                    //if (obj.invalid) throw Error(lingo.get('invalid_user_pass_' + obj.code));

                    managers.user.checkUserExist(0, 1, uname, this.parallel());
                    managers.user.checkUserExist(0, 2, email, this.parallel());
                },
                function checkCompany(e, un, em) {
                    if (e) throw e;
                    if (un[0].length) throw Error(lingo.get('err_exists_uname'));
                    if (em[0].length) throw Error(lingo.get('err_exists_email'));

                    if (type != 1) managers.user.checkCompanyExist(company_name, this.parallel());
                    else _fn.r([], this.parallel());
                },
                function saveUser(e, cn) {
                    if (e) throw e;

                    var comId = 0;

                    if (type != 1) {
                        if (!cn[0].length) throw Error(lingo.get('err_not_exists_company'));
                        comId = cn[0][0].id;
                    }

                    upass = crypto.createHash('sha256').update(upass).digest("hex");
                    _fn.r(comId, this.parallel());
                    managers.user.insertUser(uname, upass, 0, type, email, '', '', '', 0, '', _fn.date.mysqlDate(new Date()), config.DEFAULT_AVATAR, uname, this.parallel());
                    crypto.randomBytes(48, this.parallel());
                },
                function createToken(e, cid, u, g) {
                    if (e) throw e;

                    u = u[0][0].userid;

                    if (type != 1) managers.user.insertUserCompany(u, cid, type, 0, '', '', '', this.parallel());
                    else _fn.r(null, this.parallel());

                    var role = '';
                    switch (type) {
                        case 1:
                            role = config.ROLE.PARENT;
                            break;
                        case 2:
                            role = config.ROLE.DRIVER;
                            break;
                        case 3:
                            role = config.ROLE.ASSISTANT;
                            break;
                        case 9:
                            role = config.ROLE.BUS;
                            break;
                    }

                    var token = g.toString('base64').substring(0, 8);
                    var cipher = crypto.createCipher('aes-256-cbc', email);
                    var encrypted = cipher.update(token, 'utf8', 'base64');
                    encrypted = cipher.final('base64');

                    token = _fn.sanitize(encrypted).entityEncode();

                    _fn.r(u, this.parallel());
                    _fn.r(token, this.parallel());
                    managers.acl.addUsertoRole(u.toString(), role, this.parallel());
                    managers.user.insertResetToken(email, crypto.createHash('sha256').update(token).digest("hex"), this.parallel());
                },
                function sendMail(e, r, u, g) {
                    if (e) throw e;

                    try {
                        _fn.email.systemMail(
                            email,
                            lingo.get('active_email_title'),
                            _fn.str.f(lingo.get('active_email'), {
                                uname: uname,
                                url  : config.SHARED_DOMAIN + 'register/active?email=' + email + '&token=' + encodeURIComponent(g)
                            }),
                            this
                        );
                    } catch (e) {
                        throw Error(lingo.get('err_support_info') + e.message);
                    }

                    return this;
                },
                function response(e, r) {
                    if (e) res.send({success: false, message: e.message});
                    else res.send({success: true, message: lingo.get('register_success_app')});
                }
            );
        },

        getCfg: function (req, res) {
            var role = req.session._role;
            var uid = req.session.uId;

            Step(
                function getCompany() {
                    if (role == config.ROLE.PARENT) managers.comp.getCompanyByParent(uid, this);
                    else managers.user.getUserCompany(uid, this);
                },
                function getCompanyCfg(e, r) {
                    if (e) throw e;

                    if (!r.length || ![0].length) return [];

                    var group = this.group(), cids = {};
                    r[0].forEach(function (v) {
                        if (!cids[v.id]) managers.config.getBcConfig(v.id, group());

                        cids[v.id] = v.id;
                    });
                },
                function prepare(e, r) {
                    if (e) throw e;
                    if (!r || !r.length) return {};

                    var cfg = {};
                    r.forEach(function (v) {
                        if (!cfg.map_tracking) cfg.map_tracking = v.map_tracking;
                        if (!cfg.parent_map) cfg.parent_map = v.parent_map;
                    });

                    return cfg;
                },
                function response(e, r) {
                    if (e) res.send({success: false, message: e.message});
                    else res.send({success: true, cfg: r});
                }
            );
        },

        registerDeviceToken: function (req, res) {
            var deviceToken = req.body.device_token ? _fn.sanitize(req.body.device_token).xss() : '';
            var deviceType = req.body.device_type ? _fn.sanitize(req.body.device_type).xss() : 0;
            if ([0, 1, 2].indexOf(deviceType) === -1) deviceType = 0;

            var uid = req.session.uId;

            Step(
                function valid() {
                    _fn.check(deviceToken, 'Device token invalid.').notEmpty();

                    return this;
                },
                function save(e, r) {
                    if (e) throw e;

                    managers.user.saveUserDeviceToken(uid, deviceToken, this);
                },
                function response(e, r) {
                    if (e) console.log(e);
                    else console.log('Registed device token: ' + deviceToken);

                    if (e) res.json({success: false, message: e.message});
                    else res.json({success: true});
                }
            );
        },

        removeDeviceToken: function (req, res) {
            var deviceToken = req.params.token ? _fn.sanitize(req.params.token).xss() : '';

            Step(
                function valid() {
                    _fn.check(deviceToken, 'Device token invalid.').notEmpty();

                    return this;
                },
                function save(e, r) {
                    if (e) throw e;

                    managers.user.removeUserDeviceToken(0, deviceToken, this);
                },
                function response(e, r) {
                    if (e) console.log(e);
                    else console.log('Unregisted device token: ' + deviceToken);

                    if (e) res.json({success: false, message: e.message});
                    else res.json({success: true});
                }
            );
        }
    };

})(module.exports, require);
