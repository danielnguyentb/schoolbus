(function(ns, r) {
    "use strict";

    var db, //Database connections
        managers,
        _fn;        //Utility functions

    var locale = 'company';

    var Step = r("step"),
        jwt = r('jsonwebtoken');

    ns.init = function(app, _db, fn, mn) {
        db = _db;
        _fn = fn;
        managers = mn;

        app.route('/company')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.companyManage, _fn.auth.act.get, _fn.auth.permission, routes.getCompanies)
            .post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.companyManage, _fn.auth.act.post, _fn.auth.permission, routes.updateCompany);

        app.route('/company/staff-attendance')
            .get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.get, _fn.auth.permission, routes.getCompanyStaffAttendance)
            .post(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.post, _fn.auth.permission, routes.saveCompanyStaffAttendanceStatus);

        app.route('/company/staff-count').get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.get, _fn.auth.permission, routes.getCompanyStaffCount);
        app.route('/company/current-route').get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.busManage, _fn.auth.act.get, _fn.auth.permission, routes.getCompanyCurrentRoute);

        app.route('/company/:id').get(_fn.getBundle(locale), _fn.auth.check, _fn.auth.mod.companyManage, _fn.auth.act.get, _fn.auth.permission, routes.getCompanyDetail);

        var apiRoutes = app.get('wcl:api:routes');
        apiRoutes.route('/company')
            .get(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.companyManage, _fn.auth.act.get, _fn.auth.permission, routes.apiCompanies)
            .post(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.companyManage, _fn.auth.act.post, _fn.auth.permission, routes.apiUserSetCurrentCompany)
            .put(_fn.getBundle(locale), _fn.auth.apiAuth, _fn.auth.mod.companyManage, _fn.auth.act.put, _fn.auth.permission, routes.apiUserJoinCompany);
    };

    var routes = {

        getCompanies : function(req, res) {

            var numrow = req.query.limit ? _fn.sanitize(_fn.trim(req.query.limit)).toInt() : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? _fn.sanitize(_fn.trim(req.query.page)).toInt() : 1;

            Step (
                function valid() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    if(['name', 'address', 'status'].indexOf(order) == -1) order = '';

                    managers.comp.getCompanies(1, '', page, numrow, order, desc, this);
                },
                function response(e, r) {

                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            companies  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        },

        updateCompany : function(req, res) {

            var lingo = req.lingo;
            var id              = req.body.id ? _fn.sanitize(_fn.trim(req.body.id)).toInt() : 0;
            var name            = req.body.name ? _fn.sanitize(_fn.trim(req.body.name)).xss() : '';
            var address         = req.body.address ? _fn.sanitize(_fn.trim(req.body.address)).xss() : '';
            var status          = req.body.status ? _fn.sanitize(_fn.trim(req.body.status)).toInt() : 0;
            var map_tracking    = req.body.map_tracking ? 1 : 0;
            var parent_map      = req.body.parent_map ? 1 : 0;
            var mode            = req.body.mode && Array.isArray(req.body.mode) ? req.body.mode : ["1"];

            Step (
                function valid() {

                    _fn.check(name, lingo.get('company_name_invalid')).notEmpty().len(5);

                    if(id) managers.comp.getCompanyById(id, this);
                },
                function saveCompany(e, r) {
                    if(e) throw e;
                    if(id && (!r.length || !r[0].length) ) throw Error(lingo.get('company_not_exists'));

                    if(id) managers.comp.updateCompany(id, name, 0, 0, address, status, mode.join(','), this);
                    else managers.comp.insertCompany(name, 0, 0, address, status, mode.join(','), this);
                },
                function saveConfig(e, r) {
                    if(e) throw e;

                    if(!id && r.length && r[0].length) id = r[0][0].id;

                    if(id) managers.config.saveBcConfig(id, map_tracking, parent_map, this);
                    else return this;
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({
                        success: true,
                        message: id ? lingo.get('update_company_success') : lingo.get('create_company_success')
                    })
                }
            )
        },

        getCompanyDetail : function(req, res) {

            var lingo = req.lingo;
            var id = req.params.id ? _fn.sanitize(_fn.trim(req.params.id)).toInt() : 0;

            Step (
                function getDetail() {

                    _fn.check(id, lingo.get('company_not_exists')).notEmpty().isInt();

                    managers.comp.getCompanyById(id, this.parallel());
                    managers.config.getBcConfig(id, this.parallel());
                },
                function prepare(e, r, cfg) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('company_not_exists'));

                    r = r[0][0];
                    cfg = cfg || {};

                    return _fn.extend({
                        id: r.id,
                        name: r.name,
                        address: r.address,
                        status: r.status,
                        mode: r.mode ? r.mode.split(',') : ["1"]
                    }, cfg);
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, company: r});
                }
            )
        },

        getCompanyStaffAttendance: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id;

            var numrow = req.query.limit ? _fn.sanitize(_fn.trim(req.query.limit)).toInt() : 10;
            var order = req.query.order ? _fn.sanitize(_fn.trim(req.query.order)).xss() : '';
            var page = req.query.page ? _fn.sanitize(_fn.trim(req.query.page)).toInt() : 1;
            var date = req.query.date ? _fn.sanitize(_fn.trim(req.query.date)).xss() : '';
            var now = new Date();

            Step (
                function valid() {

                    if(isNaN(page)) page = 1;
                    if(isNaN(numrow)) numrow = 10;

                    var desc = order.indexOf('-') == 0 ? 'DESC' : 'ASC';
                    order = order.replace(/^\-(.*)$/, "$1");

                    if(date != '') {
                        managers.comp.getCompanyStaffAttendanceLogByDate(companyId, date, 1, 10000, order, desc, '', this.parallel());
                        managers.comp.getCompanyAbsentsByDate(companyId, date, this.parallel());
                        managers.comp.getCompanyWorkersWithDate(companyId, 1, date, this.parallel());

                    } else managers.comp.getCompanyStaffAttendance(companyId, [now.getFullYear(), '01', '01'].join('-'), _fn.date.dateString(now), page, numrow, order, desc, this);
                },
                function prepare(e, r, ab, ws) {
                    if(e) throw e;

                    if(date == '') {
                        var items = [];
                        if(r.length && r[0].length) {
                            r[0].forEach(function(v) {
                                items.push({
                                    date_view: _fn.date.format(v.dt, '0DD-MM3-YYYY DDD'),
                                    date_stand: _fn.date.dateString(v.dt),
                                    staff: v.staff || 0,
                                    checkIn: v.checkIn || 0,
                                    checkOut: v.checkOut || 0,
                                    absent: v.absent || 0
                                });
                            });
                        }

                        return [items, [{total: items.length}]];
                    }

                    var data = {};
                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            if(!data[v.id]) {
                                data[v.id] = {
                                    id: v.id,
                                    type: v.type,
                                    name: v.fullname || v.username
                                };
                            }

                            if(v.action == 1) _fn.extend(data[v.id], {in: true, in_time: v.time, in_owner: (v.location_owner == v.id)});
                            if(v.action == 2) _fn.extend(data[v.id], {out: true, out_time: v.time});
                        });
                    }

                    if(ab.length && ab[0].length) {
                        ab[0].forEach(function(v) {
                            if(data[v.id]) return true;

                            data[v.id] = {
                                id: v.id,
                                type: v.type,
                                name: v.fullname || v.username,
                                absent: 1,
                                time: v.time
                            };
                        });
                    }

                    if(ws.length && ws[0].length) {
                        ws[0].forEach(function(v) {
                            if(data[v.id]) return true;

                            data[v.id] = {
                                id: v.id,
                                type: v.type,
                                name: v.name || v.username
                            };
                        });
                    }

                    data = _fn.array.toArray(data);

                    return [data, [{total: data.length}]];
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else {
                        res.json({
                            success: true,
                            items  : r[0],
                            total  : r[1][0].total
                        });
                    }
                }
            )
        },

        saveCompanyStaffAttendanceStatus: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.session.company_id;

            var workerId = req.body.id ? _fn.sanitize(_fn.trim(req.body.id)).toInt() : 0;
            var status = req.body.manual_status ? _fn.sanitize(_fn.trim(req.body.manual_status)).toInt() : 1;

            Step(
                function valid() {
                    _fn.check(workerId, lingo.get('worker_is_not_exists')).notEmpty();

                    managers.user.getUserById(workerId, this.parallel());
                    managers.user.getUserCompany(workerId, this.parallel());
                },
                function save(e, r, c) {
                    if(e) throw e;
                    if(!r.length || !r[0].length || !c.length || !c[0].length) throw Error(lingo.get('worker_is_not_exists'));

                    r = r[0][0];

                    var userCompanies = {};
                    c[0].forEach(function(v) {
                        if(v.status == 1  && v.company_status == 1) {
                            userCompanies[v.id] = {
                                id: v.company_id,
                                name: v.name,
                                type: v.type
                            };
                        }
                    });

                    if(!userCompanies[companyId] || [1, 2].indexOf(userCompanies[companyId].type) == -1) throw Error(lingo.get('err_permission'));

                    var type = userCompanies[companyId].type == 1 ? 1 : 5;
                    if(status == 1) {
                        var _this = this;
                        managers.card.insertLocation(companyId, uid, 0, 0, function(e, r) {
                            if(e) _this(e);
                            else managers.route.insertEventHistory(0, r[0][0].id, '', type, workerId, type, workerId, 0, '', companyId, _this);
                        });

                    } else {
                        managers.comp.insertStaffAbsent(r.id, c.company_id, uid, this);
                    }
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: lingo.get('update_worker_status_success')});
                }
            );
        },

        getCompanyStaffCount: function(req, res) {
            var companyId = req.session.company_id;

            Step(
                function count() {
                    managers.comp.countCompanyWorkersPending(companyId, this);
                },
                function prepare(e, r) {
                    if(e) throw e;

                    var drivers = 0, assistants = 0;

                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            if(v.type == 1) drivers = v.total;
                            if(v.type == 2) assistants = v.total;
                        });
                    }

                    _fn.r(drivers, this.parallel());
                    _fn.r(assistants, this.parallel());
                },
                function response(e, d, a) {
                    if(e) res.json({success: false, drivers: 0, assistants: 0});
                    else res.json({success: true, drivers: d, assistants: a});
                }
            );
        },

        getCompanyCurrentRoute: function(req, res) {
            var companyId = req.session.company_id;

            var nextHour = req.query.next_hour ? _fn.sanitize(_fn.trim(req.query.next_hour)).toInt() : 0;
            var prevHour = req.query.past_hour ? _fn.sanitize(_fn.trim(req.query.past_hour)).toInt() : 0;

            Step(
                function getRoutes() {
                    nextHour = !isNaN(nextHour) ? nextHour : 0;
                    prevHour = !isNaN(prevHour) ? prevHour : 0;

                    managers.comp.getCompanyCurrentRoute(prevHour, nextHour, companyId, this);
                },
                function prepare(e, r) {
                    if(e) throw e;

                    var data = {};
                    if(r.length && r[0].length) {
                        r[0].forEach(function(v) {
                            if(v.status != 1) return true;

                            data[v.id] = {
                                id: v.id,
                                name: v.name,
                                daily_status: v.StatusDaily
                            };
                        });

                        if(r[1].length) {
                            r[1].forEach(function(v) {
                                if(!data[v.route_trip_id]) return true;

                                if(!data[v.route_trip_id].points) data[v.route_trip_id].points = [];

                                data[v.route_trip_id].points.push({
                                    title: v.created_at ? _fn.date.format(v.created_at, '0h2:0mm A') : 'N/A',
                                    point: [v.location_y, v.location_x]
                                });
                            });
                        }
                    }

                    return data;
                },
                function response(e, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, routes: r});
                }
            );
        },

        apiCompanies : function(req, res) {
            var uid = req.session.uId;
            var role = utils.getRoleType(req.session._role);

            Step (
                function valid() {
                    managers.user.getUserCompany(uid, this);
                },
                function prepare(e, uc, c) {
                    if(e) throw e;

                    var cids = {}, companies = [];
                    if(uc.length && uc[0].length) {
                        uc[0].forEach(function(v) {
                            if(v.status == 1 && v.company_status == 1 && !cids[v.company_id] && v.type == role) {
                                companies.push({id: v.company_id, name: v.name, mode: v.mode ? v.mode.split(',') : ["1"]});

                                cids[v.company_id] = v.company_id;
                            }
                        });
                    }

                    return companies;
                },
                function response(e, c) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, companies: c});
                }
            )
        },

        apiUserSetCurrentCompany: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var companyId = req.body.companyId ? _fn.sanitize(_fn.trim(req.body.companyId)).toInt() : 0;
            var busId = req.body.busId ? _fn.sanitize(_fn.trim(req.body.busId)).toInt() : 0;
            var role = utils.getRoleType(req.session._role);

            Step(
                function valid() {
                    _fn.check(companyId, lingo.get('company_not_exists')).notEmpty().isInt();

                    managers.user.getUserCompany(uid, this.parallel());
                    managers.acl.userRoles(uid.toString(), this.parallel());
                },
                function getPermsInCompany(e, r, ur) {
                    if(e) throw e;
                    if(!r.length || !r[0].length) throw Error(lingo.get('err_anonymous'));

                    var company = null;
                    r[0].forEach(function(v) {
                        if(v.status != 1) return true;

                        if(v.company_id == companyId && v.type == role) {
                            company = {id: v.company_id, name: v.name, type: v.type, mode: v.mode ? v.mode.split(',') : ["1"]};

                            return false;
                        }
                    });

                    if(!company) throw Error(lingo.get('company_not_exists'));

                    var sysRole = req.session._role;

                    _fn.r(sysRole, this.parallel());
                    _fn.r(company, this.parallel());

                    if(ur.indexOf(sysRole) == -1) managers.acl.addUsertoRole(uid.toString(), sysRole, this.parallel());
                },
                function getResource(e, ro, uc) {
                    if(e) throw e;

                    _fn.r(ro, this.parallel());
                    _fn.r(uc, this.parallel());
                    managers.acl.whatResources(ro, this.parallel());

                    if(req.session._role == config.ROLE.BUS && busId) {
                        managers.bus.getBusById(busId, this.parallel());
                        managers.bus.getListBusInCompany(companyId, this.parallel());
                    }
                },
                function prepare(e, ro, uc, ps, b, bs) {
                    var user = {
                        uId         : uid,
                        uname       : req.session.uname,
                        company_id  : uc.id,
                        bc_mode     : uc.mode,
                        email       : req.session.email,
                        avatar      : req.session.avatar,
                        perms       : ps,
                        roles       : [ro],
                        _role       : ro
                    };

                    req.session.company_id = uc.id;
                    req.session.bc_mode = uc.mode;
                    req.session.save();
                    var t = jwt.sign(user, config.session.secret, {
                        expiresIn: "1d"
                    });

                    var bus = null;
                    if(b && b.length && b[0].length && bs && bs.length) {
                        b = b[0][0];

                        bs[0].forEach(function(v) {
                            if(v.license_plate_no == b.license_plate_no) {
                                bus = {
                                    id: v.id,
                                    name: v.license_plate_no
                                };
                            }
                        });
                    }

                    _fn.r(uc, this.parallel());
                    _fn.r(t, this.parallel());
                    _fn.r(bus, this.parallel());
                    if(bus) managers.bus.updateBusAccount(uid, bus.id, companyId, 1, this.parallel());
                },
                function response(e, c, t, b) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, company: c, token: t, bus: b});
                }
            );
        },

        apiUserJoinCompany: function(req, res) {
            var lingo = req.lingo;
            var uid = req.session.uId;
            var company = req.body.company ? _fn.sanitize(_fn.trim(req.body.company)).xss() : '';
            var role = utils.getRoleType(req.session._role);

            Step(
                function valid() {
                    _fn.check(company, lingo.get('company_not_exists')).notEmpty();

                    _fn.doAsync(this);
                },
                function getCompany(e, r) {
                    if(e) throw e;

                    managers.user.checkCompanyExist(company , this.parallel());
                    managers.user.getUserCompany(uid, this.parallel());
                },
                function prepare(e, c, uc) {
                    if(e) throw e;
                    if(!c.length || !c[0].length || c[0][0].status != 1) throw Error(lingo.get('company_not_exists'));

                    c = c[0][0];

                    var userCompanies = {};
                    uc[0].forEach(function(v) {
                        userCompanies[v.company_id + v.type] = v;
                    });

                    if(userCompanies[c.id + role] && userCompanies[c.id + role].status != 2) throw Error(lingo.get('user_company_exists'));

                    _fn.r(c, this.parallel());

                    if(userCompanies[c.id + role]) managers.user.userCompanyApprove(uid, c.id, 0, this.parallel());
                    else managers.user.insertUserCompany(uid, c.id, role, 0, '', '', '', this.parallel());
                },
                function response(e, c, r) {
                    if(e) res.json({success: false, message: e.message});
                    else res.json({success: true, message: _fn.str.f(lingo.get('user_company_join_success'), {company: c.name})});
                }
            );
        }
    };

    var utils = {
        getRoleType: function(role) {
            switch (role) {
                case config.ROLE.DRIVER:
                    return 1;
                    break;
                case config.ROLE.ASSISTANT:
                    return 2;
                    break;
                case config.ROLE.BUS:
                    return 9;
                    break;
                default:
                    return 0;
                    break;
            }
        }
    };
})(module.exports, require);
