/**
 * Created by AnhNguyen on 8/30/15.
 */
"use strict";

module.exports = function copyto(grunt) {
    // Load task
    grunt.loadNpmTasks('grunt-copy-to');

    // Options
    return {
        build: {
            files  : [
                {
                    cwd: 'public',
                    src: ['**/*'],
                    dest: 'build/public/'
                }
            ],
            options: {
                ignore: [
                    'public/css/*.css',
                    'public/globals/css/*',
                    'public/js/**',
                    'public/lib/**/*.js',
                    'public/lib/**/*.css'
                ]
            }
        },

        lang : {
            files  : [
                {
                    cwd: '.build',
                    src: ['**/*'],
                    dest: 'build/'
                }
            ]
        }
    };
};
