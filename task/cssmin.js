/**
 * Created by AnhNguyen on 8/30/15.
 */
"use strict";

module.exports = function cssmin(grunt) {
    // Load task
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Options
    return {
        build: {
            options: {
            },
            files : [
                {
                    expand: true,
                    cwd   : 'public/css',
                    src   : '**/*.css',
                    dest  : 'build/public/css'
                },
                {
                    expand: true,
                    cwd   : 'public/globals/css',
                    src   : '*.css',
                    dest  : 'build/public/globals/css'
                },
                {
                    expand: true,
                    cwd   : 'public/lib',
                    src   : '**/*.css',
                    dest  : 'build/public/lib'
                },
                {
                    expand: true,
                    cwd   : 'modules',
                    src   : '**/public/css/*.css',
                    dest  : 'build/public/modules'
                }
            ]
        }
    };
};