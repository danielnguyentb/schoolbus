/*
 * Grunt file.
 */

'use strict';

(function(r) {
    var Step = require("step"),
        Async = require('async'),
        mysql = require('mysql'),
        sql = require('mssql'),
        crypto = require('crypto');
    require('config-node')({default: 'config-dev'});

    var conn,
        managers = {},
        interval = 1000 * 60 * 10;    
    managers.prefix = 'wump_';
    managers.prefixDt = 'wump__';
    config.msdbA = [];
    config.msdbB = [];
    //List Import Station
    config.stn = [];
    config.reqTimeout = 1000 * 60 * 10;

    var decrypt = function(text) {
        if (text == "") return "";
        var decipher = crypto.createDecipher('aes-256-cbc', 'sint');
        var decrypted = decipher.update(text, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        return decrypted;
    }

    module.exports = function(grunt) {
        grunt.registerTask('sint', 'Sync FM data.', function() {
            var done = this.async();
            Async.waterfall([
                //connectMysql
                function(callback) {
                    var c = config.dbs[4];
                    var hostport = c.host.split(':');
                    var ci = {
                        host: hostport[0],
                        port: hostport[1],
                        user: c.user,
                        password: c.pass,
                        database: c.db,
                        multipleStatements: c.multi
                    };
                    conn = mysql.createPool(ci);
                    conn.on('connection', function(connection) {
                        console.log([c.name, c.type, 'is connected'].join(' '));
                    });
                    conn.on('error', function(err) {
                        console.log([c.name, c.type, 'is NOT connected'].join(' ') + ' ' + err);
                        throw err;
                    });
                    callback(null);
                },
                //getListDBA
                function(callback) {
                    var query = conn.query("SELECT * FROM `" + managers.prefix + "comp` ORDER BY cCode DESC", function(e, r) {
                        if (e) {
                            console.log(e);
                            throw e;
                        }
                        if (r != undefined && r.length > 0) {
                            config.msdbA = Array();
                            config.stn = Array();
                            for (var i in r) {
                                if (r[i]['cDB']) {
                                    var cDB = JSON.parse(r[i]['cDB']);
                                    if (cDB.user) {// && cDB.alias != 'HKJPT'
                                        var set = {
                                            user: cDB.user,
                                            password: decrypt(cDB.pass),
                                            server: cDB.server,
                                            database: cDB.database,
                                            options: { 
                                                database: cDB.database,
                                                requestTimeout: config.reqTimeout
                                            },
                                            pool: {
                                                max: 1,
                                                min: 0,
                                                idleTimeoutMillis: 30000
                                            },
                                            alias: cDB.alias
                                        };
                                        config.msdbA.push(set);
                                    } //user
                                    var a = JSON.parse(r[i]['cDB']).alias;
                                    if (a)
                                        config.stn.push(a);
                                } //cDB
                            } //for
                            console.log(config.msdbA);
                            console.log(config.stn);
                            callback(null);
                        } //if
                    });
                },                
                //Store AL
                function(callback) {
                    //Where Station
                    var whereStn = "(";
                    for (var i = 0; i < config.stn.length; i++) {
                        whereStn += "'" + config.stn[i] + "'";
                        if (i < config.stn.length - 1)
                            whereStn += ",";
                    } //for
                    whereStn += ")";
                    //Store AEJ
                    var queryStr = "SELECT AEJ.JOB_ID AS JOB_ID_AEJ,AEJ.AWB_NUMBER AS AWB_NUMBER_AEJ,BOOKING_NUMBER,JOB_NUMBER,HAWB_NUMBER,MAWB_NUMBER,AWB_TYPE,CONSIGNEE_CODE,Delivery_Agent_Code,BRANCH_CODE,Tranship_Job_Number FROM AE_JOB_TBL AS AEJ LEFT JOIN AE_JOB_MAIN_INFO_TBL AS AEM ON AEJ.JOB_ID=AEM.JOB_ID WHERE AEJ.AWB_NUMBER<>'' AND (AWB_TYPE='D' OR AWB_TYPE='M' OR (AWB_TYPE='H' AND Tranship_Job_Number NOT IN (SELECT Tranship_Job_Number FROM AE_JOB_TBL WHERE AWB_TYPE='M' OR AWB_TYPE='D'))) AND (CONSIGNEE_CODE IN " + whereStn + " OR Delivery_Agent_Code IN " + whereStn + ") ORDER BY AEJ.JOB_ID ASC";
                    //console.log(queryStr);
                    
                    Async.eachSeries(config.msdbA, function(cfg, cb1) {
                        var connection = new sql.Connection(cfg, function(err) {
                            var request = new sql.Request(connection);
                            request.query(queryStr, function(e, list) {
                                if (e) {
                                    console.log(e);
                                    throw e;
                                }
                                var FMDesc = '';
                                Async.waterfall([
                                    function(cbk){
                                        Async.eachSeries(config.msdbA, function(cfg2, cb2) {
                                            var connection1 = new sql.Connection(cfg2, function(err) {
                                                var request = new sql.Request(connection1);
                                                request.query("SELECT TOP 1 * FROM ACT_CustVend_Master_Tbl WHERE CustVend_Code = '" + cfg['alias'] + "'", function(e, cv) {
                                                    if (e) {
                                                        console.log(e);
                                                        throw e;
                                                    }
                                                    if (typeof(cv) != 'undefined' && cv.length > 0) {
                                                        FMDesc = cv[0]['CustVend_Name'];
                                                        cbk(null);
                                                    } else {
                                                        cb2(null);
                                                    }//if cv
                                                });
                                            });
                                        },cbk);
                                    },
                                    function(cbk){
                                        //check sint_al data
                                        var query = conn.query("SELECT aId FROM `sint_al` ORDER BY JobId ASC", function(err, rows) {
                                            if (err) {
                                                console.log(err);
                                                throw err;
                                            }
                                            if (typeof(rows) != 'undefined' && rows.length > 0) {
                                                //insert new one
                                                if (typeof(list) != 'undefined' && list.length > 0) {
                                                    Async.eachSeries(list, function(item, cb3) {
                                                        var x = item['JOB_ID_AEJ'];
                                                        conn.query("CALL sint_get_where_al(?,?);",[x,cfg['alias']],function(err1, row) {
                                                            if (err1) {
                                                                console.log(err1);
                                                                throw err1;
                                                            }
                                                            //console.log(row[0]);
                                                            if (typeof(row[0]) == 'undefined' || row[0] == '') {
                                                                var query2 = conn.query("CALL sint_insert_al(?,?,?,?,?,?,?,?,?,?,?,?,?);", [item['JOB_ID_AEJ'], item['AWB_NUMBER_AEJ'], item['BOOKING_NUMBER'], item['JOB_NUMBER'], item['HAWB_NUMBER'], item['MAWB_NUMBER'], item['AWB_TYPE'], item['CONSIGNEE_CODE'], item['Delivery_Agent_Code'], item['BRANCH_CODE'], cfg['alias'], FMDesc, 0], function(err2, results) {
                                                                    if (err2) {
                                                                        console.log(err2);
                                                                        throw err2;
                                                                    }
                                                                    console.log("Store AL: " + cfg['alias'] + "; AEJ ins: " + item['JOB_ID_AEJ']);
                                                                    cb3(null);
                                                                });
                                                            } else {
                                                                cb3(null);
                                                            } //if
                                                        });
                                                    },cb1);
                                                } else {
                                                    connection.close();
                                                    cb1(null);
                                                } //if
                                                //sint_al empty
                                            } else {
                                                if (typeof(list) != 'undefined' && list.length > 0) {
                                                    Async.eachSeries(list, function(item, cb3) {
                                                        var query2 = conn.query("CALL sint_insert_al(?,?,?,?,?,?,?,?,?,?,?,?,?);", [item['JOB_ID_AEJ'], item['AWB_NUMBER_AEJ'], item['BOOKING_NUMBER'], item['JOB_NUMBER'], item['HAWB_NUMBER'], item['MAWB_NUMBER'], item['AWB_TYPE'], item['CONSIGNEE_CODE'], item['Delivery_Agent_Code'], item['BRANCH_CODE'], cfg['alias'], FMDesc, 0], function(err2, results) {
                                                            if (err2) {
                                                                console.log(err2);
                                                                throw err2;
                                                            }
                                                            console.log("Store AL: " + cfg['alias'] + "; AEJ ins: " + item['JOB_ID_AEJ']);
                                                            cb3(null);
                                                        });
                                                    },cb1);
                                                } else {
                                                    connection.close();
                                                    cb1(null);
                                                } //if list
                                            } //if rows
                                        });
                                    },
                                ]);//async.waterfall                        
                            });
                        });
                    },callback);
                },
                //Store OL
                function(callback) {
                    //Where Station
                    var whereStn = "(";
                    for (var i = 0; i < config.stn.length; i++) {
                        whereStn += "'" + config.stn[i] + "'";
                        if (i < config.stn.length - 1)
                            whereStn += ",";
                    } //for
                    whereStn += ")"; //console.log(whereStn);
                    //Store SEM
                    var queryStr = "SELECT SEM.Export_Number AS Export_Number_SEM,Master_Job_Number,Job_Number,Booking_Number,HBL_Number,OBL_Number,Export_Type,Consignee_Code,Delivery_Agent_Code,Branch_Code FROM SE_Job_Master_Tbl AS SEM LEFT JOIN SE_Job_Detail_Party_Tbl AS SDPT ON SEM.Export_Number=SDPT.Export_Number WHERE (Export_Type='DBL' OR Export_Type='MBL' OR (Export_Type='HBL' AND Master_Job_Number NOT IN (SELECT Master_Job_Number FROM SE_Job_Master_Tbl WHERE Export_Type='MBL' OR Export_Type='DBL'))) AND (Consignee_Code IN " + whereStn + " OR Delivery_Agent_Code IN " + whereStn + ") ORDER BY SEM.Export_Number ASC";
                    //console.log(queryStr);
                    Async.eachSeries(config.msdbA, function(cfg, cb1) {
                        var connection = new sql.Connection(cfg, function(err) {
                            var request = new sql.Request(connection);
                            request.query(queryStr, function(e, list) {
                                if (e) {
                                    console.log(e);
                                    throw e;
                                }
                                var FMDesc = '';
                                Async.waterfall([
                                    function(cbk){
                                        Async.eachSeries(config.msdbA, function(cfg2, cb2) {
                                            var connection1 = new sql.Connection(cfg2, function(err) {
                                                var request = new sql.Request(connection1);
                                                request.query("SELECT TOP 1 * FROM ACT_CustVend_Master_Tbl WHERE CustVend_Code = '" + cfg['alias'] + "'", function(e, cv) {
                                                    if (e) {
                                                        console.log(e);
                                                        throw e;
                                                    }
                                                    if (typeof(cv) != 'undefined' && cv.length > 0) {
                                                        FMDesc = cv[0]['CustVend_Name'];
                                                        cbk(null);
                                                    } else {
                                                        cb2(null);
                                                    }//if cv
                                                });
                                            });
                                        },cbk);
                                    },
                                    function(cbk){
                                        //check sint_ol data
                                        var query = conn.query("SELECT oId FROM `sint_ol` ORDER BY ExNum ASC", function(err, rows) {
                                            if (err) {
                                                console.log(err);
                                                throw err;
                                            }
                                            if (typeof(rows) != 'undefined' && rows.length > 0) {
                                                //insert new one
                                                if (typeof(list) != 'undefined' && list.length > 0) {
                                                    Async.eachSeries(list, function(item, cb3) {
                                                        var x = item['Export_Number_SEM'];
                                                        conn.query("CALL sint_get_where_ol(?,?);",[x,cfg['alias']],function(err1, row) {
                                                            if (err1) {
                                                                console.log(err1);
                                                                throw err1;
                                                            }
                                                            if (typeof(row[0]) == 'undefined' || row[0] == '') {
                                                                var query2 = conn.query("CALL sint_insert_ol(?,?,?,?,?,?,?,?,?,?,?,?,?);", [item['Export_Number_SEM'], item['Master_Job_Number'], item['Job_Number'], item['Booking_Number'], item['HBL_Number'], item['OBL_Number'], item['Export_Type'], item['Consignee_Code'], item['Delivery_Agent_Code'], item['Branch_Code'], cfg['alias'], FMDesc, 0], function(err2, results) {
                                                                    if (err2) {
                                                                        console.log(err2);
                                                                        throw err2;
                                                                    }
                                                                    console.log("Store OL: " + cfg['alias'] + "; SEM ins: " + item['Export_Number_SEM']);
                                                                    cb3(null);
                                                                });
                                                            } else {
                                                                cb3(null);                                              
                                                            } //if
                                                        });
                                                    }, cb1);
                                                } else {
                                                    connection.close();
                                                    cb1(null);
                                                } //if
                                            } else {
                                                if (typeof(list) != 'undefined' && list.length > 0) {
                                                    Async.eachSeries(list, function(item, cb3) {
                                                        var query2 = conn.query("CALL sint_insert_ol(?,?,?,?,?,?,?,?,?,?,?,?,?);", [item['Export_Number_SEM'], item['Master_Job_Number'], item['Job_Number'], item['Booking_Number'], item['HBL_Number'], item['OBL_Number'], item['Export_Type'], item['Consignee_Code'], item['Delivery_Agent_Code'], item['Branch_Code'], cfg['alias'], FMDesc, 0], function(err2, results) {
                                                            if (err2) {
                                                                console.log(err2);
                                                                throw err2;
                                                            }
                                                            console.log("Store OL: " + cfg['alias'] + "; SEM ins: " + item['Export_Number_SEM']);
                                                            cb3(null);
                                                        });
                                                    }, cb1);
                                                } else {
                                                    connection.close();
                                                    cb1(null);
                                                } //if list
                                            } //if rows
                                        });
                                    },
                                ]);//async.waterfall                                
                            });
                        });
                    }, callback);
                },
                //Store IN
                function(callback) {
                    var arIN = [{key: 'ARM', table: 'ACT_IV_Air_Invoice_Master_Tbl'},
                                {key: 'SM', table: 'ACT_IV_Sea_Invoice_Master_Tbl'},
                                {key: 'WH', table: 'ACT_IV_Warehouse_Invoice_Master_Tbl'},
                                {key: 'TR', table: 'ACT_IV_Transport_Invoice_Master_Tbl'},
                                {key: 'MS', table: 'ACT_IV_MISC_Invoice_Master_Tbl'}];//console.log(arIN);
                    //Save or check list                    
                    Async.eachSeries(arIN, function(ele, cb1) {
                        var queryStr = "SELECT Transaction_Number,Invoice_Number,Job_Number,OBL_Number,HBL_Number,Customer_Code FROM " + ele.table + " WHERE Invoice_Type_Code <> 'GI' ORDER BY Transaction_Number ASC";
                        Async.eachSeries(config.msdbA, function(cfg, cb2) {
                            var connection = new sql.Connection(cfg, function(err) {
                                var request = new sql.Request(connection);
                                //console.log(connection);
                                //console.log(queryStr + ' ' + cfg['alias']);                          
                                request.query(queryStr, function(e, List) {
                                    if (e) {
                                        console.log(e);
                                        throw e;
                                    }
                                    var FMDesc = '';
                                    Async.waterfall([
                                        function(cbk){
                                            Async.eachSeries(config.msdbA, function(cfg2, cbk2) {
                                                var connection1 = new sql.Connection(cfg2, function(err) {
                                                    var request = new sql.Request(connection1);
                                                    request.query("SELECT TOP 1 * FROM ACT_CustVend_Master_Tbl WHERE CustVend_Code = '" + cfg['alias'] + "'", function(e1, cv) {
                                                        if (e1) {
                                                            console.log(e1);
                                                            throw e;
                                                        }
                                                        if (typeof(cv) != 'undefined' && cv.length > 0) {
                                                            FMDesc = cv[0]['CustVend_Name'];
                                                            cbk(null);
                                                        } else {
                                                            cbk2(null);
                                                        }//if cv
                                                    });
                                                });
                                            },cbk);
                                        },
                                        function(cbk){
                                            //check sint_asl data
                                            var query = conn.query("SELECT asId FROM `sint_asl` ORDER BY TransNum ASC", function(err, rows) {
                                                if (err) {
                                                    console.log(err);
                                                    throw err;
                                                }
                                                if (typeof(rows) != 'undefined' && rows.length > 0) {
                                                    //insert new one
                                                    if (typeof(List) != 'undefined' && List.length > 0) {
                                                        Async.eachSeries(List, function(item, cb3) {
                                                            var x = item['Transaction_Number'];
                                                            conn.query("CALL sint_get_where_asl(?,?,?);",[x,ele.key,cfg['alias']], function(err1, row) {
                                                                if (err1) {
                                                                    console.log(err1);
                                                                }
                                                                //console.log(row[0]);
                                                                if (typeof(row[0]) == 'undefined' || row[0] == '') {
                                                                    var query2 = conn.query("CALL sint_insert_asl(?,?,?,?,?,?,?,?,?,?,?);", [item['Transaction_Number'], item['Invoice_Number'], item['Job_Number'], item['OBL_Number'], item['HBL_Number'], ele.key, cfg['alias'], FMDesc, '', item['Customer_Code'], 0], function(err2, results) {
                                                                        if (err2) {
                                                                            console.log(err2);
                                                                            throw error;
                                                                        }
                                                                        console.log("Store " + ele.key + ": " + cfg['alias'] + "; " + ele.key + " ins: " + item['Transaction_Number']);
                                                                        cb3(null);
                                                                    });
                                                                } else {
                                                                    cb3(null);
                                                                } //if
                                                            });
                                                        },cb2);
                                                    } else {                                                
                                                        connection.close();
                                                        cb2(null);
                                                    } //if
                                                    //sint_asl empty
                                                } else {
                                                    if (typeof(List) != 'undefined' && List.length > 0) {
                                                        Async.eachSeries(List, function(item, cb3) {
                                                            var query2 = conn.query("CALL sint_insert_asl(?,?,?,?,?,?,?,?,?,?,?);", [item['Transaction_Number'], item['Invoice_Number'], item['Job_Number'], item['OBL_Number'], item['HBL_Number'], ele.key, cfg['alias'], FMDesc, '', item['Customer_Code'], 0], function(err2, results) {
                                                                if (err2) {
                                                                    console.log(err2);
                                                                    throw err2;
                                                                }
                                                                console.log("Store " + ele.key + ": " + cfg['alias'] + "; " + ele.key + " ins: " + item['Transaction_Number']);
                                                                cb3(null);
                                                            });
                                                        },cb2);
                                                    } else {
                                                        connection.close();
                                                        cb2(null);
                                                    } //if
                                                } //if
                                            });
                                        },
                                    ]);//async.waterfall                                    
                                });
                            });
                        },cb1);
                    },callback);
                },
                //Store Group IN
                function(callback) {
                    var arIN = [{key: 'ARM', table: 'ACT_IV_Air_Invoice_Master_Tbl', relation: 'AE'},
                                {key: 'SM', table: 'ACT_IV_Sea_Invoice_Master_Tbl', relation: 'SE'},
                                {key: 'WH', table: 'ACT_IV_Warehouse_Invoice_Master_Tbl', relation: 'WH'},
                                {key: 'TR', table: 'ACT_IV_Transport_Invoice_Master_Tbl', relation: 'TR'},
                                {key: 'MS', table: 'ACT_IV_MISC_Invoice_Master_Tbl', relation: 'MS'}];
                                //console.log(arIN);
                    //Save or check list                    
                    Async.eachSeries(arIN, function(ele, cb1) {
                        var queryStr = "SELECT Transaction_Number,Invoice_Number,Job_Number,OBL_Number,HBL_Number,Customer_Code FROM " + ele.table + " WHERE Invoice_Type_Code = 'GI' ORDER BY Transaction_Number ASC";
                        Async.eachSeries(config.msdbA, function(cfg, cb2) { //console.log(queryStr);
                            var connection = new sql.Connection(cfg, function(err) {
                                var request = new sql.Request(connection);
                                request.query(queryStr, function(err, List) {
                                    if (err) {
                                        console.log(err);
                                        throw err;
                                    }
                                    if (typeof(List) != 'undefined' && List.length > 0) {                                        
                                        Async.eachSeries(List, function(item, cb3) {
                                            var x = item['Transaction_Number'];                                            
                                            //check sint_asl data
                                            var query = conn.query("CALL sint_get_where_asl(?,?,?);",[x,ele.key,cfg['alias']], function(err1, rows) {
                                                if (err1) {
                                                    console.log(err1);
                                                    throw err1;
                                                }
                                                //console.log(rows[0]);
                                                if (typeof(rows[0]) == 'undefined' || rows[0] == '') {
                                                    var queryStr1 = "SELECT gpi.Trx_No"
                                                        + " FROM ACT_IV_Group_Invoice_Tbl AS gp"
                                                        + " INNER JOIN ACT_IV_Group_Invoice_Item_Tbl AS gpi ON gp.Trx_No = gpi.Trx_No"
                                                        + " WHERE gpi.Job_No IN ("
                                                        + " SELECT Job_Number"
                                                        + " FROM AM_Job_Master_Tbl"
                                                        + " WHERE System_Job_Relation = '" + ele.relation + "'"
                                                        + " ) AND gpi.Ref_Trx_No = " + x;
                                                    request.query(queryStr1, function(err2, row) {
                                                        if (err2) {
                                                            console.log(err2);
                                                            throw err2;
                                                        }
                                                        //console.log(row.length);
                                                        if (typeof(row) != 'undefined' && row.length > 0) {
                                                            var FMDesc = '';
                                                            Async.waterfall([
                                                                function(cbk){
                                                                    Async.eachSeries(config.msdbA, function(cfg2, cbk2) {
                                                                        var connection1 = new sql.Connection(cfg2, function(err) {
                                                                            var request = new sql.Request(connection1);
                                                                            request.query("SELECT TOP 1 * FROM ACT_CustVend_Master_Tbl WHERE CustVend_Code = '" + cfg['alias'] + "'", function(e1, cv) {
                                                                                if (e1) {
                                                                                    console.log(e1);
                                                                                    throw e;
                                                                                }
                                                                                if (typeof(cv) != 'undefined' && cv.length > 0) {
                                                                                    FMDesc = cv[0]['CustVend_Name'];
                                                                                    cbk(null);
                                                                                } else {
                                                                                    cbk2(null);
                                                                                }//if cv
                                                                            });
                                                                        });
                                                                    },cbk);
                                                                },
                                                                function(cbk){
                                                                    var query2 = conn.query("CALL sint_insert_asl(?,?,?,?,?,?,?,?,?,?,?);", [item['Transaction_Number'], item['Invoice_Number'], item['Job_Number'], item['OBL_Number'], item['HBL_Number'], ele.key, cfg['alias'], FMDesc, 'GI', item['Customer_Code'], 0], function(err2, results) {
                                                                        if (err2) {
                                                                            console.log(err2);
                                                                            throw error;
                                                                        }
                                                                        console.log("Store Group " + ele.key + ": " + cfg['alias'] + "; " + ele.key + " ins: " + item['Transaction_Number']);
                                                                        cb3(null);
                                                                    });
                                                                },
                                                            ]);//async.waterfall                                                            
                                                        } else {
                                                            cb3(null);
                                                        } //if
                                                    });
                                                } else {
                                                    cb3(null);
                                                }
                                            });
                                        },cb2);
                                    } else {
                                        connection.close();
                                        cb2(null);
                                    } //if
                                });
                            });
                        },cb1);
                    },callback);
                },
                function(callback) {
                    done();
                    callback(null);
                }
            ]); //Async.waterfall
        });
    };
})(require);
