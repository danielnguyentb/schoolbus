'use strict';


module.exports = function requirejs (grunt) {
    // Load task
    grunt.loadNpmTasks('grunt-contrib-requirejs');

    // Options
    return {
        build: {
            options: {
                baseUrl                : "public/js",
                mainConfigFile         : "public/js/app.js",
                name                   : "app/base",
                deps                   : [
                    'jquery',
                    'lib/jquery-ui-1.9.2.custom.min',
                    'lib/jquery-layout/jquery.layout-latest',
                    'app/base',
                    'utils/common',
                    'lib/bootstrap.min',
                    'lib/general-listing'
                ],
                out                    : "build/public/js/app.prod.js",
                waitSeconds            : 0,
                findNestedDependencies : true,
                preserveLicenseComments: false,
                map                    : {
                    '*': {
                        'css': 'require-css/css'
                    }
                },
                paths                  : {
                    wump        : '../../modules/wump/public/js',
                    sint        : '../../modules/sint/public/js',
                    tcrg        : '../../modules/tcrg/public/js',
                    rdmp        : '../../modules/rdmp/public/js',
                    design      : '../../modules/design/public/js',
                    jquery      : '../lib/jquery/jquery.min',
                    domReady    : './utils/domReady',
                    app         : './app',
                    lib         : '../lib',
                    utils       : './utils',
                    datatables  : '../lib/data-tables/media/js/jquery.dataTables',
                    'lib-dt'    : '../lib/data-tables',
                    fineuploader: '../lib/fineuploader-3.2.min',
                    underscore  : '../lib/underscore'
                },
                shim                   : {
                    'underscore'                                      : {exports: '_'},
                    'recaptcha'                                       : {exports: 'Recaptcha'},
                    'lib/bootstrap.min'                               : ['jquery'],
                    'lib/noty/jquery.noty'                            : ['jquery'],
                    'lib/noty/layouts/topcenter'                      : {
                        deps   : ['jquery', 'lib/noty/jquery.noty'],
                        exports: 'jquery'
                    },
                    'lib/noty/layouts/center'                         : {
                        deps   : ['jquery', 'lib/noty/jquery.noty'],
                        exports: 'jquery'
                    },
                    'lib/noty/themes/default'                         : {
                        deps   : ['jquery', 'lib/noty/jquery.noty'],
                        exports: 'jquery'
                    },
                    'lib/bootstrap-switch/js/bootstrap-switch.min'    : {
                        deps   : ['jquery'],
                        exports: 'jquery'
                    },
                    'lib/jscrollpane/jscrollpane.min'                 : ['jquery'],
                    'lib/spinner.min'                                 : ['jquery'],
                    'lib/jquery-ui-1.9.2.custom.min'                  : ['jquery'],
                    'lib/jquery.ui.touch-punch'                       : ['jquery'],
                    'lib/jquery.mjs.nestedSortable'                   : ['jquery', 'lib/jquery-ui-1.9.2.custom.min'],
                    'lib/bootstrap-contextmenu'                       : ['jquery'],
                    'lib/chosen/chosen.jquery'                        : {
                        deps   : ['jquery'],
                        exports: 'jquery'
                    },
                    'lib/jquery.history'                              : {
                        deps   : ['jquery'],
                        exports: 'History'
                    },
                    'lib/bootstrap-editable/js/bootstrap-editable.min': ['jquery', 'lib/bootstrap.min']
                }
            }
        }
    };
};
