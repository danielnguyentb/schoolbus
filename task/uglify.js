/**
 * Created by AnhNguyen on 8/30/15.
 */
"use strict";

module.exports = function uglify (grunt) {
    // Load task
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Options
    return {
        build: {
            options: {
                compress: {
                    drop_console: true
                }
            },
            files  : [
                {
                    expand: true,
                    cwd   : 'public/js',
                    src   : '**/*.js',
                    dest  : 'build/public/js'
                },
                {
                    expand: true,
                    cwd   : 'public/lib',
                    src   : '**/*.js',
                    dest  : 'build/public/lib'
                },
                {
                    expand: true,
                    cwd   : 'modules',
                    src   : '**/public/**/*.js',
                    dest  : 'build/public/modules'
                }
            ]
        },

        module : {
            options: {
                compress: {
                    drop_console: true
                }
            },
            files  : [
                {
                    expand: true,
                    cwd   : 'modules',
                    src   : '**/public/**/*.js',
                    dest  : 'build/public/modules'
                }
            ]
        }
    };
};